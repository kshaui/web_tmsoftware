<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Colors;
use App\Models\Manufacturer;
use App\Models\ManufacturerModel;
use App\Models\ProductType;
use App\Models\Province;
use App\Models\User;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Author;
use App\Models\Category;
use App\Models\Company;
use App\Models\Contact;
use App\Models\Favorite;
use App\Models\Post;
use App\Models\Product;
use App\Models\Publishing;
use App\Models\Widget;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function getIndex($categorySlug, $productSlug)
    {
        //  Truy van danh muc tu slug cua danh muc
        $category = CommonHelper::getFromCache('get_category_by_slug_' . $categorySlug);
        if (!$category) {
            $category = Category::where('slug', $categorySlug)->where('status', 1)->first();
            CommonHelper::putToCache('get_category_by_slug_' . $categorySlug, $category);
        }
        //  Ban sang trang loi 404 neu ko tim thay danh muc do
        if (!is_object($category))
            abort(404);
        $data['category'] = $category;
        if ($category->type == 1) {  // Chi tiet tin tuc
            //  Truy van tin tuc tu slug cua tin tuc
            $post = CommonHelper::getFromCache('get_post_by_slug_' . $productSlug);
            if (!$post) {
                $post = Post::where('slug', $productSlug)->where('status', 1)->first();
                CommonHelper::putToCache('get_post_by_slug_' . $productSlug, $post);
            }
            if (!is_object($post))
                abort(404);
            $data['post'] = $post;
            $OtherInformationPost = CommonHelper::getFromCache('get_posts_by_category_id_' . $category->id);
            if (!$OtherInformationPost) {
                $OtherInformationPost = \App\Models\Post::select(['id', 'name', 'intro', 'image', 'slug', 'multi_cat'])->where('category_id', $category->id)
                    ->where('status', 1)->where('id', '<>', $post->id)->where('deleted_at', 0)->orderBy('updated_at', 'desc')->offset(0)->limit(5)->get();;
                CommonHelper::putToCache('get_posts_by_category_id_' . $category->id, $OtherInformationPost);
            }
            if (!is_object($OtherInformationPost))
                abort(404);
            $data['OtherInformationPost'] = $OtherInformationPost;
            //  Danh cho seo
            $pageOption = [
                'type' => 'post',
                'parentUrl' => $category->slug,
                'parentName' => $category->name,
                'pageName' => $post->name,
                'link' => \URL::to($category->slug . '/' . $post->slug),
                'image' => \URL::to('filemanager/userfiles/' . $post->image),
                'title' => $post->meta_title != '' ? $post->meta_title : $post->name,
                'description' => $post->meta_description != '' ? $post->meta_description : $post->name,
                'keywords' => $post->meta_keywords != '' ? $post->meta_keywords : $post->name,
            ];
            view()->share('pageOption', $pageOption);
            //  Goi view va truyen bien $data ra view
                return view('frontend.childs.post.detail')->with($data);
        } else {    // chi tiet san pham
            //  Truy van san pham tu slug cua san pham
            $product = CommonHelper::getFromCache('get_product_by_slug_' . $productSlug);
            if (!$product) {
                $product = Product::where('slug', $productSlug)->where('status', 1)->orderBy('order_no', 'asc')->first();
                CommonHelper::putToCache('get_product_by_slug_' . $productSlug, $product);
            }
            if (!is_object($product))
                abort(404);
            $data['product'] = $product;
            //  Danh cho seo
            $pageOption = [
                'type' => 'post',
                'parentUrl' => $category->slug,
                'parentName' => $category->name,
                'pageName' => $product->name,
                'link' => \URL::to($category->slug . '/' . $product->slug),
                'image' => \URL::to('filemanager/userfiles/' . $product->image),
                'title' => $product->meta_title != '' ? $product->meta_title : $product->name,
                'description' => $product->meta_description != '' ? $product->meta_description : $product->name,
                'keywords' => $product->meta_keywords != '' ? $product->meta_keywords : $product->name,
            ];
            view()->share('pageOption', $pageOption);
            //  Goi view va truyen bien $data ra view
            return view('frontend.childs.product.index')->with($data);
        }
    }

    public function ajax_search(Request $request)
    {
        $data['products'] = CommonHelper::getFromCache('search_products_by_code_' . base64_encode(json_encode($request->all())));
        if (!$data['products']) {
            $group_id = $request->get('group_id', 0);
            $products = Product::select(['name', 'slug', 'multi_cat', 'type'])->where('status', 1);
            if ($request->has('keyword')) {
                $products = $products->where('name', 'LIKE', "%" . $request->get('keyword', '') . "%");
            }
            if ($group_id == 70) {
                $products = $products->where('type', 'LIKE', "%|2|%");
            } elseif ($group_id != 0) {
                $products = $products->where('category_id', $group_id);
            }
            $data['products'] = $products->orderBy('updated_at', 'desc')->limit(10)->get();
            CommonHelper::putToCache('search_products_by_code_' . base64_encode(json_encode($request->all())), $data['products']);
        }
        return view('frontend.childs.product.ajax_search')->with($data);
    }

    /**danh sách các sản phẩm choxe.net*/
    public function getListProduct()
    {
        return view('frontend.childs.product.list_product');
    }

    /**chi tiết sản phẩm*/
    public function getDetailProduct($id)
    {
        $product_dang_ban = CommonHelper::getFromCache('product_dangban_by_id_first');
        if (!$product_dang_ban) {
            $product_dang_ban = Product::where('id', @$id)->where('status', 1)->wherein('pending', ['Đang bán'])->first();
            CommonHelper::putToCache('product_dangban_by_id_first', $product_dang_ban);
        }
        if ($product_dang_ban) {
            $product = $product_dang_ban;
            if (!is_object($product))
                abort(404);
            $data['product'] = $product;
            //  Danh cho seo
            $pageOption = [
                'type' => 'post',
                'parentUrl' => @$product->slug,
                'parentName' => @$product->name,
                'pageName' => @$product->name,
                'link' => \URL::to(@$product->slug . '/' . @$product->slug),
                'image' => \URL::to('filemanager/userfiles/' . @$product->image),
                'title' => $product->meta_title != '' ? $product->meta_title : $product->name,
                'description' => $product->meta_description != '' ? $product->meta_description : $product->name,
                'keywords' => $product->meta_keywords != '' ? $product->meta_keywords : $product->name,
            ];
            view()->share('pageOption', $pageOption);
            if ($product) {
                return view('frontend.childs.product.detail_product')->with($data);
            }
        } else {
            $product = CommonHelper::getFromCache('product_by_id_author_id_first');
            if (!$product) {
                $product = Product::where('id', @$id)->where('status', 1)->where('author_id',@Auth::user()->id)->first();
                CommonHelper::putToCache('product_by_id_author_id_first', $product);
            }
            if (!is_object($product))
                abort(404);
            $data['product'] = $product;
            //  Danh cho seo
            $pageOption = [
                'type' => 'post',
                'parentUrl' => @$product->slug,
                'parentName' => @$product->name,
                'pageName' => @$product->name,
                'link' => \URL::to(@$product->slug . '/' . @$product->slug),
                'image' => \URL::to('filemanager/userfiles/' . @$product->image),
                'title' => $product->meta_title != '' ? $product->meta_title : $product->name,
                'description' => $product->meta_description != '' ? $product->meta_description : $product->name,
                'keywords' => $product->meta_keywords != '' ? $product->meta_keywords : $product->name,
            ];
            view()->share('pageOption', $pageOption);
            if ($product) {
                return view('frontend.childs.product.detail_product')->with($data);
            }
        }
    }

    /**mục thêm quan tâm*/
    public function postCare(Request $request)
    {
        if ($request->ajax()) {
            $rules = array(
                'name' => 'required',
                'tel' => 'required|numeric',
                'email' => 'sometimes|required|email',
            );
            $validator = Validator::make(input::all(), $rules);
            $data = $request->except('_token');
            if ($validator->fails()) {
                return response::json(array('errors' => $validator->getMessageBag()->toarray()));
            } else {
                $count = Contact::where('user_id', $data['user_id'])->where('product_id', $data['product_id'])->where('email', $data['email'])->count();
                if ($count == 0) {
                    \Illuminate\Support\Facades\Cache::flush();
                    Contact::create($data);
                }
            }
        }
    }

    /**danh sách người quan tâm*/
    public function getPeopleinterested(Request $request)
    {
        if (Auth::user()) {
            $contact = CommonHelper::getFromCache('contact_by_author_id');
            if (!$contact) {
                $contact = Contact::select('author_id', 'product_id', 'name', 'tel', 'email')->where('author_id', @Auth::user()->id)->orderBy('updated_at', 'desc') ->get();
                CommonHelper::putToCache('contact_by_author_id', $contact);
                $data['contact'] = $contact;
            }
            $pageOption = [
                'type' => 'page',
                'pageName' => 'Danh sách người quan tâm',
                'parentName' => 'Trang chủ',
                'parentUrl' => '/thong-bao',
            ];
            view()->share('pageOption', $pageOption);
            //return json_encode($data);
            return view('frontend.childs.product.people_interested')->with($data);
        }
    }

    /**xe yêu thích*/
    public function getCarLove()
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Xe yêu thích',
            'parentName' => 'Trang chủ',
            'parentUrl' => '/danh-sach-yeu-thich',
        ];
        view()->share('pageOption', $pageOption);
        $listCarLove = CommonHelper::getFromCache('favorite_by_user_id');
        if (!$listCarLove) {
            $listCarLove = Favorite::select('id', 'user_id', 'product_id', 'author_id')->where('user_id', @Auth::user()->id)->orderBy('updated_at', 'desc') ->paginate(10);
            CommonHelper::putToCache('favorite_by_user_id', $listCarLove);
        }
        $data['listCarLove'] = $listCarLove;
        return view('frontend.childs.carlove.index')->with($data);
    }

    public function postCarLove(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->except('_token');
            $count = Favorite::where('user_id', $data['user_id'])->where('product_id', $data['product_id'])->count();
            if ($count == 0) {
                Favorite::create($data);
                \Illuminate\Support\Facades\Cache::flush();
            }
        }
    }

    /**không thích*/
    public function postDeleteLove(Request $request)
    {
        if ($request->ajax()) {
            $delete = Favorite::where('product_id', $request->id)->first();
            $delete->delete();
            \Illuminate\Support\Facades\Cache::flush();
        }
    }

    /**quản lý đăng tin*/
    public function getNewProduct()
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Đăng tin bán xe',
            'parentName' => 'Trang chủ',
            'parentUrl' => '/dang-tin',
        ];
        view()->share('pageOption', $pageOption);
        return view('frontend.childs.product.new_post');
    }

    public function getPostedProduct()
    {
        return view('frontend.childs.product.posted');
    }

    /**thông báo tin đăng hoàn tất*/
    public function getCompleted()
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Đăng tin bán xe',
            'parentName' => 'Trang chủ',
            'parentUrl' => '/dang-tin/hoan-tat',
        ];
        view()->share('pageOption', $pageOption);
        return view('frontend.childs.product.completed');
    }

    public function postNewProduct(Request $request)
    {
        $data = new Product;
        $data->author_id = \Auth::user()->id;
        $data->tinhtrangxe = $request['tinhtrangxe'];
        $data->odo = $request['odo'];
        $data->province_id = $request['province_id'];
        $province_field_name = Province::select('id', 'name')->where('id', $request->province_id)->first();
        $data->manufacturer_id = $request['manufacturer_id'];
        $manufacturer_field_name = Manufacturer::select('id', 'name')->where('id', $request->manufacturer_id)->first();
        $data->manufacturer_model_id = $request['manufacturer_model_id'];
        $manufacturer_model_field_name = ManufacturerModel::select('id', 'name')->where('id', $request->manufacturer_model_id)->first();
        $data->sub_model = $request['sub_model'];
        $data->transmission = $request['transmission'];
        $data->fuel = $request['fuel'];
        $data->drivertrain = $request['drivertrain'];
        $data->producttype_id = $request['producttype_id'];
        $productty_field_name = ProductType::select('name')->where('id', $request->producttype_id)->first();
        $data->exterior = $request['exterior'];
        $exterior_field_name = Colors::select('name')->where('id', $request->exterior)->first();
        $data->interior = $request['interior'];
        $interior_field_name = Colors::select('name')->where('id', $request->interior)->first();
        $data->year = $request['year'];
        $price_real = str_replace('.', '', $request['price']);
        $data->price_real = $price_real;
        $data->price = $request->price;
        if ($price_real >= 1000000000) {
            $data->price_text = 'Tỷ';
        }
        $data->country = $request['country'];
        $data->name = $request['name'];
        $data->phone1 = $request['phone1'];
        $data->phone2 = $request['phone2'];
        $data->email = $request['email'];
        $data->address = $request['address'];
        $data->description = $request['description'];
        $data->meta_title = $request['meta_title'];
        $data->meta_description = $request['meta_description'];
        $data->meta_keywords = $request['meta_keywords'];
        $data->status = 1;
        $data->pending = 'Đang chờ duyệt';
        $img_extra = '';
        for ($i = 1; $i <= 6; $i++) {
            if ($request->get('my_file_va_' . $i) != null && $request->get('my_file_va_' . $i) != '') {
                $img_extra .= $request->get('my_file_va_' . $i) . '|';
                if (!isset($img)) $img = $request->get('my_file_va_' . $i);
            }
        }
        $data->image_extra = substr($img_extra, 0, -1);
        $data->image = $img;
        $data->name = $request->tinhtrangxe . " " . $manufacturer_field_name->name . " " . $manufacturer_model_field_name->name . " " . $request->sub_model . " " . $request->year;
        \Illuminate\Support\Facades\Cache::flush();
        $data->save();
        return redirect()->action('Frontend\ProductController@getCompleted');
    }

    /**chờ duyệt*/
    public function getPending(Request $request)
    {
        if (Auth::user()) {
            $duongdan = $request->path();
            $data['duongdan'] = $duongdan;
            $get_product = CommonHelper::getFromCache('product_dangchoduyet_by_author_id');
            if (!$get_product) {
                $get_product = Product::wherein('pending', ['Đang chờ duyệt'])->where('status', 1)->where('author_id', Auth::user()->id)->paginate(10);
                $data['get_product'] = $get_product;
                CommonHelper::putToCache('product_dangchoduyet_by_author_id', $get_product);
            }
            $pageOption = [
                'type' => 'page',
                'pageName' => 'Danh sách tin đã đăng',
                'parentName' => 'Trang chủ',
                'parentUrl' => '/dang-cho-duyet',
            ];
            view()->share('pageOption', $pageOption);
            return view('frontend.childs.product.Pending')->with($data);
        }
    }

    /**thay đổi chờ duyệt*/
    public function getThaydoichoduyet(Request $request)
    {
        if (Auth::user()) {
            $duongdan = $request->path();
            $data['duongdan'] = $duongdan;
            $thay_doi_cho_duyet_product = CommonHelper::getFromCache('product_thaydoichoduyet_by_author_id');
            if (!$thay_doi_cho_duyet_product) {
                $thay_doi_cho_duyet_product = Product::wherein('pending', ['Thay đổi chờ duyệt'])->where('status', 1)->where('author_id', Auth::user()->id)->paginate(10);
                $data['thay_doi_cho_duyet_product'] = $thay_doi_cho_duyet_product;
                CommonHelper::putToCache('product_thaydoichoduyet_by_author_id', $thay_doi_cho_duyet_product);
            }
            $pageOption = [
                'type' => 'page',
                'pageName' => 'Danh sách tin đã đăng',
                'parentName' => 'Trang chủ',
                'parentUrl' => '/xe-thay-doi-cho-duyet',
            ];
            view()->share('pageOption', $pageOption);
            return view('frontend.childs.product.change_Pending')->with($data);
        }
    }

    /**xe đang rao bán*/
    public function getCarsale(Request $request)
    {
        if (Auth::user()) {
            $duongdan = $request->path();
            $data['duongdan'] = $duongdan;
            $xe_dang_rao_ban = CommonHelper::getFromCache('product_dangban_by_author_id');
            if (!$xe_dang_rao_ban) {
                $xe_dang_rao_ban = Product::wherein('pending', ['Đang bán'])->where('status', 1)->where('author_id', Auth::user()->id)->paginate(10);
                $data['xe_dang_rao_ban'] = $xe_dang_rao_ban;
                CommonHelper::putToCache('product_dangban_by_author_id', $xe_dang_rao_ban);
            }
            $pageOption = [
                'type' => 'page',
                'pageName' => 'Danh sách tin đã đăng',
                'parentName' => 'Trang chủ',
                'parentUrl' => '/xe-dang-rao-ban',
            ];
            view()->share('pageOption', $pageOption);
            return view('frontend.childs.product.car_are_selling')->with($data);
        }
    }

    /**xe đã bán*/
    public function getCarsold(Request $request)
    {
        if (\Auth::user()) {
            $duongdan = $request->path();
            $data['duongdan'] = $duongdan;
            $xe_da_ban = CommonHelper::getFromCache('product_daban_by_author_id');
            if (!$xe_da_ban) {
                $xe_da_ban = Product::wherein('pending', ['Đã bán'])->where('status', 1)->where('author_id', Auth::user()->id)->paginate(10);
                $data['xe_da_ban'] = $xe_da_ban;
                CommonHelper::putToCache('product_daban_by_author_id', $xe_da_ban);
            }
            $pageOption = [
                'type' => 'page',
                'pageName' => 'Danh sách tin đã đăng',
                'parentName' => 'Trang chủ',
                'parentUrl' => '/xe-da-ban',
            ];
            view()->share('pageOption', $pageOption);
            return view('frontend.childs.product.car_sold')->with($data);
        }
    }

    /**từ chối đăng xe*/
    public function getRefuse(Request $request)
    {
        if (\Auth::user()) {
            $duongdan = $request->path();
            $data['duongdan'] = $duongdan;
            $tu_choi_dang_xe = CommonHelper::getFromCache('product_tuchoi_by_author_id');
            if (!$tu_choi_dang_xe) {
                $tu_choi_dang_xe = Product::wherein('pending', ['Từ chối'])->where('status', 1)->where('author_id', Auth::user()->id)->paginate(10);
                $data['tu_choi_dang_xe'] = $tu_choi_dang_xe;
                CommonHelper::putToCache('product_tuchoi_by_author_id', $tu_choi_dang_xe);
            }
            $pageOption = [
                'type' => 'page',
                'pageName' => 'Danh sách tin đã đăng',
                'parentName' => 'Trang chủ',
                'parentUrl' => '/tu-choi',
            ];
            view()->share('pageOption', $pageOption);
            return view('frontend.childs.product.car_refuse')->with($data);
        }
    }

    /**hết hạn*/
    public function getExpired(Request $request)
    {
        if (\Auth::user()) {
            $duongdan = $request->path();
            $data['duongdan'] = $duongdan;
            $het_han = CommonHelper::getFromCache('product_hethan_by_author_id');
            if (!$het_han) {
                $het_han = Product::wherein('pending', ['Hết hạn'])->where('status', 1)->where('author_id', Auth::user()->id)->paginate(10);
                $data['het_han'] = $het_han;
                CommonHelper::putToCache('product_hethan_by_author_id', $het_han);
            }
            $pageOption = [
                'type' => 'page',
                'pageName' => 'Danh sách tin đã đăng',
                'parentName' => 'Trang chủ',
                'parentUrl' => '/het-han',
            ];
            view()->share('pageOption', $pageOption);
            return view('frontend.childs.product.expired')->with($data);
        }
    }

    /** sửa thông tin xe oto */
    public function getEditPostCar($id)
    {
        if (\Auth::user()) {
            $editproduct = CommonHelper::getFromCache('product_by_id');
            if (!$editproduct) {
                $editproduct = Product::find($id);
                $data['editproduct'] = $editproduct;
                CommonHelper::putToCache('product_by_id', $editproduct);
            }
            $pageOption = [
                'type' => 'page',
                'pageName' => 'Chỉnh sửa tin',
                'parentName' => 'Trang chủ',
                'parentUrl' => '/chinh-sua-tin',
            ];
            view()->share('pageOption', $pageOption);
            return view('frontend.childs.product.edit_post')->with($data);
        }
    }

    /**Trang cập nhật thông tin xe thành công*/
    public function getUpadateComopleted()
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Chỉnh sửa tin',
            'parentName' => 'Trang chủ',
            'parentUrl' => '/chinh-sua-tin-oto/hoan-tat',
        ];
        view()->share('pageOption', $pageOption);
        return view('frontend.childs.product.update_completed');
    }

    public function postEditPostCar(Request $request, $id)
    {
        $data = Product::find($id);
        $data->author_id = \Auth::user()->id;
        $data->tinhtrangxe = $request['tinhtrangxe'];
        $data->odo = $request['odo'];
        $data->province_id = $request['province_id'];
        $province_field_name = Province::select('id', 'name')->where('id', $request->province_id)->first();
        $data->manufacturer_id = $request['manufacturer_id'];
        $manufacturer_field_name = Manufacturer::select('id', 'name')->where('id', $request->manufacturer_id)->first();
        $data->manufacturer_model_id = $request['manufacturer_model_id'];
        $manufacturer_model_field_name = ManufacturerModel::select('id', 'name')->where('id', $request->manufacturer_model_id)->first();
        $data->sub_model = $request['sub_model'];
        $data->transmission = $request['transmission'];
        $data->fuel = $request['fuel'];
        $data->drivertrain = $request['drivertrain'];
        $data->producttype_id = $request['body_style'];
        $productty_field_name = ProductType::select('name')->where('id', $request->body_style)->first();
        $data->exterior = $request['exterior'];
        $exterior_field_name = Colors::select('name')->where('id', $request->exterior)->first();
        $data->interior = $request['interior'];
        $interior_field_name = Colors::select('name')->where('id', $request->interior)->first();
        $data->year = $request['year'];
        $price_real = str_replace('.', '', $request['price']);
        $data->price_real = $price_real;
        $data->price = $request->price;
        if ($price_real >= 1000000000) {
            $data->price_text = 'Tỷ';
        } else {
            $data->price_text = 'Triệu';
        }
        $data->country = $request['country'];
        $data->name = $request['name'];
        $data->phone1 = $request['phone1'];
        $data->phone2 = $request['phone2'];
        $data->email = $request['email'];
        $data->address = $request['address'];
        $data->description = $request['description'];
        $data->meta_title = $request['meta_title'];
        $data->meta_description = $request['meta_description'];
        $data->meta_keywords = $request['meta_keywords'];
        $data->status = 1;
        $data->pending = 'Thay đổi chờ duyệt';
        $img_extra = '';
        for ($i = 1; $i <= 6; $i++) {
            if ($request->get('my_file_va_' . $i) != null && $request->get('my_file_va_' . $i) != '') {
                $img_extra .= $request->get('my_file_va_' . $i) . '|';
                if (!isset($img)) $img = $request->get('my_file_va_' . $i);
            }
        }
        $data->image_extra = substr($img_extra, 0, -1);
        $data->image = $img;
        $data->name = $request->tinhtrangxe . " " . $manufacturer_field_name->name . " " . $manufacturer_model_field_name->name . " " . $request->sub_model . " " . $request->year;
        $data->update();
        \Illuminate\Support\Facades\Cache::flush();
        return redirect()->action('Frontend\ProductController@getUpadateComopleted');
    }

    /**xóa thông tin xe oto*/
    public function getDelete($id)
    {
        $delete = Product::find($id);
        \Illuminate\Support\Facades\Cache::flush();
        if ($delete->delete()) {
            echo "<script>alert('xóa thành công')</script>";
            return redirect()->action('Frontend\ProductController@getPending');
        }
    }

    public function getSearch(Request $request)
    {
        $data['products'] = CommonHelper::getFromCache('search_product_by_code_' . base64_encode(json_encode($request->all())));
        if (!$data['products']) {
            $products = Product::where('status', 1)->wherein('pending',['Đang bán']);
            if ($request->has('key')) {
                $products = $products->where('name', 'LIKE', "%" . $request->get('key', '') . "%");
            }
            if ($request->has('manufacturer_id') && $request->manufacturer_id != '') {
                $products = $products->where('manufacturer_id', '>=', $request->manufacturer_id);
            }
            if ($request->has('user_id') && $request->user_id != '') {
                $products = $products->where('author_id', $request->user_id);
            }
            if ($request->has('tinhtrangxe') && $request->tinhtrangxe != '') {
                $products = $products->where('tinhtrangxe', $request->tinhtrangxe);
            }
            if ($request->has('min_price') && $request->min_price != '') {
                $products = $products->where('price_real', '>=', $request->min_price);
            }
            if ($request->has('max_price') && $request->max_price != '') {
                $products = $products->where('price_real', '<=', $request->max_price);
            }
            if ($request->has('first_year') && $request->first_year != '') {
                $products = $products->where('year', '>=', $request->first_year);
            }
            if ($request->has('last_year') && $request->last_year != '') {
                $products = $products->where('year', '<=', $request->last_year);
            }
            if ((@$_GET['province_id']) && @$_GET['province_id'] != '') {
                $products = $products->where('province_id', @$_GET['province_id']);
            }
            if (@$_GET['manufacturer_id'] && @$_GET['manufacturer_id'] != '') {
                $products = $products->where('manufacturer_id', @$_GET['manufacturer_id']);
            }
            if (@$_GET['manufacturer_model_id'] && @$_GET['manufacturer_model_id'] != '') {
                $products = $products->where('manufacturer_model_id', @$_GET['manufacturer_model_id']);
            }
            if (@$_GET['product_type'] && @$_GET['product_type'] != '') {
                $products = $products->where('producttype_id', @$_GET['product_type']);
            }
            if (@$_GET['dai-ly']) {
                $user = User::select('id')->wherein('role', ['Đại lý'])->first();
                $products = $products->where('author_id', $user->id);
            }
            $total = $products;
            $data['total'] = $total->count();
            $data['products'] = $products->orderBy('updated_at', 'desc')->paginate(20);
            CommonHelper::putToCache('search_product_by_code_' . base64_encode(json_encode($request->all())), $data['products']);
        }
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Tìm kiếm',
            'parentName' => 'Trang chủ',
            'parentUrl' => '/tim-kiem',
        ];
        view()->share('pageOption', $pageOption);
        \Illuminate\Support\Facades\Cache::flush();
        return view('frontend.childs.search.list_product')->with($data);
    }
    /**--------------------------------------------------------------------------------------------------------------------------*/
}