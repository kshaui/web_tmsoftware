<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Models\Bill;
use App\Models\Order;
use App\Models\Product;
use App\Models\Settings;
use App\Models\User;
use Illuminate\Http\Request;
use Mail;
use Session;

class OrderController extends Controller
{
    public function getIndex()
    {
//        Session::forget('cart');
        if (Session::get('cart') == null) {
            $data['products'] = [];
        } else {
            $cart = Session::get('cart');
            $product_id_arr = array_keys($cart['items']);
            $data['products'] = Product::select(['id', 'name', 'slug', 'multi_cat', 'image', 'author_id', 'code', 'base_price', 'final_price', 'ebook_price'])
                ->where('status', 1)->whereIn('id', $product_id_arr)->get();
        }

        $pageOption = [
            'type' => 'page',
            'pageName' => 'Giỏ hàng',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.childs.order.view')->with($data);
    }

    public function getDelivery()
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Giao hàng',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        if (!\Auth::check()) {
            return redirect()->route('user.login');
        }

        return view('frontend.childs.order.delivery');
    }

    public function postDelivery(Request $request)
    {
        if (!\Auth::check()) {
            return redirect()->route('user.login');
        }

        $data = $request->except('_token');
        Session::put('cart_delivery', $data);

        return redirect()->route('order.pay');
    }

    public function getPay()
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Thanh toán',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.childs.order.pay');
    }

    public function addToCart(Request $request)
    {
        $product_id = $request->get('product_id', 0);
        $quantity = $request->get('quantity', 1);
        $product_type = $request->get('product_type', '');

        if (Session::has('cart')) { // Neu da co gio hang
            $cart = Session::get('cart');
            if (array_key_exists($product_id, $cart['items'])) {  //  Neu da ton tai san pham trong gio hang thi update so luong
                $cart['items'][$product_id]['quantity'] += $quantity;
            } else {
                $cart['items'][$product_id]['quantity'] = $quantity;
            }
            //  Them thuoc tinh san pham vao gio hang
            $cart['items'][$product_id]['attributes']['product_type'] = $product_type;
        } else {
            //  Cap nhat so luong san pham
            $cart['items'][$product_id]['quantity'] = $quantity;
            //  Them thuoc tinh san pham vao gio hang
            $cart['items'][$product_id]['attributes']['product_type'] = $product_type;
        }
        //  Tinh lai gia tien gio hang
        if ($product_type == 3) {        //  Neu la sach Ebook thi lay gia ebook_price
            $price = Product::select(['ebook_price'])->where('id', $product_id)->first()->ebook_price;
        } else {        //  Neu khong thi lay gia mac dinh
            $price = Product::select(['final_price'])->where('id', $product_id)->first()->final_price;
        }
        $total_price = isset($cart['total_price']) ? $cart['total_price'] + ($price * $quantity) : ($price * $quantity);
        $cart['total_price'] = $total_price;
        Session::put('cart', $cart);

        //  Dem so san pham trong gio hang
        $count_cart = 0;
        foreach ($cart['items'] as $v) {
            $count_cart += $v['quantity'];
        }
        return response()->json([
            'status' => true,
            'count_cart' => $count_cart,
            'cart_total_price' => $cart['total_price'],
            'cart_total_price_format' => number_format($cart['total_price'], 0, '.', '.') . 'đ'
        ]);
    }

    public function deleteFromCart(Request $request)
    {
        $product_id = $request->get('product_id', 0);
        if (!Session::has('cart')) {
            return false;
        }

        $cart = Session::get('cart');

        //  Cap nhat la gia gio hang
        if ($cart['items'][$product_id]['attributes']['product_type'] == 3) {        //  Neu la sach Ebook thi lay gia ebook_price
            $price = Product::select(['ebook_price'])->where('id', $product_id)->first()->ebook_price;
        } else {        //  Neu khong thi lay gia mac dinh
            $price = Product::select(['final_price'])->where('id', $product_id)->first()->final_price;
        }
        if (count($cart['items']) == 0) {
            $cart['total_price'] = 0;
        } else {
            $cart['total_price'] = $cart['total_price'] - ($price * $cart['items'][$product_id]['quantity']);
        }
        //  Xoa san pham khoi gio hang
        unset($cart['items'][$product_id]);
        Session::put('cart', $cart);

        //  Dem so luong san pham trong igo hang
        $count_cart = 0;
        foreach ($cart['items'] as $v) {
            $count_cart += $v['quantity'];
        }
        return response()->json([
            'status' => true,
            'count_cart' => $count_cart
        ]);
    }

    public function updateCart(Request $request)
    {
        $product_id = $request->get('product_id', 0);
        $quantity = $request->get('quantity', 1);
        if ($quantity == 0) {
            $this->deleteFromCart($request);
        }

        if (!Session::has('cart')) {
            return false;
        }

        $cart = Session::get('cart');
        if (!array_key_exists($product_id, $cart['items'])) {  //  Neu da ton tai san pham trong gio hang thi update so luong
            return response()->json([
                'status' => false,
                'msg' => 'Sản phẩm không tồn tại trong giỏ hàng'
            ]);
        }

        if ($cart['items'][$product_id]['attributes']['product_type'] == 3) {        //  Neu la sach Ebook thi lay gia ebook_price
            $price = Product::select(['ebook_price'])->where('id', $product_id)->first()->ebook_price;
        } else {        //  Neu khong thi lay gia mac dinh
            $price = Product::select(['final_price'])->where('id', $product_id)->first()->final_price;
        }
        $cart['total_price'] = $cart['total_price'] - ($cart['items'][$product_id]['quantity'] * $price);
        $cart['items'][$product_id]['quantity'] = $quantity;
        $cart['total_price'] = $cart['total_price'] + ($cart['items'][$product_id]['quantity'] * $price);
        Session::put('cart', $cart);

        $count_cart = 0;
        foreach ($cart['items'] as $v) {
            $count_cart += $v['quantity'];
        }
        return response()->json([
            'status' => true,
            'count_cart' => $count_cart
        ]);
    }

    public function createBill(Request $request)
    {
        if (!Session::has('cart')) {
            \Session::flash('error', 'Mật khẩu không khớp');
            return redirect()->route('order.pay');
        }
        $user_data = $request->get('user');
        $bill_data = $request->get('bill');

        $cart = Session::get('cart');
        $total_price = 0;
        $products = Product::select(['name', 'image', 'id', 'final_price', 'ebook_price'])->whereIn('id', array_keys($cart['items']))->get();
        $product_arr = [];
        foreach ($products as $v) {
            if ($cart['items'][$v->id]['attributes']['product_type'] == 3) {        //  Neu la sach Ebook thi lay gia ebook_price
                $total_price += ($v->ebook_price * $cart['items'][$v->id]['quantity']);
            } else {        //  Neu khong thi lay gia mac dinh
                $total_price += ($v->final_price * $cart['items'][$v->id]['quantity']);
            }
            $product_arr[$v->id] = $v;
        }
        if (!isset($bill_data['user_id'])) {         // Neu user chua dang nhap thi tao moi hoac tim trong db
            $user_db = User::where('tel', $user_data['tel'])->first();
            if (!is_object($user_db)) {          // Chua co user trong database thi tao moi
                $user_insert = User::create($user_data);
                $user_id = $user_insert->id;
            } else {
                $user_id = $user_db->id;
            }
            $bill_data['user_id'] = $user_id;
        }

        $bill_data['total_price'] = $total_price;
        $bill = Bill::create($bill_data);

        foreach ($cart['items'] as $product_id => $data) {
            if ($cart['items'][$v->id]['attributes']['product_type'] == 3) {        //  Neu la sach Ebook thi lay gia ebook_price
                $price = $data['quantity'] * $product_arr[$product_id]->ebook_price;
                $product_price = $product_arr[$product_id]->ebook_price;
            } else {        //  Neu khong thi lay gia mac dinh
                $price = $data['quantity'] * $product_arr[$product_id]->final_price;
                $product_price = $product_arr[$product_id]->final_price;
            }
            Order::create([
                'bill_id' => $bill->id,
                'price' => $price,
                'product_id' => $product_id,
                'quantity' => $data['quantity'],
                'product_name' => $product_arr[$product_id]->name,
                'product_price' => $product_price,
                'product_image' => $product_arr[$product_id]->image,
                'product_type' => $data['attributes']['product_type']
            ]);
        }

        Session::forget('cart');
        try {
            Mail::send('emails.new_order', ['bill' => $bill], function ($message) {
                $admin_config = Settings::where('type', 'admin_info')->pluck('value', 'name')->toArray();
                $message->from(env('MAIL_USERNAME'), '[' . Settings::select(['value'])->where('name', 'name')->value . ']');
                $message->to($admin_config['admin_email'], $admin_config['admin_name'])->subject('Đơn hàng mới');
            });
        } catch (\Exception $ex) {

        }

        \Session::flash('success', 'Đặt hàng thành công! Chúng tôi sẽ sớm liên hệ cho bạn để giao hàng');
        return redirect()->route('order.pay_success');
    }

    public function pay_success()
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Thanh toán thành công',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.childs.order.pay_success');
    }
}