<?php


namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Post;
use Illuminate\Http\Request;

class ManufacturerController extends Controller
{
    public function getIndex()
    {
        $pageOption = [
            'type'      => 'page',
            'pageName'  => 'Hãng xe',
            'parentName' => 'Trang chủ',
            'parentUrl' => '/hang-xe',
        ];
        view()->share('pageOption', $pageOption);
        return view('frontend.childs.menufacturer.list_manufacturer');
    }
    public function getAutomaker(){
        $pageOption = [
            'type'      => 'page',
            'pageName'  => 'Dòng xe',
            'parentName' => 'Trang chủ',
            'parentUrl' => '/dong-xe',
        ];
        view()->share('pageOption', $pageOption);
        return view('frontend.childs.menufacturer.index');
    }

}