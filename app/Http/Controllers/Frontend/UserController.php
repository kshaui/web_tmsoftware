<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Settings;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;
use Auth;

class UserController extends Controller
{
    public function getLogin()
    {
        if(Auth::check()) {
            return redirect()->route('order.getDelivery');
        }

        if (Session::get('cart') == null) {
            $data['products'] = [];
        } else {
            $cart = Session::get('cart');
            $product_id_arr = array_keys($cart);
            $data['products'] = Product::select(['id', 'name', 'slug', 'multi_cat', 'image', 'author_id', 'code', 'base_price', 'final_price'])
                ->where('status', 1)->whereIn('id', $product_id_arr)->get();
            $data['product_quantity'] = $cart;
        }

        $pageOption = [
            'type' => 'page',
            'pageName' => 'Đăng nhập',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.childs.user.login')->with($data);
    }

    public function postRegister(Request $request)
    {
        $data = $request->except('_token');

        if ($data['password'] != $data['re_password']) {
            \Session::flash('error', 'Mật khẩu không khớp');
            if($request->has('redirect_back')) {
                return redirect($request->redirect_back);
            }
            return redirect()->back();
        }

        unset($data['re_password']);
        $data['password'] = bcrypt($data['password']);
        if (!isset($data['email'])) {
            $data['email'] = $data['tel'] . '@autogenerated.com';
        }

        $user_db = User::where('email', $data['email'])->orWhere('tel', $data['tel'])->first();
        if (is_object($user_db)) {
            \Session::flash('error', 'Email hoặc số điện thoại đã tồn tại');
            if($request->has('redirect_back')) {
                return redirect($request->redirect_back);
            }
            return redirect()->back();
        }

        $user = User::create($data);
        if ($user) {
            if($request->has('ajax')) {
                return response()->json([
                    'status'    => true
                ]);
            } else {
                \Session::flash('success', 'Tạo tài khoản thành công!');
                Auth::login($user);
                if($request->has('redirect_back')) {
                    return redirect($request->redirect_back);
                }
                return redirect()->back();
            }
        }
        if($request->has('ajax')) {
            return response()->json([
                'status'    => false,
                'msg'       => 'Có lỗi xảy ra! Không tạo được tài khoản. Vui lòng load lại website và thử lại'
            ]);
        } else {
            \Session::flash('error', 'Có lỗi xảy ra! Không tạo được tài khoản. Vui lòng load lại website và thử lại');
            return redirect()->back();
        }
    }

    public function postLogin(Request $request)
    {
        $remember = true;
        $user_db = User::where('email', $request->email_tel)->orWhere('tel', $request->email_tel)->first();
        if (!is_object($user_db)) {
            if($request->has('ajax')) {
                return response()->json([
                    'status'    => false,
                    'msg'       => 'Email hoặc số điện thoại sai!'
                ]);
            } else {
                \Session::flash('error', 'Email hoặc số điện thoại sai!');
                return redirect()->back();
            }
        }

        if (Auth::attempt(['email' => $user_db->email, 'password' => $request->password], $remember)) {
            if($request->has('ajax')) {
                return response()->json([
                    'status'    => true
                ]);
            } else {
                \Session::flash('success', 'Đăng nhập thành công!');
                if($request->has('redirect_back')) {
                    return redirect($request->redirect_back);
                }
                return redirect()->back();
            }
        } else {
            if($request->has('ajax')) {
                return response()->json([
                    'status'    => false,
                    'msg'       => 'Sai mật khẩu'
                ]);
            } else {
                \Session::flash('error', 'Sai mật khẩu!');
                return redirect()->back();
            }
        }
    }
    public function getAllSalon() {
        $pageOption = [
            'type'      => 'page',
            'pageName'  => 'Danh sách đại lý',
            'parentName' => 'Trang chủ',
            'parentUrl' => '/danh-sach-salon-noi-bat',
        ];
        view()->share('pageOption', $pageOption);
        $data['salons'] = \App\Models\User::select(['id','name', 'image', 'tel','address'])->where('status', 1)->where('show_sidebar_homepage',1)->paginate(20);
        return view('frontend.childs.salon.list_salon')->with($data);
    }


    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function getLoginSingle() {
        if(Auth::check()) {
            return redirect('/');
        }

        $pageOption = [
            'type' => 'page',
            'pageName' => 'Đăng nhập',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.childs.user.login_single');
    }

    public function getRegisterSingle() {
        if(Auth::check()) {
            return redirect('/');
        }

        $pageOption = [
            'type' => 'page',
            'pageName' => 'Đăng ký',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.childs.user.register_single');
    }

    /*public function postLogin(Request $request)
    {
        $rules = array(
            'email' => 'required|email|max:200',
            'password' => 'required'
        );

        $fieldNames = array(
            'email' => 'Email',
            'password' => 'Password',
        );

        $remember = ($request->remember_me) ? true : false;

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($fieldNames);

        if ($validator->fails()) {
            if ($request->has('ajax')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Vui lòng nhập đủ thông tin'
                ]);
            }
            return back()->withErrors($validator)->withInput();
        } else {
            $email = $request->email;
            if (strpos($email, '@') == false) {
                $email = $email . '@autogenerated.com';
            }

            if (Auth::attempt(['email' => $email, 'password' => $request->password], $remember)) {
                if ($request->has('ajax')) {
                    return response()->json([
                        'status' => true
                    ]);
                }
                return redirect()->back();
            } else {
                if ($request->has('ajax')) {
                    return response()->json([
                        'status' => false,
                        'msg' => 'Sai thông tin tài khoản'
                    ]);
                }
                \Session::flash('error', 'Sai thông tin tài khoản!');
                return redirect()->route('user.login');
            }
        }
    }

    public function postRegister(Request $request)
    {
        $rules = array(
            'name' => 'required|max:255',
            'tel' => 'required|max:255',
            'email' => 'required|max:255|email|unique:users',
            'password' => 'required|min:6',
            're_password' => 'required|min:6'
        );

        $messages = array(
            'required' => ':attribute là bắt buộc.',
            'birthday_day.required' => 'Birth date field is required.',
            'name.required' => 'Tên là trường bắt buộc',
            'tel.required' => 'Số điện thoại là trường bắt buộc',
            'email.required' => 'Email là trường bắt buộc',
            'password.required' => 'Mật khẩu là trường bắt buộc',
            're_password.required' => 'Bạn chưa nhập lại mật khẩu'
        );

        $fieldNames = array(
            'name' => 'Tên',
            'tel' => 'Số điện thoại',
            'email' => 'Email',
            'password' => 'Mật khẩu',
            're_password' => 'Nhập lại mật khẩu'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($fieldNames);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            $data = $request->except('_token');
            $data['password'] = bcrypt($data['password']);
            unset($data['re_password']);
            $user = User::create($data);

            Auth::login($user);
            if($request->has('ajax')) {
                return response()->json([
                    'status'    => true
                ]);
            }

            return redirect()->back();
        }
    }*/

    public function getForgotPassword()
    {
        if (Auth::check()) {
            return redirect('/');
        }

        $pageOption = [
            'type' => 'page',
            'pageName' => 'Quên mật khẩu',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.childs.user.forgot_password');
    }

    public function postForgotPassword(Request $request)
    {
        $this->validate($request, [
            'email' => 'required'
        ], [
            'email.required' => 'Bắt buộc phải nhập email'
        ]);
        $user = User::where('email', $request->email)->first();
        if (!is_object($user)) {
            return redirect('quen-mat-khau')->with('flash_message', 'Email bạn vừa nhập không tồn tại!');
        }

        $user->change_password = $user->id . '_' . time();
        $user->save();
        $data['link'] = \URL::to('khoi-phuc-mat-khau') . '?change_password=' . $user->change_password;
        $settings = Settings::pluck('value', 'name')->toArray();
        Mail::send('emails.forgot_password', $data, function ($message) use ($settings, $user) {
            $message->from(env('MAIL_USERNAME'), '[' . $settings['name'] . ']');
            $message->to($user->email, $user->name)->subject('Khôi phục mật khẩu');
        });

        \Session::flash('success', 'Gửi yêu cầu thành công! Hãy kiểm tra mail và đổi lại mật khẩu');
        return redirect('quen-mat-khau');
    }

    public function getRecoveryPassword(Request $request)
    {
        $change_password = $request->get('change_password', '');
        $user = User::where('change_password', $change_password)->first();
        if (!is_object($user)) abort(404);
        return view('frontend.childs.user.restore_password');
    }

    public function postRecoveryPassword(Request $request)
    {
        $change_password = $request->get('change_password', '');
        $user = User::where('change_password', $change_password)->first();
        if (!is_object($user)) abort(404);

        if ($request->password != $request->confirm_password) {
            \Session::flash('error', 'Mật khẩu của bạn nhập không khớp');
            return redirect()->back();
        }

        $user->password = bcrypt($request->password);
        $user->change_password = '';
        $user->save();

        \Auth::login($user);

        \Session::flash('success', 'Thay đổi mật  khẩu thành công!');
        return redirect('/');
    }

}