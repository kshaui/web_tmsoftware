<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Models\Product;

class PostController extends Controller
{
    public function getIndex($productSlug)
    {
        $post = CommonHelper::getFromCache('post_slug_first_' . $productSlug);
        if (!$post) {
            $post = Post::where('slug', $productSlug)->where('status', 1)->first();
            CommonHelper::putToCache('post_slug_first_' . $productSlug, $post);
        }
        if (!is_object($post))
            abort(404);

        $data['post'] = $post;

        //  Danh cho seo
        $pageOption = [
            'type' => 'post',
            'parentUrl' => @$post->category->slug,
            'parentName' => @$post->category->name,
            'pageName' => $post->name,
            'link' => \URL::to(@$post->category->slug . '/' . $post->slug),
            'image' => \URL::to('filemanager/userfiles/' . $post->image),
            'title' => $post->meta_title != '' ? $post->meta_title : $post->name,
            'description' => $post->meta_description != '' ? $post->meta_description : $post->name,
            'keywords' => $post->meta_keywords != '' ? $post->meta_keywords : $post->name,
        ];
        view()->share('pageOption', $pageOption);
        return view('frontend.childs.post.detail')->with($data);
    }

    public function getNewPost()
    {
        return view('frontend.childs.post.new_post');
    }

    public function getPosted()
    {
        return view('frontend.childs.post.posted');
    }

    public function postNewPost(Request $request)
    {
        $data = new Product;
        $data->status = $request['status'];
        $data->odo = $request['odo'];
        $data->province_id = $request['province_id'];
        $data->manufacturer_id = $request['	manufacturer_id'];
        $data->manufacturer_model_id = $request['manufacturer_model_id'];
        $data->sub_model = $request['	sub_model'];
        $data->transmission = $request['transmission'];
        $data->fuel = $request['fuel'];
        $data->drivertrain = $request['	drivertrain'];
        $data->body_style = $request['body_style'];
        $data->exterior = $request['exterior'];
        $data->interior = $request['interior'];
        $data->year = $request['	year'];
        $data->price = $request['	price'];
       $data->save();
    }
    public function getSearch(Request $request)
    {
        $data['blogs'] = CommonHelper::getFromCache('post_show_homepage_1_name_' . base64_encode(json_encode($request->all())));
        if (!$data['blogs']) {
            $blogs = Post::select(['name', 'slug', 'intro','image'])->where('show_homepage',1)->where('status', 1);
            if ($request->has('key')) {
                $blogs = $blogs->where('name', 'LIKE', "%" . $request->get('key', '') . "%");
            }
            $data['blogs'] = $blogs->orderBy('updated_at', 'desc')->paginate(15);
            CommonHelper::putToCache('post_show_homepage_1_name_' . base64_encode(json_encode($request->all())), $data['blogs']);
        }
        if(count($data['blogs']) > 0){
            return view('frontend.childs.post.list_post_search')->with($data);
        }
        return view('frontend.childs.post.list_post_search')->withMessage('Thông tin bạn tìm kiếm không tồn tại');
    }

}