<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function getIndex()
    {
        $pageOption = [
            'type'      => 'page',
            'pageName'  => 'Liên hệ',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.childs.contact.index');
    }

    public function postIndex(Request $request) {
        $data = $request->except('_token');
        $data['user_ip'] = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $contact = Contact::create($data);

        try {
            Mail::send('emails.new_contact', ['bill' => $contact], function($message)
            {
                $admin_config = Settings::where('type', 'admin_info')->pluck('value', 'name')->toArray();
                $message->from(env('MAIL_USERNAME'), '['.Settings::select(['value'])->where('name', 'name')->value.']');
                $message->to($admin_config['admin_email'], $admin_config['admin_name'])->subject('Liên hệ mới');
            });
        } catch (\Exception $ex) {

        }

        return response()->json([
            'status'    => true
        ]);
    }
}