<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Author;
use App\Models\Category;
use App\Models\Company;
use App\Models\Post;
use App\Models\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function blog() {
        $pageOption = [
            'type'      => 'page',
            'pageName'  => 'Tin tức',
            'parentName' => 'Trang chủ',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);
        $data['posts'] = \App\Models\Post::select(['name', 'slug','intro','image','created_at'])->where('status', 1)->paginate(10);
        return view('frontend.childs.category.index')->with($data);
    }
    public function categoryPost($slug){
        $data['category'] = CommonHelper::getFromCache('get_category_by_slug_'.$slug);
        if (!$data['category']) {
            $data['category'] = Category::where('slug', $slug)->where('status', 1)->first();
            CommonHelper::putToCache('get_category_by_slug_'.$slug, $data['category']);
            if(!is_object($data['category']))
                abort(404);

            //  Tra ve view du lieu danh cho bo loc

        }
        $data['posts'] = CommonHelper::getFromCache('get_posts_by_category_id_'.$data['category']->id);
        if (!$data['posts']) {
            $data['posts'] = Post::select(['name','intro', 'image', 'slug', 'category_id','created_at'])->where('category_id', $data['category']->id)
                ->where('status', 1)->orderBy('updated_at', 'desc')->paginate(6);
            CommonHelper::putToCache('get_posts_by_category_id_'.$data['category']->id, $data['posts']);
        }
        return view('frontend.childs.category.category_post')->with($data);
    }

    public function audio(Request $request) {
        $data['products'] = CommonHelper::getFromCache('product_by_like_2');
        if (!$data['products']) {
            $data['products'] = Product::select(['name', 'image', 'slug', 'multi_cat', 'base_price', 'final_price', 'ebook_price', 'type'])->where('type', 'LIKE', '%|2|%')
                ->where('status', 1)->orderBy('updated_at', 'desc')->paginate(24);
            CommonHelper::putToCache('product_by_like_2', $data['products']);
        }

        $data['filter'] = $request->all();
        $data['text_filter'] = 'Sách audio';

        $pageOption = [
            'type'      => 'page',
            'pageName'  => 'Sách audio',
            'parentName' => 'Trang chủ',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.childs.category.index')->with($data);
    }

    public function getSearch(Request $request)
    {
        $data['products'] = CommonHelper::getFromCache('search_products_by_code_'.base64_encode(json_encode($request->all())));
        if (!$data['products']) {
            $group_id = $request->get('group_id', 0);
            $products = Product::select(['name', 'image', 'slug', 'multi_cat', 'base_price', 'final_price', 'ebook_price', 'type'])->where('status', 1);
            if($request->has('company_id')) {
                $products = $products->where('company_id', $request->get('company_id'));
            }
            if($request->has('author_id')) {
                $products = $products->where('author_id', $request->get('author_id'));
            }
            if($request->has('keyword')) {
                $products = $products->where('name', 'LIKE', "%".$request->get('keyword', '')."%");
            }
            if($group_id == 70) {
                $products = $products->where('type', 'LIKE', "%|2|%");
            } elseif($group_id != 0) {
                $products = $products->where('category_id', $group_id);
            }

            $data['products'] = $products->orderBy('updated_at', 'desc')->paginate(24);
            CommonHelper::putToCache('search_products_by_code_'.base64_encode(json_encode($request->all())), $data['products']);
        }

        $data['filter'] = $request->all();

        if($request->has('company_id')) {
            $data['text_filter'] = 'Công ty phát hành : ' . Company::find($request->get('company_id'))->name;
        }
        if($request->has('author_id')) {
            $data['text_filter'] = 'Tác giả : ' . Author::find($request->get('author_id'))->name;
        }
        if($request->has('keyword')) {
            $data['text_filter'] = 'Tìm kiếm với từ khóa : ' . $request->get('keyword', '');
        }

        $pageOption = [
            'type'      => 'page',
            'pageName'  => $data['text_filter'],
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.childs.category.index')->with($data);
    }
}