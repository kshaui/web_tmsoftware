<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\District;
use App\Models\ManufacturerModel;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Models\Banner;
use App\Models\Contact;
use Validator;

class HomeController extends Controller
{
    public function getIndex()
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Trang chủ',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.home.index');
    }


    public function ajax_get_model(Request $request)
    {
        $manufacturer_id = $request->get('manufacturer_id', 0);
        $models = CommonHelper::getFromCache('manufacturermodel_by_manufacturer_id_pluck_' . $manufacturer_id);
        if (!$models) {
            $models = ManufacturerModel::where('manufacturer_id', $manufacturer_id)->pluck('name', 'id');
            CommonHelper::putToCache('manufacturermodel_by_manufacturer_id_pluck_' . $manufacturer_id, $models);
        }
        $html = '<option value=""> Chọn Model</option>';
        foreach ($models as $id => $v) {
            $html .= '<option value="' . $id . '">' . $v . '</option>';
        }
        return $html;
    }

    public function ajax_get_manufacturer_model(Request $request)
    {
        $manufacturer_id = $request->get('manufacturer_id', 0);
        $manufacturer_model_id = CommonHelper::getFromCache('manufacturermodel_by_manufacturer_id_pluck_' . $manufacturer_id);
        if (!$manufacturer_model_id) {
            $manufacturer_model_id = ManufacturerModel::where('manufacturer_id', $manufacturer_id)->pluck('name', 'id');
            CommonHelper::putToCache('manufacturermodel_by_manufacturer_id_pluck_' . $manufacturer_id, $manufacturer_model_id);
        }
        $html = '<option value=""> Chọn Dòng xe</option>';
        foreach ($manufacturer_model_id as $id => $v) {
            $html .= '<option value="' . $id . '">' . $v . '</option>';
        }
        return $html;
    }

    //ký gửi nhà đất contact
    public function getContact()
    {
        return view('frontend.childs.contact.index');
    }

    public function postContact(Request $request)
    {
        $this->validate($request, [

            'txthoten' => 'required',
            'txtemail' => 'required|unique:contacts,email',
            'txtdienthoai' => 'required|unique:contacts,tel',
            'txtDiaChi' => 'required',
            'txtnoidung' => 'required'
        ]);
        $contact = new Contact;
        $contact->name = $request->txthoten;
        $contact->email = $request->txtemail;
        $contact->tel = $request->txtdienthoai;
        $contact->address = $request->txtDiaChi;
        $contact->content = $request->txtnoidung;
        $contact->save();
    }
    /**hình ảnh*/
    public function ajax_up_file(Request $request)
    {
        if ($request->has('file')) {
            $file_name = $request->file('file')->getClientOriginalName();
            $file_name = str_replace(' ', '', $file_name);
            $file_name_insert = date('s_i_') . $file_name;
            $request->file('file')->move(base_path() . '/public/filemanager/userfiles/uploads/', $file_name_insert);
        }
        return 'uploads/' . $file_name_insert;
    }

    public function cache_flush()
    {
        Cache::flush();
        return redirect('/');
    }
}