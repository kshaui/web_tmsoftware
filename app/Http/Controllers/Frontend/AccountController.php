<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\http\Requests;
class AccountController extends Controller
{
    public function getAccount()
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Tài khoản',
            'parentName' => 'Trang chủ',
            'parentUrl' => '/tai-khoan',
        ];
        view()->share('pageOption', $pageOption);
        return view('frontend.childs.account.index');
    }
    public function getEditAccount()
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Sửa thông tin',
            'parentName' => 'Trang chủ',
            'parentUrl' => '/sua-thong-tin',
        ];
        view()->share('pageOption', $pageOption);
        return view('frontend.childs.account.edit_account');
    }
    public function postEditAccount(Request $request)
    {
        if (Auth::check()) {
            $update_user = User::where('id', Auth::user()->id)->first();
            $update_user->name = $request['name'];
            $update_user->id_city = $request['province_id'];
            $update_user->address = $request['address'];
            $update_user->tel = $request['tel'];
            if ($request->has('image')) {
                $request->file('image')->move(base_path() . '/public/filemanager/userfiles/uploads/', Auth::user()->id . '_avatar.jpg');
                $update_user->image = 'uploads/' . Auth::user()->id . '_avatar.jpg';
            }
            $update_user->update();
            return redirect()->action('Frontend\AccountController@getAccount')->with('flash_message', 'Sửa thông tin thành công!');
        } else {
            return redirect('/dang-nhap');
        }
    }
    /**sửa email*/
    public function postEmail(Request $request)
    {
        if (Auth::user()) {
            $rules = array(
                'email' => 'sometimes|unique:users,email|required|email',
            );
            $validator = Validator::make (input::all(),$rules);
            if ($validator->fails()){
                return response::json(array('errors'=>$validator->getMessageBag()->toarray()));
            }else{
                $edit_email = User::find($request->id_email);
                $edit_email->email = $request->email;
                $edit_email->update();
            }
        }
    }
    /**sửa mật khẩu người dùng*/
    public function postPassword(Request $request)
    {
        if (Auth::user()) {
            $rules = array(
                'password' => 'required|min:6|max:20',
            );
            $validator = Validator::make(input::all(),$rules);
            if ($validator->fails()){
                return response::json(array('errors'=>$validator->getMessageBag()->toarray()));
            }else{
                $edit_password = User::find($request->id_password);
                $edit_password->password = bcrypt($request->password);
                $edit_password->update();
            }
        }
    }
}
