<?php
namespace App\Http\Controllers\Admin;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Http\Helpers\CommonHelper;
class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$model = Setting::select('value')->where('name','like','name')->first();
        //echo $model->value;die;
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        $menus = CommonHelper::getFromCache('all_menu');
        if (!$menus){
            $menus = Menu::orderBy('order_no','asc')->get();
            CommonHelper::putToCache('all_menu',$menus);
        }


//        return json_encode($menus);
        return view('admin.menu.index',compact('menus','settings'));
    }

    public function getRoute($path)
    {
        $model = Setting::select('value')->where('name',like,$path)->first();
        return $model->value;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        return view('admin.menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        \Illuminate\Support\Facades\Cache::flush();
        $this->validate($request,[
           'name'=>'unique:menus,name',
        ]);
        $menu = Menu::create($request->all());
        return redirect()->route('admin.menu.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        \Illuminate\Support\Facades\Cache::flush();
        $menuedit = Menu::findOrFail($id);
        return view('admin.menu.edit', compact('menuedit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        \Illuminate\Support\Facades\Cache::flush();
        $menuupdate = Menu::findOrFail($id);
        $menuupdate->update($request->all());
        return redirect()->route('admin.menu.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        \Illuminate\Support\Facades\Cache::flush();
        $menu = Menu::findOrFail($id);
        $menu->delete();
        return redirect()->route('admin.menu.index');
    }
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Menu::whereIn('id', $request->input('ids'))->get();
            \Illuminate\Support\Facades\Cache::flush();
            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
