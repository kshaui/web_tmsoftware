<?php

namespace App\Http\Helpers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Product;
use App\Models\Settings;
use View;
use Session;
use App\Models\Meta;
use App\Models\Permissions;
use App\Models\RoleAdmin;
use App\Models\PermissionRole;
use Spatie\Permission;
use App\Models\Menu;
use App\User;
use Illuminate\Support\Facades\Cache;

class CommonHelper
{

    function __construct()
    {
        setlocale(LC_ALL, 'vi_VN.UTF8');
    }

    public static function content_read($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    public static function one_time_message($class, $message)
    {
        if ($class == 'error') $class = 'danger';
        Session::flash('alert-class', 'alert-' . $class);
        Session::flash('message', $message);
    }

    public static function key_value($key, $value, $ar)
    {
        $ret = [];
        foreach ($ar as $k => $v) {
            $ret[$v[$key]] = $v[$value];
        }
        return $ret;
    }

    public static function current_action($route)
    {
        $current = explode('@', $route);
        View::share('current_action', $current[1]);
    }

    public static function has_permission($user_id, $permissions = '')
    {
        $permissions = explode('|', $permissions);
        $user_permissions = Permissions::whereIn('name', $permissions)->get();
        $permission_id = [];
        $i = 0;
        foreach ($user_permissions as $value) {
            $permission_id[$i++] = $value->id;
        }
        $role = RoleAdmin::where('admin_id', $user_id)->first();

        if (count($permission_id) && isset($role->role_id)) {
            $has_permit = PermissionRole::where('role_id', $role->role_id)->whereIn('permission_id', $permission_id);
            return $has_permit->count();
        } else return 0;
    }

    public static function meta($url, $field)
    {
        $metas = Meta::where('url', $url);

        if ($metas->count())
            return $metas->first()->$field;
        else if ($field == 'title')
            return 'Page Not Found';
        else
            return '';
    }

    public static function backup_tables($host, $user, $pass, $name, $tables = '*')
    {
        try {
            $con = mysqli_connect($host, $user, $pass, $name);
        } catch (Exception $e) {

        }

        if (mysqli_connect_errno()) {
            CommonHelper::one_time_message('danger', "Failed to connect to MySQL: " . mysqli_connect_error());
            return 0;
        }

        if ($tables == '*') {
            $tables = array();
            $result = mysqli_query($con, 'SHOW TABLES');
            while ($row = mysqli_fetch_row($result)) {
                $tables[] = $row[0];
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }

        $return = '';
        foreach ($tables as $table) {
            $result = mysqli_query($con, 'SELECT * FROM ' . $table);
            $num_fields = mysqli_num_fields($result);


            $row2 = mysqli_fetch_row(mysqli_query($con, 'SHOW CREATE TABLE ' . $table));
            $return .= "\n\n" . str_replace("CREATE TABLE", "CREATE TABLE IF NOT EXISTS", $row2[1]) . ";\n\n";

            for ($i = 0; $i < $num_fields; $i++) {
                while ($row = mysqli_fetch_row($result)) {
                    $return .= 'INSERT INTO ' . $table . ' VALUES(';
                    for ($j = 0; $j < $num_fields; $j++) {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = preg_replace("/\n/", "\\n", $row[$j]);
                        if (isset($row[$j])) {
                            $return .= '"' . $row[$j] . '"';
                        } else {
                            $return .= '""';
                        }
                        if ($j < ($num_fields - 1)) {
                            $return .= ',';
                        }
                    }
                    $return .= ");\n";
                }
            }

            $return .= "\n\n\n";
        }

        $backup_name = date('Y-m-d-His') . '.sql';

        $handle = fopen(storage_path("db-backups") . '/' . $backup_name, 'w+');
        fwrite($handle, $return);
        fclose($handle);

        return $backup_name;
    }

    public static function getUrlImageThumb($file, $width, $height)
    {
//        return \URL::asset('public/filemanager/userfiles/' . $file);
        $height = ($height != false && $height != 'auto') ? "&h=" . $height : "";
        $width = ($width != false && $width != 'auto') ? "&w=" . $width : "";
        try {
            if ($file != '' && file_exists(base_path() . '/public/filemanager/userfiles/' . $file)) {
                $file_exist = true;
            } else {
                $file_exist = false;
            }
            if ($file_exist) {
                return "/timthumb.php?src=" . \URL::asset('public/filemanager/userfiles/' . $file) . $width . $height;
            }
            return "/timthumb.php?src=" . \URL::asset('public/filemanager/userfiles/logo.png') . $width . $height;
        } catch (Exception $ex) {
            return "/timthumb.php?src=" . \URL::asset('public/filemanager/userfiles/logo.png') . $width . $height;
        }
    }

    public static function discount($base_price, $final_price, $type = '%')
    {
        $sale = $base_price - $final_price;
        if ($sale <= 0)
            return '';

        switch ($type) {
            case '-' :
                return '-' . number_format($sale, 0, '.', '.') . 'đ';
                break;
            default :
                return '-' . number_format(($sale / $base_price) * 100, 0, '.', '.') . '%';
        }
    }

    public static function getProductSlug($product)
    {
        try {
            $category_id = explode('|', $product->multi_cat)[1];
            $category_slug = CommonHelper::getFromCache('get_slug_category_by_id_'.$category_id);
            if (!$category_slug) {
                $category_slug = Category::select(['slug'])->where('id', $category_id)->first()->slug;
                CommonHelper::putToCache('get_slug_category_by_id_'.$category_id, $category_slug);
            }
            return \URL::to($category_slug . '/' . $product->slug);
        } catch (\Exception $ex) {
            return '/';
        }
    }

    public static function getFromCache($key)
    {
        return false;
        return Cache::get($key);
    }

    public static function putToCache($key, $value, $time = 60)
    {
        Cache::put($key, $value, $time);
        return true;
    }

    public static function removedFromCache($key)
    {
        Cache::forget($key);
        return true;
    }

    public static function getPriceHtml($product)
    {
        if(!isset($settings)) {
            $settings = CommonHelper::getFromCache('get_all_settings');
            if (!$settings) {
                $settings = Settings::pluck('value', 'name')->toArray();
                CommonHelper::putToCache('get_all_settings', $settings);
            }
        }

        $html = '';
        if (strpos($product->type, '|1|') !== false) {
            if ($product->base_price == 0 && $product->final_price != 0) {
                $html .= '<span class="b_price2">' . number_format($product->final_price, 0, '.', '.'). 'đ</span>';
            }
            if ($product->final_price == 0) {
                $html .= '<a href="tel:'. $settings['hotline']. '">Liên hệ</a>';
            } else {
                $html .= '<span class="b_price1">' . number_format($product->base_price, 0, '.', '.'). 'đ</span>
                                                    <span class="b_price2">' . number_format($product->final_price, 0, '.', '.'). 'đ</span>';
            }
            $discount = CommonHelper::discount($product->base_price, $product->final_price);
            if ($discount != '') {
                $html .= '<span class="b_price3">' . $discount. '</span>';
            }
        } else {
            if($product->ebook_price != 0) {
                $html .= '<span class="b_price2">' . number_format($product->ebook_price, 0, '.', '.'). 'đ</span>';
            }
        }
        return $html;
    }

    public static function getMenusByLocation($location, $limit = 10, $get_childs = true) {
        $data = CommonHelper::getFromCache('menus_by_location_'. $location);
        if (!$data) {
            $menus = \App\Models\Menu::where('location', $location)->where('parent_id', null)->orderBy('order_no', 'desc')->limit($limit)->get();
            foreach ($menus as $menu) {
                $item = [
                    'name' => $menu->name,
                    'childs' => []
                ];
                switch ($menu->type) {
                    case 'post':
                        $p = Post::find($menu->item_id);
                        $item['link'] = \URL::to(@$p->category->url .'/'. @$p->url);
                        break;
                    case 'product':
                        $p = Product::find($menu->item_id);
                        $item['link'] = \URL::to(@$p->category->url .'/'. @$p->url);
                        break;
                    case 'category':
                        $p = Category::find($menu->item_id);
                        $item['link'] = \URL::to(@$p->url);
                        break;
                    case 'option':
                        $item['link'] = $menu->url;
                        break;
                    default:
                        $item['link'] = $menu->url;
                        break;
                }
                //  get child menu
                foreach ($menu->childs as $menu) {
                    $child = [
                        'name' => $menu->name,
                        'childs' => []
                    ];
                    switch ($menu->type) {
                        case 'post':
                            $p = Post::find($menu->item_id);
                            $child['link'] = \URL::to(@$p->category->url .'/'. @$p->url);
                            break;
                        case 'product':
                            $p = Product::find($menu->item_id);
                            $child['link'] = \URL::to(@$p->category->url .'/'. @$p->url);
                            break;
                        case 'category':
                            $p = Category::find($menu->item_id);
                            $child['link'] = \URL::to(@$p->url);
                            break;
                        case 'option':
                            $child['link'] = '/tim-kiem?tinhtrangxe=' . $menu->tinhtrangxe . '&manufacturer_id=' . $menu->manufacturer_id . '&manufacturer_model_id=' . $menu->manufacturer_model_id;
                            break;
                        default:
                            $child['link'] = $menu->url;
                            break;
                    }

                    //  get child menu
                    foreach ($menu->childs as $menu) {
                        $child2 = [
                            'name' => $menu->name,
                            'childs' => []
                        ];
                        switch ($menu->type) {
                            case 'post':
                                $p = Post::find($menu->item_id);
                                $child2['link'] = \URL::to(@$p->category->slug . '/' . @$p->slug);
                                break;
                            case 'product':
                                $p = Product::find($menu->item_id);
                                $child2['link'] = \URL::to(@$p->category->slug . '/' . @$p->slug);
                                break;
                            case 'category':
                                $p = Category::find($menu->item_id);
                                $child2['link'] = \URL::to(@$p->slug);
                                break;
                            case 'option':
                                $child2['link'] = '/tim-kiem?tinhtrangxe=' . $menu->tinhtrangxe . '&manufacturer_id=' . $menu->manufacturer_id . '&manufacturer_model_id=' . $menu->manufacturer_model_id;
                                break;
                            default:
                                $child2['link'] = $menu->slug;
                                break;
                        }
                        $child['childs'][] = $child2;
                    }
                    $item['childs'][] = $child;
                }
                $data[] = $item;
            }
            CommonHelper::putToCache('menus_by_location_' . $location, $data);
        }
        return $data;
    }
}