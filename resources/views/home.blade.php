@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-xs-6 col-sm-6">
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>17</h3>
                            <p>Xe</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-home"></i>
                        </div>
                        <a href="http://demo2.webhobasoft.com/admin/product" class="small-box-footer">Xem thêm <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>9</h3>
                            <p>Bài viết</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-newspaper-o"></i>
                        </div>
                        <a href="http://demo2.webhobasoft.com/admin/post" class="small-box-footer">Xem thêm <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <div class="small-box bg-purple">
                        <div class="inner">
                            <h3>18</h3>
                            <p>Khách hàng</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-users"></i>
                        </div>
                        <a href="http://demo2.webhobasoft.com/admin/user" class="small-box-footer">Xem thêm <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <!-- small box -->
                    <div class="small-box bg-maroon">
                        <div class="inner">
                            <h3>5</h3>
                            <p>Ký gửi</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bill"></i>
                        </div>
                        <a href="http://demo2.webhobasoft.com/admin/contact" class="small-box-footer">Xem thêm <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <div class="small-box bg-teal">
                        <div class="inner">
                            <h3>3</h3>
                            <p>Nhân viên</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="/admin/admin_users" class="small-box-footer">Xem thêm <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <div class="small-box bg-teal">
                        <div class="inner">
                            <h3>16</h3>
                            <p>Hãng xe</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bill"></i>
                        </div>
                        <a href="/admin/manufacturers" class="small-box-footer">Xem thêm <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <!-- small box -->
                    <div class="small-box bg-maroon">
                        <div class="inner">
                            <h3>74</h3>
                            <p>Dòng xe</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bill"></i>
                        </div>
                        <a href="/admin/manufactuter_models" class="small-box-footer">Xem thêm <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
