<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/admin/home') }}" class="logo"
       style="font-size: 16px;">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
           @lang('global.global_title')</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
           @lang('global.global_title')</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        {{--<div id="google_translate_element"></div>--}}
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav" >
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown user" >
                   <P style="padding-top: 14px" id="google_translate_element"></P>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{ URL::asset('public/adminlte/img/avatar.png') }}" class="user-image" alt="User Image">
                        <span class="hidden-xs">Admin</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ URL::asset('public/adminlte/img/avatar.png') }}" class="img-circle" alt="User Image">

                            <p>
                                Admin
                                <small>Member
                                    since May, 2018</small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="/admin/edit_admin/1" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="#logout" onclick="$('#logout').submit();" class="btn btn-default btn-flat">Đăng xuất</a>
                            </div>
                        </li>







                    </ul>
                </li>
            </ul>
        </div>


    </nav>
</header>






