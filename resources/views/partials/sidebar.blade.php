@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

            <li class="@if(url(($_SERVER['REQUEST_URI'])) == url('admin/home')) active @endif">
                <a href="{{ url('admin/home') }}">
                    <i class="fa fa-dashboard"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>
            <li class="hidden-nav-xs padder m-t m-b-sm text-xs text-muted">
                Dữ liệu
            </li>
            <li class="@if(url(($_SERVER['REQUEST_URI'])) == route('admin.menu.index')) active @endif">
                <a href="{{ route('admin.menu.index') }}">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">Menu</span>
                </a>
            </li>
            <li class="treeview"><a href="#">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">Tin tức</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li>
                        <a href="">
                            <i class="#"></i>
                            <span class="title">
                                Danh mục tin tức
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class=""></i>
                            <span class="title">
                                Tin tức
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="@if(url(($_SERVER['REQUEST_URI'])) == route('admin.widgets.index')) active @endif">
                <a href="{{ route('admin.widgets.index') }}">
                    <i class="fa fa-plus-square-o"></i>
                    <span class="title">Widgets</span>
                </a>
            </li>
            <li class="hidden-nav-xs padder m-t m-b-sm text-xs text-muted">
                Người dùng
            </li>
            @can('users_manage')
                <li class="treeview @if(url(($_SERVER['REQUEST_URI'])) == route('admin.permissions.index')) active @endif
                @if(url(($_SERVER['REQUEST_URI'])) == route('admin.roles.index')) active @endif
                @if(url(($_SERVER['REQUEST_URI'])) == route('admin.users.index')) active @endif
                        ">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span class="title">@lang('global.user-management.title')</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">

                        <li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }} ">
                            <a href="{{ route('admin.permissions.index') }}">
                                <i class=""></i>
                                <span class="title">
                                @lang('global.permissions.title')
                            </span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.roles.index') }}">
                                <i class=""></i>
                                <span class="title">
                                @lang('global.roles.title')
                            </span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.users.index') }}">
                                <i class=""></i>
                                <span class="title">
                                @lang('global.users.title')
                            </span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan

            <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
                <a href="{{ route('auth.change_password') }}">
                    <i class="fa fa-key"></i>
                    <span class="title">Thay đổi mật khẩu</span>
                </a>
            </li>

            <li class="hidden-nav-xs padder m-t m-b-sm text-xs text-muted">
                Cấu hình
            </li>
            <li class="{{ $request->segment(1) == 'widgets' ? 'active' : '' }}">
                <a href="{{ route('admin.widgets.index') }}">
                    <i class="fa fa-gears"></i>
                    <span class="title">Cấu hình{{ @$settings['name'] }}</span>
                </a>
            </li>
            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('global.app_logout')</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">@lang('global.logout')</button>
{!! Form::close() !!}
