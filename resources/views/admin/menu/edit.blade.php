@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            Sửa @lang('global.menus.title')
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Sửa mới Menu</h3>
                    </div>
                    {!! Form::open(['method' => 'PUT','class'=>'form-horizontal','enctype'=>'multipart/form-data','route' => ['admin.menu.update',@$menuedit->id]]) !!}
                    <div class="box-body">
                        <div class="form-group" id="form-group-name">
                            {!! Form::label('name', 'Tên*', ['class' => 'control-label col-sm-3']) !!}
                            <div class="col-sm-9">
                                <input type="text" value="{{ @$menuedit->name }}" name="name" class="form-control">
                                <p class="help-block"></p>
                                @if($errors->has('name'))
                                    <p class="help-block">
                                        {{ $errors->first('name') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group" id="form-group-location">
                            {!! Form::label('name', 'Thuộc menu nào?*', ['class' => 'control-label col-sm-3']) !!}
                            <div class="col-sm-9">
                                <select class="form-control" required="" name="location">
                                    <option value="main_menu" @if($menuedit->location == 'main_menu') selected @endif >main menu
                                    </option>
                                    <option value="main_menu_blog" @if($menuedit->location == 'main_menu_blog') selected @endif >
                                        main menu blog
                                    </option>
                                    <option value="menu_footer" @if($menuedit->location == 'menu_footer') selected @endif>main menu
                                        footer
                                    </option>
                                </select>
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Menu cha*</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="parent_id">
                                    <option value="">Chọn menu cha</option>
                                    @php
                                        \Illuminate\Support\Facades\Cache::flush();
                                           $menu = \App\Http\Helpers\CommonHelper::getFromCache('menu_parent_id_null_get');
                                           if (!$menu){
                                               $menu = \App\Models\Menu::select('id','name','parent_id','location')->where('parent_id',NULL)->get();
                                               \App\Http\Helpers\CommonHelper::putToCache('menu_parent_id_null_get',$menu);
                                           }
                                    @endphp
                                    @foreach($menu as $menu)
                                        <option value="{{ $menu->id }}"
                                                @if(@$menuedit->parent_id == $menu->id) selected @endif >{{ $menu->name }}</option>
                                        @php
                                            \Illuminate\Support\Facades\Cache::flush();
                                               $menus = \App\Http\Helpers\CommonHelper::getFromCache('menus_parent_id_by_id_get');
                                               if (!$menus){
                                                   $menus = \App\Models\Menu::select('id','name','parent_id')->where('parent_id',$menu->id)->get();
                                                   \App\Http\Helpers\CommonHelper::putToCache('menus_parent_id_by_id_get',$menus);
                                               }
                                        @endphp
                                        @foreach($menus as $menus)
                                            <option value="{{ $menus->id }}"
                                                    @if(@$menuedit->parent_id == $menus->id) selected @endif >
                                                ---{{ @$menus->name }}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Loại menu*</label>
                            <div class="col-sm-9">
                                <select class="form-control" required="" name="type">
                                    <option value="url" @if($menuedit->url == 'url') selected @endif>URL</option>
                                    <option value="">Tùy chọn</option>
                                </select>
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">URL*</label>
                            <div class="col-sm-9">
                                <input type="text" name="url" required class="form-control" value="{{ @$menuedit->url }}">
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Thứ tự (Số to hiển thị trước)*</label>
                            <div class="col-sm-9">
                                <input type="text" name="order_no" required class="form-control"
                                       value="{{ @$menuedit->order_no }}">
                                <p class="help-block"></p>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a type="reset" class="btn btn-s-md btn-default"
                           href="{{ route('admin.menu.index') }}">Quay lại</a>
                        <button type="reset" class="btn btn-s-md btn-primary">Reset dữ liệu</button>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop

