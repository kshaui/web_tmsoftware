@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Menu
            <small>Bảng điều khiển</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                    <h3 class="box-title">Quản lý @lang('global.menus.title')</h3>
                    <div style="float:right;">
                        <a class="btn btn-success" href="{{ route('admin.menu.create') }}">Tạo
                            mới Menu</a>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        @lang('global.app_list')
                    </div>

                    <div class="panel-body table-responsive">
                        <table class="table table-bordered table-striped {{ count($menus) > 0 ? 'datatable' : '' }} dt-select">
                            <thead>
                            <tr>
                                <th style="text-align:center;"><input type="checkbox" id="select-all"/></th>
                                <th>ID</th>
                                <th>@lang('global.menus.fields.name')</th>
                                <th>@lang('global.menus.fields.loai')</th>
                                <th>@lang('global.menus.fields.thutu')</th>
                                <th>@lang('global.menus.fields.vitri')</th>
                                <th>&nbsp;</th>

                            </tr>
                            </thead>

                            <tbody>
                            @if (count($menus) > 0)
                                @foreach ($menus as $menu)
                                    <tr data-entry-id="{{ $menu->id }}">
                                        <td></td>
                                        <td>{{ @$menu->id }}</td>
                                        <td>{{ $menu->name }}</td>
                                        <td>{{ $menu->type }}</td>
                                        <td>{{ $menu->order_no }}</td>
                                        <td>{{ @$menu->location }}</td>
                                        <td>
                                            <a href="{{ route('admin.menu.edit',[$menu->id]) }}"
                                               class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                            {!! Form::open(array(
                                                'style' => 'display: inline-block;',
                                                'method' => 'DELETE',
                                                'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                                'route' => ['admin.menu.destroy', $menu->id])) !!}
                                            {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                            {!! Form::close() !!}
                                        </td>

                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.menu.mass_destroy') }}';
    </script>
@endsection