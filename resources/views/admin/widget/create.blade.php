@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            Widget
            <small>Bảng điều khiển</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thêm mới Widgets</h3>
                    </div>
                    <form id="" method="post" action="" class="form-horizontal " enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group" id="form-group-name">
                                <label for="name" class="col-sm-3 control-label">Tiêu đề</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" class="form-control validate_field" id="name"
                                           value="" required="" placeholder="Tiêu đề">
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                            <div class="form-group" id="form-group-location">
                                <label for="location" class="col-sm-3 control-label">Vị trí</label>
                                <div class="col-sm-9">
                                    <select class="form-control validate_field" id="location" required=""
                                            name="location">
                                        <option value="text_sidebar_left_user">text_sidebar_left_user</option>
                                    </select>
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="kich-hoat">Kích hoạt</label>
                                <div class="col-sm-9">
                                    <ul style="display: inline-block;list-style-type: none;padding:0; margin:0;">
                                        <li class="checkbox" style="display: inline-block; min-width: 155px;">
                                            <label>
                                                <input type="checkbox" name="status" id="kich-hoat" value="1"
                                                       checked="">
                                                Kích hoạt
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="thu-tu" class="col-sm-3 control-label">Thứ tự</label>
                                <div class="col-sm-9">
                                    <input type="number" name="order_no" class="form-control " id="thu-tu" value="0"
                                           placeholder="Thứ tự">
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a type="reset" class="btn btn-s-md btn-default"
                               href="">Quay lại</a>
                            <button type="reset" class="btn btn-s-md btn-primary">Reset dữ liệu</button>
                            <button type="submit" class="btn btn-info pull-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
