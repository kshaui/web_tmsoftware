@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            Sửa @lang('global.menus.title')
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>



    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thêm mới Widgets</h3>
                    </div>
                    <form id="" method="post" action="http://demo2.webhobasoft.com/admin/add_widget"
                          class="form-horizontal " enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="5QVlOimDqrzbFV01px1TMII3KhmrhYMVXHldRdPo">
                        <div class="box-body">
                            <div class="form-group" id="form-group-name">
                                <label for="name" class="col-sm-3 control-label">Tiêu đề</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" class="form-control validate_field" id="name"
                                           value="" required="" placeholder="Tiêu đề">
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="noi-dung" class="col-sm-3 control-label">Nội dung</label>
                                <div class="col-sm-9">
                                    <div id="mceu_21" class="mce-tinymce mce-container mce-panel" hidefocus="1"
                                         tabindex="-1" role="application"
                                         style="visibility: hidden; border-width: 1px;">
                                        <div id="mceu_21-body" class="mce-container-body mce-stack-layout">
                                            <div id="mceu_22"
                                                 class="mce-container mce-menubar mce-toolbar mce-first mce-stack-layout-item"
                                                 role="menubar" style="border-width: 0px 0px 1px;">
                                                <div id="mceu_22-body" class="mce-container-body mce-flow-layout">
                                                    <div id="mceu_23"
                                                         class="mce-widget mce-btn mce-menubtn mce-first mce-flow-layout-item"
                                                         tabindex="-1" aria-labelledby="mceu_23" role="menuitem"
                                                         aria-haspopup="true">
                                                        <button id="mceu_23-open" role="presentation" type="button"
                                                                tabindex="-1"><span>File</span> <i
                                                                    class="mce-caret"></i></button>
                                                    </div>
                                                    <div id="mceu_24"
                                                         class="mce-widget mce-btn mce-menubtn mce-flow-layout-item"
                                                         tabindex="-1" aria-labelledby="mceu_24" role="menuitem"
                                                         aria-haspopup="true">
                                                        <button id="mceu_24-open" role="presentation" type="button"
                                                                tabindex="-1"><span>Sửa</span> <i class="mce-caret"></i>
                                                        </button>
                                                    </div>
                                                    <div id="mceu_25"
                                                         class="mce-widget mce-btn mce-menubtn mce-flow-layout-item"
                                                         tabindex="-1" aria-labelledby="mceu_25" role="menuitem"
                                                         aria-haspopup="true">
                                                        <button id="mceu_25-open" role="presentation" type="button"
                                                                tabindex="-1"><span>Chèn</span> <i
                                                                    class="mce-caret"></i></button>
                                                    </div>
                                                    <div id="mceu_26"
                                                         class="mce-widget mce-btn mce-menubtn mce-flow-layout-item"
                                                         tabindex="-1" aria-labelledby="mceu_26" role="menuitem"
                                                         aria-haspopup="true">
                                                        <button id="mceu_26-open" role="presentation" type="button"
                                                                tabindex="-1"><span>Xem</span> <i class="mce-caret"></i>
                                                        </button>
                                                    </div>
                                                    <div id="mceu_27"
                                                         class="mce-widget mce-btn mce-menubtn mce-flow-layout-item"
                                                         tabindex="-1" aria-labelledby="mceu_27" role="menuitem"
                                                         aria-haspopup="true">
                                                        <button id="mceu_27-open" role="presentation" type="button"
                                                                tabindex="-1"><span>Định dạng</span> <i
                                                                    class="mce-caret"></i></button>
                                                    </div>
                                                    <div id="mceu_28"
                                                         class="mce-widget mce-btn mce-menubtn mce-flow-layout-item"
                                                         tabindex="-1" aria-labelledby="mceu_28" role="menuitem"
                                                         aria-haspopup="true">
                                                        <button id="mceu_28-open" role="presentation" type="button"
                                                                tabindex="-1"><span>bảng</span> <i
                                                                    class="mce-caret"></i></button>
                                                    </div>
                                                    <div id="mceu_29"
                                                         class="mce-widget mce-btn mce-menubtn mce-last mce-flow-layout-item"
                                                         tabindex="-1" aria-labelledby="mceu_29" role="menuitem"
                                                         aria-haspopup="true">
                                                        <button id="mceu_29-open" role="presentation" type="button"
                                                                tabindex="-1"><span>&lt;/ code &gt;</span> <i
                                                                    class="mce-caret"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="mceu_30"
                                                 class="mce-toolbar-grp mce-container mce-panel mce-stack-layout-item"
                                                 hidefocus="1" tabindex="-1" role="group">
                                                <div id="mceu_30-body" class="mce-container-body mce-stack-layout">
                                                    <div id="mceu_31"
                                                         class="mce-container mce-toolbar mce-first mce-stack-layout-item"
                                                         role="toolbar">
                                                        <div id="mceu_31-body"
                                                             class="mce-container-body mce-flow-layout">
                                                            <div id="mceu_32"
                                                                 class="mce-container mce-first mce-flow-layout-item mce-btn-group"
                                                                 role="group">
                                                                <div id="mceu_32-body">
                                                                    <div id="mceu_0"
                                                                         class="mce-widget mce-btn mce-first mce-disabled"
                                                                         tabindex="-1" aria-labelledby="mceu_0"
                                                                         role="button" aria-label="Undo"
                                                                         aria-disabled="true">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-undo"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div id="mceu_1"
                                                                         class="mce-widget mce-btn mce-last mce-disabled"
                                                                         tabindex="-1" aria-labelledby="mceu_1"
                                                                         role="button" aria-label="Redo"
                                                                         aria-disabled="true">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-redo"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="mceu_33"
                                                                 class="mce-container mce-flow-layout-item mce-btn-group"
                                                                 role="group">
                                                                <div id="mceu_33-body">
                                                                    <div id="mceu_2"
                                                                         class="mce-widget mce-btn mce-menubtn mce-first mce-last"
                                                                         tabindex="-1" aria-labelledby="mceu_2"
                                                                         role="button" aria-haspopup="true">
                                                                        <button id="mceu_2-open" role="presentation"
                                                                                type="button" tabindex="-1"><span>Định dạng</span>
                                                                            <i class="mce-caret"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="mceu_34"
                                                                 class="mce-container mce-flow-layout-item mce-btn-group"
                                                                 role="group">
                                                                <div id="mceu_34-body">
                                                                    <div id="mceu_3"
                                                                         class="mce-widget mce-btn mce-first"
                                                                         tabindex="-1" aria-labelledby="mceu_3"
                                                                         role="button" aria-label="Bold">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-bold"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div id="mceu_4" class="mce-widget mce-btn mce-last"
                                                                         tabindex="-1" aria-labelledby="mceu_4"
                                                                         role="button" aria-label="Italic">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-italic"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="mceu_35"
                                                                 class="mce-container mce-flow-layout-item mce-btn-group"
                                                                 role="group">
                                                                <div id="mceu_35-body">
                                                                    <div id="mceu_5"
                                                                         class="mce-widget mce-btn mce-first"
                                                                         tabindex="-1" aria-labelledby="mceu_5"
                                                                         role="button" aria-label="Align left">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-alignleft"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div id="mceu_6" class="mce-widget mce-btn"
                                                                         tabindex="-1" aria-labelledby="mceu_6"
                                                                         role="button" aria-label="Align center">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-aligncenter"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div id="mceu_7" class="mce-widget mce-btn"
                                                                         tabindex="-1" aria-labelledby="mceu_7"
                                                                         role="button" aria-label="Align right">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-alignright"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div id="mceu_8" class="mce-widget mce-btn mce-last"
                                                                         tabindex="-1" aria-labelledby="mceu_8"
                                                                         role="button" aria-label="Justify">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-alignjustify"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="mceu_36"
                                                                 class="mce-container mce-flow-layout-item mce-btn-group"
                                                                 role="group">
                                                                <div id="mceu_36-body">
                                                                    <div id="mceu_9"
                                                                         class="mce-widget mce-btn mce-splitbtn mce-menubtn mce-first"
                                                                         role="button" tabindex="-1"
                                                                         aria-label="Bullet list" aria-haspopup="true">
                                                                        <button type="button" hidefocus="1"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-bullist"></i>
                                                                        </button>
                                                                        <button type="button" class="mce-open"
                                                                                hidefocus="1" tabindex="-1"><i
                                                                                    class="mce-caret"></i></button>
                                                                    </div>
                                                                    <div id="mceu_10"
                                                                         class="mce-widget mce-btn mce-splitbtn mce-menubtn"
                                                                         role="button" tabindex="-1"
                                                                         aria-label="Numbered list"
                                                                         aria-haspopup="true">
                                                                        <button type="button" hidefocus="1"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-numlist"></i>
                                                                        </button>
                                                                        <button type="button" class="mce-open"
                                                                                hidefocus="1" tabindex="-1"><i
                                                                                    class="mce-caret"></i></button>
                                                                    </div>
                                                                    <div id="mceu_11" class="mce-widget mce-btn"
                                                                         tabindex="-1" aria-labelledby="mceu_11"
                                                                         role="button" aria-label="Decrease indent">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-outdent"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div id="mceu_12"
                                                                         class="mce-widget mce-btn mce-last"
                                                                         tabindex="-1" aria-labelledby="mceu_12"
                                                                         role="button" aria-label="Increase indent"
                                                                         aria-disabled="false">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-indent"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="mceu_37"
                                                                 class="mce-container mce-last mce-flow-layout-item mce-btn-group"
                                                                 role="group">
                                                                <div id="mceu_37-body">
                                                                    <div id="mceu_13"
                                                                         class="mce-widget mce-btn mce-first"
                                                                         tabindex="-1" aria-labelledby="mceu_13"
                                                                         role="button" aria-label="Insert/edit link">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-link"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div id="mceu_14"
                                                                         class="mce-widget mce-btn mce-last"
                                                                         tabindex="-1" aria-labelledby="mceu_14"
                                                                         role="button" aria-label="Insert/edit image">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-image"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="mceu_38"
                                                         class="mce-container mce-toolbar mce-last mce-stack-layout-item"
                                                         role="toolbar">
                                                        <div id="mceu_38-body"
                                                             class="mce-container-body mce-flow-layout">
                                                            <div id="mceu_39"
                                                                 class="mce-container mce-first mce-flow-layout-item mce-btn-group"
                                                                 role="group">
                                                                <div id="mceu_39-body">
                                                                    <div id="mceu_15"
                                                                         class="mce-widget mce-btn mce-first"
                                                                         tabindex="-1" aria-labelledby="mceu_15"
                                                                         role="button" aria-label="Print">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-print"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div id="mceu_16" class="mce-widget mce-btn"
                                                                         tabindex="-1" aria-labelledby="mceu_16"
                                                                         role="button" aria-label="Preview">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-preview"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div id="mceu_17"
                                                                         class="mce-widget mce-btn mce-last"
                                                                         tabindex="-1" aria-labelledby="mceu_17"
                                                                         role="button" aria-label="Insert/edit video">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-media"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="mceu_40"
                                                                 class="mce-container mce-last mce-flow-layout-item mce-btn-group"
                                                                 role="group">
                                                                <div id="mceu_40-body">
                                                                    <div id="mceu_18"
                                                                         class="mce-widget mce-btn mce-colorbutton mce-first"
                                                                         role="button" tabindex="-1"
                                                                         aria-haspopup="true" aria-label="Text color">
                                                                        <button role="presentation" hidefocus="1"
                                                                                type="button" tabindex="-1"><i
                                                                                    class="mce-ico mce-i-forecolor"></i><span
                                                                                    id="mceu_18-preview"
                                                                                    class="mce-preview"></span></button>
                                                                        <button type="button" class="mce-open"
                                                                                hidefocus="1" tabindex="-1"><i
                                                                                    class="mce-caret"></i></button>
                                                                    </div>
                                                                    <div id="mceu_19"
                                                                         class="mce-widget mce-btn mce-colorbutton"
                                                                         role="button" tabindex="-1"
                                                                         aria-haspopup="true"
                                                                         aria-label="Background color">
                                                                        <button role="presentation" hidefocus="1"
                                                                                type="button" tabindex="-1"><i
                                                                                    class="mce-ico mce-i-backcolor"></i><span
                                                                                    id="mceu_19-preview"
                                                                                    class="mce-preview"></span></button>
                                                                        <button type="button" class="mce-open"
                                                                                hidefocus="1" tabindex="-1"><i
                                                                                    class="mce-caret"></i></button>
                                                                    </div>
                                                                    <div id="mceu_20"
                                                                         class="mce-widget mce-btn mce-last"
                                                                         tabindex="-1" aria-labelledby="mceu_20"
                                                                         role="button" aria-label="Emoticons"
                                                                         aria-haspopup="true">
                                                                        <button role="presentation" type="button"
                                                                                tabindex="-1"><i
                                                                                    class="mce-ico mce-i-emoticons"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="mceu_41"
                                                 class="mce-edit-area mce-container mce-panel mce-stack-layout-item"
                                                 hidefocus="1" tabindex="-1" role="group"
                                                 style="border-width: 1px 0px 0px;">
                                                <iframe id="noi-dung_ifr" frameborder="0" allowtransparency="true"
                                                        title="Área de texto enriquecido. Pulse ALT-F9 para el menu. Pulse ALT-F10 para la barra de </ code >. Pulse ALT-0 para ayuda"
                                                        style="width: 100%; height: 314px; display: block;"></iframe>
                                            </div>
                                            <div id="mceu_42"
                                                 class="mce-statusbar mce-container mce-panel mce-last mce-stack-layout-item"
                                                 hidefocus="1" tabindex="-1" role="group"
                                                 style="border-width: 1px 0px 0px;">
                                                <div id="mceu_42-body" class="mce-container-body mce-flow-layout">
                                                    <div id="mceu_43" class="mce-path mce-flow-layout-item mce-first">
                                                        <div role="button" class="mce-path-item mce-last" data-index="0"
                                                             tabindex="-1" id="mceu_43-0" aria-level="0">p
                                                        </div>
                                                    </div>
                                                    <label id="mceu_45"
                                                           class="mce-wordcount mce-widget mce-label mce-flow-layout-item">Palabras:
                                                        0</label>
                                                    <div id="mceu_44"
                                                         class="mce-flow-layout-item mce-resizehandle mce-last"><i
                                                                class="mce-ico mce-i-resize"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea class="form-control editor" rows="15" cols="80" id="noi-dung"
                                              name="content" aria-hidden="true" style="display: none;"></textarea>
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                            <div class="form-group" id="form-group-location">
                                <label for="location" class="col-sm-3 control-label">Vị trí</label>
                                <div class="col-sm-9">
                                    <select class="form-control validate_field" id="location" required=""
                                            name="location">
                                        <option value="text_sidebar_left_user">text_sidebar_left_user</option>
                                    </select>
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="kich-hoat">Kích hoạt</label>
                                <div class="col-sm-9">
                                    <ul style="display: inline-block;list-style-type: none;padding:0; margin:0;">
                                        <li class="checkbox" style="display: inline-block; min-width: 155px;">
                                            <label>
                                                <input type="checkbox" name="status" id="kich-hoat" value="1"
                                                       checked="">
                                                Kích hoạt
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="thu-tu" class="col-sm-3 control-label">Thứ tự</label>
                                <div class="col-sm-9">
                                    <input type="number" name="order_no" class="form-control " id="thu-tu" value="0"
                                           placeholder="Thứ tự">
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a type="reset" class="btn btn-s-md btn-default"
                               href="http://demo2.webhobasoft.com/admin/widget">Quay lại</a>
                            <button type="reset" class="btn btn-s-md btn-primary">Reset dữ liệu</button>
                            <button type="submit" class="btn btn-info pull-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@stop

