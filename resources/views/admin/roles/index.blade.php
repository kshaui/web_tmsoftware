@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('global.roles.title')
            <small>Bảng điều khiển</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                    <div style="float:right;">
                        <a class="btn btn-success"
                           href="{{ route('admin.roles.create') }}">@lang('global.app_add_new')</a>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @lang('global.app_list')
                    </div>

                    <div class="panel-body table-responsive">
                        <table class="table table-bordered table-striped {{ count($roles) > 0 ? 'datatable' : '' }} dt-select">
                            <thead>
                            <tr>
                                <th style="text-align:center;"><input type="checkbox" id="select-all"/></th>
                                <th>ID</th>
                                <th>@lang('global.roles.fields.name')</th>
                                <th>@lang('global.roles.fields.permission')</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>

                            <tbody>
                            @if (count($roles) > 0)
                                @foreach ($roles as $role)
                                    <tr data-entry-id="{{ $role->id }}">
                                        <td></td>
                                        <td>{{ @$role->id }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>
                                            @php

                                                $role1 = \App\Http\Helpers\CommonHelper::getFromCache('get_role_permissions_pluck_name');
                                                \Illuminate\Support\Facades\Cache::flush();
                                                if(!$role1){
                                                    $role1 = $role->permissions()->pluck('name');
                                                    \App\Http\Helpers\CommonHelper::putToCache('get_role_permissions_pluck_name',$role1);
                                                }
                                            @endphp
                                            @foreach ($role1 as $permission)
                                                <span class="label label-info label-many">{{ $permission }}</span>
                                            @endforeach
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.roles.edit',[$role->id]) }}"
                                               class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                            {!! Form::open(array(
                                                'style' => 'display: inline-block;',
                                                'method' => 'DELETE',
                                                'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                                'route' => ['admin.roles.destroy', $role->id])) !!}
                                            {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">@lang('global.app_no_entries_in_table')</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.roles.mass_destroy') }}';
    </script>
@endsection