@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Chỉnh sửa @lang('global.roles.title')
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thêm mới Vai trò</h3>
                    </div>
                    {!! Form::model($role, ['method' => 'PUT','class'=>'form-horizontal', 'route' => ['admin.roles.update', $role->id]]) !!}
                    <div class="box-body">
                        <div class="form-group" id="form-group-name">
                            {!! Form::label('name', 'Name*', ['class' => 'control-label col-sm-3']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('name'))
                                    <p class="help-block">
                                        {{ $errors->first('name') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Permissions*</label>
                            <div class="col-sm-9">
                                {!! Form::select('permission[]', $permissions, old('permission') ? old('permission') : $role->permissions()->pluck('name', 'name'), ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('permission'))
                                    <p class="help-block">
                                        {{ $errors->first('permission') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a type="reset" class="btn btn-s-md btn-default"
                           href="{{ route('admin.roles.index') }}">Quay lại</a>
                        <button type="reset" class="btn btn-s-md btn-primary">Reset dữ liệu</button>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop

