@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('global.permissions.title')
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thêm mới @lang('global.permissions.title')</h3>
                    </div>
                    {!! Form::open(['method' => 'POST','class'=>'form-horizontal', 'route' => ['admin.permissions.store']]) !!}
                    <div class="box-body">
                        <div class="form-group" id="form-group-name">
                            {!! Form::label('name', 'Tên*', ['class' => 'control-label col-sm-3']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('name'))
                                    <p class="help-block">
                                        {{ $errors->first('name') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a type="reset" class="btn btn-s-md btn-default"
                           href="{{ route('admin.permissions.index') }}">Quay lại</a>
                        <button type="reset" class="btn btn-s-md btn-primary">Reset dữ liệu</button>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@stop

