@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('global.permissions.title')
            <small>Bảng điều khiển</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                    <div style="float:right;">
                        <a class="btn btn-success"
                           href="{{ route('admin.permissions.create') }}">@lang('global.app_add_new')</a>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @lang('global.app_list')
                    </div>

                    <div class="panel-body table-responsive">
                        <table class="table table-bordered table-striped {{ count($permissions) > 0 ? 'datatable' : '' }} dt-select">
                            <thead>
                            <tr>
                                <th style="text-align:center;"><input type="checkbox" id="select-all"/></th>
                                <th>ID</th>
                                <th>@lang('global.permissions.fields.name')</th>
                                <th>&nbsp;</th>

                            </tr>
                            </thead>

                            <tbody>
                            @if (count($permissions) > 0)
                                @foreach ($permissions as $permission)
                                    <tr data-entry-id="{{ $permission->id }}">
                                        <td></td>
                                        <td>{{ @$permission->id }}</td>
                                        <td>{{ $permission->name }}</td>
                                        <td>
                                            <a href="{{ route('admin.permissions.edit',[$permission->id]) }}"
                                               class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                            {!! Form::open(array(
                                                'style' => 'display: inline-block;',
                                                'method' => 'DELETE',
                                                'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                                'route' => ['admin.permissions.destroy', $permission->id])) !!}
                                            {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                            {!! Form::close() !!}
                                        </td>

                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection