@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Chỉnh sửa @lang('global.users.title')
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"> Chỉnh sửa @lang('global.users.title')</h3>
                    </div>
                    {!! Form::model($user, ['method' => 'PUT','class'=>'form-horizontal', 'route' => ['admin.users.update', $user->id]]) !!}
                    <div class="box-body">
                        <div class="form-group" id="form-group-name">
                            {!! Form::label('name', 'Tên*', ['class' => 'control-label col-sm-3']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('name'))
                                    <p class="help-block">
                                        {{ $errors->first('name') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email*', ['class' => 'control-label col-sm-3']) !!}
                            <div class="col-sm-9">
                                {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('email'))
                                    <p class="help-block">
                                        {{ $errors->first('email') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('password', 'Mật khẩu*', ['class' => 'control-label col-sm-3']) !!}
                            <div class="col-sm-9">
                                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('password'))
                                    <p class="help-block">
                                        {{ $errors->first('password') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('roles', 'Roles*', ['class' => 'control-label col-sm-3']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('roles[]', $roles, old('roles') ? old('roles') : $user->roles()->pluck('name', 'name'), ['class' => 'form-control select2', 'multiple' => 'multiple', 'required' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('roles'))
                                    <p class="help-block">
                                        {{ $errors->first('roles') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a type="reset" class="btn btn-s-md btn-default"
                           href="{{ route('admin.users.index') }}">Quay lại</a>
                        <button type="reset" class="btn btn-s-md btn-primary">Reset dữ liệu</button>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@stop

