@php
    $widgets = \App\Http\Helpers\CommonHelper::getFromCache('get_footer_widgets');
    if (!$widgets){
        $widgets = \App\Models\Widget::where('status',1)->get();
        \App\Http\Helpers\CommonHelper::putToCache('get_footer_widgets',$widgets);
    }
@endphp
<div class="footer">
    <div class="container">
        <div class="row m0">
            <div class="col-xs-8 col-sm-8 p0 div-1st">
                <div class="txt-footer">
                    {!! @$settings['copyright'] !!}
                    <br>
                    @php $widgetssidebar = $widgets->where('location','footer')->first() @endphp
                    {!! @$widgetssidebar->content !!}
                    <br>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 p0 txr div-2nd">
                @php
                    $data = CommonHelper::getFromCache('menu_menu_foote');
                     if (!$data) {
                         $data = \App\Models\Menu::select(['url','name'])->where('status', 1)->where('location','menu_footer')->get();
                         CommonHelper::putToCache('menu_menu_foote', $data);
                     }
                @endphp
                <div class="list-ft">
                    @foreach($data as $item)
                        <a class="font16" href=" {{ URL::to($item->url) }}"> {{ $item->name }}</a> <span class="m5">|</span>
                    @endforeach
                </div>
                <div class="hotline mt10"> Hotline: <a href=" tel:{!! @$settings['hotline'] !!} "><span class="bold txt-orange font24">{!! @$settings['hotline'] !!}</span></a>
                </div>
            </div>
        </div>
    </div>
</div>