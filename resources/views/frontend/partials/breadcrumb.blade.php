<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="path">
            <ul class="clearfix">
                <li><a href="/" title="Trang chủ">Trang Chủ</a>&nbsp;&nbsp;/&nbsp;&nbsp;</li>
                @if(isset($pageOption))
                    @if($pageOption['type'] != 'page')
                        <li><a href="{{ URL::to($pageOption['parentUrl']) }}"
                               title="{{ $pageOption['parentName'] }}">{{ $pageOption['parentName'] }}</a>&nbsp;&nbsp;/&nbsp;&nbsp;
                        </li>
                    @endif
                    <li>{{ $pageOption['pageName'] }}</li>
                @endif
            </ul>
        </div>
    </div>
</div>