@include('frontend.partials.head_meta')
<link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i&amp;subset=vietnamese"
      rel="stylesheet">
<link rel="stylesheet" type="text/css" href=" {{URL::to('public/frontend/assets/frontend/css/common.css')}}">
<style type="text/css">iframe#_hjRemoteVarsFrame {
        display: none !important;
        width: 1px !important;
        height: 1px !important;
        opacity: 0 !important;
        pointer-events: none !important;
    }</style>
<script async="" src="{{ asset('public/frontend/js/jquery.min.js') }}"></script>
<script async="" src="{{ asset('public/frontend/js/custom.js') }}"></script>
<link rel="stylesheet" type="text/css" href=" {{URL::to('public/libs/select2/css/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href=" {{URL::to('public/frontend/css/custom.css')}}">
@yield('custom_header')
