@if(Session('flash_message'))
    <center>
        <div class="alert alert-danger" style="color:blue; font-size: 15px;border-radius: 0px ">
            {{--<li class="alert alert-danger"--}}
            {{--style="color:blue; font-size: 15px; float: left; padding-top: 7px">--}}
            {!! Session('flash_message') !!}
            {{--</li>--}}
        </div>
    </center>
@endif
<div class="header header-pc">
    <div class="header-top">
        <div class="container p17">
            <div class="col-left">
                <a style="font-size: 30px" href="{{URL::to('/')}}"><img
                            src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}"></a>
            </div>
            <div class="col-right">
                <ul class="menu-top row m0 p0">
                    <li class="list-menu-top menu-home">
                        <a class="" href="/"><i class="icon-sprite-24 icon-home"></i></a>
                    </li>
                    <li class="list-menu-top"><a href="{{ action('Frontend\ProductController@getNewProduct') }}" class="">Đăng tin bán ô tô</a></li>
                    <li class="list-menu-top"><a href="{{ URL::to('/dang-tin') }}"
                                                 class="">{!! @$settings['post_news'] !!}</a></li>

                    @if(!Auth::user())
                        <div class="li-mer">


                            <li class="dang-nhap fr ml25"><a class=""
                                                             href="{{action('Auth\LoginController@getRegister')}}">Đăng
                                    ký</a>
                            </li>

                            <li class="dang-ky fr"><a class="" href="{{action('Auth\LoginController@getLogin')}}"><i
                                            class="icon-sprite-24 icon-dky"></i>Đăng nhập</a>
                            </li>
                        </div>
                    @else
                        {{--@if(Session('flash_message'))--}}
                        {{--<div class="alert alert-danger">--}}
                        {{--<li class="alert alert-danger"--}}
                        {{--style="color:blue; font-size: 15px; float: left; padding-top: 7px">--}}
                        {{--{!! Session('flash_message') !!}--}}
                        {{--</li>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        {{--@if(Session('success'))--}}
                        {{--<div class="alert alert-danger">--}}
                        {{--<li class="alert alert-danger"--}}
                        {{--style="color:blue; font-size: 15px; float: left; padding-top: 7px">--}}
                        {{--{!! Session('success') !!}--}}
                        {{--</li>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        <script type="text/javascript"></script>
                        <li class="box-user dropdown fr">
                            <ul class="user-dropdown">
                                <li class="dropdown-toggle user" type="button" id="menu1" data-toggle="dropdown">
                                    <div class="img-user">
                                        <img src="@if(@Auth::user()->image) {{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@Auth::user()['image'], 45, 45) }} @else {{ asset('public/frontend/assets/frontend/img/avatar_default.png') }}  @endif"
                                             width="45px" height="45px" alt=""
                                             id="avatar">
                                    </div>
                                    <span class="caret"></span>
                                </li>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                    <li role="presentation">
                                        <div class="name-user">{{ Auth::user()->name }}</div>
                                        <div class="email">{{ Auth::user()->email }}</div>
                                    </li>
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1"
                                           href="{{action('Frontend\AccountController@getAccount')}}">
                                            Quản lý Tài khoản
                                        </a>
                                    </li>

                                    <li class="divider"></li>
                                    <li role="presentation" class="public_qity">
                                        <a role="menuitem" tabindex="-1"
                                           href="{{ action('Frontend\ProductController@getCarLove') }}">
                                            @php
                                                $Favorite_count = \App\Http\Helpers\CommonHelper::getFromCache('favorite_by_user_id_count');
                                                if (!$Favorite_count){
                                                    $Favorite_count = \App\Models\Favorite::where('user_id',@Auth::user()->id)->count();
                                                    \App\Http\Helpers\CommonHelper::putToCache('favorite_by_user_id_count',$Favorite_count);
                                                }
                                            @endphp
                                            Xe yêu thích @if(isset($Favorite_count)) ({{ (@$Favorite_count) }}) @else
                                        (0) @endif
                                        </a>
                                    </li>
                                    <li role="presentation" class="divider"></li>
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1"
                                           href="{{action('Frontend\ProductController@getNewProduct')}}">
                                            Đăng tin bán ô tô
                                            <span class="txt-num"></span></a></li>
                                    @php
                                        $count_dang_ban = CommonHelper::getFromCache('product_dangban_by_author_id_count');
                                        if (!$count_dang_ban){
                                        $count_dang_ban = \App\Models\Product::where('author_id', Auth::user()->id)->wherein('pending',['Đang bán'])->count();
                                        CommonHelper::putToCache('product_dangban_by_author_id_count',$count_dang_ban);
                                    }
                                    @endphp
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1"
                                           href="{{ URL::to('/xe-dang-rao-ban') }}">
                                            Xe đang rao bán @if(isset($count_dang_ban)) ({{ $count_dang_ban }}) @else
                                        (0) @endif
                                        </a>
                                    </li>
                                    @php
                                        $count_da_ban = CommonHelper::getFromCache('product_daban_by_author_id_count');
                                        if (!$count_da_ban){
                                        $count_da_ban = \App\Models\Product::where('author_id', Auth::user()->id)->wherein('pending',['Đa bán'])->count();
                                        CommonHelper::putToCache('product_daban_by_author_id_count',$count_da_ban);
                                    }
                                    @endphp
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1"
                                           href="{{ URL::to('/xe-da-ban') }}">
                                            Xe đã bán @if(isset($count_da_ban)) ({{ (@$count_da_ban) }}) @else
                                        (0) @endif
                                        </a>
                                    </li>
                                    @php
                                        $count_dang_cho_duyet= CommonHelper::getFromCache('product_dangchoduyet_by_author_id_count');
                                        if (!$count_dang_cho_duyet){
                                        $count_dang_cho_duyet = \App\Models\Product::where('author_id', Auth::user()->id)->wherein('pending',['Đang chờ duyệt'])->count();
                                        CommonHelper::putToCache('product_dangchoduyet_by_author_id_count',$count_dang_cho_duyet);
                                    }
                                    @endphp
                                    <li role="presentation"><a
                                                href="{{action('Frontend\ProductController@getPending')}}">Xe chờ
                                            duyệt @if(isset($count_dang_cho_duyet)) ({{ (@$count_dang_cho_duyet) }}) @else
                                        (0) @endif
                                        </a>
                                    </li>
                                    @php
                                        $count_thay_doi_cho_duyet= CommonHelper::getFromCache('product_thaydoichoduyet_by_author_id_count');
                                        if (!$count_thay_doi_cho_duyet){
                                        $count_thay_doi_cho_duyet = \App\Models\Product::where('author_id', Auth::user()->id)->wherein('pending',['Thay đổi chờ duyêt'])->count();
                                        CommonHelper::putToCache('product_thaydoichoduyet_by_author_id_count',$count_thay_doi_cho_duyet);
                                    }
                                    @endphp
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1"
                                           href="{{ URL::to('/xe-thay-doi-cho-duyet') }}">
                                            Xe thay đổi chờ duyệt @if(isset($count_thay_doi_cho_duyet))
                                                ({{ (@$count_thay_doi_cho_duyet) }}) @else
                                        (0) @endif
                                        </a>
                                    </li>
                                    <li role="presentation" class="logout">
                                        <a role="menuitem" tabindex="-1"
                                           href="{{action('Auth\LoginController@getLogout')}}">
                                            <i class="icon-sprite-24 icon-logout"></i>
                                            Đăng xuất
                                        </a>
                                    </li>
                                </ul>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    <div class="header-mid">
        <div class="container">
            <div class="col-xs-5 col-sm-5 col-md-5 p0">
                <div class="box-search">
                    <form action="{{ URL::to('/tim-kiem') }}" method="get">
                        <input type="text" name="key" class="form-control input-search" placeholder="Tìm kiếm"
                               value="<?= @$_GET['key'] ?>">
                        <button class="btn-search"><i class="icon-sprite-24 icon-search"></i></button>
                    </form>
                </div>
            </div>
            <div class="col-xs-7 col-sm-7 col-md-7 p0 txr">
                <a class="seen-favo" href="{{action('Frontend\ProductController@getCarLove')}}"><i
                            class="icon-40-heart"></i> <span>Xe yêu thích</span></a>
            </div>
        </div>
    </div>
    @include('frontend.partials.menu')
    <div class="short-menu">
        <div class="white-menu">
            <div class="container">
                <div class="">
                    <ul class="menu-top row m0 p0">
                        <li class="list-menu-top">
                            <a class="logo" href="/"></a></li>
                        <li class="list-menu-top menu-home">
                            <a class="" href="/"><i class="icon-sprite-24 icon-home"></i></a>
                        </li>
                        <li class="list-menu-top w180">
                            <div class="box-search">
                                <form action=" {{ URL::to('/tim-kiem') }} ">
                                    <input type="text" name="key" class="form-control input-search"
                                           placeholder="Tìm kiếm"
                                           value="<?= @$_GET['key'] ?>">
                                    <button class="btn-search"><i class="icon-sprite-24 icon-search"></i></button>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>