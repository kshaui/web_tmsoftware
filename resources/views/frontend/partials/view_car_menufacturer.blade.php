<div class="hidden-xs">
    <div class="deal hang-sx">
        <div class="title-underline">Xem xe theo hãng</div>
        @php
            $data = CommonHelper::getFromCache('all_manufacturer');
             if (!$data) {
                 $data = \App\Models\Manufacturer::select(['id','name', 'image'])->where('status', 1)->get();
                 CommonHelper::putToCache('all_manufacturer', $data);
             }
        @endphp
        <a class="txt-see-all hidden-xs" href="{{ URL::to('/hang-xe') }}">Xem tất cả hãng xe »</a>
        <ul class="row logo-hang">
            @foreach($data as $item)
                <li style="text-align: center">
                    <a class="" href="{{URL::to('/tim-kiem?manufacturer_id='.$item->id)}}">
                        <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->image,104 , 69) }}"
                             width="60" height="60">
                        {{$item->name}}
                    </a>
                </li>
            @endforeach
        </ul>
        <hr class="visible-xs m0 mt5">
        <div class="a-see-all">
            <a class="" href="{{ URL::to('/hang-xe') }}">Xem tất cả Hãng xe »</a>
        </div>
    </div>
</div>