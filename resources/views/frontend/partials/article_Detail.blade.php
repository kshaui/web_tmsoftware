@extends('frontend.layouts.master')
@section('title')
    chi tiết bài viết
@endsection
@section('main_content')
    <div class="min_wrap">


        <div class="breacrum">
            <div id="breacrum" xmlns:v="http://rdf.data-vocabulary.org/#">
        <span typeof="v:Breadcrumb">
            <a rel="v:url" property="v:title" href="http://www.bdstphcm.org/" title="Trang chủ"> Trang chủ </a>
        </span>
                <span typeof="v:Breadcrumb"> »
               <a rel="v:url" property="v:title"
                  href="http://www.bdstphcm.org/ba-ria-vung-tau.html">  Bà Rịa Vũng Tàu  </a>
            </span>
                <span typeof="v:Breadcrumb"> »
               <a rel="v:url" property="v:title" href="http://www.bdstphcm.org/ba-ria-vung-tau/thanh-pho-vung-tau.html"
                  title="Thành Phố Vũng Tàu"> Thành Phố Vũng Tàu  </a>
            </span>
                <span typeof="v:Breadcrumb"> »
               <a rel="v:url" property="v:title"
                  href="http://www.bdstphcm.org/ba-ria-vung-tau/thanh-pho-vung-tau/phuong-9.html" title="Phường 9"> Phường 9 </a>
            </span>
                <span typeof="v:Breadcrumb">  »
                <a rel="v:url" property="v:title"
                   href="http://www.bdstphcm.org/ba-ria-vung-tau/thanh-pho-vung-tau/phuong-9/nha-dat-cho-thue.html"
                   title=" Nhà đất cho thuê ">  Nhà đất cho thuê  </a>
             </span>
                <span typeof="v:Breadcrumb">»
                 <a rel="v:url" property="v:title"
                    href="http://www.bdstphcm.org/ba-ria-vung-tau/thanh-pho-vung-tau/phuong-9/nha-dat-cho-thue/cho-thue-nha-rieng.html"
                    title="Cho thuê nhà riêng"> Cho thuê nhà riêng  </a>
              </span>

                <span typeof="v:Breadcrumb">»
                 	Cho thuê biệt thự 10x20m, gần biển Vũng Tàu, 10 triệu/tháng              </span>

            </div>
        </div>

        <div class="f_cont clearfix">

            <article class="content">

                <script>
                    $(document).ready(function () {
                        $('.bxslider').bxSlider({
                            auto: true, pagerCustom: '#bx-pager'
                        });
                    });
                </script>
                <style>
                    .bx-wrapper {
                        margin-bottom: 5px !important;
                    }

                    ul.bxslider li {
                        text-align: center;
                    }

                    ul.bxslider li img {
                        display: inline;
                        height: 100%;
                    }
                </style>
                <h2 class="t_ndct">

                    Cho thuê biệt thự 10x20m, gần biển Vũng Tàu, 10 triệu/tháng
                    <div class="addthis_sharing_toolbox"
                         data-url="http://www.bdstphcm.org/cho-thue-biet-thu-10x20m-gan-bien-vung-tau-10-trieu-thang.html"
                         data-title="Cho thuê biệt thự 10x20m, gần biển Vũng Tàu, 10 triệu/tháng"
                         data-description="Không có nhu cầu sử dụng nên cần cho thuê căn biệt thự diện tích 10x20m, mặt tiền đường Trần Cao Vân, phường 9, Thành phố Vũng Tàu, Bà Rịa Vũng Tàu. Biệt thự có vị trí rất đẹp, gần biển, đặc biệt phù hợp để làm nơi nghỉ dưỡng cho người yêu thích biển."
                         style="clear: both;">
                        <div id="atstbx" class="at-share-tbx-element addthis-smartlayers addthis-animated at4-show"
                             aria-labelledby="at-a177a343-b671-47b1-94e1-412599ddd24a" role="region"><span
                                    id="at-a177a343-b671-47b1-94e1-412599ddd24a" class="at4-visually-hidden">AddThis Sharing Buttons</span>
                            <div class="at-share-btn-elements"><a role="button" tabindex="1"
                                                                  class="at-icon-wrapper at-share-btn at-svc-facebook"
                                                                  style="background-color: rgb(59, 89, 152); border-radius: 0%;"><span
                                            class="at4-visually-hidden">Share to Facebook</span><span
                                            class="at-icon-wrapper" style="line-height: 16px; height: 16px; width: 16px;"><svg
                                                xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-1"
                                                style="width: 16px; height: 16px;" class="at-icon at-icon-facebook"><title
                                                    id="at-svg-facebook-1">Facebook</title><g><path
                                                        d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z"
                                                        fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="1"
                                                                                                           class="at-icon-wrapper at-share-btn at-svc-twitter"
                                                                                                           style="background-color: rgb(29, 161, 242); border-radius: 0%;"><span
                                            class="at4-visually-hidden">Share to Twitter</span><span class="at-icon-wrapper"
                                                                                                     style="line-height: 16px; height: 16px; width: 16px;"><svg
                                                xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-twitter-2"
                                                style="width: 16px; height: 16px;" class="at-icon at-icon-twitter"><title
                                                    id="at-svg-twitter-2">Twitter</title><g><path
                                                        d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336"
                                                        fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="1"
                                                                                                           class="at-icon-wrapper at-share-btn at-svc-google_plusone_share"
                                                                                                           style="background-color: rgb(220, 78, 65); border-radius: 0%;"><span
                                            class="at4-visually-hidden">Share to Google+</span><span class="at-icon-wrapper"
                                                                                                     style="line-height: 16px; height: 16px; width: 16px;"><svg
                                                xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                viewBox="0 0 32 32" version="1.1" role="img"
                                                aria-labelledby="at-svg-google_plusone_share-3"
                                                style="width: 16px; height: 16px;" class="at-icon at-icon-google_plusone_share"><title
                                                    id="at-svg-google_plusone_share-3">Google+</title><g><path
                                                        d="M12 15v2.4h3.97c-.16 1.03-1.2 3.02-3.97 3.02-2.39 0-4.34-1.98-4.34-4.42s1.95-4.42 4.34-4.42c1.36 0 2.27.58 2.79 1.08l1.9-1.83C15.47 9.69 13.89 9 12 9c-3.87 0-7 3.13-7 7s3.13 7 7 7c4.04 0 6.72-2.84 6.72-6.84 0-.46-.05-.81-.11-1.16H12zm15 0h-2v-2h-2v2h-2v2h2v2h2v-2h2v-2z"
                                                        fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="1"
                                                                                                           class="at-icon-wrapper at-share-btn at-svc-print"
                                                                                                           style="background-color: rgb(115, 138, 141); border-radius: 0%;"><span
                                            class="at4-visually-hidden">Share to Print</span><span class="at-icon-wrapper"
                                                                                                   style="line-height: 16px; height: 16px; width: 16px;"><svg
                                                xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-print-4"
                                                style="width: 16px; height: 16px;" class="at-icon at-icon-print"><title
                                                    id="at-svg-print-4">Print</title><g><path
                                                        d="M24.67 10.62h-2.86V7.49H10.82v3.12H7.95c-.5 0-.9.4-.9.9v7.66h3.77v1.31L15 24.66h6.81v-5.44h3.77v-7.7c-.01-.5-.41-.9-.91-.9zM11.88 8.56h8.86v2.06h-8.86V8.56zm10.98 9.18h-1.05v-2.1h-1.06v7.96H16.4c-1.58 0-.82-3.74-.82-3.74s-3.65.89-3.69-.78v-3.43h-1.06v2.06H9.77v-3.58h13.09v3.61zm.75-4.91c-.4 0-.72-.32-.72-.72s.32-.72.72-.72c.4 0 .72.32.72.72s-.32.72-.72.72zm-4.12 2.96h-6.1v1.06h6.1v-1.06zm-6.11 3.15h6.1v-1.06h-6.1v1.06z"></path></g></svg></span></a><a
                                        role="button" tabindex="1" class="at-icon-wrapper at-share-btn at-svc-compact"
                                        style="background-color: rgb(255, 101, 80); border-radius: 0%;"><span
                                            class="at4-visually-hidden">Share to More</span><span class="at-icon-wrapper"
                                                                                                  style="line-height: 16px; height: 16px; width: 16px;"><svg
                                                xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-addthis-5"
                                                class="at-icon at-icon-addthis" style="width: 16px; height: 16px;"><title
                                                    id="at-svg-addthis-5">Addthis</title><g><path
                                                        d="M18 14V8h-4v6H8v4h6v6h4v-6h6v-4h-6z"
                                                        fill-rule="evenodd"></path></g></svg></span></a></div>
                        </div>
                    </div>
                    <script type="text/javascript"
                            src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52493e8d684da4cc"></script>
                </h2><!-- End .t_ndct -->

                <span class="date_ndct">Đường Trần Cao Vân,  Phường 9, Thành Phố Vũng Tàu,  Bà Rịa Vũng Tàu  </span>

                <div class="f_prod_D clearfix">

                    <div class="f1_pD">

                        <div id="gallery-1" class="royalSlider rsMinW rsHor rsWithThumbs rsWithThumbsHor">
                            <div class="rsOverflow grab-cursor" style="width: 430px; height: 360px;">
                                <div class="rsContainer"
                                     style="transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                                    <div style="left: 0px;" class="rsSlide "><img class="rsImg rsMainSlideImage"
                                                                                  src="http://www.bdstphcm.org/imagecache/image.php/images/bds/cho-thue-biet-thu-10x20m-gan-bien-Vung-Tau-10-trieu-thang-1-o0rBK.jpg?width=460&amp;height=450&amp;cropratio=3:2&amp;image=http://www.bdstphcm.org/images/bds/cho-thue-biet-thu-10x20m-gan-bien-Vung-Tau-10-trieu-thang-1-o0rBK.jpg"
                                                                                  style="visibility: visible; opacity: 1; transition: opacity 400ms ease-in-out 0s; width: 430px; height: 287px; margin-left: 0px; margin-top: 36px;">
                                    </div>
                                </div>
                            </div>
                            <div class="rsNav rsThumbs rsThumbsHor">
                                <div class="rsThumbsContainer"
                                     style="transition-property: -webkit-transform; width: 660px;">
                                    <div style="margin-right:10px;" class="rsNavItem rsThumb rsNavSelected"><img
                                                class="rsTmb"
                                                src="http://www.bdstphcm.org/imagecache/image.php/images/bds/cho-thue-biet-thu-10x20m-gan-bien-Vung-Tau-10-trieu-thang-1-o0rBK.jpg?width=94&amp;height=74&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds/cho-thue-biet-thu-10x20m-gan-bien-Vung-Tau-10-trieu-thang-1-o0rBK.jpg"
                                                alt=""></div>
                                    <div style="margin-right:10px;" class="rsNavItem rsThumb"><img class="rsTmb"
                                                                                                   src="imgs/noimage.png"
                                                                                                   alt=""></div>
                                    <div style="margin-right:10px;" class="rsNavItem rsThumb"><img class="rsTmb"
                                                                                                   src="imgs/noimage.png"
                                                                                                   alt=""></div>
                                    <div style="margin-right:10px;" class="rsNavItem rsThumb"><img class="rsTmb"
                                                                                                   src="imgs/noimage.png"
                                                                                                   alt=""></div>
                                    <div style="margin-right:10px;" class="rsNavItem rsThumb"><img class="rsTmb"
                                                                                                   src="imgs/noimage.png"
                                                                                                   alt=""></div>
                                    <div style="margin-right:10px;" class="rsNavItem rsThumb"><img class="rsTmb"
                                                                                                   src="imgs/noimage.png"
                                                                                                   alt=""></div>
                                </div>
                                <div class="rsThumbsArrow rsThumbsArrowLeft rsThumbsArrowDisabled"
                                     style="opacity: 0;">
                                    <div class="rsThumbsArrowIcn"></div>
                                </div>
                                <div class="rsThumbsArrow rsThumbsArrowRight" style="opacity: 0;">
                                    <div class="rsThumbsArrowIcn"></div>
                                </div>
                            </div>
                        </div>


                        <div class="clear"></div>

                        <div style="padding:5px;">
                            Xem ảnh lớn: &nbsp;
                            <script type="text/javascript"
                                    src="http://www.bdstphcm.org/lib/thickbox/thickbox.js"></script>
                            <style type="text/css"
                                   media="all">@import "http://www.bdstphcm.org/lib/thickbox/thickbox.css";</style>
                            <a href="http://www.bdstphcm.org/images/bds/cho-thue-biet-thu-10x20m-gan-bien-Vung-Tau-10-trieu-thang-1-o0rBK.jpg"
                               class="thickbox" rel="gallery-plants"> Ảnh 1 </a> &nbsp;
                        </div>

                    </div><!-- End .f1_pD -->

                    <div class="f2_pD">

                        <div class="prive_pD">
                            Giá
                            <span>10  Triệu/ Tháng </span>
                        </div><!-- End .prive_pD -->

                        <div class="info_pD">
                            <ul>
                                <li class="clearfix">
                        <span class="i1_pd">
                            Mã tin:
                        </span>
                                    <span class="i2_pd">
                           1569                        </span>
                                </li>
                                <li class="clearfix">
                        <span class="i1_pd">
                            Tổng diện tích đất:
                        </span>
                                    <span class="i2_pd">
                            10x20                        </span>
                                </li>
                                <li class="clearfix">
                        <span class="i1_pd">
                            Diện tích sử dụng:
                        </span>
                                    <span class="i2_pd">
                            200 m2
                        </span>
                                </li>
                                <li class="clearfix">
                        <span class="i1_pd">
                            Vị trí:
                        </span>
                                    <span class="i2_pd">
                           Bà Rịa Vũng Tàu
                        </span>
                                </li>
                                <li class="clearfix">
                        <span class="i1_pd">
                            Ngày cập nhật:
                        </span>
                                    <span class="i2_pd">
                            2014-09-29 13:57:44
                        </span>
                                </li>
                                <li class="clearfix">
                        <span class="i1_pd">
                            Số người xem:
                        </span>
                                    <span class="i2_pd">
                            44                        </span>
                                </li>
                                <li class="clearfix">
                        <span class="i1_pd">
                            Loại địa ốc:
                        </span>
                                    <span class="i2_pd">
                            Cho thuê nhà riêng                        </span>
                                </li>
                                <li class="clearfix">
                        <span class="i1_pd">
                            Tình trạng pháp lý:
                        </span>
                                    <span class="i2_pd">
                             Sổ đỏ
                        </span>
                                </li>
                                <li class="clearfix">
                        <span class="i1_pd">
                            Hướng:
                        </span>
                                    <span class="i2_pd">
                             Đông nam
                        </span>
                                </li>
                                <li class="clearfix">
                        <span class="i1_pd">
                            Rao Bán/Thuê:
                        </span>
                                    <span class="i2_pd">
                             Miễn trung gian
                        </span>
                                </li>
                            </ul>
                        </div><!-- End .info_pD -->

                    </div><!-- End .f2_pD -->

                </div><!-- End .f_prod_D -->

                <div class="nlh">
                    <span class="t_nlh">Thông tin liên hệ</span><!-- End .t_nlh -->
                    <ul>
                        <li class="clearfix">
                            <span>Tên người bán: Ms. Vân</span>

                        </li>
                        <li class="clearfix">
                            <span>Điện thoại: 0918420557</span>

                        </li>
                        <li class="clearfix">
                            <span>Email: <a href="mailto:"></a></span>

                        </li>
                        <li class="clearfix">
                            <span>Địa chỉ: đường Trần Cao Vân, phường 9, Thành phố Vũng Tàu, Bà Rịa Vũng Tàu.</span>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="block_pD">
                    <h4 class="t_prod">
            <span>
                Thông tin chi tiết
            </span>
                    </h4><!-- End .t_prod -->
                    <div class="m_pD">

                        <div class="f_detail clearfix">

                            <p>Không có nhu cầu sử dụng nên cần cho thuê căn biệt thự diện tích 10x20m, <strong>mặt
                                    tiền đường</strong> Trần Cao Vân, phường 9, Thành phố Vũng Tàu, Bà Rịa Vũng Tàu.
                                Biệt thự có vị trí rất đẹp, gần biển, đặc biệt phù hợp để làm nơi nghỉ dưỡng cho
                                người yêu thích biển.</p>

                            <p>Vị trí biệt thự cần cho thuê: đường <strong>Trần Cao Vân, phường 9, Thành phố Vũng
                                    Tàu, Bà Rịa Vũng Tàu.</strong></p>

                            <p>Biệt thự toạ lạc ngay mặt tiền đường có lộ giới rộng rãi và thông thoáng, khu vực
                                hàng ngày có nhiều xe cộ lưu thông qua lại. Từ đường Trần Cao Vân có thể nhanh chóng
                                di chuyển bằng những đường hẻm thông ra biển, hoặc ra tuyến đường lớn 30 Tháng 4 đi
                                sân bay Vũng Tàu rất tiện lợi, giao thông tại đây luôn thông suốt và thuận tiện cho
                                mọi nhu cầu đi lại, sinh hoạt.</p>

                            <p>Biệt thự cho thuê nằm trong khu dân cư tập trung sinh sống đông đúc, khu vực thường
                                xuyên có nhiều du khách trong và ngoài nước đến tham quan, nghỉ dưỡng , du lịch nên
                                an ninh tại đây luôn được đảm bảo, dân trí cao, đời sống văn minh hiện đại. Xung
                                quanh bán kính gần biệt thự là đầy đủ các dịch vụ và tiện ích công cộng như:</p>

                            <p>+ Gần chợ phường 9, gần siêu thị</p>

                            <p>+ Gần trường Quốc Tế Singapore, trường Cao đẳng nghề Dầu Khí, THCS Võ Trường Toản,
                                THPT Trần Nguyên Hãn</p>

                            <p>+ Gần biển, gần công viên Đại An</p>

                            <p>+ Gần ngân hàng TMCP Xây Dựng Việt Nam</p>

                            <p>+ Gần sân bay Vũng Tàu</p>

                            <p><strong>Diện tích biệt thự cho thuê:</strong> 10 x 20m= 200m2</p>

                            <p><strong>Pháp lý: sổ đỏ</strong></p>

                            <p><strong>Hướng:</strong> Đông Nam</p>

                            <p><strong>Giá cho thuê:</strong> 10 triệu/tháng</p>

                        </div><!-- End .f_detail -->

                    </div><!-- End .m_pD -->
                </div><!-- End .block_pD -->
                <div class="block_pD">
                    <h4 class="t_prod">
            <span>
                Đặc điểm khác
            </span>
                    </h4><!-- End .t_prod -->
                    <div class="m_pD">

                        <table class="tab_ddk">
                            <tbody>
                            <tr>
                                <td>+ Tiện để ở <img src="imgs/layout/check_ddk.jpg" alt=""></td>
                                <td>+ Gần trường <img src="imgs/layout/check_ddk.jpg" alt=""></td>
                                <td>+ Khu nội bộ</td>
                                <td>+ Truyền hình cáp</td>
                            </tr>
                            <tr>
                                <td>+ Tiện làm văn phòng</td>
                                <td>+ Gần bệnh viện <img src="imgs/layout/check_ddk.jpg" alt=""></td>
                                <td>+ Có bảo vệ <img src="imgs/layout/check_ddk.jpg" alt=""></td>
                                <td>+ Điện thoại</td>
                            </tr>
                            <tr>
                                <td>+ Tiện cho sản xuất <img src="imgs/layout/check_ddk.jpg" alt=""></td>
                                <td>+ Gần chợ - siêu thị <img src="imgs/layout/check_ddk.jpg" alt=""></td>
                                <td>+ Chỗ đậu xe hơi</td>
                                <td>+ Đồng hồ nước</td>
                            </tr>
                            <tr>
                                <td>+ Tiện kinh doanh <img src="imgs/layout/check_ddk.jpg" alt=""></td>
                                <td>+ Gần công viên</td>
                                <td>+ Internet <img src="imgs/layout/check_ddk.jpg" alt=""></td>
                                <td>+ Đồng hồ điện <img src="imgs/layout/check_ddk.jpg" alt=""></td>
                            </tr>
                            <tr>
                                <td>+ Máy nước nóng <img src="imgs/layout/check_ddk.jpg" alt=""></td>
                                <td>+ Máy lạnh <img src="imgs/layout/check_ddk.jpg" alt=""></td>
                                <td>+ Hồ bơi</td>
                                <td>+ Sân vườn</td>
                            </tr>
                            </tbody>
                        </table><!-- End .tab_ddk -->

                    </div><!-- End .m_pD -->
                </div><!-- End .block_pD -->


                <div class="tag">
                    <span>Tags</span>

                    <a target="_self" href="nhu-cau/cho-thue-nha-rieng/cho-thue-biet-thu.html">cho thue biet thu</a>
                    <a target="_self" href="nhu-cau/cho-thue-nha-rieng/cho-thue-biet-thu-gan-bien.html"> cho thue
                        biet thu gan bien</a> <a target="_self"
                                                 href="nhu-cau/cho-thue-nha-rieng/cho-thue-biet-thu-o-vung-tau.html">
                        cho thue biet thu o vung tau</a> <a target="_self"
                                                            href="nhu-cau/cho-thue-nha-rieng/cho-thue-biet-thu-o-duong-tran-cao-van.html">
                        cho thue biet thu o duong tran cao van</a></div><!-- End .tag -->
                <div class="n2h">
                    <span class="t_nlh">Lưu ý</span><!-- End .t_nlh -->
                    <div>
                        Quý khách đang xem nội dung tin rao "<strong>Cho thuê biệt thự 10x20m, gần biển Vũng Tàu, 10
                            triệu/tháng</strong>". Mọi thông tin liên quan tới tin rao này là do người đăng tin đăng tải
                        và chịu trách nhiệm. Chúng tôi luôn cố gắng để có chất lượng thông tin tốt nhất. Chúng tôi
                        khhông chịu trách nhiệm nào về bất kỳ tổn thất, hay tranh chấp liên quan đến việc quý khách
                        tham khảo thông tin trên website. Nếu quý khách phát hiện có sai sót hay vấn đề gì<a
                                href="/lien-he.html" title=" Thông báo tin lỗi, nội dung xấu" target="_blank"> xin hãy
                            thông báo cho chúng tôi</a>.
                    </div>
                </div>

                <div class="news_other">

                    <div class="block_prod">
                        <h4 class="t_prod">
                <span>
                    Bất động sản khác
                </span>
                        </h4><!-- End .t_prod -->
                        <div class="m_prod">
                            <ul>
                                <li>
                                    <div class="li_prod clearfix">
                            <span class="lp_1">
                                <a href="http://www.bdstphcm.org/cho-thue-biet-thu-10x20m-gan-bien-vung-tau-10-trieu-thang.html"
                                   title="Cho thuê biệt thự 10x20m, gần biển Vũng Tàu, 10 triệu/tháng">
                                  									<img src="http://www.bdstphcm.org/imagecache/image.php/images/bds/cho-thue-biet-thu-10x20m-gan-bien-Vung-Tau-10-trieu-thang-1-o0rBK.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds/cho-thue-biet-thu-10x20m-gan-bien-Vung-Tau-10-trieu-thang-1-o0rBK.jpg"
                                                                         alt="Cho thuê biệt thự 10x20m, gần biển Vũng Tàu, 10 triệu/tháng">
								</a>
                            </span><!-- End .lp_1 -->
                                        <span class="lp_2">
                                <h4>
                                    <a href="http://www.bdstphcm.org/cho-thue-biet-thu-10x20m-gan-bien-vung-tau-10-trieu-thang.html"
                                       title="Cho thuê biệt thự 10x20m, gần biển Vũng Tàu, 10 triệu/tháng">
                                        Cho thuê biệt thự 10x20m, gần biển Vũng Tàu, 10 triệu/tháng                                    </a>
                                </h4>
                                <span class="vt_lp">Vị trí:  Bà Rịa Vũng Tàu</span>
                                <span class="price_lp">Giá: 10  Triệu/ Tháng</span>
                            </span><!-- End .lp_2 -->
                                    </div><!-- End .li_prod -->
                                </li>
                                <li>
                                    <div class="li_prod clearfix">
                            <span class="lp_1">
                                <a href="http://www.bdstphcm.org/cho-thue-nha-1-lau-duong-ba-cu-5x23m-35-trieu-thang.html"
                                   title="Cho thuê nhà 1 lầu, đường Ba Cu, 5x23m, 35 triệu/tháng">
                                  									<img src="http://www.bdstphcm.org/imagecache/image.php/imgs/noimage.png?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/imgs/noimage.png"
                                                                         alt="Cho thuê nhà 1 lầu, đường Ba Cu, 5x23m, 35 triệu/tháng">
								</a>
                            </span><!-- End .lp_1 -->
                                        <span class="lp_2">
                                <h4>
                                    <a href="http://www.bdstphcm.org/cho-thue-nha-1-lau-duong-ba-cu-5x23m-35-trieu-thang.html"
                                       title="Cho thuê nhà 1 lầu, đường Ba Cu, 5x23m, 35 triệu/tháng">
                                        Cho thuê nhà 1 lầu, đường Ba Cu, 5x23m, 35 triệu/tháng                                    </a>
                                </h4>
                                <span class="vt_lp">Vị trí:  Bà Rịa Vũng Tàu</span>
                                <span class="price_lp">Giá: 35  Triệu/ Tháng</span>
                            </span><!-- End .lp_2 -->
                                    </div><!-- End .li_prod -->
                                </li>
                                <li>
                                    <div class="li_prod clearfix">
                            <span class="lp_1">
                                <a href="http://www.bdstphcm.org/cho-thue-nha-4-4x13m-hem-nguyen-van-tiet-2-9-trieu-thang.html"
                                   title="Cho thuê nhà 4.4x13m, hẻm Nguyễn Văn Tiết, 2.9 triệu/tháng">
                                  									<img src="http://www.bdstphcm.org/imagecache/image.php/imgs/noimage.png?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/imgs/noimage.png"
                                                                         alt="Cho thuê nhà 4.4x13m, hẻm Nguyễn Văn Tiết, 2.9 triệu/tháng">
								</a>
                            </span><!-- End .lp_1 -->
                                        <span class="lp_2">
                                <h4>
                                    <a href="http://www.bdstphcm.org/cho-thue-nha-4-4x13m-hem-nguyen-van-tiet-2-9-trieu-thang.html"
                                       title="Cho thuê nhà 4.4x13m, hẻm Nguyễn Văn Tiết, 2.9 triệu/tháng">
                                        Cho thuê nhà 4.4x13m, hẻm Nguyễn Văn Tiết, 2.9 triệu/tháng                                    </a>
                                </h4>
                                <span class="vt_lp">Vị trí:  Bình Dương</span>
                                <span class="price_lp">Giá: 2.9  Triệu/ Tháng</span>
                            </span><!-- End .lp_2 -->
                                    </div><!-- End .li_prod -->
                                </li>
                                <li>
                                    <div class="li_prod clearfix">
                            <span class="lp_1">
                                <a href="http://www.bdstphcm.org/nha-cho-thue-phu-hop-nguoi-nuoc-ngoai-o-4x8m-3-lau.html"
                                   title="Nhà cho thuê phù hợp người nước ngoài ở, 4x8m, 3 lầu">
                                  									<img src="http://www.bdstphcm.org/imagecache/image.php/images/bds/nha-cho-thue-phu-hop-nguoi-nuoc-ngoai-o-4x8m-3-lau-1-fE23j.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds/nha-cho-thue-phu-hop-nguoi-nuoc-ngoai-o-4x8m-3-lau-1-fE23j.jpg"
                                                                         alt="Nhà cho thuê phù hợp người nước ngoài ở, 4x8m, 3 lầu">
								</a>
                            </span><!-- End .lp_1 -->
                                        <span class="lp_2">
                                <h4>
                                    <a href="http://www.bdstphcm.org/nha-cho-thue-phu-hop-nguoi-nuoc-ngoai-o-4x8m-3-lau.html"
                                       title="Nhà cho thuê phù hợp người nước ngoài ở, 4x8m, 3 lầu">
                                        Nhà cho thuê phù hợp người nước ngoài ở, 4x8m, 3 lầu                                    </a>
                                </h4>
                                <span class="vt_lp">Vị trí:  Hồ Chí Minh</span>
                                <span class="price_lp">Giá: 12  Triệu/ Tháng</span>
                            </span><!-- End .lp_2 -->
                                    </div><!-- End .li_prod -->
                                </li>
                                <li>
                                    <div class="li_prod clearfix">
                            <span class="lp_1">
                                <a href="http://www.bdstphcm.org/cho-thue-nha-3-lau-o-quan-10-5x24m-35-trieu-thang.html"
                                   title="Cho thuê nhà 3 lầu ở quận 10, 5x24m, 35 triệu/tháng">
                                  									<img src="http://www.bdstphcm.org/imagecache/image.php/imgs/noimage.png?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/imgs/noimage.png"
                                                                         alt="Cho thuê nhà 3 lầu ở quận 10, 5x24m, 35 triệu/tháng">
								</a>
                            </span><!-- End .lp_1 -->
                                        <span class="lp_2">
                                <h4>
                                    <a href="http://www.bdstphcm.org/cho-thue-nha-3-lau-o-quan-10-5x24m-35-trieu-thang.html"
                                       title="Cho thuê nhà 3 lầu ở quận 10, 5x24m, 35 triệu/tháng">
                                        Cho thuê nhà 3 lầu ở quận 10, 5x24m, 35 triệu/tháng                                    </a>
                                </h4>
                                <span class="vt_lp">Vị trí:  Hồ Chí Minh</span>
                                <span class="price_lp">Giá: 35  Triệu/ Tháng</span>
                            </span><!-- End .lp_2 -->
                                    </div><!-- End .li_prod -->
                                </li>
                                <li>
                                    <div class="li_prod clearfix">
                            <span class="lp_1">
                                <a href="http://www.bdstphcm.org/cho-thue-nha-dien-tich-4x16m-1-lau-quan-phu-nhuan.html"
                                   title="Cho thuê nhà diện tích 4x16m, 1 lầu, quận Phú Nhuận">
                                  									<img src="http://www.bdstphcm.org/imagecache/image.php/imgs/noimage.png?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/imgs/noimage.png"
                                                                         alt="Cho thuê nhà diện tích 4x16m, 1 lầu, quận Phú Nhuận">
								</a>
                            </span><!-- End .lp_1 -->
                                        <span class="lp_2">
                                <h4>
                                    <a href="http://www.bdstphcm.org/cho-thue-nha-dien-tich-4x16m-1-lau-quan-phu-nhuan.html"
                                       title="Cho thuê nhà diện tích 4x16m, 1 lầu, quận Phú Nhuận">
                                        Cho thuê nhà diện tích 4x16m, 1 lầu, quận Phú Nhuận                                    </a>
                                </h4>
                                <span class="vt_lp">Vị trí:  Hồ Chí Minh</span>
                                <span class="price_lp">Giá: 10  Triệu/ Tháng</span>
                            </span><!-- End .lp_2 -->
                                    </div><!-- End .li_prod -->
                                </li>
                                <li>
                                    <div class="li_prod clearfix">
                            <span class="lp_1">
                                <a href="http://www.bdstphcm.org/cho-thue-nha-70m2-3-tang-duong-tran-dang-ninh-nam-dinh.html"
                                   title="Cho thuê nhà 70m2, 3 tầng, Đường Trần Đăng Ninh, Nam Định">
                                  									<img src="http://www.bdstphcm.org/imagecache/image.php/imgs/noimage.png?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/imgs/noimage.png"
                                                                         alt="Cho thuê nhà 70m2, 3 tầng, Đường Trần Đăng Ninh, Nam Định">
								</a>
                            </span><!-- End .lp_1 -->
                                        <span class="lp_2">
                                <h4>
                                    <a href="http://www.bdstphcm.org/cho-thue-nha-70m2-3-tang-duong-tran-dang-ninh-nam-dinh.html"
                                       title="Cho thuê nhà 70m2, 3 tầng, Đường Trần Đăng Ninh, Nam Định">
                                        Cho thuê nhà 70m2, 3 tầng, Đường Trần Đăng Ninh, Nam Định                                    </a>
                                </h4>
                                <span class="vt_lp">Vị trí:  Nam Định</span>
                                <span class="price_lp">Giá: 3  Triệu/ Tổng diện tích</span>
                            </span><!-- End .lp_2 -->
                                    </div><!-- End .li_prod -->
                                </li>
                                <li>
                                    <div class="li_prod clearfix">
                            <span class="lp_1">
                                <a href="http://www.bdstphcm.org/m-th66k-cho-thue-biet-thu-ciputra-dien-tich-198m2.html"
                                   title="M-TH66K- Cho thuê biệt thự Ciputra ,diện tích 198m2">
                                  									<img src="http://www.bdstphcm.org/imagecache/image.php/imgs/noimage.png?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/imgs/noimage.png"
                                                                         alt="M-TH66K- Cho thuê biệt thự Ciputra ,diện tích 198m2">
								</a>
                            </span><!-- End .lp_1 -->
                                        <span class="lp_2">
                                <h4>
                                    <a href="http://www.bdstphcm.org/m-th66k-cho-thue-biet-thu-ciputra-dien-tich-198m2.html"
                                       title="M-TH66K- Cho thuê biệt thự Ciputra ,diện tích 198m2">
                                        M-TH66K- Cho thuê biệt thự Ciputra ,diện tích 198m2                                    </a>
                                </h4>
                                <span class="vt_lp">Vị trí:  Hà Nội</span>
                                <span class="price_lp">Giá: 53000000  Triệu/ m2</span>
                            </span><!-- End .lp_2 -->
                                    </div><!-- End .li_prod -->
                                </li>

                            </ul>
                            <div class="clear"></div>
                        </div><!-- End .m_prod -->
                    </div><!-- End .block_prod -->

                </div><!-- End .news_other -->


            </article><!-- End .content -->

            <div class="sidebar">

                <div class="block_sb">
                    <h4 class="t_sb">
        <span>
           Tìm kiếm nhanh
        </span>
                    </h4><!-- End .t_sb -->
                    <div class="m_sb">

                        <div class="slx_ttp">
                            <style type="text/css">
                                .slx_ttp {
                                    background: #FCFCFC;
                                    padding: 10px;
                                }

                                .slx_ttp span.customSelect {
                                    background-color: #fff !important;
                                }

                                .styled {
                                    background: none repeat scroll 0 0 #f9f9f9;
                                    border: 1px solid #ddd;
                                    padding: 2px;
                                    margin: 2px 0 2px 0;
                                }
                            </style>

                            <script>
                                $(document).ready(function () {
                                    $("#tinh").select2();
                                    $("#huyen").select2();
                                    $("#phuong").select2();
                                    $("#duong").select2();
                                    $("#loai").select2();
                                    $("#ddCat").select2();
                                    $("#tinh").select2();
                                    $("#huyen").select2();
                                    $("#phuong").select2();
                                    $("#duong").select2();
                                    $("#loai").select2();
                                    $("#ddCat").select2();
                                    $("#price").select2();
                                    $("#dientich").select2();
                                    $("#tinh").change(function () {
                                        var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                        var table = "tbl_quanhuyen";
                                        var tablep = "tbl_quanhuyen_category";
                                        $("#huyen").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                    });
                                    $("#huyen").change(function () {
                                        var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                        var table = "tbl_street";
                                        var tablep = "tbl_quanhuyen";
                                        $("#duong").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                    });
                                    $("#huyen").change(function () {
                                        var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                        var table = "tbl_phuongxa";
                                        var tablep = "tbl_quanhuyen";
                                        $("#phuong").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                    });
                                    $("#loai").change(function () {
                                        var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                        var table = "tbl_rv_category";
                                        var tablep = "tbl_rv_category";
                                        $("#ddCat").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                    });
                                });
                            </script>
                            <form action="http://www.bdstphcm.org/tim-bds.html" method="POST"
                                  enctype="multipart/form-data">
                                <ul>
                                    <li>
                                        <div class="select2-container styled" id="s2id_tinh" title=""><a
                                                    href="javascript:void(0)" class="select2-choice" tabindex="-1">
                                                <span class="select2-chosen"
                                                      id="select2-chosen-7">Bà Rịa Vũng Tàu</span><abbr
                                                        class="select2-search-choice-close"></abbr> <span
                                                        class="select2-arrow" role="presentation"><b
                                                            role="presentation"></b></span></a><label for="s2id_autogen7"
                                                                                                      class="select2-offscreen"></label><input
                                                    class="select2-focusser select2-offscreen" type="text"
                                                    aria-haspopup="true" role="button"
                                                    aria-labelledby="select2-chosen-7" id="s2id_autogen7" tabindex="0">
                                            <div class="select2-drop select2-display-none select2-with-searchbox">
                                                <div class="select2-search"><label for="s2id_autogen7_search"
                                                                                   class="select2-offscreen"></label>
                                                    <input type="text" autocomplete="off" autocorrect="off"
                                                           autocapitalize="off" spellcheck="false"
                                                           class="select2-input" role="combobox"
                                                           aria-expanded="true" aria-autocomplete="list"
                                                           aria-owns="select2-results-7" id="s2id_autogen7_search"
                                                           placeholder=""></div>
                                                <ul class="select2-results" role="listbox"
                                                    id="select2-results-7"></ul>
                                            </div>
                                        </div>
                                        <select name="tinh" id="tinh" class="styled select2-offscreen" title=""
                                                tabindex="-1"> 8
                                            <option value=""> Chọn thành phố</option>

                                            <option value="ho-chi-minh">Hồ Chí Minh</option>
                                            <option value="ha-noi">Hà Nội</option>
                                            <option value="binh-duong">Bình Dương</option>
                                            <option value="hai-phong">Hải Phòng</option>
                                            <option value="da-nang">Đà Nẵng</option>
                                            <option value="ben-tre">Bến Tre</option>
                                            <option value="can-tho">Cần Thơ</option>
                                            <option value="dong-nai">Đồng Nai</option>
                                            <option value="ba-ria-vung-tau" selected="selected">Bà Rịa Vũng Tàu
                                            </option>
                                            <option value="khanh-hoa">Khánh Hòa</option>
                                            <option value="long-an">Long An</option>
                                            <option value="thua-thien-hue">Thừa Thiên Huế</option>
                                            <option value="an-giang">An Giang</option>
                                            <option value="bac-lieu">Bạc Liêu</option>
                                            <option value="bac-can">Bắc Cạn</option>
                                            <option value="bac-ninh">Bắc Ninh</option>
                                            <option value="bac-giang">Bắc Giang</option>
                                            <option value="binh-dinh">Bình Định</option>
                                            <option value="binh-phuoc">Bình Phước</option>
                                            <option value="binh-thuan">Bình Thuận</option>
                                            <option value="ca-mau">Cà Mau</option>
                                            <option value="cao-bang">Cao Bằng</option>
                                            <option value="dak-lak">Đắk Lắk</option>
                                            <option value="dong-thap">Đồng Tháp</option>
                                            <option value="gia-lai">Gia Lai</option>
                                            <option value="ha-giang">Hà Giang</option>
                                            <option value="ha-nam">Hà Nam</option>
                                            <option value="ha-tinh">Hà Tĩnh</option>
                                            <option value="hai-duong">Hải Dương</option>
                                            <option value="hoa-binh">Hòa Bình</option>
                                            <option value="hung-yen">Hưng Yên</option>
                                            <option value="kien-giang">Kiên Giang</option>
                                            <option value="kon-tum">Kon Tum</option>
                                            <option value="lai-chau">Lai Châu</option>
                                            <option value="lang-son">Lạng Sơn</option>
                                            <option value="lao-cai">Lào Cai</option>
                                            <option value="lam-dong">Lâm Đồng</option>
                                            <option value="nam-dinh">Nam Định</option>
                                            <option value="nghe-an">Nghệ An</option>
                                            <option value="ninh-binh">Ninh Bình</option>
                                            <option value="ninh-thuan">Ninh Thuận</option>
                                            <option value="phu-tho">Phú Thọ</option>
                                            <option value="phu-yen">Phú Yên</option>
                                            <option value="quang-binh">Quảng Bình</option>
                                            <option value="quang-nam">Quảng Nam</option>
                                            <option value="quang-ngai">Quảng Ngãi</option>
                                            <option value="quang-ninh">Quảng Ninh</option>
                                            <option value="quang-tri">Quảng Trị</option>
                                            <option value="soc-trang">Sóc Trăng</option>
                                            <option value="son-la">Sơn La</option>
                                            <option value="tay-ninh">Tây Ninh</option>
                                            <option value="thai-binh">Thái Bình</option>
                                            <option value="thai-nguyen">Thái Nguyên</option>
                                            <option value="thanh-hoa">Thanh Hóa</option>
                                            <option value="tien-giang">Tiền Giang</option>
                                            <option value="tra-vinh">Trà Vinh</option>
                                            <option value="tuyen-quang">Tuyên Quang</option>
                                            <option value="vinh-long">Vĩnh Long</option>
                                            <option value="vinh-phuc">Vĩnh Phúc</option>
                                            <option value="yen-bai">Yên Bái</option>
                                            <option value="dien-bien">Điện Biên</option>
                                            <option value="dak-nong">Đắk Nông</option>
                                            <option value="hau-giang">Hậu Giang</option>

                                        </select>
                                    </li>
                                    <li>
                                        <div class="select2-container styled" id="s2id_huyen" title=""><a
                                                    href="javascript:void(0)" class="select2-choice" tabindex="-1">
                                                <span class="select2-chosen"
                                                      id="select2-chosen-8">Thành Phố Vũng Tàu</span><abbr
                                                        class="select2-search-choice-close"></abbr> <span
                                                        class="select2-arrow" role="presentation"><b
                                                            role="presentation"></b></span></a><label for="s2id_autogen8"
                                                                                                      class="select2-offscreen"></label><input
                                                    class="select2-focusser select2-offscreen" type="text"
                                                    aria-haspopup="true" role="button"
                                                    aria-labelledby="select2-chosen-8" id="s2id_autogen8" tabindex="0">
                                            <div class="select2-drop select2-display-none select2-with-searchbox">
                                                <div class="select2-search"><label for="s2id_autogen8_search"
                                                                                   class="select2-offscreen"></label>
                                                    <input type="text" autocomplete="off" autocorrect="off"
                                                           autocapitalize="off" spellcheck="false"
                                                           class="select2-input" role="combobox"
                                                           aria-expanded="true" aria-autocomplete="list"
                                                           aria-owns="select2-results-8" id="s2id_autogen8_search"
                                                           placeholder=""></div>
                                                <ul class="select2-results" role="listbox"
                                                    id="select2-results-8"></ul>
                                            </div>
                                        </div>
                                        <select name="huyen" id="huyen" class="styled select2-offscreen" title=""
                                                tabindex="-1">

                                            <option value=""> Chọn Quận / Huyện</option>
                                            <option value="huyen-xuyen-moc">Huyện Xuyên Mộc</option>
                                            <option value="huyen-con-dao">Huyện Côn Đảo</option>
                                            <option value="huyen-long-dien">Huyện Long Điền</option>
                                            <option value="huyen-chau-duc">Huyện Châu Đức</option>
                                            <option value="huyen-tan-thanh">Huyện Tân Thành</option>
                                            <option value="thanh-pho-vung-tau" selected="selected">Thành Phố Vũng
                                                Tàu
                                            </option>
                                            <option value="thi-xa-ba-ria">Thị Xã Bà Rịa</option>
                                            <option value="dat-do">Đất Đỏ</option>
                                        </select>
                                    </li>
                                    <li>
                                        <div class="select2-container styled" id="s2id_phuong" title=""><a
                                                    href="javascript:void(0)" class="select2-choice" tabindex="-1">
                                                <span class="select2-chosen" id="select2-chosen-9">Phường 9</span><abbr
                                                        class="select2-search-choice-close"></abbr> <span
                                                        class="select2-arrow" role="presentation"><b
                                                            role="presentation"></b></span></a><label for="s2id_autogen9"
                                                                                                      class="select2-offscreen"></label><input
                                                    class="select2-focusser select2-offscreen" type="text"
                                                    aria-haspopup="true" role="button"
                                                    aria-labelledby="select2-chosen-9" id="s2id_autogen9" tabindex="0">
                                            <div class="select2-drop select2-display-none select2-with-searchbox">
                                                <div class="select2-search"><label for="s2id_autogen9_search"
                                                                                   class="select2-offscreen"></label>
                                                    <input type="text" autocomplete="off" autocorrect="off"
                                                           autocapitalize="off" spellcheck="false"
                                                           class="select2-input" role="combobox"
                                                           aria-expanded="true" aria-autocomplete="list"
                                                           aria-owns="select2-results-9" id="s2id_autogen9_search"
                                                           placeholder=""></div>
                                                <ul class="select2-results" role="listbox"
                                                    id="select2-results-9"></ul>
                                            </div>
                                        </div>
                                        <select name="phuong" id="phuong" class="styled select2-offscreen" title=""
                                                tabindex="-1">

                                            <option value="">Chọn Phường / Xã</option>
                                            <option value="phuong-1">Phường 1</option>
                                            <option value="phuong-10">Phường 10</option>
                                            <option value="phuong-11">Phường 11</option>
                                            <option value="phuong-12">Phường 12</option>
                                            <option value="phuong-2">Phường 2</option>
                                            <option value="phuong-3">Phường 3</option>
                                            <option value="phuong-4">Phường 4</option>
                                            <option value="phuong-5">Phường 5</option>
                                            <option value="phuong-6">Phường 6</option>
                                            <option value="phuong-7">Phường 7</option>
                                            <option value="phuong-8">Phường 8</option>
                                            <option value="phuong-9" selected="selected">Phường 9</option>
                                            <option value="phuong-an-ninh">Phường An Ninh</option>
                                            <option value="phuong-long-son">Phường Long Sơn</option>
                                            <option value="phuong-rach-dua">Phường Rạch Dừa</option>
                                            <option value="phuong-thang-nhat">Phường Thắng Nhất</option>
                                            <option value="phuong-thang-tam">Phường Thắng Tam</option>
                                        </select>
                                    </li>
                                    <li>
                                        <div class="select2-container styled" id="s2id_duong" title=""><a
                                                    href="javascript:void(0)" class="select2-choice" tabindex="-1">
                                                <span class="select2-chosen"
                                                      id="select2-chosen-10">Đường Trần Cao Vân</span><abbr
                                                        class="select2-search-choice-close"></abbr> <span
                                                        class="select2-arrow" role="presentation"><b
                                                            role="presentation"></b></span></a><label for="s2id_autogen10"
                                                                                                      class="select2-offscreen"></label><input
                                                    class="select2-focusser select2-offscreen" type="text"
                                                    aria-haspopup="true" role="button"
                                                    aria-labelledby="select2-chosen-10" id="s2id_autogen10"
                                                    tabindex="0">
                                            <div class="select2-drop select2-display-none select2-with-searchbox">
                                                <div class="select2-search"><label for="s2id_autogen10_search"
                                                                                   class="select2-offscreen"></label>
                                                    <input type="text" autocomplete="off" autocorrect="off"
                                                           autocapitalize="off" spellcheck="false"
                                                           class="select2-input" role="combobox"
                                                           aria-expanded="true" aria-autocomplete="list"
                                                           aria-owns="select2-results-10" id="s2id_autogen10_search"
                                                           placeholder=""></div>
                                                <ul class="select2-results" role="listbox"
                                                    id="select2-results-10"></ul>
                                            </div>
                                        </div>
                                        <select name="duong" id="duong" class="styled select2-offscreen" title=""
                                                tabindex="-1">

                                            <option value="">Chọn đường</option>
                                            <option value="duong-3-2">Đường 3/2</option>
                                            <option value="duong-30-4">Đường 30/4</option>
                                            <option value="duong-ba-cu">Đường Ba Cu</option>
                                            <option value="duong-ba-huyen-thanh-quan">Đường Bà Huyện Thanh Quan
                                            </option>
                                            <option value="duong-ba-trieu">Đường Bà Triệu</option>
                                            <option value="duong-bac-son">Đường Bắc Sơn</option>
                                            <option value="duong-bach-dang">Đường Bạch Đằng</option>
                                            <option value="duong-ben-nom">Đường Bến Nôm</option>
                                            <option value="duong-ben-diep">Đường Bến Điệp</option>
                                            <option value="duong-ben-do">Đường Bến Đò</option>
                                            <option value="duong-binh-gia">Đường Bình Giã</option>
                                            <option value="duong-bo-ke-rach-ben-dinh">Đường Bờ kè Rạch Bến Đình
                                            </option>
                                            <option value="duong-cao-thang">Đường Cao Thắng</option>
                                            <option value="duong-chi-lang">Đường Chi Lăng</option>
                                            <option value="duong-cho-rach-dua">Đường chợ Rạch Dừa</option>
                                            <option value="duong-co-bac">Đường Cô Bắc</option>
                                            <option value="duong-co-giang">Đường Cô Giang</option>
                                            <option value="duong-con-ban">Đường Cồn Bần</option>
                                            <option value="duong-d4">Đường D4</option>
                                            <option value="duong-d5">Đường D5</option>
                                            <option value="duong-duong-bach-mai">Đường Dương Bạch Mai</option>
                                            <option value="duong-duong-van-nga">Đường Dương Vân Nga</option>
                                            <option value="duong-h1-cach-mang-thang-8">Đường H1 - Cách Mạng Tháng
                                                8
                                            </option>
                                            <option value="duong-h1-ha-huy-tap">Đường H1 - Hà Huy Tập</option>
                                            <option value="duong-h2-cach-mang-thang-8">Đường H2 - Cách Mạng Tháng
                                                8
                                            </option>
                                            <option value="duong-h2-le-duan">Đường H2 - Lê Duẩn</option>
                                            <option value="duong-h2-nguyen-thi-dinh">Đường H2 – Nguyễn Thị Định
                                            </option>
                                            <option value="duong-h3-ha-huy-tap">Đường H3 – Hà Huy Tập</option>
                                            <option value="duong-h4-le-duan">Đường H4 - Lê Duẩn</option>
                                            <option value="duong-h4-nguyen-thi-dinh">Đường H4 – Nguyễn Thị Định
                                            </option>
                                            <option value="duong-h6-cach-mang-thang-8">Đường H6 - Cách Mạng Tháng
                                                8
                                            </option>
                                            <option value="duong-ha-huy-tap">Đường Hà Huy Tập</option>
                                            <option value="duong-ha-long">Đường Hạ Long</option>
                                            <option value="duong-hai-thuong-lan-ong">Đường Hải Thượng Lãn Ông
                                            </option>
                                            <option value="duong-hai-dang">Đường Hải Đăng</option>
                                            <option value="duong-han-mac-tu">Đường Hàn Mạc Tử</option>
                                            <option value="duong-han-thuyen">Đường Hàn Thuyên</option>
                                            <option value="duong-hem-02-04-36-50-nguyen-luong-bang">Đường Hẻm 02,
                                                04, 36, 50 Nguyễn Lương Bằng
                                            </option>
                                            <option value="duong-hem-43-61-duong-pham-ngoc-thach">Đường Hẻm 43, 61
                                                đường Phạm Ngọc Thạch
                                            </option>
                                            <option value="duong-hem-135-le-quang-dinh">Đường Hẻm 135 Lê Quang
                                                Định
                                            </option>
                                            <option value="duong-hem-30-luong-the-vinh">Đường Hẻm 30 Lương Thế
                                                Vinh
                                            </option>
                                            <option value="duong-hem-40-phan-dang-luu">Đường Hẻm 40 - Phan Đăng
                                                Lưu
                                            </option>
                                            <option value="duong-hem-492-duong-30-4">Đường Hẻm 492 - đường 30/4
                                            </option>
                                            <option value="duong-hem-524-duong-30-4">Đường Hẻm 524 - đường 30/4
                                            </option>
                                            <option value="duong-hem-58-nguyen-gia-thieu">Đường Hẻm 58 Nguyễn Gia
                                                Thiều
                                            </option>
                                            <option value="duong-hem-825-va-875-binh-gia">Đường Hẻm 825 và 875 Bình
                                                Giã
                                            </option>
                                            <option value="duong-hem-so-1-nguyen-an-ninh">Đường Hẻm số 1 Nguyễn An
                                                Ninh
                                            </option>
                                            <option value="duong-hem-so-110-pham-hong-thai">Đường Hẻm số 110 Phạm
                                                Hồng Thái
                                            </option>
                                            <option value="duong-hem-so-3-thon-5">Đường Hẻm số 3 thôn 5</option>
                                            <option value="duong-ho-tri-tan">Đường Hồ Tri Tân</option>
                                            <option value="duong-ho-xuan-huong">Đường Hồ Xuân Hương</option>
                                            <option value="duong-hoa-lu">Đường Hoa Lư</option>
                                            <option value="duong-hoang-dieu">Đường Hoàng Diệu</option>
                                            <option value="duong-hoang-hoa-tham">Đường Hoàng Hoa Thám</option>
                                            <option value="duong-hoang-van-thai">Đường Hoàng Văn Thái</option>
                                            <option value="duong-hoang-van-thu">Đường Hoàng Văn Thụ</option>
                                            <option value="duong-hoang-viet">Đường Hoàng Việt</option>
                                            <option value="duong-hung-vuong">Đường Hùng Vương</option>
                                            <option value="duong-huyen-tran-cong-chua">Đường Huyền Trân Công Chúa
                                            </option>
                                            <option value="duong-huynh-khuong-an">Đường Huỳnh Khương An</option>
                                            <option value="duong-huynh-khuong-ninh">Đường Huỳnh Khương Ninh</option>
                                            <option value="duong-kha-van-can">Đường Kha Vạn Cân</option>
                                            <option value="duong-khu-tai-dinh-cu-ben-dinh">Đường khu tái định cư Bến
                                                Đình
                                            </option>
                                            <option value="duong-khu-vuc-go-gang">Đường Khu vực Gò Găng</option>
                                            <option value="duong-kim-dong">Đường Kim Đồng</option>
                                            <option value="duong-ky-con">Đường Ký Con</option>
                                            <option value="duong-ky-dong">Đường Kỳ Đồng</option>
                                            <option value="duong-la-van-cau">Đường La Văn Cầu</option>
                                            <option value="duong-lac-long-quan">Đường Lạc Long Quân</option>
                                            <option value="duong-lang-cat-long-son">Đường Láng Cát – Long Sơn
                                            </option>
                                            <option value="duong-le-bao-tinh">Đường Lê Bảo Tịnh</option>
                                            <option value="duong-le-hoan">Đường Lê Hoàn</option>
                                            <option value="duong-le-hong-phong">Đường Lê Hồng Phong</option>
                                            <option value="duong-le-lai">Đường Lê Lai</option>
                                            <option value="duong-le-loi">Đường Lê Lợi</option>
                                            <option value="duong-le-ngoc-han">Đường Lê Ngọc Hân</option>
                                            <option value="duong-le-phung-hieu">Đường Lê Phụng Hiểu</option>
                                            <option value="duong-le-quang-dinh">Đường Lê Quang Định</option>
                                            <option value="duong-le-thanh-duy">Đường Lê Thành Duy</option>
                                            <option value="duong-le-thanh-tong">Đường Lê Thánh Tông</option>
                                            <option value="duong-le-thi-rieng">Đường Lê Thị Riêng</option>
                                            <option value="duong-le-van-loc">Đường Lê Văn Lộc</option>
                                            <option value="duong-le-van-tam">Đường Lê Văn Tâm</option>
                                            <option value="duong-len-biet-thu-doi-su">Đường lên biệt thự đồi sứ
                                            </option>
                                            <option value="duong-lien-thon-1-rach-lua">Đường Liên thôn 1- Rạch Lùa
                                            </option>
                                            <option value="duong-lien-thon-4-6">Đường Liên thôn 4-6</option>
                                            <option value="duong-lien-thon-5-8">Đường Liên thôn 5-8</option>
                                            <option value="duong-lien-thon-ben-diep">Đường Liên thôn Bến Điệp
                                            </option>
                                            <option value="duong-luong-van-can">Đường Lương Văn Can</option>
                                            <option value="duong-luu-chi-hieu">Đường Lưu Chí Hiếu</option>
                                            <option value="duong-luu-huu-phuoc">Đường Lưu Hữu Phước</option>
                                            <option value="duong-ly-thai-to">Đường Lý Thái Tổ</option>
                                            <option value="duong-ly-thuong-kiet">Đường Lý Thường Kiệt</option>
                                            <option value="duong-ly-tu-trong">Đường Lý Tự Trọng</option>
                                            <option value="duong-mac-dinh-chi">Đường Mạc Đĩnh Chi</option>
                                            <option value="duong-mai-thuc-loan">Đường Mai Thúc Loan</option>
                                            <option value="duong-nam-ky-khoi-nghia">Đường Nam Kỳ Khởi Nghĩa</option>
                                            <option value="duong-ngo-quyen">Đường Ngô Quyền</option>
                                            <option value="duong-ngo-van-huyen">Đường Ngô Văn Huyền</option>
                                            <option value="duong-ngo-duc-ke">Đường Ngô Đức Kế</option>
                                            <option value="duong-ngu-phu">Đường Ngư Phủ</option>
                                            <option value="duong-nguyen-an-ninh">Đường Nguyễn An Ninh</option>
                                            <option value="duong-nguyen-bao">Đường Nguyễn Bảo</option>
                                            <option value="duong-nguyen-binh-khiem">Đường Nguyễn Bỉnh Khiêm</option>
                                            <option value="duong-nguyen-buu">Đường Nguyễn Bửu</option>
                                            <option value="duong-nguyen-chi-thanh">Đường Nguyễn Chí Thanh</option>
                                            <option value="duong-nguyen-cong-tru">Đường Nguyễn Công Trứ</option>
                                            <option value="duong-nguyen-cu-trinh">Đường Nguyễn Cư Trinh</option>
                                            <option value="duong-nguyen-du">Đường Nguyễn Du</option>
                                            <option value="duong-nguyen-gia-thieu">Đường Nguyễn Gia Thiều</option>
                                            <option value="duong-nguyen-hien">Đường Nguyễn Hiền</option>
                                            <option value="duong-nguyen-hoi">Đường Nguyễn Hới</option>
                                            <option value="duong-nguyen-huu-canh">Đường Nguyễn Hữu Cảnh</option>
                                            <option value="duong-nguyen-huu-cau">Đường Nguyễn Hữu Cầu</option>
                                            <option value="duong-nguyen-khuyen">Đường Nguyễn Khuyến</option>
                                            <option value="duong-nguyen-kim">Đường Nguyễn Kim</option>
                                            <option value="duong-nguyen-luong-bang">Đường Nguyễn Lương Bằng</option>
                                            <option value="duong-nguyen-thai-binh">Đường Nguyễn Thái Bình</option>
                                            <option value="duong-nguyen-thai-hoc">Đường Nguyễn Thái Học</option>
                                            <option value="duong-nguyen-thien-thuat">Đường Nguyễn Thiện Thuật
                                            </option>
                                            <option value="duong-nguyen-trai">Đường Nguyễn Trãi</option>
                                            <option value="duong-nguyen-tri-phuong">Đường Nguyễn Tri Phương</option>
                                            <option value="duong-nguyen-trung-truc">Đường Nguyễn Trung Trực</option>
                                            <option value="duong-nguyen-truong-to">Đường Nguyễn Trường Tộ</option>
                                            <option value="duong-nguyen-van-con">Đường Nguyễn Văn Côn</option>
                                            <option value="duong-nguyen-van-cu">Đường Nguyễn Văn Cừ</option>
                                            <option value="duong-nguyen-van-troi">Đường Nguyễn Văn Trỗi</option>
                                            <option value="duong-no-trang-long">Đường Nơ Trang Long</option>
                                            <option value="duong-ong-hung">Đường Ông Hưng</option>
                                            <option value="duong-pasteur">Đường Pasteur</option>
                                            <option value="duong-pham-cu-lang">Đường Phạm Cự Lạng</option>
                                            <option value="duong-pham-hong-thai">Đường Phạm Hồng Thái</option>
                                            <option value="duong-pham-ngoc-thach">Đường Phạm Ngọc Thạch</option>
                                            <option value="duong-pham-ngu-lao">Đường Phạm Ngũ Lão</option>
                                            <option value="duong-pham-the-hien">Đường Phạm Thế Hiển</option>
                                            <option value="duong-pham-van-bach">Đường Phạm Văn Bạch</option>
                                            <option value="duong-pham-van-dinh">Đường Phạm Văn Dinh</option>
                                            <option value="duong-pham-van-nghi">Đường Phạm Văn Nghị</option>
                                            <option value="duong-phan-chu-trinh">Đường Phan Chu Trinh</option>
                                            <option value="duong-phan-ke-binh">Đường Phan Kế Bính</option>
                                            <option value="duong-phan-dang-luu">Đường Phan Đăng Lưu</option>
                                            <option value="duong-phan-dinh-phung">Đường Phan Đình Phùng</option>
                                            <option value="duong-pho-duc-chinh">Đường Phó Đức Chính</option>
                                            <option value="duong-phung-khac-khoan">Đường Phùng Khắc Khoan</option>
                                            <option value="duong-phuoc-thang">Đường Phước Thắng</option>
                                            <option value="duong-quang-trung">Đường Quang Trung</option>
                                            <option value="duong-quoc-lo-51b">Đường Quốc lộ 51B</option>
                                            <option value="duong-so-2-thon-5">Đường Số 2 thôn 5</option>
                                            <option value="duong-so-2-thon-6">Đường Số 2 thôn 6</option>
                                            <option value="duong-so-22">Đường số 22</option>
                                            <option value="duong-suong-nguyet-anh">Đường Sương Nguyệt Ánh</option>
                                            <option value="duong-tang-bat-ho">Đường Tăng Bạt Hổ</option>
                                            <option value="duong-tay-ho-mang-ca">Đường Tây Hồ Mang Cá</option>
                                            <option value="duong-thai-van-lung">Đường Thái Văn Lung</option>
                                            <option value="duong-thang-nhi">Đường Thắng Nhì</option>
                                            <option value="duong-thi-sach">Đường Thi Sách</option>
                                            <option value="duong-thon-2-ben-da">Đường thôn 2 Bến Đá</option>
                                            <option value="duong-thon-4">Đường thôn 4</option>
                                            <option value="duong-thon-5">Đường thôn 5</option>
                                            <option value="duong-thon-6">Đường thôn 6</option>
                                            <option value="duong-thon-7">Đường thôn 7</option>
                                            <option value="duong-thong-nhat">Đường Thống Nhất</option>
                                            <option value="duong-thu-khoa-huan">Đường Thủ Khoa Huân</option>
                                            <option value="duong-thuy-van">Đường Thùy Vân</option>
                                            <option value="duong-tien-cang">Đường Tiền Cảng</option>
                                            <option value="duong-to-hien-thanh">Đường Tô Hiến Thành</option>
                                            <option value="duong-ton-that-thuyet">Đường Tôn Thất Thuyết</option>
                                            <option value="duong-ton-that-tung">Đường Tôn Thất Tùng</option>
                                            <option value="duong-ton-dan">Đường Tôn Đản</option>
                                            <option value="duong-tong-duy-tan">Đường Tống Duy Tân</option>
                                            <option value="duong-tran-anh-tong">Đường Trần Anh Tông</option>
                                            <option value="duong-tran-binh-trong">Đường Trần Bình Trọng</option>
                                            <option value="duong-tran-cao-van" selected="selected">Đường Trần Cao
                                                Vân
                                            </option>
                                            <option value="duong-tran-huy-lieu">Đường Trần Huy Liệu</option>
                                            <option value="duong-tran-khac-chung">Đường Trần Khắc Chung</option>
                                            <option value="duong-tran-khanh-du">Đường Trần Khánh Dư</option>
                                            <option value="duong-tran-nguyen-han">Đường Trần Nguyên Hãn</option>
                                            <option value="duong-tran-nguyen-dan">Đường Trần Nguyên Đán</option>
                                            <option value="duong-tran-phu">Đường Trần Phú</option>
                                            <option value="duong-tran-quoc-toan">Đường Trần Quốc Toản</option>
                                            <option value="duong-tran-quy-cap">Đường Trần Quý Cáp</option>
                                            <option value="duong-tran-xuan-do">Đường Trần Xuân Độ</option>
                                            <option value="duong-tran-dong">Đường Trần Đồng</option>
                                            <option value="duong-trieu-viet-vuong">Đường Triệu Việt Vương</option>
                                            <option value="duong-trinh-hoai-duc">Đường Trịnh Hoài Đức</option>
                                            <option value="duong-truc-chinh">Đường Trục chính</option>
                                            <option value="duong-trung-nhi">Đường Trưng Nhị</option>
                                            <option value="duong-trung-trac">Đường Trưng Trắc</option>
                                            <option value="duong-truong-cong-dinh">Đường Trương Công Định</option>
                                            <option value="duong-truong-han-sieu">Đường Trương Hán Siêu</option>
                                            <option value="duong-truong-ngoc">Đường Trương Ngọc</option>
                                            <option value="duong-truong-van-bang">Đường Trương Văn Bang</option>
                                            <option value="duong-truong-vinh-ky">Đường Trương Vĩnh Ký</option>
                                            <option value="duong-tu-xuong">Đường Tú Xương</option>
                                            <option value="duong-van-cao">Đường Văn Cao</option>
                                            <option value="duong-vao-nha-may-dien-ba-ria">Đường vào Nhà Máy Điện Bà
                                                Rịa
                                            </option>
                                            <option value="duong-ven-bien-hai-dang">Đường ven biển Hải Đăng</option>
                                            <option value="duong-vi-ba">Đường Vi Ba</option>
                                            <option value="duong-vo-thi-sau">Đường Võ Thị Sáu</option>
                                            <option value="duong-vo-van-kiet">Đường Võ Văn Kiệt</option>
                                            <option value="duong-vo-van-tan">Đường Võ Văn Tần</option>
                                            <option value="duong-vo-dinh-thanh">Đường Võ Đình Thành</option>
                                            <option value="duong-xi-nghiep-quyet-tien">Đường Xí nghiệp Quyết Tiến
                                            </option>
                                            <option value="duong-xo-viet-nghe-tinh">Đường Xô Viết Nghệ Tĩnh</option>
                                            <option value="duong-yen-bai">Đường Yên Bái</option>
                                            <option value="duong-yen-do">Đường Yên Đổ</option>
                                            <option value="duong-yersin">Đường Yersin</option>
                                            <option value="duong-dinh-tien-hoang">Đường Đinh Tiên Hoàng</option>
                                            <option value="duong-do-chieu">Đường Đồ Chiểu</option>
                                            <option value="duong-do-luong">Đường Đô Lương</option>
                                            <option value="duong-doan-thi-diem">Đường Đoàn Thị Điểm</option>
                                            <option value="duong-doi-can">Đường Đội cấn</option>
                                            <option value="duong-dong-ho-mang-ca">Đường Đông Hồ Mang Cá</option>
                                            <option value="duong-dong-khoi">Đường Đồng Khởi</option>
                                            <option value="duong-dong-tay-giao-su-dung-lac">Đường Đông Tây Giáo Sứ
                                                Dũng Lạc
                                            </option>
                                            <option value="duong-dong-da">Đường Đống Đa</option>
                                            <option value="duong-ong-ich-khiem">Đường Ông Ích Khiêm</option>
                                            <option value="duong-phan-boi-chau">Đường Phan Bội Châu</option>
                                            <option value="duong-dao-duy-tu">Đường Đào Duy Từ</option>

                                        </select>
                                    </li>
                                    <li>
                                        <div class="select2-container styled" id="s2id_loai" title=""><a
                                                    href="javascript:void(0)" class="select2-choice" tabindex="-1">
                                                <span class="select2-chosen"
                                                      id="select2-chosen-11">  Nhà đất cho thuê</span><abbr
                                                        class="select2-search-choice-close"></abbr> <span
                                                        class="select2-arrow" role="presentation"><b
                                                            role="presentation"></b></span></a><label for="s2id_autogen11"
                                                                                                      class="select2-offscreen"></label><input
                                                    class="select2-focusser select2-offscreen" type="text"
                                                    aria-haspopup="true" role="button"
                                                    aria-labelledby="select2-chosen-11" id="s2id_autogen11"
                                                    tabindex="0">
                                            <div class="select2-drop select2-display-none select2-with-searchbox">
                                                <div class="select2-search"><label for="s2id_autogen11_search"
                                                                                   class="select2-offscreen"></label>
                                                    <input type="text" autocomplete="off" autocorrect="off"
                                                           autocapitalize="off" spellcheck="false"
                                                           class="select2-input" role="combobox"
                                                           aria-expanded="true" aria-autocomplete="list"
                                                           aria-owns="select2-results-11" id="s2id_autogen11_search"
                                                           placeholder=""></div>
                                                <ul class="select2-results" role="listbox"
                                                    id="select2-results-11"></ul>
                                            </div>
                                        </div>
                                        <select name="loai" id="loai" class="styled select2-offscreen" title=""
                                                tabindex="-1">

                                            <option value="">Chọn nhu cầu</option>
                                            <option value="nha-dat-ban"> Nhà đất bán</option>
                                            <option value="nha-dat-cho-thue" selected="selected"> Nhà đất cho thuê
                                            </option>

                                        </select>
                                    </li>
                                    <li>
                                        <div class="select2-container styled" id="s2id_ddCat" title=""><a
                                                    href="javascript:void(0)" class="select2-choice" tabindex="-1">
                                                <span class="select2-chosen"
                                                      id="select2-chosen-12">  Cho thuê nhà riêng</span><abbr
                                                        class="select2-search-choice-close"></abbr> <span
                                                        class="select2-arrow" role="presentation"><b
                                                            role="presentation"></b></span></a><label for="s2id_autogen12"
                                                                                                      class="select2-offscreen"></label><input
                                                    class="select2-focusser select2-offscreen" type="text"
                                                    aria-haspopup="true" role="button"
                                                    aria-labelledby="select2-chosen-12" id="s2id_autogen12"
                                                    tabindex="0">
                                            <div class="select2-drop select2-display-none select2-with-searchbox">
                                                <div class="select2-search"><label for="s2id_autogen12_search"
                                                                                   class="select2-offscreen"></label>
                                                    <input type="text" autocomplete="off" autocorrect="off"
                                                           autocapitalize="off" spellcheck="false"
                                                           class="select2-input" role="combobox"
                                                           aria-expanded="true" aria-autocomplete="list"
                                                           aria-owns="select2-results-12" id="s2id_autogen12_search"
                                                           placeholder=""></div>
                                                <ul class="select2-results" role="listbox"
                                                    id="select2-results-12"></ul>
                                            </div>
                                        </div>
                                        <select name="ddCat" id="ddCat" class="styled select2-offscreen" title=""
                                                tabindex="-1">

                                            <option value="">Chọn hình thức</option>
                                            <option value="cho-thue-dat"> Cho thuê đất</option>
                                            <option value="cho-thue-nha-hang-khach-san"> Cho thuê nhà hàng, khách
                                                sạn
                                            </option>
                                            <option value="cho-thue-nha-tro-phong-tro"> Cho thuê nhà trọ, phòng
                                                trọ
                                            </option>
                                            <option value="cho-thue-cua-hang-ki-ot"> Cho thuê cửa hàng, ki ốt
                                            </option>
                                            <option value="cho-thue-can-ho-chung-cu"> Cho thuê căn hộ chung cư
                                            </option>
                                            <option value="cho-thue-nha-rieng" selected="selected"> Cho thuê nhà
                                                riêng
                                            </option>
                                            <option value="cho-thue-nha-mat-pho"> Cho thuê nhà mặt phố</option>
                                            <option value="cho-thue-van-phong"> Cho thuê văn phòng</option>
                                        </select>
                                    </li>
                                    <li>
                                        <div class="select2-container styled" id="s2id_price"><a
                                                    href="javascript:void(0)" class="select2-choice" tabindex="-1">
                                                <span class="select2-chosen" id="select2-chosen-13">Chọn giá</span><abbr
                                                        class="select2-search-choice-close"></abbr> <span
                                                        class="select2-arrow" role="presentation"><b
                                                            role="presentation"></b></span></a><label for="s2id_autogen13"
                                                                                                      class="select2-offscreen"></label><input
                                                    class="select2-focusser select2-offscreen" type="text"
                                                    aria-haspopup="true" role="button"
                                                    aria-labelledby="select2-chosen-13" id="s2id_autogen13"
                                                    tabindex="0">
                                            <div class="select2-drop select2-display-none select2-with-searchbox">
                                                <div class="select2-search"><label for="s2id_autogen13_search"
                                                                                   class="select2-offscreen"></label>
                                                    <input type="text" autocomplete="off" autocorrect="off"
                                                           autocapitalize="off" spellcheck="false"
                                                           class="select2-input" role="combobox"
                                                           aria-expanded="true" aria-autocomplete="list"
                                                           aria-owns="select2-results-13" id="s2id_autogen13_search"
                                                           placeholder=""></div>
                                                <ul class="select2-results" role="listbox"
                                                    id="select2-results-13"></ul>
                                            </div>
                                        </div>
                                        <select id="price" name="price" class="styled select2-offscreen"
                                                tabindex="-1" title="">
                                            <option value="">Chọn giá</option>
                                            <option value="nho-hon-500-trieu"> &lt; 500 triệu</option>
                                            <option value="800-trieu-den-1-ti">800 - 1 tỉ</option>
                                            <option value="1-den-3-ti">1-3 tỉ</option>
                                            <option value="4-den-5-ti">4-5 tỉ</option>
                                            <option value="6-den-9-ti">6-9 tỉ</option>
                                            <option value="lon-hon-10-ti"> &gt;10 tỉ</option>
                                        </select>
                                    </li>
                                    <li>
                                        <div class="select2-container styled" id="s2id_dientich"><a
                                                    href="javascript:void(0)" class="select2-choice" tabindex="-1">
                                                <span class="select2-chosen"
                                                      id="select2-chosen-14"> Chọn diện tích </span><abbr
                                                        class="select2-search-choice-close"></abbr> <span
                                                        class="select2-arrow" role="presentation"><b
                                                            role="presentation"></b></span></a><label for="s2id_autogen14"
                                                                                                      class="select2-offscreen"></label><input
                                                    class="select2-focusser select2-offscreen" type="text"
                                                    aria-haspopup="true" role="button"
                                                    aria-labelledby="select2-chosen-14" id="s2id_autogen14"
                                                    tabindex="0">
                                            <div class="select2-drop select2-display-none select2-with-searchbox">
                                                <div class="select2-search"><label for="s2id_autogen14_search"
                                                                                   class="select2-offscreen"></label>
                                                    <input type="text" autocomplete="off" autocorrect="off"
                                                           autocapitalize="off" spellcheck="false"
                                                           class="select2-input" role="combobox"
                                                           aria-expanded="true" aria-autocomplete="list"
                                                           aria-owns="select2-results-14" id="s2id_autogen14_search"
                                                           placeholder=""></div>
                                                <ul class="select2-results" role="listbox"
                                                    id="select2-results-14"></ul>
                                            </div>
                                        </div>
                                        <select name="dientich" id="dientich" class="styled select2-offscreen"
                                                tabindex="-1" title="">
                                            <option value=""> Chọn diện tích</option>
                                            <option value="nho-hon-30-m2"> &lt; 30 m2</option>
                                            <option value="30-50-m2"> 30 - 50 m2</option>
                                            <option value="50-80-m2"> 50 - 80 m2</option>
                                            <option value="80-100-m2"> 80 - 100 m2</option>
                                            <option value="100-150-m2"> 100 - 150 m2</option>
                                            <option value="150-200-m2"> 150 - 200 m2</option>
                                            <option value="200-250-m2"> 200 - 250 m2</option>
                                            <option value="250-300-m2"> 250 - 300 m2</option>
                                            <option value="lon-hon-300m2"> &gt; 300 m2</option>
                                        </select>
                                    </li>
                                </ul>

                                <div style="text-align: right; padding-top: 7px;">
                                    <input type="hidden" name="guitin" value="guitin">
                                    <input id="timbds" name="timbds" class="btn_Search" type="submit"
                                           value="Tìm kiếm">
                                </div>
                            </form>
                        </div>

                    </div><!-- End .m_sb -->
                </div>
                <div class="block_sb" id="street_t">
                    <h4 class="t_sb">
        <span>
			<a title=" Nhà đất TP HCM " href="http://www.bdstphcm.org/ho-chi-minh.html">
			Nhà đất TP HCM
		   </a>
        </span>
                    </h4><!-- End .t_sb -->
                    <div class="m_sb">
                        <div id="content_ndb" class="f_rv mCustomScrollbar _mCS_1">
                            <div class="mCustomScrollBox mCS-dark-thin" id="mCSB_1"
                                 style="position:relative; height:100%; overflow:hidden; max-width:100%;">
                                <div class="mCSB_container mCS_no_scrollbar" style="position:relative; top:0;">
                                    <ul>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Bình Chánh"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-binh-chanh.html"> Huyện Bình Chánh</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Cần Giờ"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-can-gio.html"> Huyện Cần Giờ</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Củ Chi"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-cu-chi.html"> Huyện Củ Chi</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Hóc Môn"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-hoc-mon.html"> Huyện Hóc Môn</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Nhà Bè"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-nha-be.html"> Huyện Nhà Bè</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 1"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-1.html"> Quận 1</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 10" href="http://www.bdstphcm.org/ho-chi-minh/quan-10.html"> Quận 10</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 11" href="http://www.bdstphcm.org/ho-chi-minh/quan-11.html"> Quận 11</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 12" href="http://www.bdstphcm.org/ho-chi-minh/quan-12.html"> Quận 12</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 2"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-2.html"> Quận 2</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 3"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-3.html"> Quận 3</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 4"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-4.html"> Quận 4</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 5"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-5.html"> Quận 5</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 6"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-6.html"> Quận 6</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 7"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-7.html"> Quận 7</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 8"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-8.html"> Quận 8</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 9"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-9.html"> Quận 9</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Bình Tân"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-binh-tan.html"> Quận Bình Tân</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Bình Thạnh"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-binh-thanh.html"> Quận Bình Thạnh</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Gò Vấp"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-go-vap.html"> Quận Gò Vấp</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Phú Nhuận"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-phu-nhuan.html"> Quận Phú Nhuận</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Tân Bình"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-tan-binh.html"> Quận Tân Bình</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Tân Phú"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-tan-phu.html"> Quận Tân Phú</a>
                    </span>
                                        </li>
                                        <li class="clearfix">
                                            <span class="icon_rv"></span>
                                            <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Thủ Đức"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-thu-duc.html"> Quận Thủ Đức</a>
                    </span>
                                        </li>

                                    </ul>
                                </div>
                                <div class="mCSB_scrollTools" style="position: absolute; display: none;">
                                    <div class="mCSB_draggerContainer">
                                        <div class="mCSB_dragger" style="position: absolute; top: 0px;"
                                             oncontextmenu="return false;">
                                            <div class="mCSB_dragger_bar" style="position:relative;"></div>
                                        </div>
                                        <div class="mCSB_draggerRail"></div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- End .f_rv -->

                    </div><!-- End .m_sb -->
                </div>


                <!-- <div class="block_sb">
<h4 class="t_sb">
    <span>
        Video giới thiệu
    </span>
</h4>
<div class="m_sb">

    <div class="f_video">


    <object width="100%" height="170">
        <param name="movie" value="http://www.youtube.com/v/vURcezzpM2Y?hl=vi_VN&amp;version=3&amp;autoplay=0"></param>
        <param name="allowFullScreen" value="true"></param>
        <param name="allowscriptaccess" value="always"></param>
        <embed src="http://www.youtube.com/v/XaN41AgHB2Q?hl=vi_VN&amp;version=3&amp;autoplay=0" type="application/x-shockwave-flash" width="100%" height="170" allowscriptaccess="always" allowfullscreen="true"></embed>
    </object>

    </div>

</div>
</div> -->


                <div class="block_sb" id="quangcao_t">
                    <h4 class="t_sb">
            <span>
                Quảng cáo
            </span>
                    </h4><!-- End .t_sb -->
                    <div class="m_sb">

                        <div class="f_adv">

                            <ul>
                                <li>
                                    <img src="http://www.bdstphcm.org/images/bds_adv/sliders6.jpg"
                                         alt="Bản Đồ Quy Hoạch Định Hướng Phát Triển không Gian 2010 - 2020">
                                    <a href="https://www.youtube.com/watch?v=hwsgvaBd0ZY&amp;t=17s"
                                       target="_blank"></a>
                                </li>
                                <li>
                                    <img src="http://www.bdstphcm.org/images/bds_adv/sliders4.gif"
                                         alt="Cho vay tiêu dùng không thuế chấp">
                                    <a href="#" target="_blank"></a>
                                </li>
                                <li>
                                    <img src="http://www.bdstphcm.org/images/bds_adv/sliders3.png" alt="Samsung">
                                    <a href="" target="_blank"></a>
                                </li>
                                <li>
                                    <img src="http://www.bdstphcm.org/images/bds_adv/sliders1.png"
                                         alt="asd asdasas">
                                    <a href="#" target="_blank"></a>
                                </li>
                            </ul>

                        </div><!-- End .f_adv -->

                    </div><!-- End .m_sb -->
                </div><!-- End .block_sb -->

                <div class="block_sb">
                    <h4 class="t_sb">

                    </h4><!-- End .t_sb -->
                    <div class="m_sb">
                        <!-- End .m_sb -->
                    </div><!-- End .block_sb -->

                </div><!-- End .sidebar -->
            </div><!-- End .f_cont -->

        </div><!-- End .min_wrap -->
    </div><!-- End #container -->

    @stop