@extends('frontend.layouts.master')
@section('title')
    danh mục bài viết
@endsection
@section('main_content')

    <div class="min_wrap">


        <div class="breacrum">
            <div id="breacrum" xmlns:v="http://rdf.data-vocabulary.org/#">
        <span typeof="v:Breadcrumb">
            <a rel="v:url" property="v:title" href="http://www.bdstphcm.org/" title="Trang chủ"> Trang chủ </a>
        </span>
                <span typeof="v:Breadcrumb">&raquo;
                    {{$PostCategory->name}}         </span>
            </div>
        </div>

        <div class="f_cont clearfix">

            <article class="content">

                <div class="f_prod">
                    <div class="block_prod">


                        <div class="m_prod">
                            <ul>
                                <?php
                                $getPost = DB::table('posts')->where('category_id', $PostCategory->id)->where('status', 1)->orderBy('created_at', 'asc')->paginate(4);
                                ?>
                                @foreach($getPost as $item)
                                    <li>
                                        <div class="li_prod clearfix">
                                <span class="lp_1">
                                    <a href="{{action('ControllerBatdongsan\IndexController@getArticleDetail',$item->id)}}" title="{{$item->name}} ">
                                        <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds/cho-thue-biet-thu-10x20m-gan-bien-Vung-Tau-10-trieu-thang-1-o0rBK.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds/cho-thue-biet-thu-10x20m-gan-bien-Vung-Tau-10-trieu-thang-1-o0rBK.jpg"
                                                                                 alt="{{$item->name}}">
									</a>
                                </span><!-- End .lp_1 -->
                                            <span class="lp_2">
                                    <h4>
                                        <a href="{{action('ControllerBatdongsan\IndexController@getArticleDetail',$item->id)}}"
                                           title="{{$item->name}}">
                                            {{$item->name}} {{$item->id}}                                     </a>
                                    </h4>
                                    <span class="vt_lp">Vị trí: {{$item->address}}</span>

                                </span><!-- End .lp_2 -->
                                        </div><!-- End .li_prod -->
                                    </li>

                                @endforeach

                            </ul>

                            <div class="clear"></div>
                        </div><!-- End .m_prod -->
                    </div><!-- End .block_prod -->

                </div>

                <div class="row">
                    <div class="col-lg-12">
                        {{$getPost->links()}}
                    </div>
                </div>


                <div class="page">
                    <div class="PageNum">
                    </div>
                    <div class="clear"></div>
                </div>


            </article><!-- End .content -->

            <div class="sidebar">

                <div class="block_sb">
                    <h4 class="t_sb">
        <span>
           Tìm kiếm nhanh
        </span>
                    </h4><!-- End .t_sb -->
                    <div class="m_sb">

                        <div class="slx_ttp">
                            <style type="text/css">
                                .slx_ttp {
                                    background: #FCFCFC;
                                    padding: 10px;
                                }

                                .slx_ttp span.customSelect {
                                    background-color: #fff !important;
                                }

                                .styled {
                                    background: none repeat scroll 0 0 #f9f9f9;
                                    border: 1px solid #ddd;
                                    padding: 2px;
                                    margin: 2px 0 2px 0;
                                }
                            </style>

                            <script>
                                $(document).ready(function () {
                                    $("#tinh").select2();
                                    $("#huyen").select2();
                                    $("#phuong").select2();
                                    $("#duong").select2();
                                    $("#loai").select2();
                                    $("#ddCat").select2();
                                    $("#tinh").select2();
                                    $("#huyen").select2();
                                    $("#phuong").select2();
                                    $("#duong").select2();
                                    $("#loai").select2();
                                    $("#ddCat").select2();
                                    $("#price").select2();
                                    $("#dientich").select2();
                                    $("#tinh").change(function () {
                                        var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                        var table = "tbl_quanhuyen";
                                        var tablep = "tbl_quanhuyen_category";
                                        $("#huyen").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                    });
                                    $("#huyen").change(function () {
                                        var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                        var table = "tbl_street";
                                        var tablep = "tbl_quanhuyen";
                                        $("#duong").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                    });
                                    $("#huyen").change(function () {
                                        var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                        var table = "tbl_phuongxa";
                                        var tablep = "tbl_quanhuyen";
                                        $("#phuong").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                    });
                                    $("#loai").change(function () {
                                        var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                        var table = "tbl_rv_category";
                                        var tablep = "tbl_rv_category";
                                        $("#ddCat").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                    });
                                });
                            </script>
                            <form action="http://www.bdstphcm.org/tim-bds.html" method="POST"
                                  enctype="multipart/form-data">
                                <ul>
                                    <li>
                                        <select name="tinh" id="tinh" class="styled">
                                            <option value=""> Chọn thành phố</option>

                                            <option value="ho-chi-minh">Hồ Chí Minh</option>
                                            <option value="ha-noi">Hà Nội</option>
                                            <option value="binh-duong">Bình Dương</option>
                                            <option value="hai-phong">Hải Phòng</option>
                                            <option value="da-nang">Đà Nẵng</option>
                                            <option value="ben-tre">Bến Tre</option>
                                            <option value="can-tho">Cần Thơ</option>
                                            <option value="dong-nai">Đồng Nai</option>
                                            <option value="ba-ria-vung-tau">Bà Rịa Vũng Tàu</option>
                                            <option value="khanh-hoa">Khánh Hòa</option>
                                            <option value="long-an">Long An</option>
                                            <option value="thua-thien-hue">Thừa Thiên Huế</option>
                                            <option value="an-giang">An Giang</option>
                                            <option value="bac-lieu">Bạc Liêu</option>
                                            <option value="bac-can">Bắc Cạn</option>
                                            <option value="bac-ninh">Bắc Ninh</option>
                                            <option value="bac-giang">Bắc Giang</option>
                                            <option value="binh-dinh">Bình Định</option>
                                            <option value="binh-phuoc">Bình Phước</option>
                                            <option value="binh-thuan">Bình Thuận</option>
                                            <option value="ca-mau">Cà Mau</option>
                                            <option value="cao-bang">Cao Bằng</option>
                                            <option value="dak-lak">Đắk Lắk</option>
                                            <option value="dong-thap">Đồng Tháp</option>
                                            <option value="gia-lai">Gia Lai</option>
                                            <option value="ha-giang">Hà Giang</option>
                                            <option value="ha-nam">Hà Nam</option>
                                            <option value="ha-tinh">Hà Tĩnh</option>
                                            <option value="hai-duong">Hải Dương</option>
                                            <option value="hoa-binh">Hòa Bình</option>
                                            <option value="hung-yen">Hưng Yên</option>
                                            <option value="kien-giang">Kiên Giang</option>
                                            <option value="kon-tum">Kon Tum</option>
                                            <option value="lai-chau">Lai Châu</option>
                                            <option value="lang-son">Lạng Sơn</option>
                                            <option value="lao-cai">Lào Cai</option>
                                            <option value="lam-dong">Lâm Đồng</option>
                                            <option value="nam-dinh">Nam Định</option>
                                            <option value="nghe-an">Nghệ An</option>
                                            <option value="ninh-binh">Ninh Bình</option>
                                            <option value="ninh-thuan">Ninh Thuận</option>
                                            <option value="phu-tho">Phú Thọ</option>
                                            <option value="phu-yen">Phú Yên</option>
                                            <option value="quang-binh">Quảng Bình</option>
                                            <option value="quang-nam">Quảng Nam</option>
                                            <option value="quang-ngai">Quảng Ngãi</option>
                                            <option value="quang-ninh">Quảng Ninh</option>
                                            <option value="quang-tri">Quảng Trị</option>
                                            <option value="soc-trang">Sóc Trăng</option>
                                            <option value="son-la">Sơn La</option>
                                            <option value="tay-ninh">Tây Ninh</option>
                                            <option value="thai-binh">Thái Bình</option>
                                            <option value="thai-nguyen">Thái Nguyên</option>
                                            <option value="thanh-hoa">Thanh Hóa</option>
                                            <option value="tien-giang">Tiền Giang</option>
                                            <option value="tra-vinh">Trà Vinh</option>
                                            <option value="tuyen-quang">Tuyên Quang</option>
                                            <option value="vinh-long">Vĩnh Long</option>
                                            <option value="vinh-phuc">Vĩnh Phúc</option>
                                            <option value="yen-bai">Yên Bái</option>
                                            <option value="dien-bien">Điện Biên</option>
                                            <option value="dak-nong">Đắk Nông</option>
                                            <option value="hau-giang">Hậu Giang</option>

                                        </select>
                                    </li>
                                    <li>
                                        <select name="huyen" id="huyen" class="styled">

                                            <option value=""> Chọn Quận / Huyện</option>
                                        </select>
                                    </li>
                                    <li>
                                        <select name="phuong" id="phuong" class="styled">

                                            <option value="">Chọn Phường / Xã</option>
                                        </select>
                                    </li>
                                    <li>
                                        <select name="duong" id="duong" class="styled">

                                            <option value="">Chọn đường</option>
                                        </select>
                                    </li>
                                    <li>
                                        <select name="loai" id="loai" class="styled">

                                            <option value="">Chọn nhu cầu</option>
                                            <option value="nha-dat-ban"> Nhà đất bán</option>
                                            <option value="nha-dat-cho-thue" selected="selected"> Nhà đất cho thuê
                                            </option>

                                        </select>
                                    </li>
                                    <li>
                                        <select name="ddCat" id="ddCat" class="styled">

                                            <option value="">Chọn hình thức</option>
                                            <option value="cho-thue-dat"> Cho thuê đất</option>
                                            <option value="cho-thue-nha-hang-khach-san"> Cho thuê nhà hàng, khách
                                                sạn
                                            </option>
                                            <option value="cho-thue-nha-tro-phong-tro"> Cho thuê nhà trọ, phòng
                                                trọ
                                            </option>
                                            <option value="cho-thue-cua-hang-ki-ot"> Cho thuê cửa hàng, ki ốt
                                            </option>
                                            <option value="cho-thue-can-ho-chung-cu"> Cho thuê căn hộ chung cư
                                            </option>
                                            <option value="cho-thue-nha-rieng" selected="selected"> Cho thuê nhà
                                                riêng
                                            </option>
                                            <option value="cho-thue-nha-mat-pho"> Cho thuê nhà mặt phố</option>
                                            <option value="cho-thue-van-phong"> Cho thuê văn phòng</option>
                                        </select>
                                    </li>
                                    <li>
                                        <select id="price" name="price" class="styled">
                                            <option value="">Chọn giá</option>
                                            <option value="nho-hon-500-trieu"> < 500 triệu</option>
                                            <option value="800-trieu-den-1-ti">800 - 1 tỉ</option>
                                            <option value="1-den-3-ti">1-3 tỉ</option>
                                            <option value="4-den-5-ti">4-5 tỉ</option>
                                            <option value="6-den-9-ti">6-9 tỉ</option>
                                            <option value="lon-hon-10-ti"> >10 tỉ</option>
                                        </select>
                                    </li>
                                    <li>
                                        <select name="dientich" id="dientich" class="styled">
                                            <option value=""> Chọn diện tích</option>
                                            <option value="nho-hon-30-m2"> < 30 m2</option>
                                            <option value="30-50-m2"> 30 - 50 m2</option>
                                            <option value="50-80-m2"> 50 - 80 m2</option>
                                            <option value="80-100-m2"> 80 - 100 m2</option>
                                            <option value="100-150-m2"> 100 - 150 m2</option>
                                            <option value="150-200-m2"> 150 - 200 m2</option>
                                            <option value="200-250-m2"> 200 - 250 m2</option>
                                            <option value="250-300-m2"> 250 - 300 m2</option>
                                            <option value="lon-hon-300m2"> > 300 m2</option>
                                        </select>
                                    </li>
                                </ul>

                                <div style="text-align: right; padding-top: 7px;">
                                    <input type="hidden" name="guitin" value="guitin"/>
                                    <input id="timbds" name="timbds" class="btn_Search" type="submit"
                                           value="Tìm kiếm">
                                </div>
                            </form>
                        </div>

                    </div><!-- End .m_sb -->
                </div>
                <div class="block_sb" id="street_t">
                    <h4 class="t_sb">
        <span>
			<a title=" Nhà đất TP HCM " href="http://www.bdstphcm.org/ho-chi-minh.html">
			Nhà đất TP HCM
		   </a>
        </span>
                    </h4><!-- End .t_sb -->
                    <div class="m_sb">
                        <div id="content_ndb" class="f_rv">
                            <ul>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Bình Chánh"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-binh-chanh.html"> Huyện Bình Chánh</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Cần Giờ"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-can-gio.html"> Huyện Cần Giờ</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Củ Chi"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-cu-chi.html"> Huyện Củ Chi</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Hóc Môn"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-hoc-mon.html"> Huyện Hóc Môn</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Nhà Bè"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-nha-be.html"> Huyện Nhà Bè</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 1"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-1.html"> Quận 1</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 10" href="http://www.bdstphcm.org/ho-chi-minh/quan-10.html"> Quận 10</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 11" href="http://www.bdstphcm.org/ho-chi-minh/quan-11.html"> Quận 11</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 12" href="http://www.bdstphcm.org/ho-chi-minh/quan-12.html"> Quận 12</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 2"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-2.html"> Quận 2</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 3"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-3.html"> Quận 3</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 4"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-4.html"> Quận 4</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 5"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-5.html"> Quận 5</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 6"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-6.html"> Quận 6</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 7"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-7.html"> Quận 7</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 8"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-8.html"> Quận 8</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 9"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-9.html"> Quận 9</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Bình Tân"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-binh-tan.html"> Quận Bình Tân</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Bình Thạnh"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-binh-thanh.html"> Quận Bình Thạnh</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Gò Vấp"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-go-vap.html"> Quận Gò Vấp</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Phú Nhuận"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-phu-nhuan.html"> Quận Phú Nhuận</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Tân Bình"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-tan-binh.html"> Quận Tân Bình</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Tân Phú"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-tan-phu.html"> Quận Tân Phú</a>
                    </span>
                                </li>
                                <li class="clearfix">
                                    <span class="icon_rv"></span>
                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Thủ Đức"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-thu-duc.html"> Quận Thủ Đức</a>
                    </span>
                                </li>

                            </ul>
                        </div><!-- End .f_rv -->

                    </div><!-- End .m_sb -->
                </div>


                <!-- <div class="block_sb">
                <h4 class="t_sb">
                    <span>
                        Video giới thiệu
                    </span>
                </h4>
                <div class="m_sb">

                    <div class="f_video">


                    <object width="100%" height="170">
                        <param name="movie" value="http://www.youtube.com/v/vURcezzpM2Y?hl=vi_VN&amp;version=3&amp;autoplay=0"></param>
                        <param name="allowFullScreen" value="true"></param>
                        <param name="allowscriptaccess" value="always"></param>
                        <embed src="http://www.youtube.com/v/XaN41AgHB2Q?hl=vi_VN&amp;version=3&amp;autoplay=0" type="application/x-shockwave-flash" width="100%" height="170" allowscriptaccess="always" allowfullscreen="true"></embed>
                    </object>

                    </div>

                </div>
            </div> -->


                <div class="block_sb" id="quangcao_t">
                    <h4 class="t_sb">
            <span>
                Quảng cáo
            </span>
                    </h4><!-- End .t_sb -->
                    <div class="m_sb">

                        <div class="f_adv">

                            <ul>
                                <li>
                                    <img src="http://www.bdstphcm.org/images/bds_adv/sliders6.jpg"
                                         alt="Bản Đồ Quy Hoạch Định Hướng Phát Triển không Gian 2010 - 2020">
                                    <a href="https://www.youtube.com/watch?v=hwsgvaBd0ZY&t=17s" target="_blank"></a>
                                </li>
                                <li>
                                    <img src="http://www.bdstphcm.org/images/bds_adv/sliders4.gif"
                                         alt="Cho vay tiêu dùng không thuế chấp">
                                    <a href="#" target="_blank"></a>
                                </li>
                                <li>
                                    <img src="http://www.bdstphcm.org/images/bds_adv/sliders3.png" alt="Samsung">
                                    <a href="" target="_blank"></a>
                                </li>
                                <li>
                                    <img src="http://www.bdstphcm.org/images/bds_adv/sliders1.png"
                                         alt="asd asdasas">
                                    <a href="#" target="_blank"></a>
                                </li>
                            </ul>

                        </div><!-- End .f_adv -->

                    </div><!-- End .m_sb -->
                </div><!-- End .block_sb -->

                <div class="block_sb">
                    <h4 class="t_sb">

                    </h4><!-- End .t_sb -->
                    <div class="m_sb">
                        <!-- End .m_sb -->
                    </div><!-- End .block_sb -->

                </div><!-- End .sidebar -->
            </div><!-- End .f_cont -->

        </div><!-- End .min_wrap -->
    </div><!-- End #container -->

@stop