<title>@if(isset($pageOption)){{ isset($pageOption['title']) && $pageOption['title'] != '' ? $pageOption['title'] : $pageOption['pageName'] }}@else{!! @$settings['name'] !!}@endif</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="@if(isset($pageOption)){{ isset($pageOption['title']) && $pageOption['title'] != '' ? $pageOption['title'] : $pageOption['pageName'] }}@else{!! @$settings['name'] !!}@endif"
      name="title">
<meta content="@if(isset($pageOption)){{ isset($pageOption['keywords']) && $pageOption['keywords'] != '' ? $pageOption['keywords'] : $pageOption['pageName'] }}@else{!!@$settings['default_meta_keywords'] !!}@endif"
      name="keywords">
<meta content="@if(isset($pageOption)){!! isset($pageOption['description']) && $pageOption['description'] != '' ? $pageOption['description'] : $pageOption['pageName'] !!}@else{!!@$settings['default_meta_description'] !!}@endif"
      name="description">
<meta content="{{@$settings['robots'] }}" name="robots">
<link rel="shortcut icon" href="{{ URL::asset('public/filemanager/userfiles/' .@$settings['favicon']) }}" type="image/x-icon">
<link rel="alternate" hreflang="vi" href="{{ URL::to('/') }}" />
<meta property="og:title" content="@if(isset($pageOption)){{ isset($pageOption['title']) && $pageOption['title'] != '' ? $pageOption['title'] : $pageOption['pageName'] }}@else{!!@$settings['name'] !!}@endif" />
<meta property="og:description" content="@if(isset($pageOption)){!! isset($pageOption['description']) && $pageOption['description'] != '' ? $pageOption['description'] : $pageOption['pageName'] !!}@else{!!@$settings['default_meta_description'] !!}@endif" />
<meta property="og:image" content="@if(isset($pageOption['image'])){{ $pageOption['image'] }}@else{{ URL::asset('public/filemanager/userfiles/' .@$settings['logo']) }}@endif" />
<meta property="og:url" content="@if(isset($pageOption['link'])){{ $pageOption['link'] }}@else{{ URL::to('/') }}@endif" />
<link rel="canonical" href="{{ URL::to('/') }}"/>
<meta property="og:type" content="website" />
<meta property="og:site_name" content="{{ URL::to('/') }}" />
    <link rel="stylesheet" type="text/css" href=" {{URL::to('public/frontend/assets/frontend/css/common.css?v=7') }}">
    <script src="{{ asset('public/frontend/js/jquery.min.js') }}"></script>
    <script src="{{ asset('public/frontend/js/custom.js') }}"></script>
    <script src="{{ asset('public/frontend/js/common.js') }}"></script>
    <link rel="stylesheet" type="text/css" href=" {{URL::to('public/frontend/css/custom.css')}}">
