<div class="map-link hidden-xs cbreadcrumb">
    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a class="" href="/" itemprop="item"><span
                    itemprop="name">Trang chủ</span></a>
    </li>
    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        &gt; <a href="{{ URL::to(@$pageOption['parentUrl']) }}" itemprop="item"><span
                    itemprop="name">{{ @$pageOption['pageName'] }}</span></a>
    </li>
    </ul>
</div>