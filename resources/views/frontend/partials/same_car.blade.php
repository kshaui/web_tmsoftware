@php
    $data = CommonHelper::getFromCache('product_by_id_manufacturer_id_manufacturer_model_id_dangban');
     if (!$data) {
         $data = \App\Models\Product::where('id','<>',$product->id)->where('manufacturer_id',@$Manufacturer->id)
         ->where('manufacturer_model_id',@$Manufacturer_model->id)->whereIn('pending', ['Đang bán'])->where('status', 1)->get();
         CommonHelper::putToCache('product_by_id_manufacturer_id_manufacturer_model_id_dangban', $data);
     }
@endphp
@if(count($data)>0)
    <div class="title-underline" style="overflow: hidden; text-overflow: ellipsis;">Tương tự
        <span class="hidden-xs">{{ @$product->name }}</span></div>
    <table class="table list-news">
        <tbody>
        @include('frontend.childs.product.show_item')
        </tbody>
    </table>
@endif