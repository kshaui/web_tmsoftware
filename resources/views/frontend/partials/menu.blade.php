<div class="header-menu">
    <div class="container p0">
        <nav>
            <ul class='nav'>
                @php
                    $data = CommonHelper::getFromCache('menu_parent_by_location_main_menu');
                    if (!$data) {
                        $data = \App\Models\Menu::select(['id', 'url', 'name'])->where('location', 'main_menu')->where('parent_id', null)->orderBy('order_no', 'desc')->limit(10)->get();
                        CommonHelper::putToCache('menu_parent_by_location_main_menu', $data);
                    }
                @endphp
                @foreach($data as $menu)
                    <li @if($_SERVER['REQUEST_URI'] == '/' . $menu->url) class="active" @endif>
                        <a href="{{ URL::to($menu->url) }}" title="{{ $menu->name }}">
                            <span class="line_menu_1">
                                <span class="line_menu_2">
                                    {{ $menu->name }}
                                </span>
                            </span>
                        </a>
                        @php
                            $menuChilds = CommonHelper::getFromCache('get_menu_childs_by_menu_id' . $menu->id);
                            if (!$menuChilds) {
                                $menuChilds = $menu->childs;
                                CommonHelper::putToCache('get_menu_childs_by_menu_id' . $menu->id, $menuChilds);
                            }
                        @endphp

                        @if($menuChilds != false && count($menuChilds) > 0)
                            <ul class="menu_child">
                                @foreach($menuChilds as $menuChild)
                                    <li class="active">
                                        <a href="{{ URL::to($menuChild->url) }}" style="color: #000000"
                                           title="{{ $menuChild->name }}">{{ $menuChild->name }}</a>
                                        @php
                                            $menuChild2s = CommonHelper::getFromCache('get_menu_childs2_by_menu_id' . $menuChild->id);
                                            if (!$menuChild2s) {
                                                $menuChild2s = $menuChild->childs;
                                                CommonHelper::putToCache('get_menu_childs2_by_menu_id' . $menuChild->id, $menuChild2s);
                                            }
                                        @endphp
                                        @if($menuChild2s != false && count($menuChild2s) > 0)
                                            <ul class="menu_child">
                                                @foreach($menuChild2s as $menuChild2)
                                                    <li class="active" style="background-color: #a3d0ef">
                                                        <a href="{{ URL::to($menuChild2->url) }}"
                                                           style="color: #000000"
                                                           title="{{ $menuChild2->name }}">{{ $menuChild2->name }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
        </nav>
    </div>
</div>