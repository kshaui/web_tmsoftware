<footer id="site-footer" role="contentinfo">
    <!--start copyrights-->
    <div class="copyrights">
        <div class="container">
            <div class="row" id="copyright-note">
                <span><a href="{{ URL::to('/blog') }}"><img src="{{CommonHelper::getUrlImageThumb(@$settings['logo'], 175, 60) }}"></a></span>
                <div class="top">
                    <a href="#top" class="toplink"> Lên đầu trang </a>
                </div>
            </div>
        </div>
    </div>
    <!--end copyrights-->
</footer><!-- #site-footer -->