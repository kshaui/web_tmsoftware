<div class="block_sb" id="quangcao_t">
    <h4 class="t_sb">
        <span>
            Quảng cáo
        </span>
    </h4><!-- End .t_sb -->
    <div class="m_sb">
        <div class="f_adv">
            @php
                $data = CommonHelper::getFromCache('banner_home_banner');
                if (!$data) {
                    $data = \App\Models\Banner::select(['id', 'name', 'image', 'link'])->where('location', 'home_banner')->where('status', 1)->get();
                    CommonHelper::putToCache('banner_home_banner', $data);
                }
            @endphp
            <ul>
                @foreach($data as $item)
                    <li style="list-style: none; margin-top: 5px">
                        <a href="{{ $item->link != '' ? $item->link : '#' }}"
                           target="_blank" title="{{ $item->name }}"/>
                        <img src="{{ CommonHelper::getUrlImageThumb($item->image, 270, '') }}"
                             alt="{{$item->name}}" title="{{ $item->name }}">
                    </li>
                @endforeach
            </ul>
        </div><!-- End .f_adv -->
    </div><!-- End .m_sb -->
</div>
