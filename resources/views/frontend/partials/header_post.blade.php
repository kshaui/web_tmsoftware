<header id="site-header" role="banner">
    <div class="container clear">
        <div class="site-branding">
            <h2 id="logo" class="site-title" itemprop="headline">
                <a href="{{URL::to('/')}}"><img
                            class=""
                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo'], 175, 60) }}"
                            width="175"
                            height="118"></a>
            </h2>
        </div>
    </div>
    <div class="primary-navigation">
        <a href="#" id="pull" class="toggle-mobile-menu">Menu</a>
        <div class="container clear">
            @php
                $data = CommonHelper::getFromCache('all_category');
                 if (!$data) {
                     $data = \App\Models\Category::select(['name', 'slug'])->where('status',1)
                     ->orderBy('order_no', 'asc')->orderBy('id', 'asc')->get();
                     CommonHelper::putToCache('all_category', $data);
                 }
            @endphp
            <nav id="navigation" class="primary-navigation mobile-menu-wrapper" role="navigation">
                <ul class="menu clearfix toggle-menu">
                    @foreach($data as $item)
                        <li class="cat-item cat-item-3">
                            <a href="{{ URL::to('danh-muc/'. $item->slug) }}"
                               title="{{ $item->intro }}">
                                {{ $item->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
                <div id="mobile-menu-overlay" style="display: none;"></div>
            </nav>
        </div>
        <div id="mobile-menu-overlay"></div>
    </div>
</header>