<div class="quick-filter mt15 hidden-xs">
    @php
        $data = CommonHelper::getFromCache('users_show_sidebar_homepage_limit_8');
         if (!$data) {
             $data = \App\Models\User::select(['id','name', 'image', 'tel','address'])->where('status', 1)->where('show_sidebar_homepage',1)->limit(8)->get();
             CommonHelper::putToCache('users_show_sidebar_homepage_limit_8', $data);
         }
    @endphp
    <div class="title-bg">Salon nổi bật</div>
    <div class="list-loai-xe p15mb">
        @foreach($data as $item)
            <div class="row m0 pt15">
                <a class="list-moi-gioi" href="{{ URL::to('tim-kiem/?user_id=' . $item->id) }}">
                    <div class="avatar">
                        <img class=""
                             src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, 75, 75) }}"
                             alt="{{ $item->name }}" width="75" height="75">
                    </div>
                    <div class="infor-moi-gioi">
                        <div class="name-moi-gioi"
                             style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">{{ $item->name }}</div>
                        <p class="m0" style="line-height: 10px; font-size: 12px;">
                            <i class="icon-sprite-24 icon-address"></i> {{ $item->address }}
                        </p>
                        <p class="m0" style="line-height: 10px; font-size: 12px;"><i
                                    class="icon-sprite-24 icon-tel"></i>{{ $item->tel }}</p>
                    </div>
                </a>
            </div>
        @endforeach
        <div class="row m0" style="padding-top: 15px;">
            <div class="txc">
                <a class="txt-blue" href="{{ URL::to('/danh-sach-salon-noi-bat') }}">Xem tất cả »</a>
            </div>
        </div>
    </div>
</div>


<div class="quick-filter mt15 hidden-xs">
    <div class="title-bg"><a href="{{ URL::to('/blog') }}" style="color: white;">Blogs</a>
    </div>
    @php
        $data = CommonHelper::getFromCache('posts_show_homepage_1');
         if (!$data) {
             $data = \App\Models\Post::select(['image','slug','name'])->where('status', 1)->where('show_homepage',1)->get();
             CommonHelper::putToCache('posts_show_homepage_1', $data);
         }
    @endphp
    <div class="list-loai-xe p15mb">
        @foreach($data as $item)
            <div class="row m0 pt15">
                <a class="list-moi-gioi" href="{{ URL::to('blog/'.@$item->slug) }}" target="_blank">
                    <div class="avatar">
                        <img class=""
                             src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, 75, 80) }}"
                             height="70px" width="75">
                    </div>
                    <div class="infor-moi-gioi">
                        <div class="name-moi-gioi"
                             style="overflow: hidden; text-overflow: ellipsis;font-weight: 500;">
                            {{ $item->name }}
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
            <div class="row m0"  style="padding-top:59px">
                <div class="txc">
                    <a class="txt-blue" href="{{ URL::to('/blog') }}">Xem tất cả »</a>
                </div>
            </div>
    </div>
</div>





