@extends('frontend.layouts.master')
@section('title')
    Danh sách tin tức bán đất
@endsection
@section('main_content')




    <div id="wrapper">


        <div id="container">
            <div class="min_wrap">

                <div class="breacrum">
                    <div id="breacrum" xmlns:v="http://rdf.data-vocabulary.org/#">
         <span typeof="v:Breadcrumb">
            <a href="http://www.bdstphcm.org/" rel="v:url" property="v:title" title="Trang chủ"> Trang chủ </a>
        </span>
                        <span typeof="v:Breadcrumb"> »
           <a rel="v:url" property="v:title" href= title="Trang chủ">  Tin tức nhà đất  </a>
        </span>
                    </div>
                </div>
                <div class="f_cont clearfix">

                    <article class="content">

                        <div class="f_news">
                            <ul>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/khach-hang-bat-dau-quan-tam-den-nhung-can-ho-cao-cap.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos724.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos724.jpg"
                                                             alt="khách hàng bắt đầu quan tâm đến những căn hộ cao cấp">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/khach-hang-bat-dau-quan-tam-den-nhung-can-ho-cao-cap.html"
                                title="khách hàng bắt đầu quan tâm đến những căn hộ cao cấp">khách hàng bắt đầu quan tâm đến những căn hộ cao cấp</a>
                        </h4>
                        <span>(2014-09-04 15:31:48)</span>
                        <p>
                            Nếu như nói cách đây khoảng 2 năm trở lại thì thị trường các chung cư có căn hộ cao cấp ít được khác hàng quan tâm thì giờ đây theo một con số mới thống ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/cach-bo-tri-ban-vi-tinh-sao-cho-hop-phong-thuy-nha-o.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos722.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos722.jpg"
                                                             alt="Cách bố trí bàn vi tính sao cho hợp phong thủy nhà ở">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/cach-bo-tri-ban-vi-tinh-sao-cho-hop-phong-thuy-nha-o.html"
                                title="Cách bố trí bàn vi tính sao cho hợp phong thủy nhà ở">Cách bố trí bàn vi tính sao cho hợp phong thủy nhà ở</a>
                        </h4>
                        <span>(2014-09-04 15:01:22)</span>
                        <p>
                            Theo thuật phong thủy thì hầu hết những vật dụng quanh ta đều có một yếu tố may mắn nhất định nào đó, may mắn này có thể đem lại sức khỏe hoặc có thể đem ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/thanh-hoa-chuan-bi-cho-thi-cong-du-an-hang-chuc-trieu-do.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos717.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos717.jpg"
                                                             alt="Thanh Hóa chuẩn bị cho thi công dự án hàng chục triệu đô">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/thanh-hoa-chuan-bi-cho-thi-cong-du-an-hang-chuc-trieu-do.html"
                                title="Thanh Hóa chuẩn bị cho thi công dự án hàng chục triệu đô">Thanh Hóa chuẩn bị cho thi công dự án hàng chục triệu đô</a>
                        </h4>
                        <span>(2014-09-04 14:24:10)</span>
                        <p>
                            Đã từ rất lâu rồi người ta không còn nghe đến Thanh Hóa với các dự án lớn thì giờ đây UBND nơi đây vừa kiểm duyệt và đang bắt đầu cho thi công một dự ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/chi-con-vai-can-ho-tai-starcity-le-van-luong.html" title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos712.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos712.jpg"
                                                             alt="Chỉ còn vài căn hộ tại Starcity Lê Văn Lương">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/chi-con-vai-can-ho-tai-starcity-le-van-luong.html"
                                title="Chỉ còn vài căn hộ tại Starcity Lê Văn Lương">Chỉ còn vài căn hộ tại Starcity Lê Văn Lương</a>
                        </h4>
                        <span>(2014-09-04 13:34:30)</span>
                        <p>
                            Điều này hoàn toàn dễ hiểu khi mà hầu hết hiện nay các căn hộ giá rẻ được người dân săn đón rất là nhiều đặc biệt là ở các khu chung cư.                            </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/meo-nho-giup-su-dung-may-dieu-hoa-nha-o-an-toan.html" title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos689.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos689.jpg"
                                                             alt="Mẹo nhỏ giúp sử dụng máy điều hòa nhà ở an toàn">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/meo-nho-giup-su-dung-may-dieu-hoa-nha-o-an-toan.html"
                                title="Mẹo nhỏ giúp sử dụng máy điều hòa nhà ở an toàn">Mẹo nhỏ giúp sử dụng máy điều hòa nhà ở an toàn</a>
                        </h4>
                        <span>(2014-09-03 15:24:45)</span>
                        <p>
                            Đối với những ngày hè oi bức như hiện nay thì hầu hết đối với những gia đình có máy lạnh đều được mở tối đa công suất để giảm nhiệt.                              </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/khuyen-mai-lon-chua-tung-co-khu-mua-can-ho-tropic-garden.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos687.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos687.jpg"
                                                             alt="Khuyến mãi lớn chưa từng có khu mua căn hộ Tropic Garden">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/khuyen-mai-lon-chua-tung-co-khu-mua-can-ho-tropic-garden.html"
                                title="Khuyến mãi lớn chưa từng có khu mua căn hộ Tropic Garden">Khuyến mãi lớn chưa từng có khu mua căn hộ Tropic Garden</a>
                        </h4>
                        <span>(2014-09-03 14:52:39)</span>
                        <p>
                            Nếu ai thích sự tiện nghi cũng như muốn giành riêng cho mình nơi nghỉ dưỡng lỹ tưởng thì sự lựa chọn đến với căn hộ Tropic Garden là hoàn toàn hợp lý.                            </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/mot-thach-thuc-moi-cho-tttm-lotte-center-ha-noi.html" title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos685.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos685.jpg"
                                                             alt="Một thách thức mới cho TTTM Lotte Center Hà Nội">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/mot-thach-thuc-moi-cho-tttm-lotte-center-ha-noi.html"
                                title="Một thách thức mới cho TTTM Lotte Center Hà Nội">Một thách thức mới cho TTTM Lotte Center Hà Nội</a>
                        </h4>
                        <span>(2014-09-03 14:21:44)</span>
                        <p>
                            Có lẽ trong thời gian vừa qua có quá nhiều trung tâm thương mại rơi vafocanhr ế ẩm nên đã khiến cho nhiều nhà đầu tư không dám mạo hiểm với thị trường này.                           </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/nha-dau-tu-nuoc-ngoai-bo-cuoc-vi-bat-dong-san-viet-nam.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos683.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos683.jpg"
                                                             alt="Nhà đầu tư nước ngoài bỏ cuộc vì bất động sản Việt Nam">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/nha-dau-tu-nuoc-ngoai-bo-cuoc-vi-bat-dong-san-viet-nam.html"
                                title="Nhà đầu tư nước ngoài bỏ cuộc vì bất động sản Việt Nam">Nhà đầu tư nước ngoài bỏ cuộc vì bất động sản Việt Nam</a>
                        </h4>
                        <span>(2014-09-03 13:41:03)</span>
                        <p>
                            Nếu tính về khoảng vài năm trở về trước thì thị trường bất động sản Việt nam vô cùng sôi động vì có sự tham gia của nhiều đại gia bất động sản nước ngoài đầu ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/cac-nha-dau-tu-dang-dot-tien-vao-phu-quoc-ha-long.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos652.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos652.jpg"
                                                             alt="Các nhà đầu tư đang đốt tiền vào Phú Quốc, Hạ Long">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/cac-nha-dau-tu-dang-dot-tien-vao-phu-quoc-ha-long.html"
                                title="Các nhà đầu tư đang đốt tiền vào Phú Quốc, Hạ Long">Các nhà đầu tư đang đốt tiền vào Phú Quốc, Hạ Long</a>
                        </h4>
                        <span>(2014-09-02 12:07:52)</span>
                        <p>
                            Điều này cũng dễ hiểu khi mà vài năm trở lại đây Phú Quốc và Hạ Long là một miếng mồi quá bẻo bở để các đại gia bất động sản không ngần ngại để đổ ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/hyatt-regency-danang-residences-sap-chay-hang-cuoi-thang-9.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos651.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos651.jpg"
                                                             alt="Hyatt Regency Danang Residences sắp cháy hàng cuối tháng 9">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/hyatt-regency-danang-residences-sap-chay-hang-cuoi-thang-9.html"
                                title="Hyatt Regency Danang Residences sắp cháy hàng cuối tháng 9">Hyatt Regency Danang Residences sắp cháy hàng cuối tháng 9</a>
                        </h4>
                        <span>(2014-09-02 11:49:02)</span>
                        <p>
                            Mặc dù giá thành của mỗi căn hộ khá cao nhưng từ khi dự án hoàn thành xong cho đên bây giờ thì Hyatt Regency Danang Residences được nhiều khách hàng trong và ngoài nước qua ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/hang-ty-do-la-von-fdi-danh-cho-bat-dong-san.html" title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos642.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos642.jpg"
                                                             alt="Hàng tỷ đô la vốn FDI dành cho bất động sản">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/hang-ty-do-la-von-fdi-danh-cho-bat-dong-san.html"
                                title="Hàng tỷ đô la vốn FDI dành cho bất động sản">Hàng tỷ đô la vốn FDI dành cho bất động sản</a>
                        </h4>
                        <span>(2014-09-01 12:11:51)</span>
                        <p>
                            Chính vì lợi nhuận liên tục sinh lời nên việc đầu tư vốn FDI vào bất động sản được các nhà đầu tư đầu tư khá mạnh trong vài năm trở lại đây.                            </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/du-kien-vinpearl-resort-se-hoan-thanh-vao-thang-11-toi.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos641.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos641.jpg"
                                                             alt="Dự kiến Vinpearl Resort sẽ hoàn thành vào tháng 11 tới">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/du-kien-vinpearl-resort-se-hoan-thanh-vao-thang-11-toi.html"
                                title="Dự kiến Vinpearl Resort sẽ hoàn thành vào tháng 11 tới">Dự kiến Vinpearl Resort sẽ hoàn thành vào tháng 11 tới</a>
                        </h4>
                        <span>(2014-09-01 12:00:53)</span>
                        <p>
                            Đây là một dự án có lẽ là được mọi người mong chờ nhất trong năm nay, được biết Vinpearl Resort không những hứa hẹn là một khu nghỉ dưỡng lý tưởng mà còn là nơi ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/du-kien-nha-quoc-hoi-hoan-thanh-truoc-thang-9.html" title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos631.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos631.jpg"
                                                             alt="Dự kiến Nhà Quốc hội hoàn thành trước tháng 9">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/du-kien-nha-quoc-hoi-hoan-thanh-truoc-thang-9.html"
                                title="Dự kiến Nhà Quốc hội hoàn thành trước tháng 9">Dự kiến Nhà Quốc hội hoàn thành trước tháng 9</a>
                        </h4>
                        <span>(2014-08-30 11:21:13)</span>
                        <p>
                            Đây là dự án mang tính quốc gia nên từ khi thi công công trình được bảo vệ để tránh các sự cố không cần thiết.                                   </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/gia-phong-khach-san-xuong-the-tham-trong-vai-nam-qua.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos630.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos630.jpg"
                                                             alt="Giá phòng khách sạn xuống thê thảm trong vài năm qua">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/gia-phong-khach-san-xuong-the-tham-trong-vai-nam-qua.html"
                                title="Giá phòng khách sạn xuống thê thảm trong vài năm qua">Giá phòng khách sạn xuống thê thảm trong vài năm qua</a>
                        </h4>
                        <span>(2014-08-30 10:59:49)</span>
                        <p>
                            Một thông tin đáng buồn cho các nhà đầu tư khi quyết định đầu tư vào thị trường khách sạn, theo đánh giá từ các chuyên gia thì trong vòng vài năm trở lại đây giá ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/nha-thiet-ke-nhieu-anh-sang-giup-them-nhieu-sinh-khi.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos611.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos611.jpg"
                                                             alt="Nhà thiết kế nhiều ánh sáng giúp thêm nhiều sinh khí">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/nha-thiet-ke-nhieu-anh-sang-giup-them-nhieu-sinh-khi.html"
                                title="Nhà thiết kế nhiều ánh sáng giúp thêm nhiều sinh khí">Nhà thiết kế nhiều ánh sáng giúp thêm nhiều sinh khí</a>
                        </h4>
                        <span>(2014-08-29 15:18:25)</span>
                        <p>
                            Đối với phong thủy thì việc nhà bạn được thiết kế có nhiều ánh sáng chiếu vào thì sẽ giúp gia đình bạn luôn tràn ngập sinh khí.                                 </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/lai-suat-thap-danh-cho-cac-ho-ngheo-vay-von-xay-nha.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos609.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos609.jpg"
                                                             alt="Lãi suất thấp dành cho các hộ nghèo vay vốn xây nhà">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/lai-suat-thap-danh-cho-cac-ho-ngheo-vay-von-xay-nha.html"
                                title="Lãi suất thấp dành cho các hộ nghèo vay vốn xây nhà">Lãi suất thấp dành cho các hộ nghèo vay vốn xây nhà</a>
                        </h4>
                        <span>(2014-08-29 14:35:58)</span>
                        <p>
                            Đây là cơ hội qua tốt khi mà các ngân sách nhà nước đang có chính sách ưu đãi giành cho các hộ dân đang có ý định vay đất xây nhà, chính sách này được ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/tinh-hinh-bat-dong-san-binh-duong-bat-dau-hut-khach.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos607.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos607.jpg"
                                                             alt="Tình hình bất động sản Bình Dương bắt đầu hút khách">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/tinh-hinh-bat-dong-san-binh-duong-bat-dau-hut-khach.html"
                                title="Tình hình bất động sản Bình Dương bắt đầu hút khách">Tình hình bất động sản Bình Dương bắt đầu hút khách</a>
                        </h4>
                        <span>(2014-08-29 13:59:54)</span>
                        <p>
                            Mặc dù ở các thành phố lớn tình hình bất đống sản trở nên khó khăn hơn bao giờ hết nhưng tại TP Bình Dương thì hoàn toàn ngược lại khi mà thị trường bất động ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/novaland-tiep-tuc-thi-cong-du-an-moi-tai-quan-6.html" title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos604.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos604.jpg"
                                                             alt="Novaland tiếp tục thi công dự án mới tại quận 6">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/novaland-tiep-tuc-thi-cong-du-an-moi-tai-quan-6.html"
                                title="Novaland tiếp tục thi công dự án mới tại quận 6">Novaland tiếp tục thi công dự án mới tại quận 6</a>
                        </h4>
                        <span>(2014-08-29 13:26:23)</span>
                        <p>
                            Trong những năm gần đây liên tiếp là những thành công mà Novaland mang lại cho các dự án bất động sản, với những khu chung cư hay những khu phúc hợp được tập đoàn này ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/suc-khoe-doi-dao-nho-biet-treo-dong-ho-hop-phong-thuy.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos586.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos586.jpg"
                                                             alt="Sức khỏe dồi dào nhờ biết treo đồng hồ hợp phong thủy">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/suc-khoe-doi-dao-nho-biet-treo-dong-ho-hop-phong-thuy.html"
                                title="Sức khỏe dồi dào nhờ biết treo đồng hồ hợp phong thủy">Sức khỏe dồi dào nhờ biết treo đồng hồ hợp phong thủy</a>
                        </h4>
                        <span>(2014-08-28 15:19:48)</span>
                        <p>
                            Nói ra có lẽ rất ít người tin vào chuyện phong thủy, nếu ngẫm nghĩ thì không có lửa thì làm sao có khói, chắc chắn một điều rằng mọi phong tục cũng như lời khuyên ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/chua-tron-2-ngay-da-ban-duoc-600-can-ho-tai-times-city.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos583.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos583.jpg"
                                                             alt="Chưa tròn 2 ngày đã bán được 600 căn hộ tại Times City">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/chua-tron-2-ngay-da-ban-duoc-600-can-ho-tai-times-city.html"
                                title="Chưa tròn 2 ngày đã bán được 600 căn hộ tại Times City">Chưa tròn 2 ngày đã bán được 600 căn hộ tại Times City</a>
                        </h4>
                        <span>(2014-08-28 14:26:18)</span>
                        <p>
                            Có lẽ đây chính là kỷ lục trong giới bất động sản trong nhiều năm qua, được biết chỉ trong vòng 2 ngày cho mở bán căn hộ tại Times City thì đã có tới 600 ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/tra-truoc-30-chi-phi-se-co-ngay-can-ho-the-eastern.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos580.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos580.jpg"
                                                             alt="Trả trước 30% chi phí sẽ có ngay căn hộ The Eastern ">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/tra-truoc-30-chi-phi-se-co-ngay-can-ho-the-eastern.html"
                                title="Trả trước 30% chi phí sẽ có ngay căn hộ The Eastern ">Trả trước 30% chi phí sẽ có ngay căn hộ The Eastern </a>
                        </h4>
                        <span>(2014-08-28 13:47:21)</span>
                        <p>
                            Là một dự án vừa mới hoàn thành xong nên các nhà đầu tư đã mở ra nhiều chính sách ưu đãi nhằm lôi kéo khách hàng.                                  </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/vinacapital-duoc-trao-tang-danh-hieu-cao-quy.html" title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos579.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos579.jpg"
                                                             alt="VinaCapital được trao tặng danh hiệu cao quý">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/vinacapital-duoc-trao-tang-danh-hieu-cao-quy.html"
                                title="VinaCapital được trao tặng danh hiệu cao quý">VinaCapital được trao tặng danh hiệu cao quý</a>
                        </h4>
                        <span>(2014-08-28 13:27:13)</span>
                        <p>
                            Đây là giải thưởng cao quý nhằm trao tặng cho các doanh nhân về lĩnh vực bất động sản, có thể nói thành công lớn nhất mà VinaCapital mang lại đó là những dự án lớn ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/su-dung-mau-sac-cho-hop-phong-thuy-trong-nha-o.html" title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos562.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos562.jpg"
                                                             alt="Sử dụng màu sắc cho hợp phong thủy trong nhà ở">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/su-dung-mau-sac-cho-hop-phong-thuy-trong-nha-o.html"
                                title="Sử dụng màu sắc cho hợp phong thủy trong nhà ở">Sử dụng màu sắc cho hợp phong thủy trong nhà ở</a>
                        </h4>
                        <span>(2014-08-27 15:22:03)</span>
                        <p>
                            Đa số hiện nay ta thường thấy các căn nhà có 2 màu sắc chủ đạo đó là màu trắng và màu xanh hoặc những màu liên quan đến cuộc sống của họ.                            </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/nhanh-tay-dang-ky-mua-dat-nen-gia-uu-dai-tai-la-casa.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos557.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos557.jpg"
                                                             alt="Nhanh tay đăng ký mua đất nền giá ưu đãi tại La Casa">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/nhanh-tay-dang-ky-mua-dat-nen-gia-uu-dai-tai-la-casa.html"
                                title="Nhanh tay đăng ký mua đất nền giá ưu đãi tại La Casa">Nhanh tay đăng ký mua đất nền giá ưu đãi tại La Casa</a>
                        </h4>
                        <span>(2014-08-27 14:36:29)</span>
                        <p>
                            Đợt mở bán đất nền lần này tương đối lớn tại La Casa, mặc dù mới được mở bán được vài tuần nhưng đã được nhiều khách hàng ghé thăm cũng như có nhiều nhà đầu ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/chung-cu-mini-luon-duoc-nhieu-khach-hang-san-don.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos554.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos554.jpg"
                                                             alt="Chung cư mini luôn được nhiều khách hàng săn đón">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/chung-cu-mini-luon-duoc-nhieu-khach-hang-san-don.html"
                                title="Chung cư mini luôn được nhiều khách hàng săn đón">Chung cư mini luôn được nhiều khách hàng săn đón</a>
                        </h4>
                        <span>(2014-08-27 13:58:22)</span>
                        <p>
                            Được biết các dự án chung cư mini đã được các nhà đầu tư cho xây dựng nhiều năm nay và đang được rất nhiều khách hàng ưa chuộng cho đến thời điểm hiện tại                          </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/gan-120-can-ho-mipec-riverside-duoc-ban-trong-ngay-dau.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos553.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos553.jpg"
                                                             alt="Gần 120 căn hộ Mipec Riverside được bán trong ngày đầu">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/gan-120-can-ho-mipec-riverside-duoc-ban-trong-ngay-dau.html"
                                title="Gần 120 căn hộ Mipec Riverside được bán trong ngày đầu">Gần 120 căn hộ Mipec Riverside được bán trong ngày đầu</a>
                        </h4>
                        <span>(2014-08-27 13:41:08)</span>
                        <p>
                            Một con số không phải khu chung cư nào cũng làm được trong ngày mở bán đầu tiên, được biết chỉ trong chưa đầy 1 ngày đã có tới 120 khách hàng dọn đến ở tại ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/lam-an-kham-kha-nho-biet-coi-phong-thuy-van-phong.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos535.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos535.jpg"
                                                             alt="Làm ăn khấm khá nhờ biết coi phong thủy văn phòng">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/lam-an-kham-kha-nho-biet-coi-phong-thuy-van-phong.html"
                                title="Làm ăn khấm khá nhờ biết coi phong thủy văn phòng">Làm ăn khấm khá nhờ biết coi phong thủy văn phòng</a>
                        </h4>
                        <span>(2014-08-26 15:49:38)</span>
                        <p>
                            Tại sao khi mở văn phòng lại có nhiều người than vãn làm ăn không hiệu quả mặc dù đã chọn địa điểm thích hợp, và trong khi đó lại có nhiều người tuy mở văn ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/ngan-hang-hua-se-ung-ho-6-nghin-ty-de-dau-tu-giao-thong.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos533.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos533.jpg"
                                                             alt="Ngân hàng hứa sẽ ủng hộ 6 nghìn tỷ để đầu tư giao thông">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/ngan-hang-hua-se-ung-ho-6-nghin-ty-de-dau-tu-giao-thong.html"
                                title="Ngân hàng hứa sẽ ủng hộ 6 nghìn tỷ để đầu tư giao thông">Ngân hàng hứa sẽ ủng hộ 6 nghìn tỷ để đầu tư giao thông</a>
                        </h4>
                        <span>(2014-08-26 15:21:19)</span>
                        <p>
                            Đây là một tin đáng mừng đối với nghành giao thông nước ta hiện nay, bởi vì hiện nay ngành giao thông nước ta đang trong giai đoạn khó khăn khi mà thiếu các nguồn vốn ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/du-an-watermark-dang-buoc-vao-giai-doan-hoan-thien.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos529.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos529.jpg"
                                                             alt="Dự án Watermark đang bước vào giai đoạn hoàn thiện">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/du-an-watermark-dang-buoc-vao-giai-doan-hoan-thien.html"
                                title="Dự án Watermark đang bước vào giai đoạn hoàn thiện">Dự án Watermark đang bước vào giai đoạn hoàn thiện</a>
                        </h4>
                        <span>(2014-08-26 14:42:18)</span>
                        <p>
                            Để có thể hoàn thành sớm trong năm nay nên các chủ đầu tư đang thúc đẩy quá trình thi công dự án Watermark một cách triệt để nhất.                                </p>
                    </span><!-- End .fn_2 -->
                                </li>
                                <li class="clearfix">
                    <span class="fn_1">
                        <a href="http://www.bdstphcm.org/mot-vai-meo-nho-trang-tri-nha-ve-sinh-hop-phong-thuy.html"
                           title="">
							                            <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos519.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos519.jpg"
                                                             alt="Một vài mẹo nhỏ trang trí nhà vệ sinh hợp phong thủy">
                        </a>
                    </span><!-- End .fn_1 -->
                                    <span class="fn_2">
                        <h4>
                             <a href="http://www.bdstphcm.org/mot-vai-meo-nho-trang-tri-nha-ve-sinh-hop-phong-thuy.html"
                                title="Một vài mẹo nhỏ trang trí nhà vệ sinh hợp phong thủy">Một vài mẹo nhỏ trang trí nhà vệ sinh hợp phong thủy</a>
                        </h4>
                        <span>(2014-08-25 15:04:14)</span>
                        <p>
                            Nhiều hộ gia đình khi xây nhà xong thì họ liền mua các vật dụng dành cho nhà tắm và bái trí sao cho hợp lý nhất, đó là ý kiến chung của mỗi người chúng ...                        </p>
                    </span><!-- End .fn_2 -->
                                </li>

                            </ul>
                        </div><!-- End .f_news -->

                        <div class="page">
                            <div class="PageNum">
                                <span>1</span><a href="http://www.bdstphcm.org/page-tin-tuc-nha-dat/p/2.html">2</a><a
                                        href="http://www.bdstphcm.org/page-tin-tuc-nha-dat/p/3.html">3</a><a
                                        href="http://www.bdstphcm.org/page-tin-tuc-nha-dat/p/4.html">4</a><a
                                        href="http://www.bdstphcm.org/page-tin-tuc-nha-dat/p/2.html"> › </a><a
                                        href="http://www.bdstphcm.org/page-tin-tuc-nha-dat/p/4.html"> » </a></div>
                            <div class="clear"></div>
                        </div><!-- End .page -->


                    </article><!-- End .content -->

                    <div class="sidebar">

                        <div class="block_sb">
                            <h4 class="t_sb">
        <span>
           Tìm kiếm nhanh
        </span>
                            </h4><!-- End .t_sb -->
                            <div class="m_sb">

                                <div class="slx_ttp">
                                    <style type="text/css">
                                        .slx_ttp {
                                            background: #FCFCFC;
                                            padding: 10px;
                                        }

                                        .slx_ttp span.customSelect {
                                            background-color: #fff !important;
                                        }

                                        .styled {
                                            background: none repeat scroll 0 0 #f9f9f9;
                                            border: 1px solid #ddd;
                                            padding: 2px;
                                            margin: 2px 0 2px 0;
                                        }
                                    </style>

                                    <script>
                                        $(document).ready(function () {
                                            $("#tinh").select2();
                                            $("#huyen").select2();
                                            $("#phuong").select2();
                                            $("#duong").select2();
                                            $("#loai").select2();
                                            $("#ddCat").select2();
                                            $("#tinh").select2();
                                            $("#huyen").select2();
                                            $("#phuong").select2();
                                            $("#duong").select2();
                                            $("#loai").select2();
                                            $("#ddCat").select2();
                                            $("#price").select2();
                                            $("#dientich").select2();
                                            $("#tinh").change(function () {
                                                var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                                var table = "tbl_quanhuyen";
                                                var tablep = "tbl_quanhuyen_category";
                                                $("#huyen").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                            });
                                            $("#huyen").change(function () {
                                                var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                                var table = "tbl_street";
                                                var tablep = "tbl_quanhuyen";
                                                $("#duong").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                            });
                                            $("#huyen").change(function () {
                                                var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                                var table = "tbl_phuongxa";
                                                var tablep = "tbl_quanhuyen";
                                                $("#phuong").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                            });
                                            $("#loai").change(function () {
                                                var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                                var table = "tbl_rv_category";
                                                var tablep = "tbl_rv_category";
                                                $("#ddCat").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                            });
                                        });
                                    </script>
                                    <form action="http://www.bdstphcm.org/tim-bds.html" method="POST"
                                          enctype="multipart/form-data">
                                        <ul>
                                            <li>
                                                <div class="select2-container styled" id="s2id_tinh" title=""><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1"> <span class="select2-chosen"
                                                                                 id="select2-chosen-7"> Chọn thành phố </span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen7" class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-7" id="s2id_autogen7"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen7_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-7"
                                                                   id="s2id_autogen7_search" placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-7"></ul>
                                                    </div>
                                                </div>
                                                <select name="tinh" id="tinh" class="styled select2-offscreen" title=""
                                                        tabindex="-1">
                                                    <option value=""> Chọn thành phố</option>

                                                    <option value="ho-chi-minh">Hồ Chí Minh</option>
                                                    <option value="ha-noi">Hà Nội</option>
                                                    <option value="binh-duong">Bình Dương</option>
                                                    <option value="hai-phong">Hải Phòng</option>
                                                    <option value="da-nang">Đà Nẵng</option>
                                                    <option value="ben-tre">Bến Tre</option>
                                                    <option value="can-tho">Cần Thơ</option>
                                                    <option value="dong-nai">Đồng Nai</option>
                                                    <option value="ba-ria-vung-tau">Bà Rịa Vũng Tàu</option>
                                                    <option value="khanh-hoa">Khánh Hòa</option>
                                                    <option value="long-an">Long An</option>
                                                    <option value="thua-thien-hue">Thừa Thiên Huế</option>
                                                    <option value="an-giang">An Giang</option>
                                                    <option value="bac-lieu">Bạc Liêu</option>
                                                    <option value="bac-can">Bắc Cạn</option>
                                                    <option value="bac-ninh">Bắc Ninh</option>
                                                    <option value="bac-giang">Bắc Giang</option>
                                                    <option value="binh-dinh">Bình Định</option>
                                                    <option value="binh-phuoc">Bình Phước</option>
                                                    <option value="binh-thuan">Bình Thuận</option>
                                                    <option value="ca-mau">Cà Mau</option>
                                                    <option value="cao-bang">Cao Bằng</option>
                                                    <option value="dak-lak">Đắk Lắk</option>
                                                    <option value="dong-thap">Đồng Tháp</option>
                                                    <option value="gia-lai">Gia Lai</option>
                                                    <option value="ha-giang">Hà Giang</option>
                                                    <option value="ha-nam">Hà Nam</option>
                                                    <option value="ha-tinh">Hà Tĩnh</option>
                                                    <option value="hai-duong">Hải Dương</option>
                                                    <option value="hoa-binh">Hòa Bình</option>
                                                    <option value="hung-yen">Hưng Yên</option>
                                                    <option value="kien-giang">Kiên Giang</option>
                                                    <option value="kon-tum">Kon Tum</option>
                                                    <option value="lai-chau">Lai Châu</option>
                                                    <option value="lang-son">Lạng Sơn</option>
                                                    <option value="lao-cai">Lào Cai</option>
                                                    <option value="lam-dong">Lâm Đồng</option>
                                                    <option value="nam-dinh">Nam Định</option>
                                                    <option value="nghe-an">Nghệ An</option>
                                                    <option value="ninh-binh">Ninh Bình</option>
                                                    <option value="ninh-thuan">Ninh Thuận</option>
                                                    <option value="phu-tho">Phú Thọ</option>
                                                    <option value="phu-yen">Phú Yên</option>
                                                    <option value="quang-binh">Quảng Bình</option>
                                                    <option value="quang-nam">Quảng Nam</option>
                                                    <option value="quang-ngai">Quảng Ngãi</option>
                                                    <option value="quang-ninh">Quảng Ninh</option>
                                                    <option value="quang-tri">Quảng Trị</option>
                                                    <option value="soc-trang">Sóc Trăng</option>
                                                    <option value="son-la">Sơn La</option>
                                                    <option value="tay-ninh">Tây Ninh</option>
                                                    <option value="thai-binh">Thái Bình</option>
                                                    <option value="thai-nguyen">Thái Nguyên</option>
                                                    <option value="thanh-hoa">Thanh Hóa</option>
                                                    <option value="tien-giang">Tiền Giang</option>
                                                    <option value="tra-vinh">Trà Vinh</option>
                                                    <option value="tuyen-quang">Tuyên Quang</option>
                                                    <option value="vinh-long">Vĩnh Long</option>
                                                    <option value="vinh-phuc">Vĩnh Phúc</option>
                                                    <option value="yen-bai">Yên Bái</option>
                                                    <option value="dien-bien">Điện Biên</option>
                                                    <option value="dak-nong">Đắk Nông</option>
                                                    <option value="hau-giang">Hậu Giang</option>

                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_huyen" title=""><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1"> <span class="select2-chosen"
                                                                                 id="select2-chosen-8"> Chọn Quận / Huyện </span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen8" class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-8" id="s2id_autogen8"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen8_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-8"
                                                                   id="s2id_autogen8_search" placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-8"></ul>
                                                    </div>
                                                </div>
                                                <select name="huyen" id="huyen" class="styled select2-offscreen"
                                                        title="" tabindex="-1">

                                                    <option value=""> Chọn Quận / Huyện</option>
                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_phuong" title=""><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1"> <span class="select2-chosen"
                                                                                 id="select2-chosen-9">Chọn Phường / Xã</span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen9" class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-9" id="s2id_autogen9"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen9_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-9"
                                                                   id="s2id_autogen9_search" placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-9"></ul>
                                                    </div>
                                                </div>
                                                <select name="phuong" id="phuong" class="styled select2-offscreen"
                                                        title="" tabindex="-1">

                                                    <option value="">Chọn Phường / Xã</option>
                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_duong" title=""><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1"> <span class="select2-chosen"
                                                                                 id="select2-chosen-10">Chọn đường</span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen10"
                                                            class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-10" id="s2id_autogen10"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen10_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-10"
                                                                   id="s2id_autogen10_search" placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-10"></ul>
                                                    </div>
                                                </div>
                                                <select name="duong" id="duong" class="styled select2-offscreen"
                                                        title="" tabindex="-1">

                                                    <option value="">Chọn đường</option>
                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_loai" title=""><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1"> <span class="select2-chosen"
                                                                                 id="select2-chosen-11">Chọn nhu cầu</span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen11"
                                                            class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-11" id="s2id_autogen11"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen11_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-11"
                                                                   id="s2id_autogen11_search" placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-11"></ul>
                                                    </div>
                                                </div>
                                                <select name="loai" id="loai" class="styled select2-offscreen" title=""
                                                        tabindex="-1">

                                                    <option value="">Chọn nhu cầu</option>
                                                    <option value="nha-dat-ban"> Nhà đất bán</option>
                                                    <option value="nha-dat-cho-thue"> Nhà đất cho thuê</option>

                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_ddCat" title=""><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1"> <span class="select2-chosen"
                                                                                 id="select2-chosen-12">Chọn hình thức</span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen12"
                                                            class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-12" id="s2id_autogen12"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen12_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-12"
                                                                   id="s2id_autogen12_search" placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-12"></ul>
                                                    </div>
                                                </div>
                                                <select name="ddCat" id="ddCat" class="styled select2-offscreen"
                                                        title="" tabindex="-1">

                                                    <option value="">Chọn hình thức</option>
                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_price"><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1"> <span class="select2-chosen"
                                                                                 id="select2-chosen-13">Chọn giá</span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen13"
                                                            class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-13" id="s2id_autogen13"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen13_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-13"
                                                                   id="s2id_autogen13_search" placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-13"></ul>
                                                    </div>
                                                </div>
                                                <select id="price" name="price" class="styled select2-offscreen"
                                                        tabindex="-1" title="">
                                                    <option value="">Chọn giá</option>
                                                    <option value="nho-hon-500-trieu"> &lt; 500 triệu</option>
                                                    <option value="800-trieu-den-1-ti">800 - 1 tỉ</option>
                                                    <option value="1-den-3-ti">1-3 tỉ</option>
                                                    <option value="4-den-5-ti">4-5 tỉ</option>
                                                    <option value="6-den-9-ti">6-9 tỉ</option>
                                                    <option value="lon-hon-10-ti"> &gt;10 tỉ</option>
                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_dientich"><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1"> <span class="select2-chosen"
                                                                                 id="select2-chosen-14"> Chọn diện tích </span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen14"
                                                            class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-14" id="s2id_autogen14"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen14_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-14"
                                                                   id="s2id_autogen14_search" placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-14"></ul>
                                                    </div>
                                                </div>
                                                <select name="dientich" id="dientich" class="styled select2-offscreen"
                                                        tabindex="-1" title="">
                                                    <option value=""> Chọn diện tích</option>
                                                    <option value="nho-hon-30-m2"> &lt; 30 m2</option>
                                                    <option value="30-50-m2"> 30 - 50 m2</option>
                                                    <option value="50-80-m2"> 50 - 80 m2</option>
                                                    <option value="80-100-m2"> 80 - 100 m2</option>
                                                    <option value="100-150-m2"> 100 - 150 m2</option>
                                                    <option value="150-200-m2"> 150 - 200 m2</option>
                                                    <option value="200-250-m2"> 200 - 250 m2</option>
                                                    <option value="250-300-m2"> 250 - 300 m2</option>
                                                    <option value="lon-hon-300m2"> &gt; 300 m2</option>
                                                </select>
                                            </li>
                                        </ul>

                                        <div style="text-align: right; padding-top: 7px;">
                                            <input type="hidden" name="guitin" value="guitin">
                                            <input id="timbds" name="timbds" class="btn_Search" type="submit"
                                                   value="Tìm kiếm">
                                        </div>
                                    </form>
                                </div>

                            </div><!-- End .m_sb -->
                        </div>
                        <div class="block_sb" id="street_t">
                            <h4 class="t_sb">
        <span>
			<a title=" Nhà đất TP HCM " href="http://www.bdstphcm.org/ho-chi-minh.html">
			Nhà đất TP HCM
		   </a>
        </span>
                            </h4><!-- End .t_sb -->
                            <div class="m_sb">
                                <div id="content_ndb" class="f_rv mCustomScrollbar _mCS_1">
                                    <div class="mCustomScrollBox mCS-dark-thin" id="mCSB_1"
                                         style="position:relative; height:100%; overflow:hidden; max-width:100%;">
                                        <div class="mCSB_container mCS_no_scrollbar" style="position:relative; top:0;">
                                            <ul>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Bình Chánh"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-binh-chanh.html"> Huyện Bình Chánh</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Cần Giờ"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-can-gio.html"> Huyện Cần Giờ</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Củ Chi"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-cu-chi.html"> Huyện Củ Chi</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Hóc Môn"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-hoc-mon.html"> Huyện Hóc Môn</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Nhà Bè"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-nha-be.html"> Huyện Nhà Bè</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 1"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-1.html"> Quận 1</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 10" href="http://www.bdstphcm.org/ho-chi-minh/quan-10.html"> Quận 10</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 11" href="http://www.bdstphcm.org/ho-chi-minh/quan-11.html"> Quận 11</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 12" href="http://www.bdstphcm.org/ho-chi-minh/quan-12.html"> Quận 12</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 2"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-2.html"> Quận 2</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 3"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-3.html"> Quận 3</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 4"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-4.html"> Quận 4</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 5"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-5.html"> Quận 5</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 6"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-6.html"> Quận 6</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 7"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-7.html"> Quận 7</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 8"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-8.html"> Quận 8</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 9"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-9.html"> Quận 9</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Bình Tân"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-binh-tan.html"> Quận Bình Tân</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Bình Thạnh"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-binh-thanh.html"> Quận Bình Thạnh</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Gò Vấp"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-go-vap.html"> Quận Gò Vấp</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Phú Nhuận"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-phu-nhuan.html"> Quận Phú Nhuận</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Tân Bình"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-tan-binh.html"> Quận Tân Bình</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Tân Phú"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-tan-phu.html"> Quận Tân Phú</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Thủ Đức"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-thu-duc.html"> Quận Thủ Đức</a>
                    </span>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="mCSB_scrollTools" style="position: absolute; display: none;">
                                            <div class="mCSB_draggerContainer">
                                                <div class="mCSB_dragger" style="position: absolute; top: 0px;"
                                                     oncontextmenu="return false;">
                                                    <div class="mCSB_dragger_bar" style="position:relative;"></div>
                                                </div>
                                                <div class="mCSB_draggerRail"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- End .f_rv -->

                            </div><!-- End .m_sb -->
                        </div>


                        <!-- <div class="block_sb">
                        <h4 class="t_sb">
                            <span>
                                Video giới thiệu
                            </span>
                        </h4>
                        <div class="m_sb">

                            <div class="f_video">


                            <object width="100%" height="170">
                                <param name="movie" value="http://www.youtube.com/v/vURcezzpM2Y?hl=vi_VN&amp;version=3&amp;autoplay=0"></param>
                                <param name="allowFullScreen" value="true"></param>
                                <param name="allowscriptaccess" value="always"></param>
                                <embed src="http://www.youtube.com/v/XaN41AgHB2Q?hl=vi_VN&amp;version=3&amp;autoplay=0" type="application/x-shockwave-flash" width="100%" height="170" allowscriptaccess="always" allowfullscreen="true"></embed>
                            </object>

                            </div>

                        </div>
                    </div> -->


                        <div class="block_sb" id="quangcao_t">
                            <h4 class="t_sb">
            <span>
                Quảng cáo
            </span>
                            </h4><!-- End .t_sb -->
                            <div class="m_sb">

                                <div class="f_adv">

                                    <ul>
                                        <li>
                                            <img src="http://www.bdstphcm.org/images/bds_adv/sliders6.jpg"
                                                 alt="Bản Đồ Quy Hoạch Định Hướng Phát Triển không Gian 2010 - 2020">
                                            <a href="https://www.youtube.com/watch?v=hwsgvaBd0ZY&amp;t=17s"
                                               target="_blank"></a>
                                        </li>
                                        <li>
                                            <img src="http://www.bdstphcm.org/images/bds_adv/sliders4.gif"
                                                 alt="Cho vay tiêu dùng không thuế chấp">
                                            <a href="#" target="_blank"></a>
                                        </li>
                                        <li>
                                            <img src="http://www.bdstphcm.org/images/bds_adv/sliders3.png"
                                                 alt="Samsung">
                                            <a href="" target="_blank"></a>
                                        </li>
                                        <li>
                                            <img src="http://www.bdstphcm.org/images/bds_adv/sliders1.png"
                                                 alt="asd asdasas">
                                            <a href="#" target="_blank"></a>
                                        </li>
                                    </ul>

                                </div><!-- End .f_adv -->

                            </div><!-- End .m_sb -->
                        </div><!-- End .block_sb -->

                        <div class="block_sb">
                            <h4 class="t_sb">

                            </h4><!-- End .t_sb -->
                            <div class="m_sb">
                                <!-- End .m_sb -->
                            </div><!-- End .block_sb -->

                        </div><!-- End .sidebar -->
                    </div><!-- End .f_cont -->

                </div><!-- End .min_wrap -->
            </div><!-- End #container -->

            <footer id="footer">
                <div class="min_wrap">

                    <div class="f_nmg">
                        <h4 class="t_nmg">
                            <a href="http://www.bdstphcm.org/doanh-nghiep.html" title="Nhà môi giới">Nhà môi giới</a>
                        </h4><!-- End .t_nmg -->
                        <div class="m_nmg">

                            <div class="swiper-container3" style="cursor: -webkit-grab;">
                                <div class="swiper-wrapper"
                                     style="padding-left: 0px; padding-right: 0px; transform: translate3d(-490px, 0px, 0px); transition-duration: 0.3s; width: 1950px; height: 80px;">
                                    <div class="swiper-slide">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-kim-oanh.html"
                           title="Công ty cổ phần địa ốc Kim Oanh"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/infos95.jpg"
                                    alt="Công ty cổ phần địa ốc Kim Oanh"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-kim-oanh.html"
                           title="Công ty cổ phần địa ốc Kim Oanh">
                            Công ty cổ phần địa ốc Kim Oanh                        </a>
                        <p>
                        	 Khởi nghiệp từ năm 2006, công ty Địa ốc Kim Oanh đã gặp phải không ít những khó khăn, thử thách của một doanh nghiệp Bất động sản nhỏ                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-thang-long.html"
                           title="Công ty cổ phần địa ốc Thăng Long"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/infos96.jpg"
                                    alt="Công ty cổ phần địa ốc Thăng Long"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-thang-long.html"
                           title="Công ty cổ phần địa ốc Thăng Long">
                            Công ty cổ phần địa ốc Thăng Long                        </a>
                        <p>
                        	 Công ty Cổ Phần Địa Ốc Thăng Long được thành lập vào năm 2009. Ngành nghề kinh doanh chủ yếu là đầu tư và phát triển các dự án bất động sản, tư vấn, tiếp thị, môi giới bất động sản, khu resort, du lịch nghỉ dưỡng                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide swiper-slide-visible swiper-slide-active">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-acb.html"
                           title="Công ty cổ phần địa ốc ACB"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/infos97.jpg"
                                    alt="Công ty cổ phần địa ốc ACB"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-acb.html"
                           title="Công ty cổ phần địa ốc ACB">
                            Công ty cổ phần địa ốc ACB                        </a>
                        <p>
                        	 Chính thức thành lập từ 2001, Công ty CP Địa Ốc ACB (ACBR) liên tục phát triển và khẳng định thương hiệu hàng đầu trong lĩnh vực bất động sản với sự hỗ trợ tích cực từ hệ thống Ngân hàng Á Châu (ACB)                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide swiper-slide-visible">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-sai-gon.html"
                           title="Công ty cổ phần địa ốc Sài Gòn"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/infos98.jpg"
                                    alt="Công ty cổ phần địa ốc Sài Gòn"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-sai-gon.html"
                           title="Công ty cổ phần địa ốc Sài Gòn">
                            Công ty cổ phần địa ốc Sài Gòn                        </a>
                        <p>
                        	 Công ty Cổ phần Địa ốc Sài Gòn – (Viết tắt là SAIGONRES) là doanh nghiệp  cổ phần hóa từ doanh nghiệp nhà nước Công ty Xây dựng Kinh doanh nhà Gia Định thuộc Sở Địa Chính – Nhà Đất Thành phố Hồ Chí Minh theo quyết định số 108/1999/QĐ-TTg ngày 23/04/1999 của Thủ Tướng Chính phủ.                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide swiper-slide-visible">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-thang-loi.html"
                           title="Công ty cổ phần địa ốc Thắng Lợi"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/infos120.png"
                                    alt="Công ty cổ phần địa ốc Thắng Lợi"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-thang-loi.html"
                           title="Công ty cổ phần địa ốc Thắng Lợi">
                            Công ty cổ phần địa ốc Thắng Lợi                        </a>
                        <p>
                        	 Công ty Cổ Phần Địa Ốc Thắng Lợi được thành lập xuất phát từ nhu cầu an cư lạc nghiệp của người dân và đầu tư bất động sản sinh lợi của khách hàng                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide swiper-slide-visible">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-kinh-doanh-dia-oc-him-lam.html"
                           title="Công ty cổ phần kinh doanh địa ốc Him Lam"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/cong-co-phan-kinh-doanh-bat-dong-san-him-lam-423.png"
                                    alt="Công ty cổ phần kinh doanh địa ốc Him Lam"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-kinh-doanh-dia-oc-him-lam.html"
                           title="Công ty cổ phần kinh doanh địa ốc Him Lam">
                            Công ty cổ phần kinh doanh địa ốc Him Lam                        </a>
                        <p>
                        	 Công ty cổ phần Him Lam được thành lập từ tháng 9 năm 1994 trên cơ sở chuyển đổi từ Công ty trách nhiệm hữu hạn Thương mại Him Lam. Với 18 năm kinh nghiệm kinh doanh trên thị trường bất động sản                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/tai-sao-nen-ky-gui-nha-dat-ban-cho-thue-quan-go-vap-quan-12-dich-vu-nhan-ky-goi-mua-ban-cho-thue-nha-dat-quan-go-vap-quan-12-tphcm-uy-tin-bdstphcm-org.html"
                           title="TẠI SAO NÊN KÝ GỬI NHÀ ĐẤT BÁN &amp; CHO THUÊ QUẬN GÒ VÂP, QUẬN 12? DỊCH VỤ NHẬN KÝ GỞI - MUA BÁN - CHO THUÊ NHÀ ĐẤT QUẬN GÒ VÂP, QUẬN 12 TPHCM UY TÍN | BDSTPHCM.ORG"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/logo-1-PzIiG.png"
                                    alt="TẠI SAO NÊN KÝ GỬI NHÀ ĐẤT BÁN &amp; CHO THUÊ QUẬN GÒ VÂP, QUẬN 12? DỊCH VỤ NHẬN KÝ GỞI - MUA BÁN - CHO THUÊ NHÀ ĐẤT QUẬN GÒ VÂP, QUẬN 12 TPHCM UY TÍN | BDSTPHCM.ORG"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/tai-sao-nen-ky-gui-nha-dat-ban-cho-thue-quan-go-vap-quan-12-dich-vu-nhan-ky-goi-mua-ban-cho-thue-nha-dat-quan-go-vap-quan-12-tphcm-uy-tin-bdstphcm-org.html"
                           title="TẠI SAO NÊN KÝ GỬI NHÀ ĐẤT BÁN &amp; CHO THUÊ QUẬN GÒ VÂP, QUẬN 12? DỊCH VỤ NHẬN KÝ GỞI - MUA BÁN - CHO THUÊ NHÀ ĐẤT QUẬN GÒ VÂP, QUẬN 12 TPHCM UY TÍN | BDSTPHCM.ORG">
                            TẠI SAO NÊN KÝ GỬI NHÀ ĐẤT BÁN &amp; CHO THUÊ QUẬN GÒ VÂP, QUẬN 12? DỊCH VỤ NHẬN KÝ GỞI - MUA BÁN - CHO THUÊ NHÀ ĐẤT QUẬN GÒ VÂP, QUẬN 12 TPHCM UY TÍN | BDSTPHCM.ORG                        </a>
                        <p>
                        	 Bạn đang có nhà đất cần bán hoặc cho thuê tại khu vực QUẬN GÒ VÂP, QUẬN 12 TP. HCM? Và bạn cần ký gửi nhà đất (bất động sản) đó? Nhưng bạn không biết là công ty bất động sản nào cung cấp dịch vụ nhận ký gửi bất động sản và các dịch vụ khác (mua bán nhà đất, cho thuê nhà đất và nhận ký gởi nhà đất - bất động sản khu vực TPHCM) uy tín, nhanh gọn mà vẫn đảm bảo những tiêu chí bạn đưa ra như: bảo mật thông tin, thời gian ký gửi có hạn, giá phí dịch vụ rẻ.                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/bang-gia-dat-quan-go-vap-tp-ho-chi-minh-tu-nam-2015-den-2019.html"
                           title="Bảng giá đất quận Gò Vấp, TP. Hồ Chí Minh Từ Năm 2015 đến 2019"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/bang-gia-dat-tphcm-2015-2019-1-rtntu.jpg"
                                    alt="Bảng giá đất quận Gò Vấp, TP. Hồ Chí Minh Từ Năm 2015 đến 2019"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/bang-gia-dat-quan-go-vap-tp-ho-chi-minh-tu-nam-2015-den-2019.html"
                           title="Bảng giá đất quận Gò Vấp, TP. Hồ Chí Minh Từ Năm 2015 đến 2019">
                            Bảng giá đất quận Gò Vấp, TP. Hồ Chí Minh Từ Năm 2015 đến 2019                        </a>
                        <p>
                        	 cập nhật bảng giá đất quận Gò Vấp TP. Hồ Chí Minh Từ Năm 2015 đến 2019                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->

                                </div><!-- End .swiper-wrapper -->
                            </div><!-- End .swiper-container3 -->

                            <a class="sc3_left" href="#"></a>
                            <a class="sc3_right" href="#"></a>

                            <div class="clear"></div>

                        </div><!-- End .m_nmg -->
                    </div><!-- End .f_nmg -->

                    <div class="menu_foot">
                        <ul>
                            <li>
                                <a href="http://www.bdstphcm.org/gioi-thieu.html">Giới thiệu</a>
                            </li>
                            <li>
                                <a href="http://www.bdstphcm.org/hop-tac-doi-tac.html">Hợp tác &amp; Đối tác</a>
                            </li>
                            <li>
                                <a href="http://www.bdstphcm.org/huong-dan-dang-tin.html">Hướng dẫn đăng tin</a>
                            </li>
                            <li>
                                <a href="http://www.bdstphcm.org/quy-dinh-dang-tin.html">Quy định đăng tin</a>
                            </li>
                            <li>
                                <a href="http://www.bdstphcm.org/tuyen-dung.html">Tuyển dụng</a>
                            </li>
                            <li>
                                <a href="http://www.bdstphcm.org/lien-he.html">Liên Hệ</a>
                            </li>
                        </ul>
                    </div><!-- End .menu_foot -->

                    <div class="info_foot">
                        Bản quyền © 2017 BẤT ĐỘNG SẢN TP.HCM - All rights reserved<br>
                        <div>
                            Địa chỉ: 778/56. Thống Nhất, Phường 15, Gò Vâp. tp.hcm
                        </div>
                    </div><!-- End .info_foot -->


                </div><!-- End .min_wrap -->
            </footer><!-- End #footer -->

        </div><!-- End #wrapper -->



@stop