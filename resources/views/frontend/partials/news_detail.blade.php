@extends('frontend.layouts.master')
@section('title')
    chi tiết tin tức bán đất
@endsection
@section('main_content')

    <div id="wrapper">

        <div id="container">
            <div class="min_wrap">


                <div class="breacrum">
                    <div id="breacrum" xmlns:v="http://rdf.data-vocabulary.org/#">
        <span typeof="v:Breadcrumb">
            <a rel="v:url" property="v:title" href="/" title="Trang chủ"> Trang chủ </a>
        </span>

                        <span typeof="v:Breadcrumb"> »
                   <a rel="v:url" property="v:title" href=""
                      title="Tin tức bất động sản"> Tin tức bất động sản </a>
                </span>

                        <span typeof="v:Breadcrumb">»
				 khách hàng bắt đầu quan tâm đến những căn hộ cao cấp            	</span>

                    </div>
                </div>

                <div class="f_cont clearfix">

                    <article class="content">

                        <script>
                            $(document).ready(function () {
                                $('.bxslider').bxSlider({
                                    auto: true, pagerCustom: '#bx-pager'
                                });
                            });
                        </script>
                        <style>
                            .bx-wrapper {
                                margin-bottom: 5px !important;
                            }

                            ul.bxslider li {
                                text-align: center;
                            }

                            ul.bxslider li img {
                                display: inline;
                                height: 100%;
                            }
                        </style>

                        <h2 class="t_ndct">
                            khách hàng bắt đầu quan tâm đến những căn hộ cao cấp
                            <div class="addthis_sharing_toolbox"
                                 data-url=""
                                 data-title="khách hàng bắt đầu quan tâm đến những căn hộ cao cấp"
                                 data-description="Nếu như nói cách đây khoảng 2 năm trở lại thì thị trường các chung cư có căn hộ cao cấp ít được khác hàng quan tâm thì giờ đây theo một con số mới thống kê cho thấy hiện tại có rất nhiều người dân đến hỏi mua các căn hộ cao cấp với ý định sống lâu dài."
                                 style="clear: both;">
                                <div id="atstbx"
                                     class="at-share-tbx-element addthis-smartlayers addthis-animated at4-show"
                                     aria-labelledby="at-92a020a6-19b8-47eb-b461-9d6d28ca89fe" role="region"><span
                                            id="at-92a020a6-19b8-47eb-b461-9d6d28ca89fe" class="at4-visually-hidden">AddThis Sharing Buttons</span>
                                    <div class="at-share-btn-elements"><a role="button" tabindex="1"
                                                                          class="at-icon-wrapper at-share-btn at-svc-facebook"
                                                                          style="background-color: rgb(59, 89, 152); border-radius: 0%;"><span
                                                    class="at4-visually-hidden">Share to Facebook</span><span
                                                    class="at-icon-wrapper"
                                                    style="line-height: 16px; height: 16px; width: 16px;"><svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32"
                                                        version="1.1" role="img" aria-labelledby="at-svg-facebook-1"
                                                        style="width: 16px; height: 16px;"
                                                        class="at-icon at-icon-facebook"><title
                                                            id="at-svg-facebook-1">Facebook</title><g><path
                                                                d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z"
                                                                fill-rule="evenodd"></path></g></svg></span></a><a
                                                role="button" tabindex="1"
                                                class="at-icon-wrapper at-share-btn at-svc-twitter"
                                                style="background-color: rgb(29, 161, 242); border-radius: 0%;"><span
                                                    class="at4-visually-hidden">Share to Twitter</span><span
                                                    class="at-icon-wrapper"
                                                    style="line-height: 16px; height: 16px; width: 16px;"><svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32"
                                                        version="1.1" role="img" aria-labelledby="at-svg-twitter-2"
                                                        style="width: 16px; height: 16px;"
                                                        class="at-icon at-icon-twitter"><title
                                                            id="at-svg-twitter-2">Twitter</title><g><path
                                                                d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336"
                                                                fill-rule="evenodd"></path></g></svg></span></a><a
                                                role="button" tabindex="1"
                                                class="at-icon-wrapper at-share-btn at-svc-google_plusone_share"
                                                style="background-color: rgb(220, 78, 65); border-radius: 0%;"><span
                                                    class="at4-visually-hidden">Share to Google+</span><span
                                                    class="at-icon-wrapper"
                                                    style="line-height: 16px; height: 16px; width: 16px;"><svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32"
                                                        version="1.1" role="img"
                                                        aria-labelledby="at-svg-google_plusone_share-3"
                                                        style="width: 16px; height: 16px;"
                                                        class="at-icon at-icon-google_plusone_share"><title
                                                            id="at-svg-google_plusone_share-3">Google+</title><g><path
                                                                d="M12 15v2.4h3.97c-.16 1.03-1.2 3.02-3.97 3.02-2.39 0-4.34-1.98-4.34-4.42s1.95-4.42 4.34-4.42c1.36 0 2.27.58 2.79 1.08l1.9-1.83C15.47 9.69 13.89 9 12 9c-3.87 0-7 3.13-7 7s3.13 7 7 7c4.04 0 6.72-2.84 6.72-6.84 0-.46-.05-.81-.11-1.16H12zm15 0h-2v-2h-2v2h-2v2h2v2h2v-2h2v-2z"
                                                                fill-rule="evenodd"></path></g></svg></span></a><a
                                                role="button" tabindex="1"
                                                class="at-icon-wrapper at-share-btn at-svc-print"
                                                style="background-color: rgb(115, 138, 141); border-radius: 0%;"><span
                                                    class="at4-visually-hidden">Share to Print</span><span
                                                    class="at-icon-wrapper"
                                                    style="line-height: 16px; height: 16px; width: 16px;"><svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32"
                                                        version="1.1" role="img" aria-labelledby="at-svg-print-4"
                                                        style="width: 16px; height: 16px;"
                                                        class="at-icon at-icon-print"><title
                                                            id="at-svg-print-4">Print</title><g><path
                                                                d="M24.67 10.62h-2.86V7.49H10.82v3.12H7.95c-.5 0-.9.4-.9.9v7.66h3.77v1.31L15 24.66h6.81v-5.44h3.77v-7.7c-.01-.5-.41-.9-.91-.9zM11.88 8.56h8.86v2.06h-8.86V8.56zm10.98 9.18h-1.05v-2.1h-1.06v7.96H16.4c-1.58 0-.82-3.74-.82-3.74s-3.65.89-3.69-.78v-3.43h-1.06v2.06H9.77v-3.58h13.09v3.61zm.75-4.91c-.4 0-.72-.32-.72-.72s.32-.72.72-.72c.4 0 .72.32.72.72s-.32.72-.72.72zm-4.12 2.96h-6.1v1.06h6.1v-1.06zm-6.11 3.15h6.1v-1.06h-6.1v1.06z"></path></g></svg></span></a><a
                                                role="button" tabindex="1"
                                                class="at-icon-wrapper at-share-btn at-svc-compact"
                                                style="background-color: rgb(255, 101, 80); border-radius: 0%;"><span
                                                    class="at4-visually-hidden">Share to More</span><span
                                                    class="at-icon-wrapper"
                                                    style="line-height: 16px; height: 16px; width: 16px;"><svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32"
                                                        version="1.1" role="img" aria-labelledby="at-svg-addthis-5"
                                                        style="width: 16px; height: 16px;"
                                                        class="at-icon at-icon-addthis"><title
                                                            id="at-svg-addthis-5">AddThis</title><g><path
                                                                d="M18 14V8h-4v6H8v4h6v6h4v-6h6v-4h-6z"
                                                                fill-rule="evenodd"></path></g></svg></span></a></div>
                                </div>
                            </div>
                            <script type="text/javascript"
                                    src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52493e8d684da4cc"></script>
                        </h2><!-- End .t_ndct -->
                        <span class="date_ndct">(2014-09-04 15:31:48)</span>
                        <h4 class="tt_ndct">
                            Nếu như nói cách đây khoảng 2 năm trở lại thì thị trường các chung cư có căn hộ cao cấp ít
                            được
                            khác hàng quan tâm thì giờ đây theo một con số mới thống kê cho thấy hiện tại có rất nhiều
                            người
                            dân đến hỏi mua các căn hộ cao cấp với ý định sống lâu dài. </h4><!-- End .tt_ndct -->

                        <div class="f_detail clearfix">

                            <p><span style="line-height:1.6">Điều này cũng dễ hiểu bởi vì từ khi thị trường bất động sản biến động trong thời gian&nbsp; qua đã khiến các nhà đầu tư không còn đầu tư vào các dự án căn hộ cao cấp, điều này đã khiến cầu lớn hơn cung, chính vì vậy hiện tại các chung cư có căn hộ cao cấp được người dân đặc biệt quan tâm.</span>
                            </p>

                            <p style="text-align: center;"><img alt=""
                                                                src="https://lh6.googleusercontent.com/-vfjyZ3__l4I/VAgkwMucr0I/AAAAAAAABuc/eEZ9G-KQfcE/w406-h260-no/can%2Bho%2Bcao%2Bcap.jpg"
                                                                style="height:288px; width:450px"></p>

                            <p>Sở dĩ điều này được các chuyên gia bắt đầu để ý khi nhu cầu mua nhà giá rẻ của người dân
                                đã
                                giảm sút đáng kể và thay vào đó là nhiều khách hàng đã chọn cho mình những căn hộ cao
                                cấp
                                với mong muốn sống lâu dài cũng như hưởng thụ các dịch vụ đẳng cấp nơi đây mang lại.</p>

                            <p>Điều đáng mừng nhất là vừa qua dự án căn hộ cao cấp có tên là Dophin Plaza do Công ty Cổ
                                phần
                                TID làm chủ đầu tư, mặc dù chỉ là ngày đầu tiên mở bán nhưng dự án này đã bán được tổng
                                cộng
                                50 căn hộ đã giao dịch thành công. Đó là chưa kể đến dự án Mipec Riverside&nbsp; (số 2
                                Long
                                Biên II) do Công ty CP Hóa dầu Quân đội (MIPEC) làm chủ đầu tư, mặc dù ngày đầu mở bán
                                với
                                giá tương đối cao nhưng đã có tới vài chục khách hàng đăng ký mua căn hộ.</p>

                            <p>Nói chung thị trường bất động sản nước ta đang bước vào giai đoạn khởi sắc trở lại với
                                nhiều
                                dự án thành công vang dội, đáng tuyên dương nhất có lẽ là tập đoàn Vingroup chỉ tính
                                riêng
                                trong nửa tháng gần đây, đã có 96 căn hộ được giao dịch thành công.</p>

                            <p>Đáng chú ý nhất có lẽ là thị trường bất động sản ở Hà Nội, đây là thị trường có rất nhiều
                                dự
                                án đã thành công trước đó bởi các nhà đầu tư trong nước và ngoài nước, tuy nhiên mặc dù
                                có
                                tiển triển vượt bậc nhưng các nhà đầu tư và các ủy ban cần cân nhất kỹ lưỡng các loại
                                giấy
                                tờ và làm sao để hoàn thành thủ tục mua nhà một cách đơn giản và hiệu quả nhất.</p>

                        </div><!-- End .f_detail -->


                        <div class="tag">
                            <span>Tags</span>
                            <a target="_self" href="">phân khúc
                                căn
                                hộ cao cấp </a> <a target="_self"
                                                   href=""> bất động
                                sản
                                cao cấp </a> <a target="_self"
                                                href="tags/tin-tuc-bat-dong-san/thị-trường-bất-động-sản.html"> thị
                                trường
                                bất động sản </a> <a target="_self"
                                                     href="tags/tin-tuc-bat-dong-san/tập-đoàn-vingroup.html">
                                tập đoàn vingroup </a> <a target="_self"
                                                          href="tags/tin-tuc-bat-dong-san/giao-dịch-bất-động-sản.html">
                                giao
                                dịch bất động sản</a></div><!-- End .tag -->


                        <div class="news_other">

                            <div class="block_prod">
                                <h4 class="t_prod">
                <span>
                    Tin tức khác
                </span>
                                </h4><!-- End .t_prod -->
                                <div class="f_news">
                                    <ul>
                                        <li class="clearfix">
                        <span class="fn_1">
                            <a href="http://www.bdstphcm.org/dua-nhau-mua-nha-cityland-center-hills-voi-uu-dai-hap-dan.html"
                               title="Đua nhau mua nhà Cityland Center Hills với ưu đãi hấp dẫn">
								                                <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos155.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos155.jpg"
                                                                     alt="Đua nhau mua nhà Cityland Center Hills với ưu đãi hấp dẫn">
                            </a>
                        </span><!-- End .fn_1 -->
                                            <span class="fn_2">
                            <h4>
                                <a href="http://www.bdstphcm.org/dua-nhau-mua-nha-cityland-center-hills-voi-uu-dai-hap-dan.html"
                                   title="Đua nhau mua nhà Cityland Center Hills với ưu đãi hấp dẫn">
                                	Đua nhau mua nhà Cityland Center Hills với ưu đãi hấp dẫn                                </a>
                            </h4>
                            <p>
                                Đối với ai có dự định sinh sống lâu dài cũng như kinh doanh được tốt hơn thì sự lựa chọn đến với Cityland Center Hills là vô cùng hợp lý, được biết đây là dự ...                            </p>
                        </span><!-- End .fn_2 -->
                                        </li>
                                        <li class="clearfix">
                        <span class="fn_1">
                            <a href="http://www.bdstphcm.org/nguoi-dan-tranh-nhau-thue-dat-o-song-hong-de-chan-nuoi.html"
                               title="Người dân tranh nhau thuê đất ở sông Hồng để chăn nuôi">
								                                <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos354.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos354.jpg"
                                                                     alt="Người dân tranh nhau thuê đất ở sông Hồng để chăn nuôi">
                            </a>
                        </span><!-- End .fn_1 -->
                                            <span class="fn_2">
                            <h4>
                                <a href="http://www.bdstphcm.org/nguoi-dan-tranh-nhau-thue-dat-o-song-hong-de-chan-nuoi.html"
                                   title="Người dân tranh nhau thuê đất ở sông Hồng để chăn nuôi">
                                	Người dân tranh nhau thuê đất ở sông Hồng để chăn nuôi                                </a>
                            </h4>
                            <p>
                                Có lẽ đây là cơ hội tốt cho những ai quyết định chăn nuôi với quy mô lớn, được biết vừa qua UBND TP Hà Nội đã họp và quyết định sử dụng số đất hiện ...                            </p>
                        </span><!-- End .fn_2 -->
                                        </li>
                                        <li class="clearfix">
                        <span class="fn_1">
                            <a href="http://www.bdstphcm.org/ngam-ve-dep-sang-trong-the-park-residence-tai-sai-gon.html"
                               title="Ngắm vẻ đẹp sang trọng The Park Residence tại Sài Gòn">
								                                <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos179.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos179.jpg"
                                                                     alt="Ngắm vẻ đẹp sang trọng The Park Residence tại Sài Gòn">
                            </a>
                        </span><!-- End .fn_1 -->
                                            <span class="fn_2">
                            <h4>
                                <a href="http://www.bdstphcm.org/ngam-ve-dep-sang-trong-the-park-residence-tai-sai-gon.html"
                                   title="Ngắm vẻ đẹp sang trọng The Park Residence tại Sài Gòn">
                                	Ngắm vẻ đẹp sang trọng The Park Residence tại Sài Gòn                                </a>
                            </h4>
                            <p>
                                Đối với các thành phố lơn thì thương hiệu mang tên MIK Corporation không còn xa lạ gì đối với giới chuyên gia bất động sản hiện nay, vừa qua tập toàn này đã vừa cho ...                            </p>
                        </span><!-- End .fn_2 -->
                                        </li>
                                        <li class="clearfix">
                        <span class="fn_1">
                            <a href="http://www.bdstphcm.org/chao-don-nhung-khach-hang-dau-tien-den-voi-diamond-blue.html"
                               title="Chào đón những khách hàng đầu tiên đến với Diamond Blue">
								                                <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos435.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos435.jpg"
                                                                     alt="Chào đón những khách hàng đầu tiên đến với Diamond Blue">
                            </a>
                        </span><!-- End .fn_1 -->
                                            <span class="fn_2">
                            <h4>
                                <a href="http://www.bdstphcm.org/chao-don-nhung-khach-hang-dau-tien-den-voi-diamond-blue.html"
                                   title="Chào đón những khách hàng đầu tiên đến với Diamond Blue">
                                	Chào đón những khách hàng đầu tiên đến với Diamond Blue                                </a>
                            </h4>
                            <p>
                                Vừa hoàn thành xong trong tháng 7 vừa qua thì các căn hộ tại Diamond Blue đã được nhiều khách hàng mới đăng ký, không những thế điều đặc biết đáng chú ý nhất trong dự ...                            </p>
                        </span><!-- End .fn_2 -->
                                        </li>
                                        <li class="clearfix">
                        <span class="fn_1">
                            <a href="http://www.bdstphcm.org/vay-mua-nha-dat-dang-duoc-rat-nhieu-ho-dan-quang-tam.html"
                               title="Vay mua nhà đất đang được rất nhiều hộ dân quang tâm">
								                                <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos182.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos182.jpg"
                                                                     alt="Vay mua nhà đất đang được rất nhiều hộ dân quang tâm">
                            </a>
                        </span><!-- End .fn_1 -->
                                            <span class="fn_2">
                            <h4>
                                <a href="http://www.bdstphcm.org/vay-mua-nha-dat-dang-duoc-rat-nhieu-ho-dan-quang-tam.html"
                                   title="Vay mua nhà đất đang được rất nhiều hộ dân quang tâm">
                                	Vay mua nhà đất đang được rất nhiều hộ dân quang tâm                                </a>
                            </h4>
                            <p>
                                Với lãi suất chỉ 5% một năm đã khiến nhiều hộ gia đình đang ồ ạt tham gia chính sách vay tiền để mua nhà đất, điều này có thể dễ dàng lý bởi vì hiện ...                            </p>
                        </span><!-- End .fn_2 -->
                                        </li>
                                        <li class="clearfix">
                        <span class="fn_1">
                            <a href="http://www.bdstphcm.org/bat-dau-mo-ban-chung-cu-gia-re-tai-da-nang.html"
                               title="Bắt đầu mở bán chung cư giá rẻ tại Đà Nẵng ">
								                                <img src="http://www.bdstphcm.org/imagecache/image.php/images/bds_news/infos221.jpg?width=100&amp;height=100&amp;cropratio=1:1&amp;image=http://www.bdstphcm.org/images/bds_news/infos221.jpg"
                                                                     alt="Bắt đầu mở bán chung cư giá rẻ tại Đà Nẵng ">
                            </a>
                        </span><!-- End .fn_1 -->
                                            <span class="fn_2">
                            <h4>
                                <a href="http://www.bdstphcm.org/bat-dau-mo-ban-chung-cu-gia-re-tai-da-nang.html"
                                   title="Bắt đầu mở bán chung cư giá rẻ tại Đà Nẵng ">
                                	Bắt đầu mở bán chung cư giá rẻ tại Đà Nẵng                                 </a>
                            </h4>
                            <p>
                                Có lẽ trong thời gian vừa qua bất động sản tại địa bàn TP Đà Nẵng cho thấy sự xuống cấp nghiêm trọng khi mà hàng loạt các dự án bất động sản đang mở bán ...                            </p>
                        </span><!-- End .fn_2 -->
                                        </li>
                                    </ul>
                                </div><!-- End .f_news -->
                            </div><!-- End .block_prod -->

                        </div>


                    </article><!-- End .content -->

                    <div class="sidebar">

                        <div class="block_sb">
                            <h4 class="t_sb">
        <span>
           Tìm kiếm nhanh
        </span>
                            </h4><!-- End .t_sb -->
                            <div class="m_sb">

                                <div class="slx_ttp">
                                    <style type="text/css">
                                        .slx_ttp {
                                            background: #FCFCFC;
                                            padding: 10px;
                                        }

                                        .slx_ttp span.customSelect {
                                            background-color: #fff !important;
                                        }

                                        .styled {
                                            background: none repeat scroll 0 0 #f9f9f9;
                                            border: 1px solid #ddd;
                                            padding: 2px;
                                            margin: 2px 0 2px 0;
                                        }
                                    </style>

                                    <script>
                                        $(document).ready(function () {
                                            $("#tinh").select2();
                                            $("#huyen").select2();
                                            $("#phuong").select2();
                                            $("#duong").select2();
                                            $("#loai").select2();
                                            $("#ddCat").select2();
                                            $("#tinh").select2();
                                            $("#huyen").select2();
                                            $("#phuong").select2();
                                            $("#duong").select2();
                                            $("#loai").select2();
                                            $("#ddCat").select2();
                                            $("#price").select2();
                                            $("#dientich").select2();
                                            $("#tinh").change(function () {
                                                var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                                var table = "tbl_quanhuyen";
                                                var tablep = "tbl_quanhuyen_category";
                                                $("#huyen").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                            });
                                            $("#huyen").change(function () {
                                                var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                                var table = "tbl_street";
                                                var tablep = "tbl_quanhuyen";
                                                $("#duong").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                            });
                                            $("#huyen").change(function () {
                                                var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                                var table = "tbl_phuongxa";
                                                var tablep = "tbl_quanhuyen";
                                                $("#phuong").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                            });
                                            $("#loai").change(function () {
                                                var id = $(this).val();//val(1) gan vao gia tri 1 dung trong form
                                                var table = "tbl_rv_category";
                                                var tablep = "tbl_rv_category";
                                                $("#ddCat").load("http://www.bdstphcm.org/module/getChildSubject.php?table=" + table + "&tablep=" + tablep + "&id=" + id); //alert(idthanhpho)
                                            });
                                        });
                                    </script>
                                    <form action="http://www.bdstphcm.org/tim-bds.html" method="POST"
                                          enctype="multipart/form-data">
                                        <ul>
                                            <li>
                                                <div class="select2-container styled" id="s2id_tinh" title=""><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1">
                                                    <span class="select2-chosen"
                                                          id="select2-chosen-7"> Chọn thành phố </span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen7" class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-7" id="s2id_autogen7"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen7_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-7"
                                                                   id="s2id_autogen7_search"
                                                                   placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-7"></ul>
                                                    </div>
                                                </div>
                                                <select name="tinh" id="tinh" class="styled select2-offscreen" title=""
                                                        tabindex="-1">
                                                    <option value=""> Chọn thành phố</option>

                                                    <option value="ho-chi-minh">Hồ Chí Minh</option>
                                                    <option value="ha-noi">Hà Nội</option>
                                                    <option value="binh-duong">Bình Dương</option>
                                                    <option value="hai-phong">Hải Phòng</option>
                                                    <option value="da-nang">Đà Nẵng</option>
                                                    <option value="ben-tre">Bến Tre</option>
                                                    <option value="can-tho">Cần Thơ</option>
                                                    <option value="dong-nai">Đồng Nai</option>
                                                    <option value="ba-ria-vung-tau">Bà Rịa Vũng Tàu</option>
                                                    <option value="khanh-hoa">Khánh Hòa</option>
                                                    <option value="long-an">Long An</option>
                                                    <option value="thua-thien-hue">Thừa Thiên Huế</option>
                                                    <option value="an-giang">An Giang</option>
                                                    <option value="bac-lieu">Bạc Liêu</option>
                                                    <option value="bac-can">Bắc Cạn</option>
                                                    <option value="bac-ninh">Bắc Ninh</option>
                                                    <option value="bac-giang">Bắc Giang</option>
                                                    <option value="binh-dinh">Bình Định</option>
                                                    <option value="binh-phuoc">Bình Phước</option>
                                                    <option value="binh-thuan">Bình Thuận</option>
                                                    <option value="ca-mau">Cà Mau</option>
                                                    <option value="cao-bang">Cao Bằng</option>
                                                    <option value="dak-lak">Đắk Lắk</option>
                                                    <option value="dong-thap">Đồng Tháp</option>
                                                    <option value="gia-lai">Gia Lai</option>
                                                    <option value="ha-giang">Hà Giang</option>
                                                    <option value="ha-nam">Hà Nam</option>
                                                    <option value="ha-tinh">Hà Tĩnh</option>
                                                    <option value="hai-duong">Hải Dương</option>
                                                    <option value="hoa-binh">Hòa Bình</option>
                                                    <option value="hung-yen">Hưng Yên</option>
                                                    <option value="kien-giang">Kiên Giang</option>
                                                    <option value="kon-tum">Kon Tum</option>
                                                    <option value="lai-chau">Lai Châu</option>
                                                    <option value="lang-son">Lạng Sơn</option>
                                                    <option value="lao-cai">Lào Cai</option>
                                                    <option value="lam-dong">Lâm Đồng</option>
                                                    <option value="nam-dinh">Nam Định</option>
                                                    <option value="nghe-an">Nghệ An</option>
                                                    <option value="ninh-binh">Ninh Bình</option>
                                                    <option value="ninh-thuan">Ninh Thuận</option>
                                                    <option value="phu-tho">Phú Thọ</option>
                                                    <option value="phu-yen">Phú Yên</option>
                                                    <option value="quang-binh">Quảng Bình</option>
                                                    <option value="quang-nam">Quảng Nam</option>
                                                    <option value="quang-ngai">Quảng Ngãi</option>
                                                    <option value="quang-ninh">Quảng Ninh</option>
                                                    <option value="quang-tri">Quảng Trị</option>
                                                    <option value="soc-trang">Sóc Trăng</option>
                                                    <option value="son-la">Sơn La</option>
                                                    <option value="tay-ninh">Tây Ninh</option>
                                                    <option value="thai-binh">Thái Bình</option>
                                                    <option value="thai-nguyen">Thái Nguyên</option>
                                                    <option value="thanh-hoa">Thanh Hóa</option>
                                                    <option value="tien-giang">Tiền Giang</option>
                                                    <option value="tra-vinh">Trà Vinh</option>
                                                    <option value="tuyen-quang">Tuyên Quang</option>
                                                    <option value="vinh-long">Vĩnh Long</option>
                                                    <option value="vinh-phuc">Vĩnh Phúc</option>
                                                    <option value="yen-bai">Yên Bái</option>
                                                    <option value="dien-bien">Điện Biên</option>
                                                    <option value="dak-nong">Đắk Nông</option>
                                                    <option value="hau-giang">Hậu Giang</option>

                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_huyen" title=""><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1">
                                                        <span class="select2-chosen" id="select2-chosen-8"> Chọn Quận / Huyện </span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen8" class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-8" id="s2id_autogen8"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen8_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-8"
                                                                   id="s2id_autogen8_search"
                                                                   placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-8"></ul>
                                                    </div>
                                                </div>
                                                <select name="huyen" id="huyen" class="styled select2-offscreen"
                                                        title=""
                                                        tabindex="-1">

                                                    <option value=""> Chọn Quận / Huyện</option>
                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_phuong" title=""><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1">
                                                    <span class="select2-chosen"
                                                          id="select2-chosen-9">Chọn Phường / Xã</span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen9" class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-9" id="s2id_autogen9"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen9_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-9"
                                                                   id="s2id_autogen9_search"
                                                                   placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-9"></ul>
                                                    </div>
                                                </div>
                                                <select name="phuong" id="phuong" class="styled select2-offscreen"
                                                        title=""
                                                        tabindex="-1">

                                                    <option value="">Chọn Phường / Xã</option>
                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_duong" title=""><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1">
                                                    <span class="select2-chosen"
                                                          id="select2-chosen-10">Chọn đường</span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen10"
                                                            class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-10" id="s2id_autogen10"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen10_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-10"
                                                                   id="s2id_autogen10_search"
                                                                   placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-10"></ul>
                                                    </div>
                                                </div>
                                                <select name="duong" id="duong" class="styled select2-offscreen"
                                                        title=""
                                                        tabindex="-1">

                                                    <option value="">Chọn đường</option>
                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_loai" title=""><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1">
                                                    <span class="select2-chosen"
                                                          id="select2-chosen-11">Chọn nhu cầu</span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen11"
                                                            class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-11" id="s2id_autogen11"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen11_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-11"
                                                                   id="s2id_autogen11_search"
                                                                   placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-11"></ul>
                                                    </div>
                                                </div>
                                                <select name="loai" id="loai" class="styled select2-offscreen" title=""
                                                        tabindex="-1">

                                                    <option value="">Chọn nhu cầu</option>
                                                    <option value="nha-dat-ban"> Nhà đất bán</option>
                                                    <option value="nha-dat-cho-thue"> Nhà đất cho thuê</option>

                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_ddCat" title=""><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1">
                                                    <span class="select2-chosen"
                                                          id="select2-chosen-12">Chọn hình thức</span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen12"
                                                            class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-12" id="s2id_autogen12"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen12_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-12"
                                                                   id="s2id_autogen12_search"
                                                                   placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-12"></ul>
                                                    </div>
                                                </div>
                                                <select name="ddCat" id="ddCat" class="styled select2-offscreen"
                                                        title=""
                                                        tabindex="-1">

                                                    <option value="">Chọn hình thức</option>
                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_price"><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1">
                                                    <span class="select2-chosen"
                                                          id="select2-chosen-13">Chọn giá</span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen13"
                                                            class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-13" id="s2id_autogen13"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen13_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-13"
                                                                   id="s2id_autogen13_search"
                                                                   placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-13"></ul>
                                                    </div>
                                                </div>
                                                <select id="price" name="price" class="styled select2-offscreen"
                                                        tabindex="-1" title="">
                                                    <option value="">Chọn giá</option>
                                                    <option value="nho-hon-500-trieu"> &lt; 500 triệu</option>
                                                    <option value="800-trieu-den-1-ti">800 - 1 tỉ</option>
                                                    <option value="1-den-3-ti">1-3 tỉ</option>
                                                    <option value="4-den-5-ti">4-5 tỉ</option>
                                                    <option value="6-den-9-ti">6-9 tỉ</option>
                                                    <option value="lon-hon-10-ti"> &gt;10 tỉ</option>
                                                </select>
                                            </li>
                                            <li>
                                                <div class="select2-container styled" id="s2id_dientich"><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1">
                                                    <span class="select2-chosen"
                                                          id="select2-chosen-14"> Chọn diện tích </span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen14"
                                                            class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-14" id="s2id_autogen14"
                                                            tabindex="0">
                                                    <div class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"><label for="s2id_autogen14_search"
                                                                                           class="select2-offscreen"></label>
                                                            <input type="text" autocomplete="off" autocorrect="off"
                                                                   autocapitalize="off" spellcheck="false"
                                                                   class="select2-input" role="combobox"
                                                                   aria-expanded="true" aria-autocomplete="list"
                                                                   aria-owns="select2-results-14"
                                                                   id="s2id_autogen14_search"
                                                                   placeholder=""></div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-14"></ul>
                                                    </div>
                                                </div>
                                                <select name="dientich" id="dientich" class="styled select2-offscreen"
                                                        tabindex="-1" title="">
                                                    <option value=""> Chọn diện tích</option>
                                                    <option value="nho-hon-30-m2"> &lt; 30 m2</option>
                                                    <option value="30-50-m2"> 30 - 50 m2</option>
                                                    <option value="50-80-m2"> 50 - 80 m2</option>
                                                    <option value="80-100-m2"> 80 - 100 m2</option>
                                                    <option value="100-150-m2"> 100 - 150 m2</option>
                                                    <option value="150-200-m2"> 150 - 200 m2</option>
                                                    <option value="200-250-m2"> 200 - 250 m2</option>
                                                    <option value="250-300-m2"> 250 - 300 m2</option>
                                                    <option value="lon-hon-300m2"> &gt; 300 m2</option>
                                                </select>
                                            </li>
                                        </ul>

                                        <div style="text-align: right; padding-top: 7px;">
                                            <input type="hidden" name="guitin" value="guitin">
                                            <input id="timbds" name="timbds" class="btn_Search" type="submit"
                                                   value="Tìm kiếm">
                                        </div>
                                    </form>
                                </div>

                            </div><!-- End .m_sb -->
                        </div>
                        <div class="block_sb" id="street_t">
                            <h4 class="t_sb">
        <span>
			<a title=" Nhà đất TP HCM " href="http://www.bdstphcm.org/ho-chi-minh.html">
			Nhà đất TP HCM
		   </a>
        </span>
                            </h4><!-- End .t_sb -->
                            <div class="m_sb">
                                <div id="content_ndb" class="f_rv mCustomScrollbar _mCS_1">
                                    <div class="mCustomScrollBox mCS-dark-thin" id="mCSB_1"
                                         style="position:relative; height:100%; overflow:hidden; max-width:100%;">
                                        <div class="mCSB_container mCS_no_scrollbar" style="position:relative; top:0;">
                                            <ul>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Bình Chánh"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-binh-chanh.html"> Huyện Bình Chánh</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Cần Giờ"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-can-gio.html"> Huyện Cần Giờ</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Củ Chi"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-cu-chi.html"> Huyện Củ Chi</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Hóc Môn"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-hoc-mon.html"> Huyện Hóc Môn</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Huyện Nhà Bè"
                          href="http://www.bdstphcm.org/ho-chi-minh/huyen-nha-be.html"> Huyện Nhà Bè</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 1"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-1.html"> Quận 1</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 10" href="http://www.bdstphcm.org/ho-chi-minh/quan-10.html"> Quận 10</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 11" href="http://www.bdstphcm.org/ho-chi-minh/quan-11.html"> Quận 11</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 12" href="http://www.bdstphcm.org/ho-chi-minh/quan-12.html"> Quận 12</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 2"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-2.html"> Quận 2</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 3"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-3.html"> Quận 3</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 4"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-4.html"> Quận 4</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 5"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-5.html"> Quận 5</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 6"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-6.html"> Quận 6</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 7"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-7.html"> Quận 7</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 8"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-8.html"> Quận 8</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận 9"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-9.html"> Quận 9</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Bình Tân"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-binh-tan.html"> Quận Bình Tân</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Bình Thạnh"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-binh-thanh.html"> Quận Bình Thạnh</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Gò Vấp"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-go-vap.html"> Quận Gò Vấp</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Phú Nhuận"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-phu-nhuan.html"> Quận Phú Nhuận</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Tân Bình"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-tan-binh.html"> Quận Tân Bình</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Tân Phú"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-tan-phu.html"> Quận Tân Phú</a>
                    </span>
                                                </li>
                                                <li class="clearfix">
                                                    <span class="icon_rv"></span>
                                                    <span class="text_rv">
                       <a title=" Mua bán nhà đất Quận Thủ Đức"
                          href="http://www.bdstphcm.org/ho-chi-minh/quan-thu-duc.html"> Quận Thủ Đức</a>
                    </span>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="mCSB_scrollTools" style="position: absolute; display: none;">
                                            <div class="mCSB_draggerContainer">
                                                <div class="mCSB_dragger" style="position: absolute; top: 0px;"
                                                     oncontextmenu="return false;">
                                                    <div class="mCSB_dragger_bar" style="position:relative;"></div>
                                                </div>
                                                <div class="mCSB_draggerRail"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- End .f_rv -->

                            </div><!-- End .m_sb -->
                        </div>


                        <!-- <div class="block_sb">
                        <h4 class="t_sb">
                            <span>
                                Video giới thiệu
                            </span>
                        </h4>
                        <div class="m_sb">

                            <div class="f_video">


                            <object width="100%" height="170">
                                <param name="movie" value="http://www.youtube.com/v/vURcezzpM2Y?hl=vi_VN&amp;version=3&amp;autoplay=0"></param>
                                <param name="allowFullScreen" value="true"></param>
                                <param name="allowscriptaccess" value="always"></param>
                                <embed src="http://www.youtube.com/v/XaN41AgHB2Q?hl=vi_VN&amp;version=3&amp;autoplay=0" type="application/x-shockwave-flash" width="100%" height="170" allowscriptaccess="always" allowfullscreen="true"></embed>
                            </object>

                            </div>

                        </div>
                    </div> -->


                        <div class="block_sb" id="quangcao_t">
                            <h4 class="t_sb">
            <span>
                Quảng cáo
            </span>
                            </h4><!-- End .t_sb -->
                            <div class="m_sb">

                                <div class="f_adv">

                                    <ul>
                                        <li>
                                            <img src="http://www.bdstphcm.org/images/bds_adv/sliders6.jpg"
                                                 alt="Bản Đồ Quy Hoạch Định Hướng Phát Triển không Gian 2010 - 2020">
                                            <a href="https://www.youtube.com/watch?v=hwsgvaBd0ZY&amp;t=17s"
                                               target="_blank"></a>
                                        </li>
                                        <li>
                                            <img src="http://www.bdstphcm.org/images/bds_adv/sliders4.gif"
                                                 alt="Cho vay tiêu dùng không thuế chấp">
                                            <a href="#" target="_blank"></a>
                                        </li>
                                        <li>
                                            <img src="http://www.bdstphcm.org/images/bds_adv/sliders3.png"
                                                 alt="Samsung">
                                            <a href="" target="_blank"></a>
                                        </li>
                                        <li>
                                            <img src="http://www.bdstphcm.org/images/bds_adv/sliders1.png"
                                                 alt="asd asdasas">
                                            <a href="#" target="_blank"></a>
                                        </li>
                                    </ul>

                                </div><!-- End .f_adv -->

                            </div><!-- End .m_sb -->
                        </div><!-- End .block_sb -->

                        <div class="block_sb">
                            <h4 class="t_sb">

                            </h4><!-- End .t_sb -->
                            <div class="m_sb">
                                <!-- End .m_sb -->
                            </div><!-- End .block_sb -->

                        </div><!-- End .sidebar -->
                    </div><!-- End .f_cont -->

                </div><!-- End .min_wrap -->
            </div><!-- End #container -->

            <footer id="footer">
                <div class="min_wrap">

                    <div class="f_nmg">
                        <h4 class="t_nmg">
                            <a href="http://www.bdstphcm.org/doanh-nghiep.html" title="Nhà môi giới">Nhà môi giới</a>
                        </h4><!-- End .t_nmg -->
                        <div class="m_nmg">

                            <div class="swiper-container3" style="cursor: -webkit-grab;">
                                <div class="swiper-wrapper"
                                     style="padding-left: 0px; padding-right: 0px; transform: translate3d(-245px, 0px, 0px); transition-duration: 0.3s; width: 1950px; height: 80px;">
                                    <div class="swiper-slide">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-kim-oanh.html"
                           title="Công ty cổ phần địa ốc Kim Oanh"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/infos95.jpg"
                                    alt="Công ty cổ phần địa ốc Kim Oanh"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-kim-oanh.html"
                           title="Công ty cổ phần địa ốc Kim Oanh">
                            Công ty cổ phần địa ốc Kim Oanh                        </a>
                        <p>
                        	 Khởi nghiệp từ năm 2006, công ty Địa ốc Kim Oanh đã gặp phải không ít những khó khăn, thử thách của một doanh nghiệp Bất động sản nhỏ                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide swiper-slide-visible swiper-slide-active">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-thang-long.html"
                           title="Công ty cổ phần địa ốc Thăng Long"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/infos96.jpg"
                                    alt="Công ty cổ phần địa ốc Thăng Long"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-thang-long.html"
                           title="Công ty cổ phần địa ốc Thăng Long">
                            Công ty cổ phần địa ốc Thăng Long                        </a>
                        <p>
                        	 Công ty Cổ Phần Địa Ốc Thăng Long được thành lập vào năm 2009. Ngành nghề kinh doanh chủ yếu là đầu tư và phát triển các dự án bất động sản, tư vấn, tiếp thị, môi giới bất động sản, khu resort, du lịch nghỉ dưỡng                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide swiper-slide-visible">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-acb.html"
                           title="Công ty cổ phần địa ốc ACB"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/infos97.jpg"
                                    alt="Công ty cổ phần địa ốc ACB"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-acb.html"
                           title="Công ty cổ phần địa ốc ACB">
                            Công ty cổ phần địa ốc ACB                        </a>
                        <p>
                        	 Chính thức thành lập từ 2001, Công ty CP Địa Ốc ACB (ACBR) liên tục phát triển và khẳng định thương hiệu hàng đầu trong lĩnh vực bất động sản với sự hỗ trợ tích cực từ hệ thống Ngân hàng Á Châu (ACB)                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide swiper-slide-visible">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-sai-gon.html"
                           title="Công ty cổ phần địa ốc Sài Gòn"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/infos98.jpg"
                                    alt="Công ty cổ phần địa ốc Sài Gòn"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-sai-gon.html"
                           title="Công ty cổ phần địa ốc Sài Gòn">
                            Công ty cổ phần địa ốc Sài Gòn                        </a>
                        <p>
                        	 Công ty Cổ phần Địa ốc Sài Gòn – (Viết tắt là SAIGONRES) là doanh nghiệp  cổ phần hóa từ doanh nghiệp nhà nước Công ty Xây dựng Kinh doanh nhà Gia Định thuộc Sở Địa Chính – Nhà Đất Thành phố Hồ Chí Minh theo quyết định số 108/1999/QĐ-TTg ngày 23/04/1999 của Thủ Tướng Chính phủ.                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide swiper-slide-visible">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-thang-loi.html"
                           title="Công ty cổ phần địa ốc Thắng Lợi"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/infos120.png"
                                    alt="Công ty cổ phần địa ốc Thắng Lợi"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-dia-oc-thang-loi.html"
                           title="Công ty cổ phần địa ốc Thắng Lợi">
                            Công ty cổ phần địa ốc Thắng Lợi                        </a>
                        <p>
                        	 Công ty Cổ Phần Địa Ốc Thắng Lợi được thành lập xuất phát từ nhu cầu an cư lạc nghiệp của người dân và đầu tư bất động sản sinh lợi của khách hàng                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-kinh-doanh-dia-oc-him-lam.html"
                           title="Công ty cổ phần kinh doanh địa ốc Him Lam"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/cong-co-phan-kinh-doanh-bat-dong-san-him-lam-423.png"
                                    alt="Công ty cổ phần kinh doanh địa ốc Him Lam"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/cong-ty-co-phan-kinh-doanh-dia-oc-him-lam.html"
                           title="Công ty cổ phần kinh doanh địa ốc Him Lam">
                            Công ty cổ phần kinh doanh địa ốc Him Lam                        </a>
                        <p>
                        	 Công ty cổ phần Him Lam được thành lập từ tháng 9 năm 1994 trên cơ sở chuyển đổi từ Công ty trách nhiệm hữu hạn Thương mại Him Lam. Với 18 năm kinh nghiệm kinh doanh trên thị trường bất động sản                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/tai-sao-nen-ky-gui-nha-dat-ban-cho-thue-quan-go-vap-quan-12-dich-vu-nhan-ky-goi-mua-ban-cho-thue-nha-dat-quan-go-vap-quan-12-tphcm-uy-tin-bdstphcm-org.html"
                           title="TẠI SAO NÊN KÝ GỬI NHÀ ĐẤT BÁN &amp; CHO THUÊ QUẬN GÒ VÂP, QUẬN 12? DỊCH VỤ NHẬN KÝ GỞI - MUA BÁN - CHO THUÊ NHÀ ĐẤT QUẬN GÒ VÂP, QUẬN 12 TPHCM UY TÍN | BDSTPHCM.ORG"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/logo-1-PzIiG.png"
                                    alt="TẠI SAO NÊN KÝ GỬI NHÀ ĐẤT BÁN &amp; CHO THUÊ QUẬN GÒ VÂP, QUẬN 12? DỊCH VỤ NHẬN KÝ GỞI - MUA BÁN - CHO THUÊ NHÀ ĐẤT QUẬN GÒ VÂP, QUẬN 12 TPHCM UY TÍN | BDSTPHCM.ORG"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/tai-sao-nen-ky-gui-nha-dat-ban-cho-thue-quan-go-vap-quan-12-dich-vu-nhan-ky-goi-mua-ban-cho-thue-nha-dat-quan-go-vap-quan-12-tphcm-uy-tin-bdstphcm-org.html"
                           title="TẠI SAO NÊN KÝ GỬI NHÀ ĐẤT BÁN &amp; CHO THUÊ QUẬN GÒ VÂP, QUẬN 12? DỊCH VỤ NHẬN KÝ GỞI - MUA BÁN - CHO THUÊ NHÀ ĐẤT QUẬN GÒ VÂP, QUẬN 12 TPHCM UY TÍN | BDSTPHCM.ORG">
                            TẠI SAO NÊN KÝ GỬI NHÀ ĐẤT BÁN &amp; CHO THUÊ QUẬN GÒ VÂP, QUẬN 12? DỊCH VỤ NHẬN KÝ GỞI - MUA BÁN - CHO THUÊ NHÀ ĐẤT QUẬN GÒ VÂP, QUẬN 12 TPHCM UY TÍN | BDSTPHCM.ORG                        </a>
                        <p>
                        	 Bạn đang có nhà đất cần bán hoặc cho thuê tại khu vực QUẬN GÒ VÂP, QUẬN 12 TP. HCM? Và bạn cần ký gửi nhà đất (bất động sản) đó? Nhưng bạn không biết là công ty bất động sản nào cung cấp dịch vụ nhận ký gửi bất động sản và các dịch vụ khác (mua bán nhà đất, cho thuê nhà đất và nhận ký gởi nhà đất - bất động sản khu vực TPHCM) uy tín, nhanh gọn mà vẫn đảm bảo những tiêu chí bạn đưa ra như: bảo mật thông tin, thời gian ký gửi có hạn, giá phí dịch vụ rẻ.                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->
                                    <div class="swiper-slide">

                    <span class="sws_1">
                        <a href="http://www.bdstphcm.org/bang-gia-dat-quan-go-vap-tp-ho-chi-minh-tu-nam-2015-den-2019.html"
                           title="Bảng giá đất quận Gò Vấp, TP. Hồ Chí Minh Từ Năm 2015 đến 2019"><img
                                    src="http://www.bdstphcm.org/images/bds_dn/bang-gia-dat-tphcm-2015-2019-1-rtntu.jpg"
                                    alt="Bảng giá đất quận Gò Vấp, TP. Hồ Chí Minh Từ Năm 2015 đến 2019"></a>
                    </span><!-- End .sws_1 -->

                                        <span class="sws_2">
                        <a href="http://www.bdstphcm.org/bang-gia-dat-quan-go-vap-tp-ho-chi-minh-tu-nam-2015-den-2019.html"
                           title="Bảng giá đất quận Gò Vấp, TP. Hồ Chí Minh Từ Năm 2015 đến 2019">
                            Bảng giá đất quận Gò Vấp, TP. Hồ Chí Minh Từ Năm 2015 đến 2019                        </a>
                        <p>
                        	 cập nhật bảng giá đất quận Gò Vấp TP. Hồ Chí Minh Từ Năm 2015 đến 2019                        </p>
                    </span><!-- End .sws_2 -->

                                        <div class="clear"></div>

                                    </div><!-- End .swiper-slide -->

                                </div><!-- End .swiper-wrapper -->
                            </div><!-- End .swiper-container3 -->

                            <a class="sc3_left" href="#"></a>
                            <a class="sc3_right" href="#"></a>

                            <div class="clear"></div>

                        </div><!-- End .m_nmg -->
                    </div><!-- End .f_nmg -->

                    <div class="menu_foot">
                        <ul>
                            <li>
                                <a href="http://www.bdstphcm.org/gioi-thieu.html">Giới thiệu</a>
                            </li>
                            <li>
                                <a href="http://www.bdstphcm.org/hop-tac-doi-tac.html">Hợp tác &amp; Đối tác</a>
                            </li>
                            <li>
                                <a href="http://www.bdstphcm.org/huong-dan-dang-tin.html">Hướng dẫn đăng tin</a>
                            </li>
                            <li>
                                <a href="http://www.bdstphcm.org/quy-dinh-dang-tin.html">Quy định đăng tin</a>
                            </li>
                            <li>
                                <a href="http://www.bdstphcm.org/tuyen-dung.html">Tuyển dụng</a>
                            </li>
                            <li>
                                <a href="http://www.bdstphcm.org/lien-he.html">Liên Hệ</a>
                            </li>
                        </ul>
                    </div><!-- End .menu_foot -->

                    <div class="info_foot">
                        Bản quyền © 2017 BẤT ĐỘNG SẢN TP.HCM - All rights reserved<br>
                        <div>
                            Địa chỉ: 778/56. Thống Nhất, Phường 15, Gò Vâp. tp.hcm
                        </div>
                    </div><!-- End .info_foot -->


                </div><!-- End .min_wrap -->
            </footer><!-- End #footer -->

        </div><!-- End #wrapper -->

        <div class="menu_mobile" style="visibility: hidden;">
    <span class="close_menu_mobile">
        <a class="icon_close_menu_mobile" href="javascript:void(0)"></a>
    </span>

            <div class="accordion ui-accordion ui-widget ui-helper-reset ui-accordion-icons" role="tablist">

                <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                   href="http://www.bdstphcm.org/" title="Trang chủ" role="tab" aria-expanded="false"
                   aria-selected="false"
                   tabindex="0"><span class="ui-icon ui-icon-triangle-1-e"></span>Trang chủ</a>
                <div class="m_accordion ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                     role="tabpanel" style="height: 256px; display: none;">
                </div><!-- End .m_accordion -->

                <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                   href="javascript:void(0)" role="tab" aria-expanded="false" aria-selected="false" tabindex="-1"><span
                            class="ui-icon ui-icon-triangle-1-e"></span>Tin nhà đất</a>
                <div class="m_accordion ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                     role="tabpanel" style="height: 256px; display: none;">
                    <div class="accordion ui-accordion ui-widget ui-helper-reset ui-accordion-icons" role="tablist">

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/kien-truc-phong-thuy.html" role="tab" aria-expanded="false"
                           aria-selected="false" tabindex="0"><span class="ui-icon ui-icon-triangle-1-e"></span> Kiến
                            trúc -
                            phong thủy</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/van-ban-phap-luat.html" role="tab" aria-expanded="false"
                           aria-selected="false" tabindex="-1"><span class="ui-icon ui-icon-triangle-1-e"></span> Văn
                            bản
                            pháp luật</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>


                    </div><!-- End .accordion -->
                </div><!-- End .m_accordion -->


                <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                   href="javascript:void(0)" role="tab" aria-expanded="false" aria-selected="false" tabindex="-1"><span
                            class="ui-icon ui-icon-triangle-1-e"></span>Nhà đất bán</a>
                <div class="m_accordion ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                     role="tabpanel" style="height: 256px; display: none;">
                    <div class="accordion ui-accordion ui-widget ui-helper-reset ui-accordion-icons" role="tablist">

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/dat-ban.html" title="Đất bán" role="tab" aria-expanded="false"
                           aria-selected="false" tabindex="0"><span class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Đất
                            bán</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/ban-nha-mat-tien.html" title="Bán nhà mặt tiền" role="tab"
                           aria-expanded="false" aria-selected="false" tabindex="-1"><span
                                    class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Bán nhà mặt
                            tiền</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/ban-nha-trong-hem.html" title="Bán nhà trong hẻm" role="tab"
                           aria-expanded="false" aria-selected="false" tabindex="-1"><span
                                    class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Bán nhà trong
                            hẻm</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/ban-can-ho-chung-cu.html" title="Bán căn hộ, chung cư"
                           role="tab"
                           aria-expanded="false" aria-selected="false" tabindex="-1"><span
                                    class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Bán căn hộ,
                            chung
                            cư</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/khach-san-nha-nghi.html" title="Khách sạn, nhà nghỉ" role="tab"
                           aria-expanded="false" aria-selected="false" tabindex="-1"><span
                                    class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Khách sạn, nhà
                            nghỉ</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/trang-trai-nong-truong.html" title="Trang trại, nông trường"
                           role="tab" aria-expanded="false" aria-selected="false" tabindex="-1"><span
                                    class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Trang trại,
                            nông
                            trường</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/biet-thu-villa-penthouse.html"
                           title="Biệt thự, Villa, Penthouse"
                           role="tab" aria-expanded="false" aria-selected="false" tabindex="-1"><span
                                    class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Biệt thự,
                            Villa,
                            Penthouse</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>


                    </div><!-- End .accordion -->
                </div><!-- End .m_accordion -->


                <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                   href="javascript:void(0)" role="tab" aria-expanded="false" aria-selected="false" tabindex="-1"><span
                            class="ui-icon ui-icon-triangle-1-e"></span>Nhà đất cho thuê</a>
                <div class="m_accordion ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                     role="tabpanel" style="height: 256px; display: none;">
                    <div class="accordion ui-accordion ui-widget ui-helper-reset ui-accordion-icons" role="tablist">

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/cho-thue-dat.html" title="Cho thuê đất" role="tab"
                           aria-expanded="false" aria-selected="false" tabindex="0"><span
                                    class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Cho thuê
                            đất</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/cho-thue-nha-rieng.html" title="Cho thuê nhà riêng" role="tab"
                           aria-expanded="false" aria-selected="false" tabindex="-1"><span
                                    class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Cho thuê nhà
                            riêng</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/cho-thue-van-phong.html" title="Cho thuê văn phòng" role="tab"
                           aria-expanded="false" aria-selected="false" tabindex="-1"><span
                                    class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Cho thuê văn
                            phòng</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/cho-thue-nha-mat-pho.html" title="Cho thuê nhà mặt phố"
                           role="tab"
                           aria-expanded="false" aria-selected="false" tabindex="-1"><span
                                    class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Cho thuê nhà
                            mặt
                            phố</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/cho-thue-can-ho-chung-cu.html" title="Cho thuê căn hộ chung cư"
                           role="tab" aria-expanded="false" aria-selected="false" tabindex="-1"><span
                                    class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Cho thuê căn hộ
                            chung cư</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/cho-thue-nha-tro-phong-tro.html"
                           title="Cho thuê nhà trọ, phòng trọ" role="tab" aria-expanded="false" aria-selected="false"
                           tabindex="-1"><span class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Cho
                            thuê
                            nhà trọ, phòng trọ</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/cho-thue-cua-hang-ki-ot.html" title="Cho thuê cửa hàng, ki ốt"
                           role="tab" aria-expanded="false" aria-selected="false" tabindex="-1"><span
                                    class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Cho thuê cửa
                            hàng,
                            ki ốt</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/cho-thue-nha-hang-khach-san.html"
                           title="Cho thuê nhà hàng, khách sạn" role="tab" aria-expanded="false" aria-selected="false"
                           tabindex="-1"><span class="ui-icon ui-icon-triangle-1-e"></span> &nbsp;&nbsp;&nbsp;&nbsp;Cho
                            thuê
                            nhà hàng, khách sạn</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>


                    </div><!-- End .accordion -->
                </div><!-- End .m_accordion -->


                <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                   href="javascript:void(0)" role="tab" aria-expanded="false" aria-selected="false" tabindex="-1"><span
                            class="ui-icon ui-icon-triangle-1-e"></span>Dự án BĐS</a>
                <div class="m_accordion ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                     role="tabpanel" style="height: 256px; display: none;">
                    <div class="accordion ui-accordion ui-widget ui-helper-reset ui-accordion-icons" role="tablist">

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/cao-oc-van-phong.html" role="tab" aria-expanded="false"
                           aria-selected="false" tabindex="0"><span class="ui-icon ui-icon-triangle-1-e"></span> Cao ốc
                            văn
                            phòng</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/cong-trinh-cong-cong.html" role="tab" aria-expanded="false"
                           aria-selected="false" tabindex="-1"><span class="ui-icon ui-icon-triangle-1-e"></span> Công
                            trình
                            công cộng</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/khu-can-ho.html" role="tab" aria-expanded="false"
                           aria-selected="false" tabindex="-1"><span class="ui-icon ui-icon-triangle-1-e"></span> Khu
                            căn hộ</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                    </div><!-- End .accordion -->
                </div><!-- End .m_accordion -->

                <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                   href="http://www.bdstphcm.org/video.html" role="tab" aria-expanded="false" aria-selected="false"
                   tabindex="-1"><span class="ui-icon ui-icon-triangle-1-e"></span>Video dự án</a>
                <div class="m_accordion ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                     role="tabpanel" style="height: 256px; display: none;">
                </div><!-- End .m_accordion -->

                <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                   href="javascript:void(0)" role="tab" aria-expanded="false" aria-selected="false" tabindex="-1"><span
                            class="ui-icon ui-icon-triangle-1-e"></span>Nhà môi giới</a>
                <div class="m_accordion ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                     role="tabpanel" style="height: 256px; display: none;">
                    <div class="accordion ui-accordion ui-widget ui-helper-reset ui-accordion-icons" role="tablist">

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/moi-gioi-dia-oc.html" role="tab" aria-expanded="false"
                           aria-selected="false" tabindex="0"><span class="ui-icon ui-icon-triangle-1-e"></span>-:- Môi
                            giới
                            địa ốc</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                        <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                           href="http://www.bdstphcm.org/vat-lieu-xay-dung.html" role="tab" aria-expanded="false"
                           aria-selected="false" tabindex="-1"><span class="ui-icon ui-icon-triangle-1-e"></span>-:- Vật
                            liệu xây dựng</a>
                        <ul class="ul_accordion_1 ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                            role="tabpanel" style="height: 0px; display: none;">
                        </ul>

                    </div><!-- End .accordion -->
                </div><!-- End .m_accordion -->

                <a class="t_accordion ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"
                   href="http://www.bdstphcm.org/lien-he.html" title="Liên hệ" role="tab" aria-expanded="false"
                   aria-selected="false" tabindex="-1"><span class="ui-icon ui-icon-triangle-1-e"></span>Liên hệ</a>
                <div class="m_accordion ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
                     role="tabpanel" style="height: 256px; display: none;">
                </div><!-- End .m_accordion -->

            </div><!-- End #accordion -->

        </div><!-- End .menu_mobile -->

        <link rel="stylesheet" type="text/css" href="http://www.bdstphcm.org/scripts/swiper/idangerous.swiper.css">
        <script type="text/javascript" src="http://www.bdstphcm.org/scripts/swiper/idangerous.swiper.min.js"></script>
        <script type="text/javascript"
                src="http://www.bdstphcm.org/scripts/jquery.customSelect-master/jquery.customSelect.min.js"></script>
        <link rel="stylesheet" type="text/css" href="scripts/royalslider/assets/royalslider/royalslider.css">
        <link rel="stylesheet" type="text/css"
              href="scripts/royalslider/assets/royalslider/skins/minimal-white/rs-minimal-white.css">
        <script type="text/javascript"
                src="http://www.bdstphcm.org/scripts/royalslider/assets/royalslider/jquery.royalslider.min.js"></script>
        <link rel="stylesheet" type="text/css"
              href="http://www.bdstphcm.org/scripts/custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
        <script type="text/javascript"
                src="http://www.bdstphcm.org/scripts/custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="http://www.bdstphcm.org/scripts/frame_script.js"></script>


    </div>
    <div id="_atssh"
         style="visibility: hidden; height: 1px; width: 1px; position: absolute; top: -9999px; z-index: 100000;">
        <iframe id="_atssh749" title="AddThis utility frame"
                style="height: 1px; width: 1px; position: absolute; top: 0px; z-index: 100000; border: 0px; left: 0px;"
                src="http://s7.addthis.com/static/sh.e4e8af4de595fdb10ec1459d.html#rand=0.8177128819484389&amp;iit=1542252382494&amp;tmr=load%3D1542252381810%26core%3D1542252381929%26main%3D1542252382484%26ifr%3D1542252382501&amp;cb=0&amp;cdn=0&amp;md=0&amp;kw=ph%C3%A2n%20kh%C3%BAc%20c%C4%83n%20h%E1%BB%99%20cao%20c%E1%BA%A5p%2Cb%E1%BA%A5t%20%C4%91%E1%BB%99ng%20s%E1%BA%A3n%20cao%20c%E1%BA%A5p%2Cth%E1%BB%8B%20tr%C6%B0%E1%BB%9Dng%20b%E1%BA%A5t%20%C4%91%E1%BB%99ng%20s%E1%BA%A3n%2CT%E1%BA%ADp%20%C4%91o%C3%A0n%20Vingroup%2Cgiao%20d%E1%BB%8Bch%20b%E1%BA%A5t%20%C4%91%E1%BB%99ng%20s%E1%BA%A3n&amp;ab=-&amp;dh=www.bdstphcm.org&amp;dr=&amp;du=http%3A%2F%2Fwww.bdstphcm.org%2Fkhach-hang-bat-dau-quan-tam-den-nhung-can-ho-cao-cap.html&amp;href=http%3A%2F%2Fwww.bdstphcm.org%2Fkhach-hang-bat-dau-quan-tam-den-nhung-can-ho-cao-cap.html&amp;dt=kh%C3%A1ch%20h%C3%A0ng%20b%E1%BA%AFt%20%C4%91%E1%BA%A7u%20quan%20t%C3%A2m%20%C4%91%E1%BA%BFn%20nh%E1%BB%AFng%20c%C4%83n%20h%E1%BB%99%20cao%20c%E1%BA%A5p&amp;dbg=0&amp;cap=tc%3D0%26ab%3D0&amp;inst=1&amp;jsl=1&amp;prod=undefined&amp;lng=en&amp;ogt=image%2Cimage%2Ctype%3Darticle%2Csite_name%2Curl%2Cdescription%2Ctitle&amp;pc=men&amp;pub=ra-52493e8d684da4cc&amp;ssl=0&amp;sid=5bece75daab56cee&amp;srf=0.01&amp;ver=300&amp;xck=0&amp;xtr=0&amp;og=locale%3Dvi_VN%26title%3Dkh%25C3%25A1ch%2520h%25C3%25A0ng%2520b%25E1%25BA%25AFt%2520%25C4%2591%25E1%25BA%25A7u%2520quan%2520t%25C3%25A2m%2520%25C4%2591%25E1%25BA%25BFn%2520nh%25E1%25BB%25AFng%2520c%25C4%2583n%2520h%25E1%25BB%2599%2520cao%2520c%25E1%25BA%25A5p%26description%3DN%25E1%25BA%25BFu%2520nh%25C6%25B0%2520n%25C3%25B3i%2520c%25C3%25A1ch%2520%25C4%2591%25C3%25A2y%2520kho%25E1%25BA%25A3ng%25202%2520n%25C4%2583m%2520tr%25E1%25BB%259F%2520l%25E1%25BA%25A1i%2520th%25C3%25AC%2520th%25E1%25BB%258B%2520tr%25C6%25B0%25E1%25BB%259Dng%2520c%25C3%25A1c%2520chung%2520c%25C6%25B0%2520c%25C3%25B3%2520c%25C4%2583n%2520h%25E1%25BB%2599%2520cao%2520c%25E1%25BA%25A5p%2520%25C3%25ADt%2520%25C4%2591%25C6%25B0%25E1%25BB%25A3c%2520kh%25C3%25A1c%2520h%25C3%25A0ng%2520quan%2520t%25C3%25A2m%2520th%25C3%25AC%2520gi%25E1%25BB%259D%2520%25C4%2591%25C3%25A2y%2520theo%2520m%25E1%25BB%2599t%2520con%2520s%25E1%25BB%2591%2520m%25E1%25BB%259Bi%2520th%25E1%25BB%2591ng%2520k%25C3%25AA%2520cho%2520th%25E1%25BA%25A5y%2520hi%25E1%25BB%2587n%2520t%25E1%25BA%25A1i%2520c%25C3%25B3%2520r%25E1%25BA%25A5t%2520nhi%25E1%25BB%2581u%2520ng%25C6%25B0%25E1%25BB%259Di%2520d%25C3%25A2n%2520%25C4%2591%25E1%25BA%25BFn%2520h%25E1%25BB%258Fi%2520mua%2520c%25C3%25A1c%2520c%25C4%2583n%2520h%25E1%25BB%2599%2520cao%2520c%25E1%25BA%25A5p%2520v%25E1%25BB%259Bi%2520%25C3%25BD%2520%25C4%2591%25E1%25BB%258Bnh%2520s%25E1%25BB%2591ng%2520l%25C3%25A2u%2520d%25C3%25A0i.%26url%3Dhttp%253A%252F%252Fbdstphcm.com%252Fkhach-hang-bat-dau-quan-tam-den-nhung-can-ho-cao-cap.html%26site_name%3DB%25E1%25BA%25A5t%2520%25C4%2590%25E1%25BB%2599ng%2520S%25E1%25BA%25A3n%2520TP%2520HCM%26type%3Darticle%26image%3Dhttp%253A%252F%252Fwww.bdstphcm.org%252Fimgs%252Fnoimage.png&amp;csi=undefined&amp;rev=v8.3.32-wp&amp;ct=1&amp;xld=1&amp;xd=1"></iframe>
    </div>
    <style id="service-icons-0"></style>
    <div id="topcontrol" style="z-index: 999; position: fixed; bottom: 60px; right: 20px; opacity: 0; cursor: pointer;"
         title="Scroll Back to Top"><img src="http://www.kenhcantho.com/imgs/layout/back_to_top.png"
                                         style="width:25px; height:25px; position:relative; z-index:999;"></div>
    <span role="status" aria-live="polite" class="select2-hidden-accessible"></span><span role="status"
                                                                                          aria-live="polite"
                                                                                          class="select2-hidden-accessible"></span>
    <span
            role="status" aria-live="polite" class="select2-hidden-accessible"></span><span role="status"
                                                                                            aria-live="polite"
                                                                                            class="select2-hidden-accessible"></span>
    <span
            role="status" aria-live="polite" class="select2-hidden-accessible"></span><span role="status"
                                                                                            aria-live="polite"
                                                                                            class="select2-hidden-accessible"></span>
    <span
            role="status" aria-live="polite" class="select2-hidden-accessible"></span><span role="status"
                                                                                            aria-live="polite"
                                                                                            class="select2-hidden-accessible"></span>

@stop