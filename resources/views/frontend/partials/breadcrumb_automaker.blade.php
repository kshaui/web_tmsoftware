<div class="map-link hidden-xs cbreadcrumb">
    <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a class="" href="/" itemprop="item"><span
                        itemprop="name">Trang chủ &gt;</span></a>
        </li>
        @if(isset($pageOption))
            @if($pageOption['type'] != 'page')
                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                    @endif
                    <a href="{{ URL::to(@$pageOption['parentUrl']) }}">
                        <span itemprop="name">{{ @$pageOption['pageName'] }}</span>
                    </a>
                </li>
            @endif
    </ul>
</div>