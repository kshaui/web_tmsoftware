<div class="breadcrumb"><span class="root"><a
                rel="v:url" property="v:title"
                href="/">Trang chủ</a></span>

    @if(isset($pageOption))
        @if($pageOption['type'] != 'page')
            <span><i class="ribbon-icon icon-angle-double-right"></i></span>
            <span typeof="v:Breadcrumb">
                <a href="{{ URL::to('/danh-muc/'.@$pageOption['parentUrl']) }}" rel="v:url"
                   property="v:title">{{ @$pageOption['parentName'] }}</a>
            </span>
        @endif
        <span><i class="ribbon-icon icon-angle-double-right"></i></span><span><span>{{ $post->name }}</span></span>
    @endif
</div>