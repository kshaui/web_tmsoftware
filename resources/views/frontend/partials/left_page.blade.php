<div class="col-left">
    <div class="box-filter box-fill">
        <div class="tab-content">
            <div id="xe-cu" class="tab-pane fade in active">
                <form method="get" action="tim-kiem" enctype="multipart/form-data">
                    <input type="hidden" value="1" id="auto_number">
                    <div class="sl-filter">
                        @php
                            $data = CommonHelper::getFromCache('all_manufacturer_pluck');
                             if (!$data) {
                                 $data = \App\Models\Manufacturer::pluck('name', 'id');
                                 CommonHelper::putToCache('all_manufacturer_pluck', $data);
                             }
                          /**manufacturer model get*/
                         if(isset($_GET['manufacturer_model_id']) && $_GET['manufacturer_model_id'] != '' ) {
                            $manufacturer_id = \App\Http\Helpers\CommonHelper::getFromCache('get_manufacturermodel_by_manufacturer_model_id_first_'. $_GET['manufacturer_model_id']);
                            if (!$manufacturer_id){
                                $manufacturer_id = \App\Models\ManufacturerModel::select('id','name','manufacturer_id')->where('id',$_GET['manufacturer_model_id'])->first();
                                \App\Http\Helpers\CommonHelper::putToCache('get_manufacturermodel_by_manufacturer_model_id_first_'.$_GET['manufacturer_model_id'],$manufacturer_id);
                            }else{
                                $manufacturer_id = [];
                            }
                        }
                        @endphp
                        <select name="manufacturer_id" id="manufacturer"
                                class="styled select2-offscreen" title=""
                                tabindex="-1">
                            <option value="">Hãng sản xuất</option>
                            @foreach($data as $id => $name)
                                <option value="{{ $id }}"
                                        @if(@$_GET['manufacturer_id'] == $id || @$manufacturer_id->manufacturer_id == $id) selected @endif> {{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="sl-filter">
                        @php
                            if(isset($_GET['manufacturer_id']) && $_GET['manufacturer_id'] != '' ) {
                                $data = CommonHelper::getFromCache('get_manufacturermodel_by_manufacturer_id_pluck_' . $_GET['manufacturer_id']);
                                if (!$data) {
                                    $data = \App\Models\ManufacturerModel::select(['id', 'name'])
                                    ->where('manufacturer_id', @$_GET['manufacturer_id'])
                                    ->pluck('name', 'id');
                                    CommonHelper::putToCache('get_manufacturermodel_by_manufacturer_id_pluck_' . $_GET['manufacturer_id'], $data);
                                }
                            } else {
                                $data = [];}
                        /**manufacturer model get*/
                         if(isset($_GET['manufacturer_model_id']) && $_GET['manufacturer_model_id'] != '' ) {
                            $manufacturer_model_id = CommonHelper::getFromCache('get_manufacturermodel_by_manufacturer_model_id_first_'. $_GET['manufacturer_model_id']);
                            if (!$manufacturer_model_id){
                                $manufacturer_model_id = \App\Models\ManufacturerModel::select('id','name','manufacturer_id')->where('id',$_GET['manufacturer_model_id'])->first();
                                CommonHelper::putToCache('get_manufacturermodel_by_manufacturer_model_id_first_'.$_GET['manufacturer_model_id'],$manufacturer_model_id);
                            }
                            $manufacturer_model = CommonHelper::getFromCache('manufacturermodel_by_manufacturer_id_pluck_'. $_GET['manufacturer_model_id']);
                            if (!$manufacturer_model){
                                $manufacturer_model = \App\Models\ManufacturerModel::select('id','name')->where('manufacturer_id',@$manufacturer_model_id->manufacturer_id)->pluck('name','id');
                                CommonHelper::putToCache('manufacturermodel_by_manufacturer_id_pluck'.$_GET['manufacturer_model_id'],$manufacturer_model);
                            }else{
                                $manufacturer_model= [];
                            }
                        }
                        @endphp
                        <select name="manufacturer_model_id" id="model" class="styled select2-offscreen"
                                title=""
                                tabindex="-1">
                            <option value="">Model</option>
                            @if(@$_GET['manufacturer_model_id'] != '')
                                @foreach(@$manufacturer_model as $id => $name)
                                    <option value="{{ $id }}"
                                            @if(@$_GET['manufacturer_model_id'] == @$id) selected @endif>
                                        {{ @$name }}
                                    </option>
                                @endforeach
                            @endif
                            @if(@$_GET['manufacturer_id'])
                                @foreach(@$data as $id => $name)
                                    <option value="{{ $id }}">
                                        {{ @$name }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        <input type="text" id="slm" value="" class="hidden">
                    </div>
                    <script>
                        $(document).ready(function () {
                            $("#manufacturer").select2();
                            $("#model").select2();
                            $('select[name=manufacturer_id]').change(function () {
                                console.log('manufacturer_model_id');
                                loading();
                                $.ajax({
                                    url: '/ajax-get-model',
                                    type: 'GET',
                                    data: {
                                        manufacturer_id: $(this).val()
                                    },
                                    success: function (resp) {
                                        $('select[name=manufacturer_model_id]').html(resp);
                                        stopLoading();
                                    },
                                    error: function () {
                                        stopLoading();
                                        alert('Có lỗi xảy ra. Vui lòng load lại trang và thử lại!');
                                    }
                                })
                            });
                        });
                    </script>
                    <div class="sl-filter">
                        <div class="col-xs-6 col-sm-6 p0">
                            <label class="lbinput" style="color: #fff;">
                                <div class="icheckbox_square-blue" style="position: relative;"><input
                                            type="checkbox" name="tinhtrangxe"
                                            @if(@$_GET['tinhtrangxe'] == 'Xe cũ') checked @endif class="icheck "
                                            value="Xe cũ"
                                            style="position: absolute; opacity: 0;">
                                    <ins class="iCheck-helper"
                                         style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div>
                                Xe cũ
                            </label>
                        </div>
                        <div class="col-xs-6 col-sm-6 p0">
                            <label class="lbinput" style="color: #fff;">
                                <div class="icheckbox_square-blue" style="position: relative;"><input
                                            type="checkbox" name="tinhtrangxe"
                                            @if(@$_GET['tinhtrangxe'] == 'Xe mới') checked @endif  class="icheck"
                                            value="Xe mới"
                                            style="position: absolute; opacity: 0;">
                                    <ins class="iCheck-helper"
                                         style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div>
                                Xe mới
                            </label>
                        </div>
                    </div>
                    <div class="row m0">
                        <div class="title">Giá bán</div>
                        <div class="col-xs-6 col-sm-6 col-md-6 p0 pr3">
                            <div class="sl-filter">
                                <select class="form-control"
                                        id="sl-from-gia" name="min_price" tabindex="-1"
                                        aria-hidden="true">
                                    <option value="" selected="">Từ</option>
                                    <option value="100000000" @if(@$_GET['min_price'] ==100000000 ) selected @endif >100
                                        triệu
                                    </option>
                                    <option value="200000000" @if(@$_GET['min_price'] ==200000000 ) selected @endif >200
                                        triệu
                                    </option>
                                    <option value="300000000" @if(@$_GET['min_price'] ==300000000 ) selected @endif >300
                                        triệu
                                    </option>
                                    <option value="400000000" @if(@$_GET['min_price'] ==400000000 ) selected @endif >400
                                        triệu
                                    </option>
                                    <option value="500000000" @if(@$_GET['min_price'] ==500000000 ) selected @endif >500
                                        triệu
                                    </option>
                                    <option value="600000000" @if(@$_GET['min_price'] ==600000000 ) selected @endif >600
                                        triệu
                                    </option>
                                    <option value="700000000" @if(@$_GET['min_price'] ==700000000 ) selected @endif >700
                                        triệu
                                    </option>
                                    <option value="800000000" @if(@$_GET['min_price'] ==800000000 ) selected @endif >800
                                        triệu
                                    </option>
                                    <option value="1000000000" @if(@$_GET['min_price'] ==1000000000 ) selected @endif >1
                                        tỷ
                                    </option>
                                    <option value="1200000000" @if(@$_GET['min_price'] ==1200000000 ) selected @endif >
                                        1.2 tỷ
                                    </option>
                                    <option value="1400000000" @if(@$_GET['min_price'] ==1400000000 ) selected @endif >
                                        1.4 tỷ
                                    </option>
                                    <option value="1600000000" @if(@$_GET['min_price'] ==1600000000 ) selected @endif >
                                        1.6 tỷ
                                    </option>
                                    <option value="2000000000" @if(@$_GET['min_price'] ==2000000000 ) selected @endif >2
                                        tỷ
                                    </option>
                                    <option value="3000000000" @if(@$_GET['min_price'] ==3000000000 ) selected @endif>3
                                        tỷ
                                    </option>
                                    <option value="5000000000" @if(@$_GET['min_price'] ==5000000000 ) selected @endif>5
                                        tỷ
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 p0 pl3">
                            <div class="sl-filter">
                                <select class="form-control "
                                        id="sl-to-gia" name="max_price" tabindex="-1" aria-hidden="true">
                                    <option value="" selected="">Đến</option>
                                    <option value="100000000" @if(@$_GET['max_price'] ==100000000 ) selected @endif >100
                                        triệu
                                    </option>
                                    <option value="200000000" @if(@$_GET['max_price'] ==200000000 ) selected @endif >200
                                        triệu
                                    </option>
                                    <option value="300000000" @if(@$_GET['max_price'] ==300000000 ) selected @endif >300
                                        triệu
                                    </option>
                                    <option value="400000000" @if(@$_GET['max_price'] ==400000000 ) selected @endif >400
                                        triệu
                                    </option>
                                    <option value="500000000" @if(@$_GET['max_price'] ==500000000 ) selected @endif >500
                                        triệu
                                    </option>
                                    <option value="600000000" @if(@$_GET['max_price'] ==600000000 ) selected @endif >600
                                        triệu
                                    </option>
                                    <option value="700000000" @if(@$_GET['max_price'] ==700000000 ) selected @endif >700
                                        triệu
                                    </option>
                                    <option value="800000000" @if(@$_GET['max_price'] ==800000000 ) selected @endif >800
                                        triệu
                                    </option>
                                    <option value="1000000000" @if(@$_GET['max_price'] ==1000000000 ) selected @endif >1
                                        tỷ
                                    </option>
                                    <option value="1200000000" @if(@$_GET['max_price'] ==1200000000 ) selected @endif >
                                        1.2 tỷ
                                    </option>
                                    <option value="1400000000" @if(@$_GET['max_price'] ==1400000000 ) selected @endif >
                                        1.4 tỷ
                                    </option>
                                    <option value="1600000000" @if(@$_GET['max_price'] ==1600000000 ) selected @endif >
                                        1.6 tỷ
                                    </option>
                                    <option value="2000000000" @if(@$_GET['max_price'] ==2000000000 ) selected @endif >2
                                        tỷ
                                    </option>
                                    <option value="3000000000" @if(@$_GET['max_price'] ==3000000000 ) selected @endif>3
                                        tỷ
                                    </option>
                                    <option value="5000000000" @if(@$_GET['max_price'] ==5000000000 ) selected @endif>5
                                        tỷ
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="hidden more-search">
                        <div class="sl-filter">
                            @php
                                $data = CommonHelper::getFromCache('all_province_pluck');

                            if (!$data) {
                            $data = \App\Models\Province::select(['id', 'name'])->pluck('name', 'id');
                            CommonHelper::putToCache('all_province_pluck', $data);
                            }
                            @endphp
                            <select class="form-control bantinh-vali" id="bantaitinh" name="province_id">
                                <option value="">Chọn thành phố</option>
                                @foreach($data as $id => $name)
                                    <option value="{{ $id }}"
                                            @if(@$_GET['province_id'] == @$id) selected @endif>{{ @$name }}</option>
                                @endforeach
                            </select>
                            <p class="text-red mt10 mb0 font12 hidden error_submit"></p>
                        </div>
                        <div class="row m0">
                            <div class="title">Năm sản xuất</div>
                            <div class="col-xs-6 col-sm-6 col-md-6 p0 pr3">
                                <div class="sl-filter">
                                    <select class="select2 form-control select2-hidden-accessible"
                                            id="sl-from-nam" name="year_from" tabindex="-1"
                                            aria-hidden="true">
                                        <option value="" selected="">Từ</option>
                                        @for($i = 0; $i <= 30; $i ++)
                                            <option value="{{ date("Y",strtotime("-".$i." year")) }}"
                                                    @if(@$_GET['year_from'] == date("Y",strtotime("-".$i." year")) ) selected @endif>{{ date("Y",strtotime("-".$i." year")) }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 p0 pl3">
                                <div class="sl-filter">
                                    <select class="select2 form-control select2-hidden-accessible"
                                            id="sl-to-nam" name="year_to" tabindex="-1"
                                            aria-hidden="true">
                                        <option value="" selected="">Đến</option>
                                        @for($i = 0; $i <= 30; $i ++)
                                            <option value="{{ date("Y",strtotime("-".$i." year")) }}"
                                                    @if(@$_GET['year_to'] == date("Y",strtotime("-".$i." year")) ) selected @endif>{{ date("Y",strtotime("-".$i." year")) }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="sl-filter">
                            @php
                                $data = CommonHelper::getFromCache('all_producttype_pluck');
                                 if (!$data) {
                                     $data = \App\Models\ProductType::select(['id', 'name'])->pluck('name', 'id');
                                     CommonHelper::putToCache('all_producttype_pluck', $data);
                                 }
                            @endphp
                            <select class="form-control " id="dongxe-banxe"
                                    name="product_type">
                                <option value="">Chọn loại xe</option>
                                @foreach($data as $id => $name)
                                    <option value="{{$id}}"
                                            @if(@$_GET['product_type'] == @$id) selected @endif>{{$name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-orange-48 mt15">
                        <i class="icon-sprite-24 icon-search-white"></i> Tìm Kiếm
                    </button>
                    <div class="txc mt10">
                        <a class="tk-nang-cao" href="#">Tìm kiếm nâng cao »</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="quick-filter mt15 hidden-xs">
        <div class="title-bg">Lọc nhanh theo loại xe</div>
        <div class="list-loai-xe">
            @php
                $data = CommonHelper::getFromCache('producttype_limit_9');
                 if (!$data) {
                     $data = \App\Models\ProductType::select(['id', 'name','image'])->limit(9)->get();
                     CommonHelper::putToCache('producttype_limit_9', $data);
                 }
            @endphp
            <div class="row m0">
                @foreach($data as $item)
                    <div class="col-xs-4 col-sm-4 col-md-4 p0">
                        <a class="loai-xe" href=" {{ URL::to('/tim-kiem?product_type='.$item->id) }} ">
                            <img src="{{ asset('public/filemanager/userfiles/'.$item->image) }}"
                                 alt="{{ $item->name }}">
                            {{ $item->name }}
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="box-left hidden-xs">
        <ul class="list-tit">
            <li class="tit-mua-ban">Bán
                <ul class="list-link">
                    <li><a class="" href="{{action('Frontend\ProductController@getNewProduct')}}">Đăng tin bán ô tô</a>
                    </li>
                    <li><a class="" href="{{ URL::to('/xe-dang-rao-ban') }}">Xe đang rao bán</a>
                    </li>
                    <li><a class="" href="{{ URL::to('/xe-da-ban') }}">Xe đã bán</a></li>
                    <li><a class="" href="{{action('Frontend\ProductController@getPending')}}">Xe chờ duyệt </a>
                    </li>
                </ul>
            </li>
            <li class="tit-mua-ban">Mua
                <ul class="list-link">
                    <li><a class="" href="{{ URL::to('/tim-kiem?tinhtrangxe='.'Xe mới') }}">Mua Ô tô mới</a></li>
                    <li><a class="" href="{{ URL::to('/tim-kiem?tinhtrangxe='.'Xe cũ') }}">Mua Xe cũ</a></li>
                    <li><a class="" href="{{ URL::to('/tim-kiem?dai-ly') }}">Tìm kiếm đại lý</a></li>
                    <li><a class="" href="{{action('Frontend\ProductController@getCarLove')}}">Xe yêu thích</a></li>
                </ul>
            </li>
        </ul>
    </div>
    @include('frontend.partials.sidebar')
    @include('frontend.partials.banner')
</div>