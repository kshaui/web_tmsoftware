<div class="slider-salon hidden-xs">
    <div class="container">
        <div class="title-white">NGƯỜI BÁN Ô TÔ NỔI BẬT</div>
        <div class="relative">
            <div class="slider-salon-noi slider mt15 slick-initialized slick-slider">
                <div aria-live="polite" class="slick-list draggable">
                    @php
                        $data = CommonHelper::getFromCache('user_show_footer_homepage_1_limit_6');
                        if (!$data) {
                             $data = \App\Models\User::select(['id','name', 'image','tel','address','show_footer_homepage'])->where('status', 1)->where('show_footer_homepage',1)->limit(6)->get();
                            CommonHelper::putToCache('user_show_footer_homepage_1_limit_6', $data);
                        }
                    @endphp
                    <div class="slick-track" role="listbox"
                         style="opacity: 1; width: 30000px; transform: translate3d(0px, 0px, 0px);">
                        @foreach($data as $item)
                            <div class="slider-item slick-slide slick-current slick-active" data-slick-index="0"
                                 aria-hidden="false" tabindex="-1" role="option"
                                 aria-describedby="slick-slide00">
                                <a class="salon-info" href="{{ URL::to('tim-kiem/?user_id='. $item->id) }}"
                                   tabindex="{{ $item->order_no }}">
                                    <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, 175, 118) }}"
                                         width="175" height="118">
                                    <div class="name-salon">{{ $item->name }}</div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="bg-blur"></div>
        </div>
    </div>
</div>