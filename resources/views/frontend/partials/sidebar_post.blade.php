    <aside class="sidebar c-4-12">
        <div id="sidebars" class="sidebar">
            <div class="sidebar_list">
                <div id="search-2" class="widget widget_search">
                    <form method="get" id="searchform" class="search-form" action="<?php echo e(URL::to('/tim-kiem-tin-tuc')); ?>" _lpchecked="1">
                        <fieldset>
                            <input type="text" name="key" id="s" placeholder="Tìm kiếm trong trang này"
                                   onblur="if (this.value == '') {this.value = 'Search this site...';}"
                                   onfocus="if (this.value == 'Search this site...') {this.value = '';}" value="<?= @$_GET['key'] ?>">
                            <input type="submit" value="Tìm kiếm">
                        </fieldset>
                    </form>
                </div>
                <div id="categories-2" class="widget widget_categories">
                    @php
                        $data = CommonHelper::getFromCache('all_category');
                         if (!$data) {
                             $data = \App\Models\Category::select(['name', 'slug','intro'])->where('status', 1)->get();
                             CommonHelper::putToCache('all_category', $data);
                         }
                    @endphp
                    <h3 class="widget-title">Danh mục</h3>
                    <ul>
                        @foreach($data as $item)
                            <li class="cat-item cat-item-3">
                                <a href="{{ URL::to('danh-muc/'. $item->slug) }}"
                                   title="{{$item->intro }}">
                                    {{$item->name}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div id="recent-posts-2" class="widget widget_recent_entries">
                    @php
                        $data = CommonHelper::getFromCache('post_is_new_post_1');
                         if (!$data) {
                             $data = \App\Models\Post::select(['name', 'slug','is_new_post'])->where('status', 1)->where('is_new_post',1)->get();
                             CommonHelper::putToCache('post_is_new_post_1', $data);
                         }
                    @endphp
                    <h3 class="widget-title">Bài mới nhất</h3>
                    <ul>
                        @foreach($data as $item)
                            <li>
                                <a href="{{ URL::to('/blog/'. $item->slug) }}">
                                    {{ $item-> name }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        @include('frontend.partials.banner_blog')
    </aside>