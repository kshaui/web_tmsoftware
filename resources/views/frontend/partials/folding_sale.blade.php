<div class="deal table-xe" id="block-2nd">
    <div class="title-underline">
        Bán gấp
    </div>
    <a class="txt-see-all hidden-xs" href="/tim-kiem?folding_sale=1">Xem tất cả xe bán gấp »</a>
    <table class="table list-news">
        <tbody>
        @php
            $data = CommonHelper::getFromCache('product_dangban_folding_sale_1_paginate');
             if (!$data) {
                 $data = \App\Models\Product::whereIn('pending', ['Đang bán'])->where('folding_sale', 1)->orderBy('updated_at', 'desc')->paginate(20);
                 CommonHelper::putToCache('product_dangban_folding_sale_1_paginate', $data);
             }
        @endphp
        @include('frontend.childs.product.show_item')
        </tbody>
    </table>
    {!! $data->links() !!}
</div>