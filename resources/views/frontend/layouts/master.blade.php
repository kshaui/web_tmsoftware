<html lang="">
<head>
    @include('frontend.partials.head')
</head>
<body class="bd-home-index">
@include('frontend.partials.header')

<div class="header-mobile">
    <nav class="nav-mobile">
        <div class="toggle-nav">
            <span></span><span></span><span></span>
        </div>
        <div class="icon-frm">
            <a href="#">
            <i class="icon-sprite-24 icon-dky"></i>
            <span class="caret"></span>
            </a>
        </div>
        {{--<a class="btn-open-menu" data-toggle="modal" data-target="#menu_right">
            <i class="icon-sprite-24 icon-bars"></i>
        </a>--}}
        <a class="logo" href="/"></a>
        {{--<a class="icon-right" data-toggle="modal" data-target="#menu_right">--}}
            {{--<i class="icon-sprite-24 icon-dky"></i>--}}
            {{--<span class="caret"></span>--}}
        {{--</a>--}}
    </nav>
    <!-- Modal Menu Right -->
    @if(Auth::user())
        <div class="modal right fade modal-menu modal-menu-right" id="menu_right" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="row m0">
                        <div class="col-xs-12 p0">
                            <a class="a-dky" href="{{ action('Frontend\AccountController@getAccount') }}">
                                <i class="icon-sprite-24 icon-dky"></i>{{@Auth::user()->name}}</a>
                        </div>
                    </div>
                    <div class="modal-header">
                        <ul class="list-menu-mb">
                            <li><a class="" href="{{ action('Frontend\AccountController@getAccount') }}">Quản lý Tài
                                </a></li>
                        </ul>
                    </div>
                    <div class="modal-header">
                        <ul class="list-menu-mb">
                            <li><a class="" href="{{ action('Frontend\ProductController@getNewProduct') }}">Đăng tin bán
                                    ô tô <span
                                            class="txt-num"></span></a></li>
                            <li><a class="" href="{{ URL::to('/xe-dang-rao-ban') }}">Xe đang rao bán <span
                                            class="txt-num"></span></a></li>
                            <li><a class="" href="{{ URL::to('/xe-da-ban') }}">Xe đã bán <span
                                            class="txt-num"></span></a></li>
                            <li><a class="" href="{{action('Frontend\ProductController@getPending')}}">Xe chờ duyệt <span
                                            class="txt-num"></span></a></li>
                        </ul>
                    </div>
                    <div class="md-footer">
                        <ul class="list-menu-mb">
                            <li><a class="" href="{{action('Auth\LoginController@getLogout')}}">Đăng xuất</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    @endif
</div>
@yield('main_content')
@include('frontend.partials.footer')
<div id="btn-top" class="">
    <i class="icon-sprite-24 icon-to-top"></i>
</div>
@include('frontend.partials.footer_script')
@yield('custom_footer')
<script type="text/javascript">
    $('div.alert').delay(3000).slideUp();
</script>
</body>
</html>