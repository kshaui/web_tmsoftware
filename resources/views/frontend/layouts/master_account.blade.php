<!DOCTYPE html>
<html lang="">
<head>
    @include('frontend.partials.account.head_script')
</head>
<body>
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P234VKS"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- Header PC-->
<div class="header header-pc">
    <div class="header-white-full">
        <div class="row m0" style="transform: translateY(-11px)">
            <div class="pull-left">
                <a class="go-back" href="javascript:history.go(-1);"><i class="icon-sprite-24 icon-back"></i></a>
                <a class="back-home" href="/"><i
                            class="icon-sprite-24 icon-home mr5"></i> Về trang chủ</a>
            </div>
            <div class="center ">
                <a class="" style="font-size: 30px; " href="{{action('Frontend\HomeController@getIndex')}}"><img
                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo'], 175, 50)}}"
                            style="width: 100px;height: 50px;margin-top: 10px;"></a>
            </div>
            @if(!Auth::user())
                <div class="pull-right pt12">
                    <ul>
                        <li class="dang-ky"><a class="mr15" href="{{action('Auth\LoginController@getLogin')}}"
                                               style="padding-right:5px"><i
                                        class="icon-sprite-24 icon-dky"></i>Đăng nhập</a></li>
                        <li class="dang-nhap"><a class="" href="{{action('Auth\LoginController@getRegister')}}">Đăng
                                ký</a>
                        </li>
                    </ul>
                </div>
            @else
            @endif
        </div>
    </div>
</div>
<!-- End Header -->
<!-- Header Mobile -->
<div class="header-mobile header-back">
    <nav class="nav-mobile">
        <a class="btn-back" href="javascript:history.go(-1);"><i class="icon-sprite-24 icon-back"></i></a>
        <a class="logo" href="/"></a>
        <a class="icon-right" href="#" data-toggle="modal" data-target="#menu_right">
            <i class="icon-sprite-24 icon-dky"></i>
            <span class="caret"></span>
        </a>
    </nav>
    <!-- Modal Menu Right -->
    @if(Auth::user())
        <div class="modal right fade modal-menu modal-menu-right" id="menu_right" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="row m0">
                        <div class="col-xs-12 p0">
                            <a class="a-dky" href="{{ action('Frontend\AccountController@getAccount') }}">
                                <i class="icon-sprite-24 icon-dky"></i>{{ @Auth::user()->name }}
                            </a>
                        </div>
                    </div>
                    <div class="modal-header">
                        <ul class="list-menu-mb">
                            <li><a class="" href="{{ action('Frontend\AccountController@getAccount') }}"> Quản lý tài
                                    khoản</a></li>
                            <li><a class="" href="{{action('Frontend\ProductController@getNewProduct')}}">Đăng tin bán ô
                                    tô</a></li>
                        </ul>
                    </div>
                    <div class="modal-header">
                        <ul class="list-menu-mb">
                            <li><a class="" href="{{ URL::to('xe-dang-rao-ban') }}">Xe đang rao bán <span
                                            class="txt-num"></span></a></li>
                            <li><a class="" href="{{action('Frontend\ProductController@getPending')}}">Xe chờ duyệt
                                    <span
                                            class="txt-num"></span></a></li>
                            <li><a class="" href="{{URL::to('xe-da-ban')}}">Xe đã bán <span
                                            class="txt-num"></span></a></li>
                        </ul>
                    </div>
                    <div class="md-footer">
                        <ul class="list-menu-mb">
                            <li><a class="" href="{{action('Auth\LoginController@getLogout')}}">Đăng xuất</a></li>
                        </ul>
                    </div>
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div>
@else
        <div class="modal right fade modal-menu modal-menu-right" id="menu_right" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="row m0">
                        <div class="col-xs-6 p0">
                            <a class="a-dky" href="{{ action('Auth\LoginController@getRegister') }}">
                                <i class="icon-sprite-24 icon-dky"></i> Đăng ký
                            </a>
                        </div>
                        <div class="col-xs-6 p0">
                            <a class="a-dnhap" href="{{ action('Auth\LoginController@getLogin') }}">
                                Đăng nhập
                            </a>
                        </div>
                    </div>
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div>
@endif
<!-- End Modal Menu Right -->
</div>
<!-- End Header mobile -->
<!-- Body -->
@yield('main_body')
<!-- End Body -->
<!-- Footer -->
@include('frontend.partials.footer')
<!-- End Footer -->
<!-- Button go to top -->
<div id="btn-top">
    <i class="icon-sprite-24 icon-to-top"></i>
</div>
@include('frontend.partials.footer_script')
<script type="text/javascript">
    $('div.alert').delay(3000).slideUp();
</script>
</body>
</html>