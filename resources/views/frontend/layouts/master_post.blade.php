<html lang="en-US" xmlns:fb="http://ogp.me/ns/fb#" prefix="og: http://ogp.me/ns#">
<head>
   @include('frontend.partials.head_meta')
    <script src="{{asset('public/frontend/wp-includes/js/wp-emoji-release.min.js')}}" type="text/javascript"
            defer=""></script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="stylesheet" type="text/css" href=" {{URL::to('public/frontend/assets/frontend/css/common.css')}}">
    <link rel="stylesheet" id="gdrts-gridism-css"
          href="{{asset('public/frontend/wp-content//plugins/gd-rating-system/css/gridism.css')}}" type="text/css"
          media="all">
    <link rel="stylesheet" id="gdrts-rating-css"
          href="{{asset('public/frontend/wp-content//plugins/gd-rating-system/css/rating.min.css')}}" type="text/css"
          media="all">
    <link rel="stylesheet" id="gdrts-font-css"
          href="{{asset('public/frontend/wp-content//plugins/gd-rating-system/font/default.min.css')}}" type="text/css"
          media="all">
    <link rel="stylesheet" id="ribbon-lite-style-css"
          href="{{asset('public/frontend/wp-content/themes/ribbon-lite/style.css')}}" type="text/css" media="all">
    <style id="ribbon-lite-style-inline-css" type="text/css">

        #site-header {
            background-image: url('');
        }

        #tabber .inside li .meta b, footer .widget li a:hover, .fn a, .reply a, #tabber .inside li div.info .entry-title a:hover, #navigation ul ul a:hover, .single_post a, a:hover, .sidebar.c-4-12 .textwidget a, #site-footer .textwidget a, #commentform a, #tabber .inside li a, .copyrights a:hover, a, .sidebar.c-4-12 a:hover, .top a:hover, footer .tagcloud a:hover {
            color: #f1632b;
        }

        .nav-previous a:hover, .nav-next a:hover, #commentform input#submit, #searchform input[type='submit'], .home_menu_item, .primary-navigation a:hover, .post-date-ribbon, .currenttext, .pagination a:hover, .readMore a, .mts-subscribe input[type='submit'], .pagination .current, .woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current, .woocommerce-product-search input[type="submit"], .woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button, .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button {
            background-color: #f1632b;
        }

        .woocommerce nav.woocommerce-pagination ul li span.current, .woocommerce-page nav.woocommerce-pagination ul li span.current, .woocommerce #content nav.woocommerce-pagination ul li span.current, .woocommerce-page #content nav.woocommerce-pagination ul li span.current, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce-page nav.woocommerce-pagination ul li a:hover, .woocommerce #content nav.woocommerce-pagination ul li a:hover, .woocommerce-page #content nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce-page nav.woocommerce-pagination ul li a:focus, .woocommerce #content nav.woocommerce-pagination ul li a:focus, .woocommerce-page #content nav.woocommerce-pagination ul li a:focus, .pagination .current, .tagcloud a {
            border-color: #f1632b;
        }

        .corner {
            border-color: transparent transparent #f1632b transparent;
        }

        .primary-navigation, footer, .readMore a:hover, #commentform input#submit:hover, .featured-thumbnail .latestPost-review-wrapper {
            background-color: #364956;
        }

        .article {
            float: right;
        }

        .sidebar.c-4-12 {
            float: left;
        }


    </style>
    <style>
        .featured-thumbnail {
            width: 100%;
            height: 100px;
            overflow: hidden;
        }

        .featured-thumbnail img {
            width: 100%;
            height: 100%;
        }
    </style>
    <link rel="stylesheet" id="theme-slug-fonts-css" href="//fonts.googleapis.com/css?family=Monda%3A400%2C700"
          type="text/css" media="all">
    <script type="text/javascript" src="{{asset('public/frontend/wp-includes/js/jquery/jquery.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('public/frontend/wp-includes/js/jquery/jquery-migrate.min.js.1')}}"></script>
    <script type="text/javascript"
            src="{{asset('public/frontend/wp-content/themes/ribbon-lite/js/customscripts.js')}}"></script>
    <link rel="https://api.w.org/" href="https://choxe.net/blog/wp-json/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://choxe.net/blog/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="{{asset('public/frontend/wp-includes/wlwmanifest.xml')}}">
    <meta name="generator" content="WordPress 4.9.8">
    <script type="text/javascript">var ajaxurl = "https://choxe.net/blog/wp-admin/admin-ajax.php";</script>
    <meta property="fb:app_id" content="1891135751174652">
    <style type="text/css" id="wp-custom-css">
        /*




*/

        #content p {
            text-align: justify;
        }        </style>
    <style type="text/css">.fb_hidden {
            position: absolute;
            top: -10000px;
            z-index: 10001
        }

        .fb_reposition {
            overflow: hidden;
            position: relative
        }

        .fb_invisible {
            display: none
        }

        .fb_reset {
            background: none;
            border: 0;
            border-spacing: 0;
            color: #000;
            cursor: auto;
            direction: ltr;
            font-family: "lucida grande", tahoma, verdana, arial, sans-serif;
            font-size: 11px;
            font-style: normal;
            font-variant: normal;
            font-weight: normal;
            letter-spacing: normal;
            line-height: 1;
            margin: 0;
            overflow: visible;
            padding: 0;
            text-align: left;
            text-decoration: none;
            text-indent: 0;
            text-shadow: none;
            text-transform: none;
            visibility: visible;
            white-space: normal;
            word-spacing: normal
        }

        .fb_reset > div {
            overflow: hidden
        }

        @keyframes fb_transform {
            from {
                opacity: 0;
                transform: scale(.95)
            }
            to {
                opacity: 1;
                transform: scale(1)
            }
        }

        .fb_animate {
            animation: fb_transform .3s forwards
        }

        .fb_dialog {
            background: rgba(82, 82, 82, .7);
            position: absolute;
            top: -10000px;
            z-index: 10001
        }

        .fb_dialog_advanced {
            border-radius: 8px;
            padding: 10px
        }

        .fb_dialog_content {
            background: #fff;
            color: #373737
        }

        .fb_dialog_close_icon {
            background: url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 0 transparent;
            cursor: pointer;
            display: block;
            height: 15px;
            position: absolute;
            right: 18px;
            top: 17px;
            width: 15px
        }

        .fb_dialog_mobile .fb_dialog_close_icon {
            left: 5px;
            right: auto;
            top: 5px
        }

        .fb_dialog_padding {
            background-color: transparent;
            position: absolute;
            width: 1px;
            z-index: -1
        }

        .fb_dialog_close_icon:hover {
            background: url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -15px transparent
        }

        .fb_dialog_close_icon:active {
            background: url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px transparent
        }

        .fb_dialog_iframe {
            line-height: 0
        }

        .fb_dialog_content .dialog_title {
            background: #6d84b4;
            border: 1px solid #365899;
            color: #fff;
            font-size: 14px;
            font-weight: bold;
            margin: 0
        }

        .fb_dialog_content .dialog_title > span {
            background: url(https://static.xx.fbcdn.net/rsrc.php/v3/yd/r/Cou7n-nqK52.gif) no-repeat 5px 50%;
            float: left;
            padding: 5px 0 7px 26px
        }

        body.fb_hidden {
            height: 100%;
            left: 0;
            margin: 0;
            overflow: visible;
            position: absolute;
            top: -10000px;
            transform: none;
            width: 100%
        }

        .fb_dialog.fb_dialog_mobile.loading {
            background: url(https://static.xx.fbcdn.net/rsrc.php/v3/ya/r/3rhSv5V8j3o.gif) white no-repeat 50% 50%;
            min-height: 100%;
            min-width: 100%;
            overflow: hidden;
            position: absolute;
            top: 0;
            z-index: 10001
        }

        .fb_dialog.fb_dialog_mobile.loading.centered {
            background: none;
            height: auto;
            min-height: initial;
            min-width: initial;
            width: auto
        }

        .fb_dialog.fb_dialog_mobile.loading.centered #fb_dialog_loader_spinner {
            width: 100%
        }

        .fb_dialog.fb_dialog_mobile.loading.centered .fb_dialog_content {
            background: none
        }

        .loading.centered #fb_dialog_loader_close {
            clear: both;
            color: #fff;
            display: block;
            font-size: 18px;
            padding-top: 20px
        }

        #fb-root #fb_dialog_ipad_overlay {
            background: rgba(0, 0, 0, .4);
            bottom: 0;
            left: 0;
            min-height: 100%;
            position: absolute;
            right: 0;
            top: 0;
            width: 100%;
            z-index: 10000
        }

        #fb-root #fb_dialog_ipad_overlay.hidden {
            display: none
        }

        .fb_dialog.fb_dialog_mobile.loading iframe {
            visibility: hidden
        }

        .fb_dialog_mobile .fb_dialog_iframe {
            position: sticky;
            top: 0
        }

        .fb_dialog_content .dialog_header {
            background: linear-gradient(from(#738aba), to(#2c4987));
            border-bottom: 1px solid;
            border-color: #1d3c78;
            box-shadow: white 0 1px 1px -1px inset;
            color: #fff;
            font: bold 14px Helvetica, sans-serif;
            text-overflow: ellipsis;
            text-shadow: rgba(0, 30, 84, .296875) 0 -1px 0;
            vertical-align: middle;
            white-space: nowrap
        }

        .fb_dialog_content .dialog_header table {
            height: 43px;
            width: 100%
        }

        .fb_dialog_content .dialog_header td.header_left {
            font-size: 12px;
            padding-left: 5px;
            vertical-align: middle;
            width: 60px
        }

        .fb_dialog_content .dialog_header td.header_right {
            font-size: 12px;
            padding-right: 5px;
            vertical-align: middle;
            width: 60px
        }

        .fb_dialog_content .touchable_button {
            background: linear-gradient(from(#4267B2), to(#2a4887));
            background-clip: padding-box;
            border: 1px solid #29487d;
            border-radius: 3px;
            display: inline-block;
            line-height: 18px;
            margin-top: 3px;
            max-width: 85px;
            padding: 4px 12px;
            position: relative
        }

        .fb_dialog_content .dialog_header .touchable_button input {
            background: none;
            border: none;
            color: #fff;
            font: bold 12px Helvetica, sans-serif;
            margin: 2px -12px;
            padding: 2px 6px 3px 6px;
            text-shadow: rgba(0, 30, 84, .296875) 0 -1px 0
        }

        .fb_dialog_content .dialog_header .header_center {
            color: #fff;
            font-size: 16px;
            font-weight: bold;
            line-height: 18px;
            text-align: center;
            vertical-align: middle
        }

        .fb_dialog_content .dialog_content {
            background: url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) no-repeat 50% 50%;
            border: 1px solid #4a4a4a;
            border-bottom: 0;
            border-top: 0;
            height: 150px
        }

        .fb_dialog_content .dialog_footer {
            background: #f5f6f7;
            border: 1px solid #4a4a4a;
            border-top-color: #ccc;
            height: 40px
        }

        #fb_dialog_loader_close {
            float: left
        }

        .fb_dialog.fb_dialog_mobile .fb_dialog_close_button {
            text-shadow: rgba(0, 30, 84, .296875) 0 -1px 0
        }

        .fb_dialog.fb_dialog_mobile .fb_dialog_close_icon {
            visibility: hidden
        }

        #fb_dialog_loader_spinner {
            animation: rotateSpinner 1.2s linear infinite;
            background-color: transparent;
            background-image: url(https://static.xx.fbcdn.net/rsrc.php/v3/yD/r/t-wz8gw1xG1.png);
            background-position: 50% 50%;
            background-repeat: no-repeat;
            height: 24px;
            width: 24px
        }

        @keyframes rotateSpinner {
            0% {
                transform: rotate(0deg)
            }
            100% {
                transform: rotate(360deg)
            }
        }

        .fb_iframe_widget {
            display: inline-block;
            position: relative
        }

        .fb_iframe_widget span {
            display: inline-block;
            position: relative;
            text-align: justify
        }

        .fb_iframe_widget iframe {
            position: absolute
        }

        .fb_iframe_widget_fluid_desktop, .fb_iframe_widget_fluid_desktop span, .fb_iframe_widget_fluid_desktop iframe {
            max-width: 100%
        }

        .fb_iframe_widget_fluid_desktop iframe {
            min-width: 220px;
            position: relative
        }

        .fb_iframe_widget_lift {
            z-index: 1
        }

        .fb_iframe_widget_fluid {
            display: inline
        }

        .fb_iframe_widget_fluid span {
            width: 100%
        }

        .fb_customer_chat_bounce_in_v2 {
            animation-duration: 300ms;
            animation-name: fb_bounce_in_v2;
            transition-timing-function: ease-in
        }

        .fb_customer_chat_bounce_out_v2 {
            animation-duration: 300ms;
            animation-name: fb_bounce_out_v2;
            transition-timing-function: ease-in
        }

        .fb_customer_chat_bounce_in_v2_mobile_chat_started {
            animation-duration: 300ms;
            animation-name: fb_bounce_in_v2_mobile_chat_started;
            transition-timing-function: ease-in
        }

        .fb_customer_chat_bounce_out_v2_mobile_chat_started {
            animation-duration: 300ms;
            animation-name: fb_bounce_out_v2_mobile_chat_started;
            transition-timing-function: ease-in
        }

        .fb_customer_chat_bubble_pop_in {
            animation-duration: 250ms;
            animation-name: fb_customer_chat_bubble_bounce_in_animation
        }

        .fb_customer_chat_bubble_animated_no_badge {
            box-shadow: 0 3px 12px rgba(0, 0, 0, .15);
            transition: box-shadow 150ms linear
        }

        .fb_customer_chat_bubble_animated_no_badge:hover {
            box-shadow: 0 5px 24px rgba(0, 0, 0, .3)
        }

        .fb_customer_chat_bubble_animated_with_badge {
            box-shadow: -5px 4px 14px rgba(0, 0, 0, .15);
            transition: box-shadow 150ms linear
        }

        .fb_customer_chat_bubble_animated_with_badge:hover {
            box-shadow: -5px 8px 24px rgba(0, 0, 0, .2)
        }

        .fb_invisible_flow {
            display: inherit;
            height: 0;
            overflow-x: hidden;
            width: 0
        }

        .fb_mobile_overlay_active {
            background-color: #fff;
            height: 100%;
            overflow: hidden;
            position: fixed;
            visibility: hidden;
            width: 100%
        }

        @keyframes fb_bounce_in_v2 {
            0% {
                opacity: 0;
                transform: scale(0, 0);
                transform-origin: bottom right
            }
            50% {
                transform: scale(1.03, 1.03);
                transform-origin: bottom right
            }
            100% {
                opacity: 1;
                transform: scale(1, 1);
                transform-origin: bottom right
            }
        }

        @keyframes fb_bounce_in_v2_mobile_chat_started {
            0% {
                opacity: 0;
                top: 20px
            }
            100% {
                opacity: 1;
                top: 0
            }
        }

        @keyframes fb_bounce_out_v2 {
            0% {
                opacity: 1;
                transform: scale(1, 1);
                transform-origin: bottom right
            }
            100% {
                opacity: 0;
                transform: scale(0, 0);
                transform-origin: bottom right
            }
        }

        @keyframes fb_bounce_out_v2_mobile_chat_started {
            0% {
                opacity: 1;
                top: 0
            }
            100% {
                opacity: 0;
                top: 20px
            }
        }

        @keyframes fb_customer_chat_bubble_bounce_in_animation {
            0% {
                bottom: 6pt;
                opacity: 0;
                transform: scale(0, 0);
                transform-origin: center
            }
            70% {
                bottom: 18pt;
                opacity: 1;
                transform: scale(1.2, 1.2)
            }
            100% {
                transform: scale(1, 1)
            }
        }</style>
    @yield('custom_header')
</head>

<body class="archive category category-salon-o-to category-417 group-blog hfeed" style="background-color: #ffffff">
<div class="main-container">
    <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
    @include('frontend.partials.header_post')
    @yield('main_content')
</div>
@include('frontend.partials.footer_post')
</body>
</html>
