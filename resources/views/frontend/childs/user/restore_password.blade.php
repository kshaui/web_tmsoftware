<html lang="">
<head>
@include('frontend.partials.head')
<body>
<div class="header header-pc">
    <div class="header-white-full">
        <div class="row m0">
            <div class="pull-left">
                <a class="go-back" href="javascript:history.go(-1);">
                    <i class="icon-sprite-24 icon-back"></i>
                </a>
                <a class="back-home" href="/"><i class="icon-sprite-24 icon-home mr5"></i> Về trang chủ</a>
            </div>
            <div class="center ">
                <a class="logo mt15" href="/"></a>
            </div>
            <div class="pull-right pt12">
                <ul>
                    <li class="dang-ky"><a class="mr15" href="{{ URL::to('/dang-nhap') }}"><i class="icon-sprite-24 icon-dky"></i>Đăng nhập</a></li>
                    <li class="dang-nhap"><a class="" href="{{ URL::to('/dang-ky') }}">Đăng ký</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="main-content bg-grey" style="min-height: 488px;">
    <div class="container">
        <div class="row m0 ">
            <div class="form_action">
                <h4 class="title-form">Khôi Phục Mật Khẩu</h4>
                <div class="div_form">
                    <form class="validate-form" method="post" action="" enctype="multipart/form-data">
                        @if(Session('error'))
                            <div class="alert alert-danger" style="color:red">
                                {!! Session('error') !!}
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="password" class="">Mật khẩu mới <span class="text-red">*</span></label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <div class="form-group">
                            <label for="confirm_password" class="">Nhập lại mật khẩu mới <span class="text-red">*</span></label>
                            <input type="password" class="form-control" id="confirm_password" name="confirm_password">
                        </div>
                        <div class="form-group text-right mb0">
                            <button type="submit" class="btn btn-accept">KHÔI PHỤC MẬT KHẨU</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('frontend.partials.footer-salon')
<div id="btn-top">
    <i class="icon-sprite-24 icon-to-top"></i>
</div>
@include('frontend.partials.footer_script')