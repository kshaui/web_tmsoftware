<div class="col-sm-4 text_sidebar_left">
    @php
        $data = CommonHelper::getFromCache('widget_text_sidebar_left_user');
                                if (!$data) {
                                    $data = \App\Models\Widget::select(['content', 'location', 'name'])->whereIn('location', ['text_sidebar_left_user'])
                ->where('status', 1)->orderBy('order_no', 'asc')->get();
                                    CommonHelper::putToCache('widget_text_sidebar_left_user', $data);
                                }
            @endphp
    @foreach($data as $v)
        {!! $v->content !!}
    @endforeach
</div>