<html lang="">
<head>
    @include('frontend.partials.head')
<body>
<div class="header header-pc">
    <div class="header-white-full">
        <div class="row m0">
            <div class="pull-left">
                <a class="go-back" href="javascript:history.go(-1);">
                    <i class="icon-sprite-24 icon-back"></i>
                </a>
                <a class="back-home" href="/"><i class="icon-sprite-24 icon-home mr5"></i> Về trang chủ</a>
            </div>
            <div class="center ">
                <a class="logo mt15" href="/"></a>
            </div>
            <div class="pull-right pt12">
                <ul>
                    <li class="dang-ky"><a class="mr15" href="{{ URL::to('/dang-nhap') }}"><i class="icon-sprite-24 icon-dky"></i>Đăng nhập</a></li>
                    <li class="dang-nhap"><a class="" href="{{ URL::to('/dang-ky') }}">Đăng ký</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="main-content bg-grey" style="min-height: 439px;">
    <div class="container">
        <div class="row m0 ">
            <div class="form_action">
                <h4 class="title-form">Quên mật khẩu</h4>
                <div class="div_form">
                    <form method="post" enctype="multipart/form-data">
                        <div class="form-group note_form">
                            <div class="form-group">
                                @if(Session('flash_message'))
                                    <div class="alert alert-danger" style="color:red">
                                        {!! Session('flash_message') !!}
                                    </div>
                                @endif
                                    @if(session('success')) <span class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
                                <label for="" class="">Vui lòng nhập Email đã đăng ký</label>
                                <input type="text"  required=""  name="email" id="email" class="form-control">
                            </div>
                            <div class="form-group text-right mb0">
                                <a href="/" class="btn btn-cancel w83 mr10">HỦY</a>
                                <button type="submit" class="btn btn-accept w145">GỬI</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('frontend.partials.footer-salon')
<div id="btn-top">
    <i class="icon-sprite-24 icon-to-top"></i>
</div>
@include('frontend.partials.footer_script')
