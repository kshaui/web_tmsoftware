@extends('frontend.layouts.master')
@section('main_content')
    <div id="wrapper">
        <div id="container_wr">
            <div id="container">
                <h2 id="container_title"><span title="현재접속자">현재접속자</span></h2>
                <div id="current_connect">
                    @php
                        $data = CommonHelper::getFromCache('all_user');
                        if (!$data) {
                        $data = \App\Models\User::select('id','intro','icon')->where('status',1)->get();
                        CommonHelper::putToCache('all_user', $data);
                        }
                    @endphp
                    <ul>
                        @foreach($data as $item)
                            <li class="">
                                <div class="inner">
                                    <span class="crt_num">{{ $item->id }}</span>
                                    <span class="crt_name">
                                    <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->icon,60,60) }}" alt="profile_image">
                                    <br>223.♡.60.30</span>
                                    <span class="crt_lct"> {{ $item->intro }}</span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @include('frontend.partials.aside')
        </div>
    </div>
@endsection