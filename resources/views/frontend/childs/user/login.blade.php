@extends('frontend.layouts.master')
@section('main_content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="path">
                        <ul class="clearfix">
                            <li>1. Khách hàng mới / Đăng nhập</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="box-white mt30 pb30 clearfix">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="step clearfix">
                                <ul class="clearfix">
                                    <li class="active"><span>Đăng nhập</span><span class="number">1</span></li>
                                    <li><span>Địa chỉ giao hàng</span><span class="number">2</span></li>
                                    <li><span>Thanh toán &amp; đặt mua</span><span class="number">3</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="dnBox clearfix">
                                <h3>Thanh toán đơn hàng trong chỉ một bước với:</h3>
                                <p><a href="#"><img src="{{ URL::asset('public/frontend/images/dn_fb.png') }}" alt=""/></a>
                                    <span>Hoặc</span><a href="#"><img
                                                src="{{ URL::asset('public/frontend/images/dn_gg.png') }}" alt=""/></a>
                                    <span>Hoặc</span><a href="#"><img
                                                src="{{ URL::asset('public/frontend/images/dn_zl.png') }}" alt=""/></a>
                                </p>
                            </div>
                            @if(session('success')) <span
                                    class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
                            @if(session('error')) <span
                                    class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif
                        </div>
                        <form action="{{ route('user.postLogin') }}" method="POST" class="col-sm-4 col-md-4 col-lg-4">
                            {!! csrf_field() !!}
                            <div class="dn_col1">
                                <h3 class="sh_dn">Đăng nhập</h3>
                                <p class="txt_dn">Đã là thành viên khosach.net</p>
                                <form>
                                    <ul>
                                        <li>
                                            <label for="">Email / Số điện thoại</label>
                                            <input type="text" id="" name="email" placeholder="Email / Số điện thoại"
                                                   value="" required/>
                                        </li>
                                        <li>
                                            <label for="">Mật khẩu</label>
                                            <input type="password" id="" name="password" placeholder="Mật khẩu" value=""
                                                   required/>
                                        </li>
                                    </ul>
                                    <p class="txt_dn_form">Quên mật khẩu? Khôi phục mật khẩu <a
                                                href="{{ route('user.forgot_password') }}">tại đây</a></p>
                                    <div class="alert alert-error hide">Sai thông tin tài khoản</div>
                                    <p class="btn_dn">
                                        <button id="" type="button" class="login-btn">Đăng nhập</button>
                                    </p>
                                </form>
                            </div>
                        </form>
                        <form method="POST" action="{{ route('user.postRegister') }}"
                              class="col-sm-4 col-md-4 col-lg-4">
                            {!! csrf_field() !!}
                            <div class="dn_col1">
                                <h3 class="sh_dn">Đăng ký</h3>
                                <p class="txt_dn">Dành cho thành viên mới</p>
                                <form action="">
                                    <ul>
                                        <li>
                                            <label for="">Họ tên</label>
                                            <input type="text" id="" name="name" placeholder="Nhập họ tên" value=""
                                                   required/>
                                        </li>
                                        <li>
                                            <label for="">Số điện thoại</label>
                                            <input type="text" id="" name="tel" placeholder="Nhập số điện thoại"
                                                   value="" required/>
                                        </li>
                                        <li>
                                            <label for="">Email</label>
                                            <input type="text" id="" name="email" placeholder="Nhập email" value=""
                                                   required/>
                                        </li>
                                        <li>
                                            <label for="">Mật khẩu</label>
                                            <input type="password" id="" name="password"
                                                   placeholder="Nhập mật khẩu từ 6 đến 8 ký tự" value="" required/>
                                        </li>
                                        <li>
                                            <label for="">Nhập lại mật khẩu</label>
                                            <input type="password" id="" name="re_password"
                                                   placeholder="Nhập mật khẩu từ 6 đến 8 ký tự" value="" required/>
                                        </li>
                                    </ul>
                                    <p class="txt_dn_form">Khi bạn nhấn Đăng ký, bạn đã đồng ý thực hiện mọi giao dịch
                                        mua bán
                                        theo <a href="#">điều kiện sử dụng và chính sách của khosach.net</a></p>
                                    <p class="btn_dn">
                                        <button id="" type="submit">Đăng ký</button>
                                    </p>
                                </form>
                            </div>
                        </form>
                        @include('frontend.childs.order.block_order')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

