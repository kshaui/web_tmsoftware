@extends('frontend.layouts.master')
@section('main_content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="path">
                        <ul class="clearfix">
                            <li>1. Khách hàng mới / Đăng nhập</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="box-white mt30 pb30 clearfix">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            @if(session('success')) <span
                                    class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
                            @if(session('error')) <span
                                    class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif
                        </div>
                        @include('frontend.childs.user.text_sidebar_left')
                        <form method="POST" action="{{ route('user.postRegister') }}"
                              class="col-sm-4 col-md-4 col-lg-4">
                            {!! csrf_field() !!}
                            <div class="dn_col1">
                                <h3 class="sh_dn">Đăng ký</h3>
                                <p class="txt_dn">Dành cho thành viên mới</p>
                                <form action="">
                                    <ul>
                                        <li>
                                            <label for="">Họ tên</label>
                                            <input type="text" id="" name="name" placeholder="Nhập họ tên" value=""
                                                   required/>
                                        </li>
                                        <li>
                                            <label for="">Số điện thoại</label>
                                            <input type="text" id="" name="tel" placeholder="Nhập số điện thoại"
                                                   value="" required/>
                                        </li>
                                        <li>
                                            <label for="">Email</label>
                                            <input type="text" id="" name="email" placeholder="Nhập email" value=""
                                                   required/>
                                        </li>
                                        <li>
                                            <label for="">Mật khẩu</label>
                                            <input type="password" id="" name="password"
                                                   placeholder="Nhập mật khẩu từ 6 đến 8 ký tự" value="" required/>
                                        </li>
                                        <li>
                                            <label for="">Nhập lại mật khẩu</label>
                                            <input type="password" id="" name="re_password"
                                                   placeholder="Nhập mật khẩu từ 6 đến 8 ký tự" value="" required/>
                                        </li>
                                    </ul>
                                    <p class="txt_dn_form">Khi bạn nhấn Đăng ký, bạn đã đồng ý thực hiện mọi giao dịch
                                        mua bán
                                        theo <a href="#">điều kiện sử dụng và chính sách của khosach.net</a></p>
                                    <p class="btn_dn">
                                        <button id="" type="submit">Đăng ký</button>
                                    </p>
                                </form>
                            </div>
                        </form>
                        @include('frontend.childs.user.login_register_sidebar')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('custom_footer')
    <script type="text/javascript">
        $(function () {
            $('#BB-nav').affix({
                offset: {
                    top: $('#BB-nav').height()
                }
            });
        });
    </script>
    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "100%";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }

        $(document).ready(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 500) {
                    $('.scrollToTop').fadeIn();
                } else {
                    $('.scrollToTop').fadeOut();
                }
            });
            $('.scrollToTop').click(function () {
                $('html, body').animate({scrollTop: 0}, 800);
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function (e) {
            $('.search-panel .dropdown-menu').find('a').click(function (e) {
                e.preventDefault();
                var param = $(this).attr("href").replace("#", "");
                var concept = $(this).text();
                $('.search-panel span#search_concept').text(concept);
                $('.input-group #search_param').val(param);
            });
        });
    </script>

    {{--Custom code--}}
    <script>
        $('.login-btn').click(function () {
            var object = $(this);
            object.parents('form').find('input').attr('disabled', 'disabled');
            object.parents('form').find('.alert-error').addClass('hide');
            $.ajax({
                url: "{{ route('user.login') }}",
                type: 'POST',
                dataType: 'json',
                data: {
                    email_tel: object.parents('form').find('input[name=email]').val(),
                    password: object.parents('form').find('input[name=password]').val(),
                    ajax: 'true'
                },
                success: function (result) {
                    object.parents('form').find('input').removeAttr('disabled');
                    if (result.status == false) {
                        object.parents('form').find('.alert-error').html(result.msg);
                        object.parents('form').find('.alert-error').removeClass('hide');
                    } else {
                        @if(isset($_GET['redirect_back']))
                            window.location.href = "{{ $_GET['redirect_back'] }}";
                        @else
                        location.reload();
                        @endif
                    }
                },
                error: function () {
                    alert('Có lỗi xảy ra. Vui lòng load lại website và thử lại!');
                }
            });
        });
    </script>
@endsection