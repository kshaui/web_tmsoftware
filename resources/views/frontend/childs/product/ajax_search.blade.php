@foreach($products as $item)
    @php
        if ($item->type == '|2|') {
        $type = ' - Sách nói';
    } elseif ($item->type == '|1|') {
        $type = ' - Sách giấy';
    } elseif ($item->type == '|3|') {
        $type = ' - Sách điện tử';
    } elseif ($item->type == '|2|3|') {
        $type = ' - Sách nói và sách điện tử';
    } elseif ($item->type == '|1|2|') {
        $type = ' - Sách nói và giách giấy';
    } else {
        $type = '';
    }
            @endphp
    <li><a href="{{ CommonHelper::getProductSlug($item) }}">{{ $item->name . $type }}</a></li>
@endforeach