<div class="col-right pl30">
    @include('frontend.partials.breadcrumb_automaker')
    <div class="deal mt10">
        @php
            $productIDS = \App\Models\Favorite::where('user_id', \Auth::user()->id)->pluck('product_id')->toArray();
            $data = CommonHelper::getFromCache('product_dangban_by_id');
             if (!$data) {
                 $data = \App\Models\Product::whereIn('pending', ['Đang bán'])->where('id', $productIDS)->paginate(10);
                 CommonHelper::putToCache('product_dangban_by_id', $data);
             }
        @endphp
        @include('frontend.childs.product.show_item')
    </div>
</div>
