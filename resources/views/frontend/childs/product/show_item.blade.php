@foreach($data as $item)
    @php
        /* Manufacturer id*/
        $Manufacturer = CommonHelper::getFromCache('manufacturer_by_manufacturer_id_first');
        if (!$Manufacturer) {
            $Manufacturer = \App\Models\Manufacturer::select(['id', 'name'])->where('id',$item->manufacturer_id)->first();
            CommonHelper::putToCache('manufacturer_by_manufacturer_id_first', $Manufacturer);
        }
    /*$Manufacturer model id*/
     $Manufacturer_model = CommonHelper::getFromCache('manufacturermodel_by_manufacturer_model_id_first');
        if (!$Manufacturer_model) {
            $Manufacturer_model = \App\Models\ManufacturerModel::select(['id', 'name'])->where('id',$item->manufacturer_model_id)->first();
            CommonHelper::putToCache('manufacturermodel_by_manufacturer_model_id_first', $Manufacturer_model);
        }
    /**thành phố*/
     $province_index= CommonHelper::getFromCache('province_by_province_id_first');
        if (!$province_index) {
            $province_index = \App\Models\Province::select(['id', 'name'])->where('id',$item->province_id)->first();
            CommonHelper::putToCache('province_by_province_id_first', $province_index);
        }
    /**user*/
    $user_role = CommonHelper::getFromCache('user_by_author_id_first');
    if (!$user_role){
        $user_role = \App\Models\User::select('id','name','role')->where('id',@$item->author_id)->first();
        CommonHelper::putToCache('user_by_author_id_first',$user_role);
    }
    @endphp
    <tr>
        <td class="row news  items-{{ @$item->id }} pos-0">
            <div class="news-detail">
                <div class="info-left">
                    <a href="{{ URL::to('chi-tiet-san-pham/' . $item['id']) }}" class="img"><img
                                class=""
                                src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item['image'], 175, 118) }}"
                                width="175"
                                height="118"
                                alt="{{ @$item->name }}"></a>


                    <div class="block-txt-mb">
                        <a class="tit-news"
                           href="{{ URL::to('chi-tiet-san-pham/' . @$item['id']) }}">
                            <h2>{{ @$item->name }}</h2>
                        </a>
                        <span class="dai-ly">{{ @$user_role->role }}</span>
                        <div class="price visible-xs">{{ (@$item->price) }} {{ @$item->price_text }}</div>

                        <div class="row" style="margin: 0">
                            <div class="col-xs-3 col-sm-3 list-char">
                                <i class="icon-24 icon-sprite-24 icon-status"></i>
                                {{ @$item->tinhtrangxe }}
                            </div>

                            <div class="col-xs-3 col-sm-3 list-char">
                                <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                {{ @$item->year }}
                            </div>

                            <div class="col-xs-3 col-sm-3 list-char">
                                <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                {{  number_format(@$item->odo) }}<span class="hidden-xs"> km</span>
                            </div>

                            <div class="col-xs-3 col-sm-3 list-char">
                                <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                {{ @$item->transmission }}
                            </div>
                        </div>
                        <div class="name-store visible-xs">
                            <a href="\tim-kiem?province_id='.'{{ @$item->province_id }}">
                                <i class="icon-24 icon-sprite-24 icon-address"></i> {{ @$province_index->name }}</a>
                        </div>
                        <div class="old-news">Đăng:{{date(" H:i a d-m-Y",strtotime(@$item->updated_at)) }}</div>
                        <a class="name-store visible-xs"
                           href="{{ URL::to('/tim-kiem?user_id='. @$user_role->id) }}">{{ @$user_role->name }}</a>
                    </div>
                    <div class="row list-opt hidden-xs">

                        {{--<div class="col-xs-6 text-center col-sm-6 p0">--}}
                            {{--<button class="btn-hide-phone-list"--}}
                                    {{--onclick="showPhoneNumner('{{ @$item->id }}', '{{ @$item->phone1 }} ')"--}}
                                    {{--id="salon_id_{{ @$item->id }}"><i--}}
                                        {{--class="icon-sprite-24 icon-phone"></i>{{ @$item->phone1 }} </button>--}}


                        {{--</div>--}}

                        <script>
                            var dataLayer = window.dataLayer = window.dataLayer || [];
                            dataLayer.push({
                                'event': 'view_homepage',
                                'google_tag_params': {
                                    'emailaddress': '',
                                    'emailaddress_raw': '',
                                    'sitetype': 'd',
                                    'dynx_pagetype': 'home'
                                }
                            });

                            var ga_tracker_name = null;
                            function init_ga_name() {
                                if (typeof ga === 'function') {
                                    ga(function() {
                                        var ga_trackers = ga.getAll();
                                        ga_tracker_name = ga_trackers[0].get('name');
                                        console.log('GA: ' + ga_tracker_name);
                                    });
                                } else {
                                    console.log('GA Not loaded');
                                    setTimeout(function(){
                                        init_ga_name();
                                    }, 500);
                                }
                            }
                            init_ga_name();
                        </script>
                        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                            })(window,document,'script','dataLayer','GTM-P234VKS');</script>
                        <div class="col-xs-3 col-sm-5 p0">
                            <button class="btn-show-phone-list" onclick="showPhoneNumner(293727, '0969369710')" id="salon_id_293727">
                                Xem số điện thoại
                            </button>
                        </div>

                        <div class="col-xs-6 text-center col-sm-3 p0 ">
                            @php
                                $count = \App\Models\Favorite::where('user_id',@Auth::user()->id)->where('product_id',$item->id)->count();
                            @endphp
                            @if($count == 0)
                                {{csrf_field()}}
                                <a class="favor  yeuthich "
                                   data-toggle="tooltip" title=""
                                   data-placement="bottom"
                                   data-original-title="Thích xe này"
                                   data-item_id="{{@$item->id}}"
                                   data-count_favorite="{{ $count }}"
                                   data-user_id="{{@Auth::user()->id}}"
                                   data-author_id="{{@$item['author_id']}}"
                                   data-product_id="{{ @$item['id'] }}"
                                   type="button"><i
                                            class="icon-sprite-24 icon-heart"></i> Yêu thích
                                </a>
                            @else
                                {{csrf_field()}}
                                <a class="favor active khongyeuthich" data-toggle="tooltip" title=""
                                   data-placement="bottom" data-original-title="Bỏ thích xe này"
                                   data-item_id="{{@$item->id}}"
                                   data-count_favorite="{{ $count }}"
                                   data-user_id="{{@Auth::user()->id}}"
                                   data-author_id="{{@$item['author_id']}}"
                                   data-product_id="{{ @$item['id'] }}" aria-describedby="tooltip514963">
                                    <i class="icon-sprite-24 icon-heart"></i>
                                    Bỏ thích
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="price-addr txr hidden-xs">
                <div class="price">{{ (@$item->price)  }} {{ @$item->price_text }}</div>
                <div class="city">
                    <a href="\tim-kiem?province_id={{ @$item->province_id }}">
                        <i class="icon-24 icon-sprite-24 icon-address"></i>{{ @$province_index->name }}</a></div>
                <a class="name-store"
                   href="{{ URL::to('/tim-kiem?user_id='. @$user_role->id) }}">{{ @$user_role->name }}</a>
            </div>
        </td>
    </tr>
@endforeach
<script type="text/javascript">
    $('.yeuthich').click(function () {
        var item_id = $(this).data('item_id');
        var user_id = $(this).data('user_id');
        var author_id = $(this).data('author_id');
        var product_id = $(this).data('product_id');
        var count_favorite = $(this).data('count_favorite');
        var object = $(this);
        if (user_id == '') {
            if (confirm('Bạn phải đăng nhập trước khi lưu tin vào yêu thích')) {
                window.location = '{{action("Auth\LoginController@getLogin")}}';
            }
        } else {
            if (count_favorite == 0) {
                $.ajax({
                    type: 'POST',
                    url: '{{action("Frontend\ProductController@postCarLove")}}',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        'item_id': item_id,
                        'user_id': user_id,
                        'author_id': author_id,
                        'product_id': product_id,
                    },
                    success: function (data) {
                        console.log(data);
                        location.reload();
                    }
                });
            }
        }
    })
    $('.khongyeuthich').click(function () {
        var id = $(this).data('item_id');
        $.ajax({
            type: 'POST',
            url: '/khong-thich',
            data: {
                '_token': '{{ csrf_token() }}',
                'id': id,
            },
            success: function (data) {
                console.log(data);
                location.reload();
            }
        });
    })
</script>