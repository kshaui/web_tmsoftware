<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="ql_account dflex">
                {{--menu left--}}
                @include('frontend.childs.product.menu_left_product')
                <div class="flex">
                    <div class="map-link hidden-xs cbreadcrumb">
                        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a class="" href="/" itemprop="item"><span
                                            itemprop="name">{{ @$settings['name'] }}</span></a>
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                &gt; <a href="#"
                                        itemprop="item"><span itemprop="name">Xe đăng bán</span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="deal">
                        <div class="title-underline mt25">Danh sách tin đã đăng</div>
                        <div class="btn-group btn-action mt25">
                            @php
                                $count_dang_ban = CommonHelper::getFromCache('product_dangban_by_author_id_count');
                                if (!$count_dang_ban){
                                $count_dang_ban = \App\Models\Product::where('author_id', Auth::user()->id)->wherein('pending',['Đang bán'])->count();
                                CommonHelper::putToCache('product_dangban_by_author_id_count',$count_dang_ban);
                            }
                            @endphp
                            <a href="{{ URL::to('xe-dang-rao-ban') }}"
                               class="btn btn-default @if(@$duongdan =='xe-dang-rao-ban') active @endif ">Đang bán <span
                                        class="txt-num-tab">@if(isset($count_dang_ban)) ({{ $count_dang_ban }}) @else
                                        (0) @endif</span></a>
                            @php
                                $count_da_ban = CommonHelper::getFromCache('product_daban_by_author_id_count');
                                if (!$count_da_ban){
                                $count_da_ban = \App\Models\Product::where('author_id', Auth::user()->id)->wherein('pending',['Đa bán'])->count();
                                CommonHelper::putToCache('product_daban_by_author_id_count',$count_da_ban);
                            }
                            @endphp
                            <a href="{{ URL::to('xe-da-ban') }}"
                               class="btn btn-default @if(@$duongdan =='xe-da-ban') active @endif  ">Đã bán <span
                                        class="txt-num-tab"> @if(isset($count_da_ban)) ({{ (@$count_da_ban) }}) @else
                                        (0) @endif </span></a>
                            @php
                                $count_dang_cho_duyet= CommonHelper::getFromCache('product_dangchoduyet_by_author_id_count');
                                if (!$count_dang_cho_duyet){
                                $count_dang_cho_duyet = \App\Models\Product::where('author_id', Auth::user()->id)->wherein('pending',['Đang chờ duyệt'])->count();
                                CommonHelper::putToCache('product_dangchoduyet_by_author_id_count',$count_dang_cho_duyet);
                            }
                            @endphp
                            <a href="{{ URL::to('dang-cho-duyet') }}"
                               class="btn btn-default @if(@$duongdan == 'dang-cho-duyet') active @endif">Đang
                                chờ duyệt <span
                                        class="txt-num-tab"> @if(isset($count_dang_cho_duyet)) ({{ (@$count_dang_cho_duyet) }}) @else
                                        (0) @endif </span></a>
                            @php
                                $count_thay_doi_cho_duyet= CommonHelper::getFromCache('product_thaydoichoduyet_by_author_id_count');
                                if (!$count_thay_doi_cho_duyet){
                                $count_thay_doi_cho_duyet = \App\Models\Product::where('author_id', Auth::user()->id)->wherein('pending',['Thay đổi chờ duyêt'])->count();
                                CommonHelper::putToCache('product_thaydoichoduyet_by_author_id_count',$count_thay_doi_cho_duyet);
                            }
                            @endphp
                            <a href="{{ URL::to('xe-thay-doi-cho-duyet') }}"
                               class="btn btn-default @if(@$duongdan == 'xe-thay-doi-cho-duyet') active @endif ">Thay
                                đổi chờ duyệt <span
                                        class="txt-num-tab"> @if(isset($count_thay_doi_cho_duyet)) ({{ (@$count_thay_doi_cho_duyet) }}) @else
                                        (0) @endif </span></a>
                            @php
                                $count_tu_choi= CommonHelper::getFromCache('product_tuchoi_by_author_id_count');
                                if (!$count_tu_choi){
                                $count_tu_choi = \App\Models\Product::where('author_id', Auth::user()->id)->wherein('pending',['Từ chối'])->count();
                                CommonHelper::putToCache('product_tuchoi_by_author_id_count',$count_tu_choi);
                            }
                            @endphp
                            <a href="{{ URL::to('tu-choi') }}"
                               class="btn btn-default @if(@$duongdan =='tu-choi') active @endif  ">Từ chối <span
                                        class="txt-num-tab">@if(isset($count_tu_choi)) ({{ (@$count_tu_choi) }}) @else
                                        (0) @endif</span></a>
                            @php
                                $count_het_han= CommonHelper::getFromCache('product_hethan_by_author_id_count');
                                if (!$count_het_han){
                                $count_het_han = \App\Models\Product::where('author_id', Auth::user()->id)->wherein('pending',['Hết hạn'])->count();
                                CommonHelper::putToCache('product_hethan_by_author_id_count',$count_het_han);
                            }
                            @endphp
                            <a href="{{ URL::to('het-han') }}"
                               class="btn btn-default @if(@$duongdan == 'het-han') active @endif ">Hết hạn <span
                                        class="txt-num-tab">@if(isset($count_het_han)) ({{ (@$count_het_han) }}) @else
                                        (0) @endif </span></a>
                        </div>
                        <div class="mt20 mb20 list-product-style2">

                            <table class="table list-news">
                                <tbody>
                                @yield('menu_product')
                                @include('frontend.childs.product.show_product_post')
                                </tbody>
                            </table>
                            <div class="row">
                                <div>
                                    {{ $data->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>