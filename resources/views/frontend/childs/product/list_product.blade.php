@extends('frontend.layouts.master')
@section('title')
    Mua bán, trao đổi xe TOYOTA VIOS ✔️mới, cũ chất lượng trên toàn quốc ✔
@endsection
@section('main_content')
    <div class="main-content bg-light-blue">
        <div class="container">
            <div class="row m0">

                <!-- Column Left -->
                <div class="col-left hidden-xs">
                    <div class="header-mobile header-back header-blue" style="position: absolute; top: 0; left: 0;">
                        <nav class="nav-mobile">
                            <a class="btn-back toggle-filter" href="#"><i class="icon-sprite-24 icon-back"></i></a>
                            <div class="title-page">Lọc tìm kiếm</div>
                        </nav>
                    </div>

                    <form id="car-filter-form" name="car-filter-form" method="get" action="https://choxe.net/xe"
                          enctype="multipart/form-data">
                        <div class="col-filter">
                            <div class="row m0">
                                <div class="col-xs-6 col-sm-6 p0">
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue checked" style="position: relative;"><input
                                                    type="checkbox" name="status[]" checked="checked" class="icheck"
                                                    value="old"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        Xe cũ
                                    </label>
                                </div>
                                <div class="col-xs-6 col-sm-6 p0">
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" name="status[]" class="icheck" value="new"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        Xe mới
                                    </label>
                                </div>
                            </div>
                            <div class="select2 sl-comm">
                                <select class="select2 form-control sl-font14 select2-hidden-accessible"
                                        id="sl-tinh-thanh"
                                        name="province_id" tabindex="-1" aria-hidden="true">
                                    <option value="" selected="">Chọn tỉnh thành</option>
                                    <option value="1">Hà Nội</option>
                                    <option value="2">Hồ Chí Minh</option>
                                    <option value="21">Hải Dương</option>
                                    <option value="31">Bình Dương</option>
                                    <option value="9">Hải Phòng</option>
                                    <option value="61">Thái Nguyên</option>
                                    <option value="53">Phú Thọ</option>
                                    <option value="26">Bà Rịa Vũng Tàu</option>
                                    <option value="40">Đồng Nai</option>
                                    <option value="67">Vĩnh Phúc</option>
                                    <option value="4">Đà Nẵng</option>
                                    <option value="19">Bắc Giang</option>
                                    <option value="16">Bắc Ninh</option>
                                    <option value="13">Nghệ An</option>
                                    <option value="10">Quảng Ninh</option>
                                    <option value="69">Thanh Hóa</option>
                                    <option value="32">Bình Phước</option>
                                    <option value="63">Tiền Giang</option>
                                    <option value="7">Cần Thơ</option>
                                    <option value="46">Khánh Hòa</option>
                                    <option value="22">Hưng Yên</option>
                                    <option value="15">Ninh Bình</option>
                                    <option value="50">Long An</option>
                                    <option value="44">Hà Nam</option>
                                    <option value="20">Hòa Bình</option>
                                    <option value="23">Thái Bình</option>
                                    <option value="37">Đắc Lắc</option>
                                    <option value="6">An Giang</option>
                                    <option value="55">Quảng Bình</option>
                                    <option value="45">Hà Tĩnh</option>
                                    <option value="42">Gia Lai</option>
                                    <option value="60">Tây Ninh</option>
                                    <option value="51">Nam Định</option>
                                    <option value="49">Lâm Đồng</option>
                                    <option value="12">Lào Cai</option>
                                    <option value="18">Yên Bái</option>
                                    <option value="29">Biên Hòa</option>
                                    <option value="11">Tuyên Quang</option>
                                    <option value="59">Sơn La</option>
                                    <option value="14">Quảng Trị</option>
                                    <option value="8">Lạng Sơn</option>
                                    <option value="30">Bình Định</option>
                                    <option value="5">Thừa Thiên Huế</option>
                                    <option value="41">Đồng Tháp</option>
                                    <option value="48">Lai Châu</option>
                                    <option value="65">Kiên Giang</option>
                                    <option value="33">Bình Thuận</option>
                                    <option value="56">Quảng Nam</option>
                                    <option value="57">Quảng Ngãi</option>
                                    <option value="47">Kon Tum</option>
                                    <option value="66">Vĩnh Long</option>
                                    <option value="58">Sóc Trăng</option>
                                    <option value="71">Đắk Nông</option>
                                    <option value="54">Phú Yên</option>
                                    <option value="64">Trà Vinh</option>
                                </select><span class="select2 select2-container select2-container--default" dir="ltr"
                                               style="width: 223px;"><span class="selection"><span
                                                class="select2-selection select2-selection--single" role="combobox"
                                                aria-haspopup="true"
                                                aria-expanded="false" tabindex="0"
                                                aria-labelledby="select2-sl-tinh-thanh-container"><span
                                                    class="select2-selection__rendered"
                                                    id="select2-sl-tinh-thanh-container"
                                                    title="Chọn tỉnh thành">Chọn tỉnh thành</span><span
                                                    class="select2-selection__arrow"
                                                    role="presentation"><b
                                                        role="presentation"></b></span></span></span><span
                                            class="dropdown-wrapper"
                                            aria-hidden="true"></span></span>
                            </div>
                        </div>

                        <div class="col-filter-2">
                            <div class="filter-bg-gray">
                                <div class="txt-head">
                                    Hãng sản xuất và Model
                                    <a data-toggle="collapse" href="#collapse1-mobile" class="visible-xs"><i
                                                class="icon-sprite-24 icon-plus-2"></i></a>
                                </div>
                            </div>
                            <div class="filter-content in" style="padding: 20px;" id="collapse1-mobile">
                                <div class="select2 sl-comm pb10">
                                    <select class="select2 form-control sl-font14 sl-make-mb select2-hidden-accessible"
                                            id="sl-hang-sx-mb" name="make[]" tabindex="-1" aria-hidden="true">
                                        <option value="">Hãng sản xuất</option>
                                        <option value="443">KIA</option>
                                        <option value="742" selected="selected">Toyota</option>
                                        <option value="361">Hyundai</option>
                                        <option value="260">Ford</option>
                                        <option value="488">Mazda</option>
                                        <option value="122">Chevrolet</option>
                                        <option value="522">Mercedes-Benz</option>
                                        <option value="476">Lexus</option>
                                        <option value="588">Nissan</option>
                                        <option value="313">Honda</option>
                                        <option value="721">Suzuki</option>
                                        <option value="553">Mitsubishi</option>
                                        <option value="1010">Hãng Khác</option>
                                        <option value="1351">THACO</option>
                                        <option value="72">BMW</option>
                                        <option value="648">Peugeot</option>
                                        <option value="471">Land Rover</option>
                                        <option value="193">Daewoo</option>
                                        <option value="34">Audi</option>
                                        <option value="399">Isuzu</option>
                                        <option value="1334">DongFeng</option>
                                        <option value="840">Volkswagen</option>
                                        <option value="962">Lincoln</option>
                                        <option value="1316">HOWO</option>
                                        <option value="1399">Zoyte</option>
                                        <option value="423">Jaguar</option>
                                        <option value="1015">Renault</option>
                                        <option value="62">Bentley</option>
                                        <option value="679">Porsche</option>
                                        <option value="999">Haima</option>
                                        <option value="1310">Hino</option>
                                        <option value="693">SsangYong</option>
                                        <option value="390">Infiniti</option>
                                        <option value="1322">Sinotruck</option>
                                        <option value="94">Cadillac</option>
                                        <option value="1277">Volvo</option>
                                        <option value="548">MINI</option>
                                        <option value="895">Luxgen</option>
                                        <option value="14">Acura</option>
                                        <option value="88">BYD</option>
                                        <option value="171">Chrysler</option>
                                        <option value="688">Rolls-Royce</option>
                                        <option value="1370">Baic</option>
                                        <option value="217">Ferrari</option>
                                        <option value="233">Fiat</option>
                                        <option value="409">Daihatsu</option>
                                        <option value="435">Jeep</option>
                                        <option value="703">Subaru</option>
                                        <option value="669">Lamborghini</option>
                                        <option value="903">Samsung</option>
                                        <option value="835">Smart</option>
                                        <option value="1006">Geely</option>
                                        <option value="1389">Citroen</option>
                                        <option value="1339">Shacman</option>
                                        <option value="1367">CMC</option>
                                        <option value="1419">Đầu Kéo Mỹ</option>
                                    </select><span class="select2 select2-container select2-container--default"
                                                   dir="ltr"
                                                   style="width: 213px;"><span class="selection"><span
                                                    class="select2-selection select2-selection--single" role="combobox"
                                                    aria-haspopup="true" aria-expanded="false" tabindex="0"
                                                    aria-labelledby="select2-sl-hang-sx-mb-container"><span
                                                        class="select2-selection__rendered"
                                                        id="select2-sl-hang-sx-mb-container"
                                                        title="Toyota">Toyota</span><span
                                                        class="select2-selection__arrow"
                                                        role="presentation"><b
                                                            role="presentation"></b></span></span></span><span
                                                class="dropdown-wrapper"
                                                aria-hidden="true"></span></span>
                                </div>

                                <div class="select2 sl-comm">
                                    <select class="select2 form-control sl-font14 sl-model-mb select2-hidden-accessible"
                                            id="sl-model-mb" name="model[]" tabindex="-1" aria-hidden="true">
                                        <option value="">Model</option>
                                        <option value="880">Innova</option>
                                        <option value="755">Camry</option>
                                        <option value="772">Fortuner</option>
                                        <option selected="selected" value="877">Vios</option>
                                        <option value="777">Highlander</option>
                                        <option value="778">Hilux</option>
                                        <option value="1168">Corolla Altis</option>
                                        <option value="783">Prado</option>
                                        <option value="828">Yaris</option>
                                        <option value="804">RAV4</option>
                                        <option value="879">Zace</option>
                                        <option value="743">4Runner</option>
                                        <option value="793">86</option>
                                        <option value="944">Alphard</option>
                                        <option value="748">Auris</option>
                                        <option value="749">Avalon</option>
                                        <option value="750">Avensis</option>
                                        <option value="751">Aygo</option>
                                        <option value="761">Corolla</option>
                                        <option value="758">Celica</option>
                                        <option value="782">Land Cruiser</option>
                                        <option value="762">Corona</option>
                                        <option value="765">Crown</option>
                                        <option value="771">FJ Cruiser</option>
                                        <option value="776">HiAce</option>
                                        <option value="1004">iQ</option>
                                        <option value="788">Matrix</option>
                                        <option value="798">Previa</option>
                                        <option value="799">Prius</option>
                                        <option value="807">Sequoia</option>
                                        <option value="809">Sienna</option>
                                        <option value="814">Supra</option>
                                        <option value="815">Tacoma</option>
                                        <option value="829">Venza</option>
                                        <option value="1248">Verso</option>
                                        <option value="820">Vista</option>
                                        <option value="827">Wish</option>
                                        <option value="1253">Cresseda</option>
                                    </select><span class="select2 select2-container select2-container--default"
                                                   dir="ltr"
                                                   style="width: 213px;"><span class="selection"><span
                                                    class="select2-selection select2-selection--single" role="combobox"
                                                    aria-haspopup="true" aria-expanded="false" tabindex="0"
                                                    aria-labelledby="select2-sl-model-mb-container"><span
                                                        class="select2-selection__rendered"
                                                        id="select2-sl-model-mb-container" title="Vios">Vios</span><span
                                                        class="select2-selection__arrow" role="presentation"><b
                                                            role="presentation"></b></span></span></span><span
                                                class="dropdown-wrapper"
                                                aria-hidden="true"></span></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-filter-2">
                            <div class="filter-bg-gray">
                                <div class="txt-head pb3">
                                    Giá bán
                                    <a data-toggle="collapse" class="collapsed" href="#collapse2"><i
                                                class="icon-sprite-24 icon-plus-2"></i></a>
                                </div>
                            </div>
                            <div class="filter-content collapse" aria-expanded="false" id="collapse2">
                                <div class="ver-scroll" tabindex="0"
                                     style="width: 93px; padding-right: 22px; outline: none; overflow: hidden;">
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="price[]" value="2"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        100 triệu - 200 triệu </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="price[]" value="3"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        200 triệu - 300 triệu </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="price[]" value="4"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        300 triệu - 400 triệu </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="price[]" value="5"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        400 triệu - 500 triệu </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="price[]" value="6"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        500 triệu - 600 triệu </label>
                                </div>
                                <div style="position: absolute; z-index: 1; margin: 0px; padding: 0px; display: none; left: 100px; top: 0px;">
                                    <div class="enscroll-track vertical-track" style="position: relative;"><a href=""
                                                                                                              class="vertical-handle"
                                                                                                              style="position: absolute; z-index: 1;">
                                            <div class="top"></div>
                                            <div class="bottom"></div>
                                        </a></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-filter-2">
                            <div class="filter-bg-gray">
                                <div class="txt-head pb3">
                                    Năm sản xuất
                                    <a data-toggle="collapse" class="collapsed" href="#collapse3"><i
                                                class="icon-sprite-24 icon-plus-2"></i></a>
                                </div>
                            </div>
                            <div class="filter-content collapse" aria-expanded="false" id="collapse3">
                                <div class="ver-scroll" tabindex="0"
                                     style="width: 93px; padding-right: 22px; outline: none; overflow: hidden;">
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2018"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2018 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2017"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2017 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2016"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2016 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2015"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2015 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2014"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2014 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2013"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2013 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2012"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2012 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2011"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2011 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2010"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2010 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2009"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2009 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2008"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2008 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2007"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2007 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2006"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2006 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2005"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2005 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2004"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2004 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2003"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2003 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2002"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2002 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2001"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2001 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="2000"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        2000 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="1999"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        1999 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="1998"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        1998 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="1997"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        1997 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="1996"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        1996 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="1995"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        1995 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="1994"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        1994 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="1993"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        1993 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="1992"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        1992 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="1991"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        1991 </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="year[]" value="1990"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        1990 </label>
                                </div>
                                <div style="position: absolute; z-index: 1; margin: 0px; padding: 0px; display: none; left: 100px; top: 0px;">
                                    <div class="enscroll-track vertical-track" style="position: relative;"><a href=""
                                                                                                              class="vertical-handle"
                                                                                                              style="position: absolute; z-index: 1;">
                                            <div class="top"></div>
                                            <div class="bottom"></div>
                                        </a></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-filter-2">
                            <div class="filter-bg-gray">
                                <div class="txt-head pb3">
                                    Người bán
                                    <a data-toggle="collapse" class="collapsed" href="#collapse4"><i
                                                class="icon-sprite-24 icon-plus-2"></i></a>
                                </div>
                            </div>
                            <div class="filter-content collapse" aria-expanded="false" id="collapse4">
                                <div class="ver-scroll row m0" tabindex="0"
                                     style="width: 93px; padding-right: 22px; outline: none; overflow: hidden;">
                                    <div class="col-xs-6 col-sm-6 p0">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" class="icheck" name="user_type[]"
                                                        value="personal"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            Cá nhân </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 p0">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" class="icheck" name="user_type[]" value="salon"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            Đại lý </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 p0">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" class="icheck" name="user_type[]" value="agency"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            Môi giới </label>
                                    </div>
                                </div>
                                <div style="position: absolute; z-index: 1; margin: 0px; padding: 0px; display: none; left: 100px; top: 0px;">
                                    <div class="enscroll-track vertical-track" style="position: relative;"><a href=""
                                                                                                              class="vertical-handle"
                                                                                                              style="position: absolute; z-index: 1;">
                                            <div class="top"></div>
                                            <div class="bottom"></div>
                                        </a></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-filter-2">
                            <div class="filter-bg-gray">
                                <div class="txt-head pb3">
                                    Hộp số
                                    <a data-toggle="collapse" class="collapsed" href="#collapse5"><i
                                                class="icon-sprite-24 icon-plus-2"></i></a>
                                </div>
                            </div>
                            <div class="filter-content collapse" aria-expanded="false" id="collapse5">
                                <div class="ver-scroll row m0" tabindex="0"
                                     style="width: 93px; padding-right: 22px; outline: none; overflow: hidden;">
                                    <div class="col-xs-6 col-sm-6 p0">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" name="transmission[]" class="icheck"
                                                        value="manual"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            Số sàn </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 p0">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" name="transmission[]" class="icheck"
                                                        value="auto"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            Số tự động </label>
                                    </div>
                                </div>
                                <div style="position: absolute; z-index: 1; margin: 0px; padding: 0px; display: none; left: 100px; top: 0px;">
                                    <div class="enscroll-track vertical-track" style="position: relative;"><a href=""
                                                                                                              class="vertical-handle"
                                                                                                              style="position: absolute; z-index: 1;">
                                            <div class="top"></div>
                                            <div class="bottom"></div>
                                        </a></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-filter-2">
                            <div class="filter-bg-gray">
                                <div class="txt-head pb3">
                                    Nhiên liệu
                                    <a data-toggle="collapse" class="collapsed" href="#collapse6"><i
                                                class="icon-sprite-24 icon-plus-2"></i></a>
                                </div>
                            </div>
                            <div class="filter-content collapse" aria-expanded="false" id="collapse6">
                                <div class="ver-scroll row m0" tabindex="0"
                                     style="width: 93px; padding-right: 22px; outline: none; overflow: hidden;">
                                    <div class="col-xs-6 col-sm-6 p0">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" name="fuel[]" class="icheck" value="gasoline"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            Xăng </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 p0">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" name="fuel[]" class="icheck" value="oil"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            Dầu </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 p0">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" name="fuel[]" class="icheck" value="hybrid"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            Hỗn hợp </label>
                                    </div>
                                </div>
                                <div style="position: absolute; z-index: 1; margin: 0px; padding: 0px; display: none; left: 100px; top: 0px;">
                                    <div class="enscroll-track vertical-track" style="position: relative;"><a href=""
                                                                                                              class="vertical-handle"
                                                                                                              style="position: absolute; z-index: 1;">
                                            <div class="top"></div>
                                            <div class="bottom"></div>
                                        </a></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-filter-2">
                            <div class="filter-bg-gray">
                                <div class="txt-head pb3">
                                    Loại xe
                                    <a data-toggle="collapse" class="collapsed" href="#collapse7"><i
                                                class="icon-sprite-24 icon-plus-2"></i></a>
                                </div>
                            </div>
                            <div class="filter-content collapse pb0" aria-expanded="false" id="collapse7">
                                <div class="list-type-xe row m0">
                                    <div class="col-xs-4 col-sm-4 p0 td-loai-xe">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" class="icheck" name="body_style[]" value="8"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <i class="icon-xe icon-xe-1"></i>
                                            Sedan </label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 p0 td-loai-xe">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" class="icheck" name="body_style[]" value="4"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <i class="icon-xe icon-xe-4"></i>
                                            SUV </label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 p0 td-loai-xe">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" class="icheck" name="body_style[]" value="10"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <i class="icon-xe icon-xe-2"></i>
                                            Hatchback </label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 p0 td-loai-xe">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" class="icheck" name="body_style[]" value="6"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <i class="icon-xe icon-xe-6"></i>
                                            Xe Bán tải </label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 p0 td-loai-xe">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" class="icheck" name="body_style[]" value="20"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <i class="icon-xe icon-xe-9"></i>
                                            Xe tải </label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 p0 td-loai-xe">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" class="icheck" name="body_style[]" value="9"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <i class="icon-xe icon-xe-8"></i>
                                            MPV </label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 p0 td-loai-xe">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" class="icheck" name="body_style[]" value="21"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <i class="icon-xe icon-xe-6"></i>
                                            Van/Minivan </label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 p0 td-loai-xe">
                                        <label class="lbinput">
                                            <div class="icheckbox_square-blue" style="position: relative;"><input
                                                        type="checkbox" class="icheck" name="body_style[]" value="3"
                                                        style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                     style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <i class="icon-xe icon-xe-5"></i>
                                            Coupe </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-filter-2">
                            <div class="filter-bg-gray">
                                <div class="txt-head pb3">
                                    Kiểu dẫn động
                                    <a data-toggle="collapse" class="collapsed" href="#collapse8"><i
                                                class="icon-sprite-24 icon-plus-2"></i></a>
                                </div>
                            </div>
                            <div class="filter-content collapse p0" aria-expanded="false" id="collapse8">
                                <div class="ver-scroll" tabindex="0"
                                     style="width: 93px; padding-right: 22px; outline: none; overflow: hidden;">
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="drivertrain[]" value="1"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        FWD: Cầu trước </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="drivertrain[]" value="2"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        RWD: Cầu sau </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="drivertrain[]" value="3"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        4WD: 4 bánh </label>
                                </div>
                                <div style="position: absolute; z-index: 1; margin: 0px; padding: 0px; display: none; left: 100px; top: 0px;">
                                    <div class="enscroll-track vertical-track" style="position: relative;"><a href=""
                                                                                                              class="vertical-handle"
                                                                                                              style="position: absolute; z-index: 1;">
                                            <div class="top"></div>
                                            <div class="bottom"></div>
                                        </a></div>
                                </div>
                            </div>
                        </div>

                        <!--    -->
                        <!--	<div class="col-filter-2">-->
                        <!--		<div class="filter-bg-gray">-->
                        <!--			<div class="txt-head pb3">-->
                        <!--				Màu xe-->
                        <!--				<a data-toggle="collapse" href="#collapse9"><i class="icon-sprite-24 icon-plus-2"></i></a>-->
                        <!--			</div>-->
                        <!--		</div>-->
                        <!--		<div class="filter-content in" id="collapse9">-->
                        <!--			<div class="ver-scroll row m0">-->
                        <!---->
                        <!--				<label class="w100p">Hợp mệnh Kim</label>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Đen-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Đỏ-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Bạc-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Đỏ-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!---->
                        <!--				<label  class="w100p">Hợp mệnh Mộc</label>-->
                        <!---->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Nâu-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Xanh-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Xám-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Be-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Vàng-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Đỏ-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!---->
                        <!--				<label  class="w100p">Hợp mệnh Thủy</label>-->
                        <!---->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Nâu-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Xanh-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Xám-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Be-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Vàng-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--				<div class="col-xs-6 col-sm-6 p0">-->
                        <!--					<label class="lbinput">-->
                        <!--						<input type="checkbox" name="mau-xe" class="icheck"> Đỏ-->
                        <!--					</label>-->
                        <!--				</div>-->
                        <!--			</div>-->
                        <!--		</div>-->
                        <!--	</div>-->

                        <div class="col-filter-2 mau-sac">
                            <div class="filter-bg-gray">
                                <div class="txt-head pb3">
                                    Xuất xứ
                                    <a data-toggle="collapse" class="collapsed" href="#collapse10"><i
                                                class="icon-sprite-24 icon-plus-2"></i></a>
                                </div>
                            </div>
                            <div class="filter-content collapse p0" aria-expanded="false" id="collapse10">
                                <div class="ver-scroll" tabindex="0"
                                     style="width: 93px; padding-right: 22px; outline: none; overflow: hidden;">
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="country[]" value="1"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        Trong nước </label>
                                    <label class="lbinput">
                                        <div class="icheckbox_square-blue" style="position: relative;"><input
                                                    type="checkbox" class="icheck" name="country[]" value="2"
                                                    style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        Nhập khẩu </label>
                                </div>
                                <div style="position: absolute; z-index: 1; margin: 0px; padding: 0px; display: none; left: 100px; top: 0px;">
                                    <div class="enscroll-track vertical-track" style="position: relative;"><a href=""
                                                                                                              class="vertical-handle"
                                                                                                              style="position: absolute; z-index: 1;">
                                            <div class="top"></div>
                                            <div class="bottom"></div>
                                        </a></div>
                                </div>
                            </div>
                        </div>

                        <div class="row m0 huy-loc-ft visible-xs">
                            <div class="col-xs-6 p0">
                                <a class="huy-ft toggle-filter" href="#">HỦY</a>
                            </div>
                            <div class="col-xs-6 p0">
                                <a class="loc-ft apply-filter" href="#">LỌC</a>
                            </div>
                        </div>
                    </form>

                    <style>
                        @media (max-width: 767px) {
                            .list-filter {
                                margin: 0 15px !important;
                            }
                        }
                    </style>
                </div>
                <!-- End Column Left -->

                <!-- Column Right -->
                <div class="col-right pl30">
                    <!-- Lọc Mobile-->
                    <div class="visible-xs">
                        <div class="div-filter-mb row m0">
                            <div class="filter-price">
                                <a href="https://choxe.net/xe-cu/toyota/vios?sort=price_asc"><i
                                            class="icon-sprite-24 icon-filter-price"></i> Giá thấp đến cao</a>
                            </div>
                            <div class="div-loc-mb">
                                <a href="#" class="toggle-filter"><i class="icon-sprite-24 icon-loc"></i> Lọc</a>
                            </div>
                        </div>
                    </div>
                    <!-- End Lọc Mobile -->

                    <div class="row m0 mt15 nonemb">
                        <div class="map-link hidden-xs cbreadcrumb">
                            <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    <a class="" href="https://choxe.net/" itemprop="item"><span
                                                itemprop="name">Chợ xe</span></a>
                                </li>
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    &gt; <a href="https://choxe.net/xe-cu" itemprop="item"><span
                                                itemprop="name">Xe cũ</span></a>
                                </li>
                                &gt;
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    <a href="https://choxe.net/xe-cu/toyota" itemprop="item"><span
                                                itemprop="name">Toyota</span></a>
                                </li>
                                &gt;
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a
                                            href="https://choxe.net/xe-cu/toyota/vios" itemprop="item"><span
                                                itemprop="name">Vios</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="list-filter row">

                        <div class="huy-loc">
                            <a href="https://choxe.net/xe"><i class="icon-sprite-24 icon-close-orange"></i> HỦY LỌC</a>
                        </div>
                        <div class="filter-com">
                            Xe cũ <span class="btn-close"><a href="https://choxe.net/xe/toyota/vios"><i
                                            class="icon-sprite-24 icon-close"></i></a></span>
                        </div>
                        <div class="filter-com">
                            Toyota <span class="btn-close"><a href="https://choxe.net/xe-cu/vios"><i
                                            class="icon-sprite-24 icon-close"></i></a></span>
                        </div>
                        <div class="filter-com">
                            Vios <span class="btn-close"><a href="https://choxe.net/xe-cu/toyota"><i
                                            class="icon-sprite-24 icon-close"></i></a></span>
                        </div>
                    </div>
                    <style>
                        @media (min-width: 767px) {
                            .pos-sl-sx {
                                top: 33px !important;
                            }
                        }
                    </style>
                    <!-- Box Xe bán gấp -->
                    <div class="deal table-xe" id="block-2nd">
                        <h1 class="title-underline">
                            Xe Toyota Vios cũ </h1>

                        <div class="row sap-xep fr pos-sl-sx hidden-xs">
                            <div class="sl-sap-xep select2 ">
                                <select class="form-control select2-hidden-accessible" id="sl-sort" tabindex="-1"
                                        aria-hidden="true">
                                    <option value="https://choxe.net/xe-cu/toyota/vios" selected="">Sắp xếp mặc định
                                    </option>
                                    <option value="https://choxe.net/xe-cu/toyota/vios?sort=price_asc">Giá thấp đến cao
                                    </option>
                                    <option value="https://choxe.net/xe-cu/toyota/vios?sort=price_desc">Giá cao đến thấp
                                    </option>
                                    <option value="https://choxe.net/xe-cu/toyota/vios?sort=timestamp_desc">Tin đăng mới
                                        nhất
                                    </option>
                                </select><span class="select2 select2-container select2-container--default" dir="ltr"
                                               style="width: 170px;"><span class="selection"><span
                                                class="select2-selection select2-selection--single" role="combobox"
                                                aria-haspopup="true"
                                                aria-expanded="false" tabindex="0"
                                                aria-labelledby="select2-sl-sort-container"><span
                                                    class="select2-selection__rendered" id="select2-sl-sort-container"
                                                    title="Sắp xếp mặc định">Sắp xếp mặc định</span><span
                                                    class="select2-selection__arrow"
                                                    role="presentation"><b
                                                        role="presentation"></b></span></span></span><span
                                            class="dropdown-wrapper"
                                            aria-hidden="true"></span></span>
                            </div>
                        </div>

                        <p class="font16 mt15 p15mb font14mb">Tìm được <span class="txt-orange">(187)</span> tin bán xe
                        </p>

                        <table class="table list-news">

                            <tbody>
                            <tr>
                                <td class="row news  items-284204 pos-1003630">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-15mt-2015-tai-ha-noi-284204.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/201811/284204/228x153-gallery5bf1215fc58a1.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios E 1.5MT 2015 284204"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-15mt-2015-tai-ha-noi-284204.html">
                                                    <h2>Xe Cũ Toyota Vios E 1.5MT 2015</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 448 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2015
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        40,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số sàn
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hà Nội
                                                </div>
                                                <div class="old-news">Đăng 3 ngày trước</div>

                                                <a class="name-store visible-xs" href="#">Nhất Huy Auto</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284204, '0969122929')"
                                                            id="salon_id_284204">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284204"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284204" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">448 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hà Nội
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/nhat-huy-auto-s818.html">Nhất
                                            Huy Auto</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-263063 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-2010-tai-ha-noi-263063.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/gallery/2018/06/228x153-5b2abd7a686d9.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios E 2010 263063"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-2010-tai-ha-noi-263063.html">
                                                    <h2>Xe Cũ Toyota Vios E 2010</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 265 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2010
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        68,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số sàn
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hà Nội
                                                </div>
                                                <div class="old-news">Đăng 21/06/2018</div>

                                                <a class="name-store visible-xs" href="#">Nguyễn Thành</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(263063, '0965204064')"
                                                            id="salon_id_263063">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=263063"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=263063" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">265 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hà Nội
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/nguyen-thanh-s807.html">Nguyễn
                                            Thành</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284601 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-2017-tai-ha-noi-284601.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bf51f20f333c.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios E 2017 284601"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-2017-tai-ha-noi-284601.html">
                                                    <h2>Xe Cũ Toyota Vios E 2017</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 515 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2017
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        10,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số sàn
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hà Nội
                                                </div>
                                                <div class="old-news">Đăng 5 tiếng trước</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284601, '094.991.6666')"
                                                            id="salon_id_284601">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284601"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284601" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">515 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hà Nội
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284616 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-mt-2005-tai-ha-noi-284616.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bf52bc0ca150.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios MT 2005 284616"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-mt-2005-tai-ha-noi-284616.html">
                                                    <h2>Xe Cũ Toyota Vios MT 2005</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 165 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2005
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        130,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số sàn
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hà Nội
                                                </div>
                                                <div class="old-news">Đăng 13 tiếng trước</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284616, '039 7466756')"
                                                            id="salon_id_284616">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284616"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284616" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">165 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hà Nội
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284515 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-15at-2017-tai-ha-noi-284515.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bf41724724cc.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios E 1.5AT 2017 284515"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-15at-2017-tai-ha-noi-284515.html">
                                                    <h2>Xe Cũ Toyota Vios E 1.5AT 2017</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 535 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2017
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        15,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số tự động
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hà Nội
                                                </div>
                                                <div class="old-news">Đăng Hôm qua</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284515, '0965430405')"
                                                            id="salon_id_284515">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284515"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284515" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">535 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hà Nội
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284614 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-mt-2010-tai-ha-noi-284614.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bf52ad3ce867.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios MT 2010 284614"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-mt-2010-tai-ha-noi-284614.html">
                                                    <h2>Xe Cũ Toyota Vios MT 2010</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 226 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2010
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        80,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số sàn
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hà Nội
                                                </div>
                                                <div class="old-news">Đăng Hôm qua</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284614, '0919686678')"
                                                            id="salon_id_284614">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284614"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284614" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">226 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hà Nội
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284445 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-15g-at-2014-tai-hai-duong-284445.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bf3c5ba29ac6.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios 1.5G AT 2014 284445"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-15g-at-2014-tai-hai-duong-284445.html">
                                                    <h2>Xe Cũ Toyota Vios 1.5G AT 2014</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 485 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2014
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        42,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số tự động
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hải Dương
                                                </div>
                                                <div class="old-news">Đăng Hôm qua</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284445, '0914388100')"
                                                            id="salon_id_284445">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284445"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284445" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">485 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hải Dương
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284401 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-at-2017-tai-hai-duong-284401.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bf38a4fa1256.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios E AT 2017 284401"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-at-2017-tai-hai-duong-284401.html">
                                                    <h2>Xe Cũ Toyota Vios E AT 2017</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 505 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2017
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        16,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số tự động
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hải Dương
                                                </div>
                                                <div class="old-news">Đăng Hôm qua</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284401, '0904.409.656 ')"
                                                            id="salon_id_284401">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284401"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284401" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">505 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hải Dương
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284531 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-mt-2010-tai-hai-duong-284531.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/201811/284531/gallery/228x153-46508439_192525215015929_1565420390031818752_n.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios MT 2010 284531"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-mt-2010-tai-hai-duong-284531.html">
                                                    <h2>Xe Cũ Toyota Vios MT 2010</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 252 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2010
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        80,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số sàn
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hải Dương
                                                </div>
                                                <div class="old-news">Đăng Hôm qua</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284531, '0988478009')"
                                                            id="salon_id_284531">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284531"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284531" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">252 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hải Dương
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284397 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-mt-2016-tai-ha-noi-284397.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bf388053685a.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios E MT 2016 284397"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-mt-2016-tai-ha-noi-284397.html">
                                                    <h2>Xe Cũ Toyota Vios E MT 2016</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 485 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2016
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        40,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số sàn
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hà Nội
                                                </div>
                                                <div class="old-news">Đăng Hôm qua</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284397, '0989824078 ')"
                                                            id="salon_id_284397">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284397"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284397" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">485 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hà Nội
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284474 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-15g-at-2017-tai-ha-noi-284474.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bf3d24fcbdd4.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios 1.5G AT 2017 284474"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-15g-at-2017-tai-ha-noi-284474.html">
                                                    <h2>Xe Cũ Toyota Vios 1.5G AT 2017</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 566 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2017
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        30,065<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số tự động
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hà Nội
                                                </div>
                                                <div class="old-news">Đăng Hôm qua</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284474, ' 0974 813 188')"
                                                            id="salon_id_284474">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284474"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284474" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">566 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hà Nội
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284449 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-g-2018-tai-ha-noi-284449.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bf3c71a4f6d3.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios G 2018 284449"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-g-2018-tai-ha-noi-284449.html">
                                                    <h2>Xe Cũ Toyota Vios G 2018</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 576 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2018
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        10,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số tự động
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hà Nội
                                                </div>
                                                <div class="old-news">Đăng Hôm qua</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284449, '0979559661 ')"
                                                            id="salon_id_284449">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284449"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284449" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">576 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hà Nội
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284541 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-mt-2005-tai-ha-noi-284541.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bf44ae5beefc.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios MT 2005 284541"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-mt-2005-tai-ha-noi-284541.html">
                                                    <h2>Xe Cũ Toyota Vios MT 2005</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 168 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2005
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        130,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số sàn
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hà Nội
                                                </div>
                                                <div class="old-news">Đăng Hôm qua</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284541, '0982750183')"
                                                            id="salon_id_284541">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284541"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284541" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">168 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hà Nội
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284434 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-15e-at-cvt-2017-tai-ha-noi-284434.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bf3c11d86be1.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios 1.5E AT CVT 2017 284434"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-15e-at-cvt-2017-tai-ha-noi-284434.html">
                                                    <h2>Xe Cũ Toyota Vios 1.5E AT CVT 2017</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 535 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2017
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        21,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số tự động
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hà Nội
                                                </div>
                                                <div class="old-news">Đăng 2 ngày trước</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284434, '0965430405 ')"
                                                            id="salon_id_284434">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284434"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284434" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">535 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hà Nội
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284224 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-g-2015-tai-vinh-phuc-284224.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bf16e8c91c12.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios G 2015 284224"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-g-2015-tai-vinh-phuc-284224.html">
                                                    <h2>Xe Cũ Toyota Vios G 2015</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 490 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2015
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        43,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số tự động
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Vĩnh Phúc
                                                </div>
                                                <div class="old-news">Đăng 3 ngày trước</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284224, '0826227666')"
                                                            id="salon_id_284224">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284224"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284224" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">490 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Vĩnh Phúc
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284070 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-15e-2012-tai-ha-noi-284070.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5beed72f89124.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios 1.5E 2012 284070"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-15e-2012-tai-ha-noi-284070.html">
                                                    <h2>Xe Cũ Toyota Vios 1.5E 2012</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 355 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2012
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        60,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số sàn
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hà Nội
                                                </div>
                                                <div class="old-news">Đăng 5 ngày trước</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284070, ' 0399000990')"
                                                            id="salon_id_284070">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284070"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284070" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">355 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hà Nội
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284077 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-at-2017-tai-hai-duong-284077.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5beedbc9d63f3.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios AT 2017 284077"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-at-2017-tai-hai-duong-284077.html">
                                                    <h2>Xe Cũ Toyota Vios AT 2017</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 505 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2017
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        15,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số tự động
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hải Dương
                                                </div>
                                                <div class="old-news">Đăng 5 ngày trước</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284077, '0914388100')"
                                                            id="salon_id_284077">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284077"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284077" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">505 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hải Dương
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-284099 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-mt-2003-tai-ha-noi-284099.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/201811/284099/gallery/228x153-46409589_2200928356843318_7790255876698275840_n.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios MT 2003 284099"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-mt-2003-tai-ha-noi-284099.html">
                                                    <h2>Xe Cũ Toyota Vios MT 2003</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 165 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2003
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        150,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số sàn
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hà Nội
                                                </div>
                                                <div class="old-news">Đăng 5 ngày trước</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(284099, '0979438522')"
                                                            id="salon_id_284099">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=284099"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=284099" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">165 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hà Nội
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-283949 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-mt-2009-tai-hai-duong-283949.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bed43b971ead.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios MT 2009 283949"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-mt-2009-tai-hai-duong-283949.html">
                                                    <h2>Xe Cũ Toyota Vios MT 2009</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 245 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2009
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        90,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số sàn
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hải Dương
                                                </div>
                                                <div class="old-news">Đăng 6 ngày trước</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(283949, '0987237641')"
                                                            id="salon_id_283949">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=283949"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=283949" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">245 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hải Dương
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row news  items-283961 pos-0">
                                    <div class="news-detail">
                                        <div class="info-left">
                                            <a href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-2008-tai-hai-duong-283961.html"
                                               class="img"><img class=""
                                                                src="/uploads/auto/2018/11/228x153-5bed4a7997645.jpg"
                                                                width="175" height="118"
                                                                alt="Xe Cũ Toyota Vios E 2008 283961"></a>
                                            <div class="block-txt-mb">
                                                <a class="tit-news"
                                                   href="https://choxe.net/xe-cu/ban-xe-cu-toyota-vios-e-2008-tai-hai-duong-283961.html">
                                                    <h2>Xe Cũ Toyota Vios E 2008</h2></a>

                                                <span class="dai-ly">Đại lý</span>
                                                <div class="price visible-xs"> 265 triệu</div>

                                                <div class="row" style="margin: 0">
                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-status"></i>
                                                        Cũ
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-calendar2"></i>
                                                        2008
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-speed2"></i>
                                                        100,000<span class="hidden-xs"> km</span>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 list-char">
                                                        <i class="icon-24 icon-sprite-24 icon-auto2"></i>
                                                        Số sàn
                                                    </div>
                                                </div>

                                                <div class="city visible-xs"><i
                                                            class="icon-24 icon-sprite-24 icon-address"></i> Hải Dương
                                                </div>
                                                <div class="old-news">Đăng 6 ngày trước</div>

                                                <a class="name-store visible-xs" href="#">Chợ Xe FB</a>
                                            </div>
                                            <div class="row list-opt hidden-xs">
                                                <!--                    <div class="col-xs-3 col-sm-3 p0">-->
                                                <!--                        <a class="compare" href="--><!--">-->
                                                <!--                            <i class="icon-sprite-24 icon-compare"></i>-->
                                                <!--                            So Sánh-->
                                                <!--                        </a>-->
                                                <!--                    </div>-->

                                                <div class="col-xs-3 col-sm-5 p0">
                                                    <button class="btn-show-phone-list"
                                                            onclick="showPhoneNumner(283961, '0976518519')"
                                                            id="salon_id_283961">
                                                        Xem số điện thoại
                                                    </button>
                                                </div>

                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-danh-sach-yeu-thich?auto_id=283961"
                                                       class="favor" data-toggle="tooltip" title=""
                                                       data-placement="bottom"
                                                       data-original-title="Thích loại xe này">
                                                        <i class="icon-sprite-24 icon-heart"></i>
                                                        Yêu thích
                                                    </a>
                                                </div>
                                                <div class="col-xs-3 col-sm-3 p0">
                                                    <a href="https://choxe.net/tao-tin-luu?auto_id=283961" class="save"
                                                       data-toggle="tooltip" title="" data-placement="bottom"
                                                       data-original-title="Lưu tin này">
                                                        <i class="icon-sprite-24 icon-save"></i>
                                                        Lưu tin
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-addr txr hidden-xs">
                                        <div class="price">265 triệu</div>
                                        <div class="city"><i class="icon-24 icon-sprite-24 icon-address"></i>Hải Dương
                                        </div>
                                        <a class="name-store" href="https://choxe.net/salon/cho-xe-fb-s784.html">Chợ Xe
                                            FB</a>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="txc page-news hidden-xs">
                            <ul class="pagination m0">


                                <li><a class="active" href="#">1</a></li>
                                <li><a class="" href="https://choxe.net/xe-cu/toyota/vios?page=2">2</a></li>

                                <li><a class="" href="https://choxe.net/xe-cu/toyota/vios?page=3">3</a></li>

                                <li><a class="" href="https://choxe.net/xe-cu/toyota/vios?page=2"><i
                                                class="icon-sprite-24 icon-next-page"></i></a></li>
                                <li><a class="" href="https://choxe.net/xe-cu/toyota/vios?page=10">Trang cuối</a></li>

                            </ul>
                        </div>

                        <!-- Pagination of mobile -->
                        <div class="visible-xs page-mb">
                            <div class="sl-page">
                                <select name="page" id="sl-page">
                                    <option value="https://choxe.net/xe-cu/toyota/vios?page=1" selected="selected">Trang
                                        1/10
                                    </option>
                                    <option value="https://choxe.net/xe-cu/toyota/vios?page=2">Trang 2/10</option>
                                    <option value="https://choxe.net/xe-cu/toyota/vios?page=3">Trang 3/10</option>
                                    <option value="https://choxe.net/xe-cu/toyota/vios?page=4">Trang 4/10</option>
                                    <option value="https://choxe.net/xe-cu/toyota/vios?page=5">Trang 5/10</option>
                                    <option value="https://choxe.net/xe-cu/toyota/vios?page=6">Trang 6/10</option>
                                    <option value="https://choxe.net/xe-cu/toyota/vios?page=7">Trang 7/10</option>
                                    <option value="https://choxe.net/xe-cu/toyota/vios?page=8">Trang 8/10</option>
                                    <option value="https://choxe.net/xe-cu/toyota/vios?page=9">Trang 9/10</option>
                                    <option value="https://choxe.net/xe-cu/toyota/vios?page=10">Trang 10/10</option>
                                </select>
                                <i class="icon-sprite-24 icon-down-small"></i>
                            </div>
                            <a class="btn-next-mb" href="https://choxe.net/xe-cu/toyota/vios?page=2"><i
                                        class="icon-sprite-24 icon-next-mb"></i></a>
                        </div>
                        <!-- End Pagination -->


                    </div>
                    <!-- End Box xe bán gấp -->

                    <div class="seo_text">
                        <p style="text-align: justify;"><strong>Toyota Vios</strong> không còn là cái tên xa lạ trên thị
                            trường ô tô hiện nay. Với phiên bản <strong>Toyota Vios 2019</strong> mới ra mắ trong thời
                            gian
                            qua, Vios nhanh chóng dành lại thị phần sau một thời gian dài vắng bóng. Với những thay đổi
                            thiết kế nội, ngoại thất bên cạnh sự, thêm trang bị, bổ sung về tính năng an toàn, Toyota
                            Vios
                            vẫn đang là cái tên được ưa chuộng được nhiều người biết đến.</p>
                        <h2 style="text-align: justify;"><strong>Giới thiệu Toyota Vios</strong></h2>
                        <h3 style="text-align: justify;"><strong>Ngoại thất</strong></h3>
                        <p style="text-align: justify;">Mang đậm chất Sedan cùng lướt tản nhiệt được thiết kế mới mẻ,
                            Toyota
                            Vios 2019 càng nhận thêm được nhiều sự ưa chuộng từ khách hàng. Bên cạnh đó, cụm đèn pha,
                            hốc
                            đèn sương mù trên phiên bản<strong> Vios 2019</strong> cũng được Toyota trau chuốt và thiết
                            kế
                            lại. Nhiều khả năng xe vẫn dùng đèn pha halogen thay vì đèn LED như một số đối thủ trong
                            phân
                            khúc và phiên bản cao cấp hơn trang bị đèn LED chạy ban ngày.</p>
                        <p style="text-align: justify;">Một điều khác biệt của xe Toyota Vios phiên bản mới đó là đèn
                            hậu sử
                            dụng bóng LED, kiểu dáng khác biệt hoàn toàn so với các phiên bản cũ. Phần thân <strong>xe
                                vios
                                2019</strong> cũng rất thu hút với kính chiếu hậu lớn hơn đi cùng nhiều đường gân nổi và
                            bộ
                            mâm có thiết kế hoàn toàn. “Nhìn bắt mắt” không kém phần đầu xe Vios là cụm đuôi với bộ đèn
                            hậu
                            được vuốt nhọn hơn và dường như đèn báo phanh là dạng LED khá sắc sảo. Cụm đèn phản quang
                            đặt
                            khá thấp và mở rộng về hai bên khá hài hoà.</p>
                        <h3 style="text-align: justify;"><strong>Nội thất</strong></h3>
                        <p style="text-align: justify;">Những trang bị nâng cấp mới trên<strong> Toyota Vios
                                2019</strong>
                            bao gồm: hệ thống ở đồng hồ, cụm điều khiển điều hòa hiện đại, vô-lăng tái thiết kế. .. Bên
                            cạnh
                            đó phiên bản Vios G còn được&nbsp; trang bị thêm màn hình giải trí DVD.</p>
                        <p style="text-align: justify;">Không chỉ trau chuốt về vẻ ngoài , phiên bản Vios 2019 còn được&nbsp;
                            trang bị nhiều công nghệ&nbsp; hiện đại hơn so với thế hệ cũ như: hệ thống an toàn 7 túi
                            khí,
                            cân bằng điện tử, phanh ABS hỗ trợ phanh khẩn cấp, hỗ trợ khởi hành ngang dốc, khởi động nút
                            bấm, camera lùi. Một số trang bị cao cấp hơn so với hiện tại, trong khi không thật sự nổi
                            trội
                            so với đối thủ trong phân khúc.</p>
                        <h3 style="text-align: justify;"><strong>Hệ thống động cơ&nbsp;</strong></h3>
                        <p style="text-align: justify;">Cho đến thời điểm hiện tại vẫn chưa có thông tin về động cơ của
                            Toyota Vios 2019 tại thị trường Việt Nam, nhưng nhiều khả năng sẽ dùng động cơ 1,2 lít Dual
                            VVT-i, sản sinh công suất 86 mã lực tại 6.000 vòng/phút và mô-men xoắn 108 Nm tại 4.000
                            vòng/phút.</p>
                        <h2 style="text-align: justify;"><strong>Xe Toyota Vios tại {!! @$$settings['name'] !!}</strong></h2>
                        <p style="text-align: justify;">Bên cạnh những cải tiến cho phiên bản Vios 2019, Toyota Vios
                            được
                            biết đến với dòng xe cũ giữ giá ổn nhất thị trường. <strong>Toyota Vios cũ</strong> luôn là
                            sự
                            lựa chọn cho nhiều khách hàng mỗi nghi tìm đến thị trường ô tô đã qua sử dụng. Xe Toyota
                            Vios cũ
                            luôn nhận được sự quan tâm, ưa chuộng hàng đầu trên thị trường. Bên cạnh <strong>xe Toyota
                                Vios
                                cũ</strong>, {!! @$$settings['name'] !!} còn có đăng tin trao đổi, mua bán các dòng xe đã qua sử dụng của
                            Toyota với giá cực hot như: Toyota Fortuner, Toyota Innova, Toyota Camry,...</p>
                        <p style="text-align: justify;">{!!  @$$settings['name']  !!} là website cung cấp thông tin về giá xe ô tô, mua bán
                            trao
                            đổi xe ô tô và cập nhật cho bạn những thông tin mới nhất, chính xác nhất về thị trường ô tô,
                            các
                            dịch vụ mua bán ô tô, những thông tin hữu ích về các dòng ô tô, những chia sẻ thú vị. Tất cả
                            những gì bạn muốn tìm kiếm liên quan đến xe hơi đều có tại {!! @$settings['name'] !!} đăng tin bán xe miễn
                            phí
                            ngay.</p>
                        <p style="text-align: justify;"><strong>Chợ xe – Tìm xe là có, đăng xe là bán</strong></p></div>
                    <style>
                        .seo_text {
                            margin: 10px 0;
                        }
                    </style>


                </div>
                <!-- End Column Right -->
            </div>
        </div>

        <!-- Slider Salon nổi bật -->
        <div class="slider-salon hidden-xs">
            <div class="container">
                <div class="title-white">NGƯỜI BÁN Ô TÔ NỔI BẬT</div>
                <div class="relative">
                    <div class="slider-salon-noi slider mt15 slick-initialized slick-slider">
                        <button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous"
                                role="button" style="display: block;">Previous
                        </button>
                        <div aria-live="polite" class="slick-list draggable">
                            <div class="slick-track" role="listbox"
                                 style="opacity: 1; width: 40000px; transform: translate3d(-1140px, 0px, 0px);">
                                <div class="slider-item slick-slide slick-cloned" data-slick-index="-6"
                                     aria-hidden="true"
                                     tabindex="-1">
                                    <a class="salon-info" href="https://choxe.net/salon/nhat-huy-auto-s818.html"
                                       tabindex="-1">
                                        <img src="/uploads/salon/818/avatar/228x153-14238357_116898918767222_8772053369138979595_n.jpg"
                                             width="175" height="118">
                                        <div class="name-salon">Nhất Huy Auto</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-cloned" data-slick-index="-5"
                                     aria-hidden="true"
                                     tabindex="-1">
                                    <a class="salon-info" href="https://choxe.net/salon/-s855.html" tabindex="-1">
                                        <img src="/uploads/salon/201811/855/avatar/228x153-IMG_0405_-_Copy_-_Copy.JPG"
                                             width="175" height="118">
                                        <div class="name-salon">MERCEDES MIỀN BẮC</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-cloned" data-slick-index="-4"
                                     aria-hidden="true"
                                     tabindex="-1">
                                    <a class="salon-info" href="https://choxe.net/salon/kia-quang-ninh-s117.html"
                                       tabindex="-1">
                                        <img src="/uploads/salon/201810/117/avatar/228x153-a757b2ad995b7905204a.jpg"
                                             width="175" height="118">
                                        <div class="name-salon">Kia Quảng Ninh</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-cloned" data-slick-index="-3"
                                     aria-hidden="true"
                                     tabindex="-1">
                                    <a class="salon-info" href="https://choxe.net/salon/o-to-anh-ly-s17.html"
                                       tabindex="-1">
                                        <img src="/uploads/salon_photo/228x153-o-to-anh-ly-57d23ea0.jpg" width="175"
                                             height="118">
                                        <div class="name-salon">Ô tô Ánh Lý</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-cloned" data-slick-index="-2"
                                     aria-hidden="true"
                                     tabindex="-1">
                                    <a class="salon-info" href="https://choxe.net/salon/kia-vinh-nghe-an-s834.html"
                                       tabindex="-1">
                                        <img src="/uploads/salon/2018/834/avatar/228x153-212212121.jpg" width="175"
                                             height="118">
                                        <div class="name-salon">KIA VINH - NGHỆ AN</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-cloned" data-slick-index="-1"
                                     aria-hidden="true"
                                     tabindex="-1">
                                    <a class="salon-info" href="https://choxe.net/salon/chevrolet-thang-long-s850.html"
                                       tabindex="-1">
                                        <img src="/uploads/salon/201810/850/avatar/228x153-2017_chevrolet_thang_long_3.jpg"
                                             width="175" height="118">
                                        <div class="name-salon">Chevrolet Thăng Long</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-current slick-active" data-slick-index="0"
                                     aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide00">
                                    <a class="salon-info" href="https://choxe.net/salon/mazda-quang-ninh-s182.html"
                                       tabindex="0">
                                        <img src="/uploads/salon/201811/182/avatar/228x153-ma1.png" width="175"
                                             height="118">
                                        <div class="name-salon">Mazda Quảng Ninh</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-active" data-slick-index="1"
                                     aria-hidden="false"
                                     tabindex="-1" role="option" aria-describedby="slick-slide01">
                                    <a class="salon-info" href="https://choxe.net/salon/kia-giai-phong-s852.html"
                                       tabindex="0">
                                        <img src="https://choxe.net/assets/frontend/img/choxe-logo-avatar-2.png"
                                             width="175"
                                             height="118">
                                        <div class="name-salon">Kia Giải Phóng</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-active" data-slick-index="2"
                                     aria-hidden="false"
                                     tabindex="-1" role="option" aria-describedby="slick-slide02">
                                    <a class="salon-info" href="https://choxe.net/salon/nhat-huy-auto-s818.html"
                                       tabindex="0">
                                        <img src="/uploads/salon/818/avatar/228x153-14238357_116898918767222_8772053369138979595_n.jpg"
                                             width="175" height="118">
                                        <div class="name-salon">Nhất Huy Auto</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-active" data-slick-index="3"
                                     aria-hidden="false"
                                     tabindex="-1" role="option" aria-describedby="slick-slide03">
                                    <a class="salon-info" href="https://choxe.net/salon/-s855.html" tabindex="0">
                                        <img src="/uploads/salon/201811/855/avatar/228x153-IMG_0405_-_Copy_-_Copy.JPG"
                                             width="175" height="118">
                                        <div class="name-salon">MERCEDES MIỀN BẮC</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-active" data-slick-index="4"
                                     aria-hidden="false"
                                     tabindex="-1" role="option" aria-describedby="slick-slide04">
                                    <a class="salon-info" href="https://choxe.net/salon/kia-quang-ninh-s117.html"
                                       tabindex="0">
                                        <img src="/uploads/salon/201810/117/avatar/228x153-a757b2ad995b7905204a.jpg"
                                             width="175" height="118">
                                        <div class="name-salon">Kia Quảng Ninh</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-active" data-slick-index="5"
                                     aria-hidden="false"
                                     tabindex="-1" role="option" aria-describedby="slick-slide05">
                                    <a class="salon-info" href="https://choxe.net/salon/o-to-anh-ly-s17.html"
                                       tabindex="0">
                                        <img src="/uploads/salon_photo/228x153-o-to-anh-ly-57d23ea0.jpg" width="175"
                                             height="118">
                                        <div class="name-salon">Ô tô Ánh Lý</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide" data-slick-index="6" aria-hidden="true"
                                     tabindex="-1"
                                     role="option" aria-describedby="slick-slide06">
                                    <a class="salon-info" href="https://choxe.net/salon/kia-vinh-nghe-an-s834.html"
                                       tabindex="-1">
                                        <img src="/uploads/salon/2018/834/avatar/228x153-212212121.jpg" width="175"
                                             height="118">
                                        <div class="name-salon">KIA VINH - NGHỆ AN</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide" data-slick-index="7" aria-hidden="true"
                                     tabindex="-1"
                                     role="option" aria-describedby="slick-slide07">
                                    <a class="salon-info" href="https://choxe.net/salon/chevrolet-thang-long-s850.html"
                                       tabindex="-1">
                                        <img src="/uploads/salon/201810/850/avatar/228x153-2017_chevrolet_thang_long_3.jpg"
                                             width="175" height="118">
                                        <div class="name-salon">Chevrolet Thăng Long</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-cloned" data-slick-index="8"
                                     aria-hidden="true"
                                     tabindex="-1">
                                    <a class="salon-info" href="https://choxe.net/salon/mazda-quang-ninh-s182.html"
                                       tabindex="-1">
                                        <img src="/uploads/salon/201811/182/avatar/228x153-ma1.png" width="175"
                                             height="118">
                                        <div class="name-salon">Mazda Quảng Ninh</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-cloned" data-slick-index="9"
                                     aria-hidden="true"
                                     tabindex="-1">
                                    <a class="salon-info" href="https://choxe.net/salon/kia-giai-phong-s852.html"
                                       tabindex="-1">
                                        <img src="https://choxe.net/assets/frontend/img/choxe-logo-avatar-2.png"
                                             width="175"
                                             height="118">
                                        <div class="name-salon">Kia Giải Phóng</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-cloned" data-slick-index="10"
                                     aria-hidden="true"
                                     tabindex="-1">
                                    <a class="salon-info" href="https://choxe.net/salon/nhat-huy-auto-s818.html"
                                       tabindex="-1">
                                        <img src="/uploads/salon/818/avatar/228x153-14238357_116898918767222_8772053369138979595_n.jpg"
                                             width="175" height="118">
                                        <div class="name-salon">Nhất Huy Auto</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-cloned" data-slick-index="11"
                                     aria-hidden="true"
                                     tabindex="-1">
                                    <a class="salon-info" href="https://choxe.net/salon/-s855.html" tabindex="-1">
                                        <img src="/uploads/salon/201811/855/avatar/228x153-IMG_0405_-_Copy_-_Copy.JPG"
                                             width="175" height="118">
                                        <div class="name-salon">MERCEDES MIỀN BẮC</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-cloned" data-slick-index="12"
                                     aria-hidden="true"
                                     tabindex="-1">
                                    <a class="salon-info" href="https://choxe.net/salon/kia-quang-ninh-s117.html"
                                       tabindex="-1">
                                        <img src="/uploads/salon/201810/117/avatar/228x153-a757b2ad995b7905204a.jpg"
                                             width="175" height="118">
                                        <div class="name-salon">Kia Quảng Ninh</div>
                                    </a>
                                </div>
                                <div class="slider-item slick-slide slick-cloned" data-slick-index="13"
                                     aria-hidden="true"
                                     tabindex="-1">
                                    <a class="salon-info" href="https://choxe.net/salon/o-to-anh-ly-s17.html"
                                       tabindex="-1">
                                        <img src="/uploads/salon_photo/228x153-o-to-anh-ly-57d23ea0.jpg" width="175"
                                             height="118">
                                        <div class="name-salon">Ô tô Ánh Lý</div>
                                    </a>
                                </div>
                            </div>
                        </div>


                        <button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next"
                                role="button" style="display: block;">Next
                        </button>
                    </div>
                    <div class="bg-blur"></div>
                </div>
            </div>
        </div>
        <!-- End Slider Salon nổi bật -->
    </div>
@stop