@extends('frontend.layouts.master')
@section('main_content')
    <section class="content">
        <div class="container">
            @include('frontend.partials.breadcrumb')
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="box-white mt30 clearfix">
                        <div class="d_box clearfix">
                            <div class="col-sm-5 col-md-5 col-lg-5">
                                <div class="d_slide clearfix">
                                    <div class="d_slide_l">
                                        <div id="sync2" class="owl-carousel">
                                            <div class="owl-item synced" style="width: 29px;"> <img src="{{ CommonHelper::getUrlImageThumb($product->image, 55, 81) }}" alt="{{ $product->name }}" /> </div>
                                            @foreach($image_extras as $item)
                                                <div class="owl-item synced" style="width: 29px;"> <img src="{{ CommonHelper::getUrlImageThumb($item, 55, 81) }}" alt="{{ $product->name }}" /> </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="d_slide_r">
                                        <div id="sync1" class="owl-carousel">
                                            <div class="item"> <img src="{{ CommonHelper::getUrlImageThumb($product->image, 294, 430) }}" alt="{{ $product->name }}" /> </div>
                                            @foreach($image_extras as $item)
                                            <div class="item"> <img src="{{ CommonHelper::getUrlImageThumb($item, 294, 430) }}" alt="{{ $product->name }}" /> </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-7 col-md-7 col-lg-7">
                                <div class="d_right">
                                    <div class="d_book_info">
                                        <h3>{{$product->name}}</h3>
                                        @php $sao = $product->id % 6; if($sao > 5) $sao = 5; if($sao < 3) $sao += 2; @endphp
                                        <p class="sao">
                                            @for($i = 1; $i <= $sao; $i ++)
                                                <img src="{{ URL::asset('public/frontend/images/sao1.png') }}"
                                                     alt="">
                                            @endfor
                                            @for($i = 1; $i <=  (5 - $sao); $i ++)
                                                <img src="{{ URL::asset('public/frontend/images/sao2.png') }}"
                                                     alt="">
                                            @endfor
                                        </p>
                                        <p><span class="c_tg">Tác giả: <a href="{{ route('search', ['author_id' => $author->id]) }}">{{$author->name}}</a></span><span class="c_sku">SKU: {{$product->code}}</span></p>
                                    </div>
                                    <div class="d_shop clearfix">
                                        <div class="d_shop_left">
                                            @php $discount = CommonHelper::discount($product->base_price, $product->final_price); @endphp
                                            <p class="d_price2">
                                                @if($discount != '')
                                                    Tiết kiệm: <span>{{ $discount }}</span> ({{ CommonHelper::discount($product->base_price, $product->final_price, '-') }} đ)<br>
                                                @endif
                                                Giá thị trường: {{ number_format($product->base_price, 0, '.', '.') }} đ</p>
                                            {!! @$widget->content !!}
                                                <div class="d_sl clearfix">
                                                <div class="cP_right"> <span class="sl">Số lượng:</span>
                                                    <div class="numbers-row">
                                                        <input type="text" name="quantity" id="" value="1">
                                                        <div class="inc button">+</div>
                                                        <div class="dec button">-</div>
                                                    </div>
                                                </div>
                                                <p class="c_buy"><a href="javascript:;" rel="nofollow" class="buy">Chọn mua</a></p>
                                            </div>
                                        </div>
                                        <div class="d_shop_r">
                                            <div id="fb-root"></div>
                                            <script>(function(d, s, id) {
                                                    var js, fjs = d.getElementsByTagName(s)[0];
                                                    if (d.getElementById(id)) return;
                                                    js = d.createElement(s); js.id = id;
                                                    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0&appId=1618627438366755&autoLogAppEvents=1';
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }(document, 'script', 'facebook-jssdk'));</script>
                                            <div class="fb-like" data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="spSlide">
                            <h3>Sách hay khác nên mua</h3>
                            <div class="spSlideInner">
                                <?php
                                $data = CommonHelper::getFromCache('get_products_by_multi_cat_like_category_id');
                                if (!$data) {
                                    $data = \App\Models\Product::select(['id','name', 'slug', 'multi_cat', 'image', 'final_price', 'type', 'ebook_price', 'base_price'])
                                        ->where('status', 1)->where('id', '!=', $product->id)->where('multi_cat', 'LIKE', '%|'.$category->id.'|%')->orderBy('order_no', 'asc')->limit(5)->get();
                                    CommonHelper::putToCache('get_products_by_multi_cat_like_category_id', $data);
                                }

                                ?>
                                <div id="spgg" class="owl-carousel">
                                    @foreach($data as $item)
                                    <div class="item">
                                        <div class="bookItem">
                                            <p class="bookImg">
                                                <a href="{{ CommonHelper::getProductSlug($item) }}"><img
                                                            src="{{ CommonHelper::getUrlImageThumb($item->image, 129, 190) }}"
                                                            alt="{{ $item->name }}"/></a></p>
                                            <p class="bookTtl"><a href="{{ CommonHelper::getProductSlug($item) }}">{{ $item->name }}</a></p>
                                            <p class="sao">
                                                <img src="{{ URL::asset('public/frontend/images/sao1.png') }}" alt="" />
                                                <img src="{{ URL::asset('public/frontend/images/sao1.png') }}" alt="" />
                                                <img src="{{ URL::asset('public/frontend/images/sao1.png') }}" alt="" />
                                                <img src="{{ URL::asset('public/frontend/images/sao1.png') }}" alt="" />
                                                <img src="{{ URL::asset('public/frontend/images/sao2.png') }}" alt="" /></p>
                                            <p class="bookPrice clearfix">
                                            {!! CommonHelper::getPriceHtml($item) !!}
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="customNavigation"> <a class="prev2"><img src="{{ URL::asset('public/frontend/images/prev3.png') }}" alt="" /></a> <a class="next2"><img src="{{ URL::asset('public/frontend/images/next3.png') }}" alt="" /></a></div>
                            </div>
                        </div>
                        <div class="ttct">
                            <h3>Thông Tin Chi Tiết</h3>
                            <table>
                                <tr>
                                    <th>Công ty phát hành</th>
                                    <td>{{@$company->name}}</td>
                                </tr>
                                <tr>
                                    <th>Nhà xuất bản</th>
                                    <td>{{@$publishing->name}}</td>
                                </tr>
                                <tr>
                                    <th>Tác giả</th>
                                    <td>{{@$author->name}}</td>
                                </tr>
                                <tr>
                                    <th>Kích thước</th>
                                    <td>{{$product->size}}</td>
                                </tr>
                                <tr>
                                    <th>Số trang</th>
                                    <td>{{$product->page_number}}</td>
                                </tr>
                                <tr>
                                    <th>Ngày xuất bản</th>
                                    <td>{{$product->ngay_phat_hanh}}</td>
                                </tr>
                                <tr>
                                    <th>SKU</th>
                                    <td>{{$product->code}}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="gts">
                            <h3>Giới thiệu sách</h3>
                            <h4>{{$product->name}}</h4>
                            {!!$product->content!!}
                        </div>
                        <div class="hdsp">
                            <div class="fb-comments" data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>" data-numposts="5"></div>
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s); js.id = id;
                                    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0&appId=1618627438366755&autoLogAppEvents=1';
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('custom_footer')
    <script type="text/javascript">
        $(function(){
            $('#BB-nav').affix({
                offset: {
                    top: $('#BB-nav').height()
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            var owl = $("#spgg");
            owl.owlCarousel({
                items : 4,
                itemsDesktop : [1200,4],
                itemsDesktopSmall : [1000,3],
                itemsTablet: [600,2],
                itemsMobile : [500,1],
                autoPlay: 3000
            });

            $(".next2").click(function(){
                owl.trigger('owl.next');
            })
            $(".prev2").click(function(){
                owl.trigger('owl.prev');
            })
        });
    </script>
    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "100%";
        }
        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }
        $(document).ready(function(){
            $(window).scroll(function(){
                if ($(this).scrollTop() > 500) {
                    $('.scrollToTop').fadeIn();
                } else {
                    $('.scrollToTop').fadeOut();
                }
            });
            $('.scrollToTop').click(function(){
                $('html, body').animate({scrollTop : 0},800);
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(e){
            $('.search-panel .dropdown-menu').find('a').click(function(e) {
                e.preventDefault();
                var param = $(this).attr("href").replace("#","");
                var concept = $(this).text();
                $('.search-panel span#search_concept').text(concept);
                $('.input-group #search_param').val(param);
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $(".numbers-row").append('<div class="inc button">+</div><div class="dec button">-</div>');
            $(".button").on("click", function() {
                var $button = $(this);
                var oldValue = $button.parent().find("input").val();
                if ($button.text() == "+") {
                    var newVal = parseFloat(oldValue) + 1;
                } else {
                    // Don't allow decrementing below zero
                    if (oldValue > 0) {
                        var newVal = parseFloat(oldValue) - 1;
                    } else {
                        newVal = 0;
                    }
                }
                $button.parent().find("input").val(newVal);
            });
        });
    </script>
    <script>
        $(document).ready(function() {

            var sync1 = $("#sync1");
            var sync2 = $("#sync2");

            sync1.owlCarousel({
                singleItem : true,
                slideSpeed : 1000,
                navigation: true,
                pagination:false,
                afterAction : syncPosition,
                responsiveRefreshRate : 200,
            });

            sync2.owlCarousel({
                items : 4,
                itemsDesktop      : [1199,4],
                itemsDesktopSmall     : [979,4],
                itemsTablet       : [767,4],
                itemsMobile       : [479,4],
                pagination:false,
                responsiveRefreshRate : 100,
                afterInit : function(el){
                    el.find(".owl-item").eq(0).addClass("synced");
                }
            });

            function syncPosition(el){
                var current = this.currentItem;
                $("#sync2")
                    .find(".owl-item")
                    .removeClass("synced")
                    .eq(current)
                    .addClass("synced")
                if($("#sync2").data("owlCarousel") !== undefined){
                    center(current)
                }

            }

            $("#sync2").on("click", ".owl-item", function(e){
                e.preventDefault();
                var number = $(this).data("owlItem");
                sync1.trigger("owl.goTo",number);
            });

            function center(number){
                var sync2visible = sync2.data("owlCarousel").owl.visibleItems;

                var num = number;
                var found = false;
                for(var i in sync2visible){
                    if(num === sync2visible[i]){
                        var found = true;
                    }
                }

                if(found===false){
                    if(num>sync2visible[sync2visible.length-1]){
                        sync2.trigger("owl.goTo", num - sync2visible.length+2)
                    }else{
                        if(num - 1 === -1){
                            num = 0;
                        }
                        sync2.trigger("owl.goTo", num);
                    }
                } else if(num === sync2visible[sync2visible.length-1]){
                    sync2.trigger("owl.goTo", sync2visible[1])
                } else if(num === sync2visible[0]){
                    sync2.trigger("owl.goTo", num-1)
                }
            }

        });
    </script>

    {{--Custom code--}}
    <script>
        $('.buy').click(function () {
            loading();
            $(this).attr('disabled', 'disabled');
            var quantity = $('input[name=quantity]').val();
            if(quantity < 1) {
                alert('Vui lòng nhập số lượng > 0');
            } else {
                $.ajax({
                    url: '{{ route('order.add_to_cart') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        product_id : '{{ $product->id }}',
                        quantity : quantity
                    },
                    success: function (result) {
                        stopLoading();
                        $(this).removeAttr('disabled');
                        if (result.status == true) {
                            $('#count-cart').html(result.count_cart);
                            $('#cart-total-price').html(result.cart_total_price_format);
                            alert('Đặt hàng thành công');
                        } else {
                            alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại');
                        }
                    },
                    error: function () {
                        stopLoading();
                        alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại');
                    }
                });
            }
        });
    </script>
@endsection