@extends('frontend.layouts.master_account')
@section('main_body')
    <script async="" src="{{ asset('public/frontend/js/jquery.min.js') }}"></script>
    <div class="main-content bg-grey pt0 row m0 bg_white">
        <div class="baner07 relative baner_mb banner_mb_thanhcong">
            <div class="bg-blend-1"></div>
            <div class="bg-blend-2"></div>
            <div class="container relative z10">
                <div class="row m0">
                    <div class="content-baner07">
                        <div class="tittle-baner07 textfff text-center">Đăng tin bán Ô tô</div>
                    </div><!--end content-baner07-->
                </div>
            </div>
        </div><!--end baner07-->
        <div class="row m0">
            <div class="container">
                <div class="row m0">
                    <div class="info-banxe banxe-thanhcong">
                        <div class="item-contentbanxe">
                            <div class="content-thanhcong salon-dkithanhcong p0">
                                <div class="tit-dkithanhcong font24 txtcol43a047">Đăng tin bán Ô tô thành công!</div>
                                <!--end tit-dkithanhcong-->
                                <div>
                                    <p style="font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 30px;color: #000;mso-line-height-rule: exactly;display: block;margin-top: 10px;margin-bottom: 16px;">
                                        Nếu bạn muốn tin hiển thị ưu tiên ngoài trang chủ - Gọi <strong
                                                style="color: #f7713d;">{{ @$settings['ho_tro_truc_tuyen'] }}</strong>
                                    </p>
                                </div>
                                <i class="img-like"></i>
                                <div class="note_xacthuc">
                                    <p class="lineh20 font16 mb25mb"> <a
                                                class="text-1069c9 font16"
                                                href="{{action('Frontend\ProductController@getPending')}}">Bấm đây để Quản lý Xe vừa
                                            đăng</a></p>
                                </div>
                                <div class="row15 hidden-xs">
                                    <div class="link-back mt40">
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
                                            <a class="dki quaylai-home text2b4560 pull-left"
                                               href="{{action('Frontend\HomeController@getIndex')}}"><i class="icon-sprite-24 icon-home "></i>trang
                                                chủ Chợ xe</a>
                                        </div><!--col-->
                                    </div><!--end link-back-->
                                </div><!--end row-->
                                <a class="dki quaylai-home mt20_style2 text2b4560 visible-xs" href="/"><i
                                            class="icon-sprite-24 icon-home "></i>trang chủ Chợ xe</a>
                            </div><!--end content-thanhcong-->
                        </div><!--end item-contentbanxe-->
                    </div><!--end info-banxe-->
                </div>
            </div>
        </div>
    </div>
@stop