@if(count($data)>0)
    @foreach($data as $product)
        @php
            /* Manufacturer id*/
            $Manufacturer = CommonHelper::getFromCache('manufacturer_by_manufacturer_id_first');
            if (!$Manufacturer) {
                $Manufacturer = \App\Models\Manufacturer::select(['id', 'name'])->where('id',$product->manufacturer_id)->first();
                CommonHelper::putToCache('manufacturer_by_manufacturer_id_first', $Manufacturer);
            }
        /*$Manufacturer model id*/
         $Manufacturer_model = CommonHelper::getFromCache('manufacturermodel_by_manufacturer_model_id_first');
            if (!$Manufacturer_model) {
                $Manufacturer_model = \App\Models\ManufacturerModel::select(['id', 'name'])->where('id',$product->manufacturer_model_id)->first();
                CommonHelper::putToCache('manufacturermodel_by_manufacturer_model_id_first', $Manufacturer_model);
            }
        @endphp
        <tr>
            <td class="row news  items-286821 pos-0">
                <div class="news-detail">
                    <div class="info-left">
                        <a href="{{ URL::to('chi-tiet-san-pham/' . @$product['id']) }}"
                           class="img"><img class=""
                                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$product['image'], 175, 118) }}"
                                            width="175" height="118"
                                            alt="{{ @$product->tinhtrangxe }} {{ @$Manufacturer->name }} {{ @$Manufacturer_model->name }} {{ @$Manufacturer->sub_model }} {{ @$Manufacturer->year }}"></a>
                        <div class="block-txt-mb">
                            <a class="tit-news"
                               href="{{ URL::to('chi-tiet-san-pham/' . @$product['id']) }}">
                                <h2>{{ @$product->name }}</h2>
                            </a>
                            <div class="price visible-xs"> {{  @$product->price  }} {{ @$product->price_text }}</div>
                        </div>
                        <div class="block-txt-mb">
                            <div class="info-detail-sp"
                                 style="background: transparent; font-size: 13px; padding-left: 0; padding-right: 0; border-top: 1px solid #e8e8e8; margin-top: 8px; padding-top: 2px; padding-bottom: 0;">
                                <div class="text75 p0">
                                    <p class="m0">
                                        <span class="mr15">{{ rand(999,9999) }} lượt xem</span>
                                    </p>
                                </div>
                                <div style="margin-top: -5px;">
                                    <a class="text-1069c9 edit-banxe btn-edit__dangban"
                                       style="border: none;background: none;padding: 0 0 0 28px;margin-left: 25px;margin-left: 0; "
                                       href="{{action('Frontend\ProductController@getEditPostCar',@$product->id)}}"><i
                                                class="icon-sprite-24 icon-pencilbx"
                                                style="left: 0;top: 10px;"></i>Chỉnh
                                        sửa tin</a>
                                    <a
                                       href="{{action('Frontend\ProductController@getDelete',@$product->id)}}"
                                       class="btn-xoa-new text-1069c9"><i
                                                class="icon-sprite-24 icon-xoa-blue"></i>Xoá
                                        tin
                                        bán
                                        xe
                                    </a>
                                </div>
                            </div><!--info-detail-sp-->
                        </div>
                    </div>
                </div>
                <div class="price-addr txr hidden-xs">
                    <div class="price">{{ (@$product->price) }} {{ @$product->price_text }}</div>
                    <div class="old-news">{{date(" H:i a d-m-Y",strtotime(@$product->updated_at)) }}</div>
                </div>
            </td>
        </tr>
    @endforeach
@else
    <div class="mt20 mb20 list-product-style2">
        <div class="news-rong font16 mt20">
            <div class="txt-red bold mb5">Không có tin đăng xe.</div>
        </div>
    </div>
@endif