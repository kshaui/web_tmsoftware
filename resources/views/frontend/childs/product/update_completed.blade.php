@extends('frontend.layouts.master_account')
@section('main_body')
    <div class="main-content bg-grey pt0 row m0 bg_white">
        <div class="baner07 relative baner_mb banner_mb_thanhcong">
            <div class="bg-blend-1"></div>
            <div class="bg-blend-2"></div>
            <div class="container relative z10">
                <div class="row m0">
                    <div class="content-baner07">
                        <div class="tittle-baner07 textfff text-center"> Cập nhật đăng tin bán xe</div>
                    </div><!--end content-baner07-->
                </div>
            </div>
        </div><!--end baner07-->
        <div class="row m0">
            <div class="container">
                <div class="row m0">
                    <div class="info-banxe banxe-thanhcong">
                        <div class="item-contentbanxe">
                            <div class="content-thanhcong salon-dkithanhcong p0">
                                <div class="tit-dkithanhcong font24 txtcol43a047">Cập nhật tin đăng thành công!</div>
                                <!--end tit-dkithanhcong-->
                                <i class="img-like"></i>
                                <div class="note_xacthuc">
                                    <div class="lineh20 font16 mb25mb">
                                        Bạn đã cập nhật tin đăng thành công.
                                        <p class="txt-red">Lưu ý: Các nội dung cập nhật đang chờ duyệt từ ban quản
                                            trị</p><a class="text-1069c9 font16"
                                                      href="{{ URL::to('xe-thay-doi-cho-duyet') }}">Bấm đây
                                            để Quản lý Xe đang chờ duyệt thay đổi.</a>
                                    </div>
                                </div>
                                <div class="row15 hidden-xs">
                                    <div class="link-back mt40">
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
                                            <a class="dki quaylai-home text2b4560 pull-left" href="/"><i
                                                        class="icon-sprite-24 icon-home "></i>trang chủ Chợ xe</a>
                                        </div><!--col-->

                                    </div><!--end link-back-->
                                </div><!--end row-->
                                <a class="dki quaylai-home mt20_style2 text2b4560 visible-xs" href="/"><i
                                            class="icon-sprite-24 icon-home "></i>trang chủ Chợ xe</a>
                            </div><!--end content-thanhcong-->
                        </div><!--end item-contentbanxe-->
                    </div><!--end info-banxe-->
                </div>
            </div>
        </div>
    </div>
@stop