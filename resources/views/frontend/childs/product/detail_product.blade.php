@extends('frontend.layouts.master')
@section('main_content')
    @php
        /* Manufacturer id*/
        $Manufacturer = CommonHelper::getFromCache('manufacturer_by_manufacturer_id_first');
        if (!$Manufacturer) {
            $Manufacturer = \App\Models\Manufacturer::select(['id', 'name'])->where('id',$product->manufacturer_id)->first();
            CommonHelper::putToCache('manufacturer_by_manufacturer_id_first', $Manufacturer);
        }
    /*$Manufacturer model id*/
     $Manufacturer_model = CommonHelper::getFromCache('manufacturermodel_by_manufacturer_model_id_first');
        if (!$Manufacturer_model) {
            $Manufacturer_model = \App\Models\ManufacturerModel::select(['id', 'name'])->where('id',$product->manufacturer_model_id)->first();
            CommonHelper::putToCache('manufacturermodel_by_manufacturer_model_id_first', $Manufacturer_model);
        }
    /**thành phố*/
     $province_index= CommonHelper::getFromCache('province_by_province_id_first');
        if (!$province_index) {
            $province_index = \App\Models\Province::select(['id', 'name'])->where('id',$product->province_id)->first();
            CommonHelper::putToCache('province_by_province_id_first', $province_index);
        }
   /**màu xe*/
   $color_car = CommonHelper::getFromCache('colors_by_exterior_first');
        if (!$color_car) {
            $color_car = \App\Models\Colors::select(['id', 'name'])->where('id',$product->exterior)->first();
            CommonHelper::putToCache('colors_by_exterior_first', $Manufacturer_model);
        }
    /**dòng xe product type*/
    $product_type = CommonHelper::getFromCache('producttype_by_producttype_id_first');
    if (!$product_type){
        $product_type = \App\Models\ProductType::select('id','name')->where('id',@$product->producttype_id)->first();
        CommonHelper::putToCache('producttype_by_producttype_id_first',$product_type);
    }
    @endphp
    <div class="main-content bg-light-blue">
        <div class="container relative">
            <div class="row m0 des-xe">
                <div class="map-link hidden-xs cbreadcrumb">
                    <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a class="" href="/" itemprop="item"><span
                                        itemprop="name">{!! @$settings['name'] !!}</span></a>
                        </li>
                        >
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item"><span
                                        itemprop="name">{{ @$product->name }}</span></a>
                        </li>
                    </ul>
                </div>
                <br>
                <h1 class="title-detail"> {{ @$product->name }}</h1>
                <div class="visible-xs price font20">{{ @$product['price'] }} {{ @$product->price_text }}</div>
                <div class="row m0 div-mt-mb">
                    <div class="col-xs-12 col-sm-4 p0">
                        <div class="list-opt list-opt-detail">
                            @php
                                $count =CommonHelper::getFromCache('favorite_by_product_id_count');
                                if (!$count){
                                    $count = \App\Models\Favorite::where('user_id',@Auth::user()->id)->where('product_id',@$product->id)->count();
                                    CommonHelper::putToCache('favorite_by_product_id_count',$count);
                                }
                            @endphp
                            @if($count == 0)
                                {{csrf_field()}}
                                <a class="favor  yeuthich "
                                   data-toggle="tooltip" title=""
                                   data-placement="bottom"
                                   data-original-title="Thích xe này"
                                   data-item_id1="{{@$product->id}}"
                                   data-count_favorite1="{{ $count }}"
                                   data-user_id1="{{@Auth::user()->id}}"
                                   data-author_id1="{{@$product['author_id']}}"
                                   data-product_id1="{{ @$product['id'] }}"
                                   type="button"><i
                                            class="icon-sprite-24 icon-heart"></i> Yêu thích
                                </a>
                            @else
                                {{csrf_field()}}
                                <a class="favor active khongyeuthich" data-toggle="tooltip" title=""
                                   data-placement="bottom" data-original-title="Bỏ thích xe này"
                                   data-item_id1="{{@$product->id}}"
                                   data-count_favorite1="{{ $count }}"
                                   data-user_id1="{{@Auth::user()->id}}"
                                   data-author_id1="{{@$product['author_id']}}"
                                   data-product_id1="{{ @$product['id'] }}" aria-describedby="tooltip514963">
                                    <i class="icon-sprite-24 icon-heart"></i>
                                    Bỏ thích
                                </a>
                            @endif
                            <script type="text/javascript">
                                $('.yeuthich').click(function () {
                                    var item_id = $(this).data('item_id1');
                                    var user_id = $(this).data('user_id1');
                                    var author_id = $(this).data('author_id1');
                                    var product_id = $(this).data('product_id1');
                                    var count_favorite = $(this).data('count_favorite1');
                                    var object = $(this);
                                    if (user_id == '') {
                                        if (confirm('Bạn phải đăng nhập trước khi lưu tin vào yêu thích')) {
                                            window.location = '{{action("Auth\LoginController@getLogin")}}';
                                        }
                                    } else {
                                        if (count_favorite == 0) {
                                            $.ajax({
                                                type: 'POST',
                                                url: '{{action("Frontend\ProductController@postCarLove")}}',
                                                data: {
                                                    '_token': '{{ csrf_token() }}',
                                                    'item_id': item_id,
                                                    'user_id': user_id,
                                                    'author_id': author_id,
                                                    'product_id': product_id,
                                                },

                                                success: function (data) {

                                                    console.log(data);
                                                    alert('Đã thích');
                                                    location.reload();
                                                }
                                            });
                                        }
                                    }
                                })
                                $('.khongyeuthich').click(function () {
                                    var id = $(this).data('item_id1');
                                    $.ajax({
                                        type: 'POST',
                                        url: '/khong-thich',
                                        data: {
                                            '_token': '{{ csrf_token() }}',
                                            'id': id,
                                        },
                                        success: function (data) {
                                            console.log(data);
                                            alert('Bỏ yêu thích');
                                            location.reload();
                                        }
                                    });
                                })
                            </script>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 p0 txr txlmb">
                        <div class="map-link mt10">
                            <span>Lượt xem: {{ rand(99, 999) }} </span> |
                            <span><span class="hidden-xs">Đăng:</span>{{date(" H:i a d-m-Y",strtotime(@$product->updated_at)) }}</span>
                            |
                            <span>ID<span class="hidden-xs"> bán xe</span>: {{ @$product['id'] }} </span>
                        </div>
                    </div>
                </div>
                <div class="row m0 hidden-xs">
                    <div class="detail-xe"><i class="icon-sprite-24 icon-status"></i> Tình trạng:
                        <b> {{ @$product['tinhtrangxe'] }}</b></div>
                    <div class="detail-xe"><i class="icon-sprite-24 icon-km"></i> Km đã đi:
                        <b> {{  number_format(@$product['odo']) }}
                            km</b></div>
                    <div class="detail-xe"><i class="icon-sprite-24 icon-hop-so"></i> Hộp số:
                        <b> {{ @$product['transmission'] }}</b></div>
                    <div class="detail-xe"><i class="icon-sprite-24 icon-nhien-lieu"></i> Nhiên liệu:
                        <b> {{ @$product['fuel'] }}</b></div>
                    @php
                        $user_role = \App\Http\Helpers\CommonHelper::getFromCache('user_by_author_id_first');
                        if (!$user_role){
                            $user_role = \App\Models\User::select('role')->where('id',@$product->author_id)->first();
                            \App\Http\Helpers\CommonHelper::putToCache('user_by_author_id_first',$user_role);
                        }
                    @endphp
                    <div class="detail-xe"><i class="icon-sprite-24 icon-ng-ban"></i> Người bán:
                        <b>{{ @$user_role->role }}</b></div>
                    <div class="detail-xe"><i class="icon-sprite-24 icon-dong-xe"></i> Dòng xe:
                        <b>  {{ @$product_type->name }}</b></div>
                    <div class="detail-xe"><i class="icon-sprite-24 icon-nam-sx"></i> Năm sản xuất:
                        <b> {{ @$product['year'] }}</b></div>
                    <div class="detail-xe"><i class="icon-sprite-24 icon-dan-dong"></i> Dẫn động:
                        <b> {{ @$product['drivertrain'] }}</b>
                    </div>
                    <div class="detail-xe"><i class="icon-sprite-24 icon-mau-xe"></i> Màu xe:
                        <b> {{ @$color_car['name'] }}</b></div>
                    <div class="detail-xe"><i class="icon-sprite-24 icon-xuat-xu"></i> Xuất xứ:
                        <b> {{ @$product['country'] }}</b></div>
                </div>
            </div>
            <div class="row m0 mt30">
                <div class="col-right pr30" id="col-left">
                    <div class="relamb bg-slider-mb bg-white-mb">
                        @php $image_extra = explode('|', $product->image_extra); $i = 0;@endphp
                        <div class="slideshow-container">
                            @foreach($image_extra as $img)
                                @if($img != '')
                                    @php $i ++; @endphp
                                    <div class="myslide" @if(!isset($flag)) style="display: block;" @php $flag = true; @endphp @else style="display: none;" @endif>
                                        <div class="number">{{ $i }}/{{ count($image_extra) }}</div>
                                        <img src="{{ asset('public/filemanager/userfiles/' . $img) }}" alt="">
                                    </div>
                                @endif
                            @endforeach
                            <span class="prev" onclick="plussShow(-1)" href="">&#10094</span>
                            <span class="next" onclick="plussShow(1)" href="">&#10095</span>
                            <div class="dot-slide">
                                @php  $i = 0; @endphp
                                @foreach($image_extra as $img)
                                    @if($img != '')
                                        @php $i ++; @endphp
                                        <span class="dot" onclick="dotclick({{ $i }})"><img
                                                    src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($img, 50, 50) }}"
                                                    alt=""></span>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <script>
                            var slideIndex = 1;
                            function plussShow(n) {
                                slideShow(slideIndex += n);
                            }
                            function dotclick(n) {
                                slideShow(slideIndex = n);
                            }
                            function slideShow(n) {
                                var slides = document.querySelectorAll('.myslide');
                                var dot = document.querySelectorAll('.dot');
                                if (n > slides.length) {
                                    slideIndex = 1;
                                }
                                if (n < 1) {
                                    slideIndex = slides.length;
                                }
                                for (var i = 0; i < slides.length; i++) {
                                    slides[i].style.display = 'none';
                                }
                                for (var j = 0; j < dot.length; j++) {
                                    dot[j].className = dot[j].className.replace(' active', '');
                                }
                                slides[slideIndex - 1].style.display = 'block';
                                dot[slideIndex - 1].className += ' active';
                            }
                        </script>
                    </div>
                    <div class="row m0 visible-xs mt0 p15mb bg-white-mb">
                        <div class="mb-call-button visible-xs">
                            <a class="btn btn-orange-48 visible-xs call-button fixed" href="tel:0969122929"
                               data-object-id="284204" data-object-type="auto"
                               onclick="ga(ga_tracker_name + '.send', 'event', 'Tin', 'Xem thông tin liên hệ', '{{ @$product->id }}'); dataLayer.push({'event':'Button_GoiDien'});"><i
                                        class="icon-sprite-24 icon-phone-white"></i> GỌI {{ @$product->phone1 }}</a>
                        </div>
                        <div class="row m0">
                            <div class="detail-xe"><i class="icon-sprite-24 icon-status"></i> Tình trạng:
                                <b> {{ @$product['tinhtrangxe'] }}</b></div>
                            <div class="detail-xe"><i class="icon-sprite-24 icon-km"></i> Km đã đi:
                                <b> {{ @$product['odo'] }}
                                    km</b></div>
                            <div class="detail-xe"><i class="icon-sprite-24 icon-hop-so"></i> Hộp số:
                                <b> {{ @$product['transmission'] }}</b></div>
                            <div class="detail-xe"><i class="icon-sprite-24 icon-nhien-lieu"></i> Nhiên liệu:
                                <b> {{ @$product['fuel'] }}</b></div>
                            <div class="detail-xe"><i class="icon-sprite-24 icon-ng-ban"></i> Người bán: <b> Đại lý</b>
                            </div>
                            <div class="detail-xe"><i class="icon-sprite-24 icon-dong-xe"></i> Dòng xe:
                                <b>  {{ @$product_type->name }}</b></div>
                            <div class="detail-xe"><i class="icon-sprite-24 icon-nam-sx"></i> Năm sản xuất:
                                <b> {{ @$product['year'] }}</b></div>
                            <div class="detail-xe"><i class="icon-sprite-24 icon-dan-dong"></i> Dẫn động:
                                <b> {{ @$product['drivertrain'] }}</b>
                            </div>
                            <div class="detail-xe"><i class="icon-sprite-24 icon-mau-xe"></i> Màu xe:
                                <b> {{ @$color_car['name'] }}</b></div>
                            <div class="detail-xe"><i class="icon-sprite-24 icon-xuat-xu"></i> Xuất xứ:
                                <b> {{ @$product['country'] }}</b></div>
                        </div>
                        <hr class="mb0">
                    </div>
                    <div class="clearfix"></div>
                    <div class="mo-ta p15mb pt20mb bg-white-mb mt0mb">
                        <div class="tit-mo-ta">Mô tả xe</div>
                        <div class="mt10">
                            {!! @$product->description !!}
                        </div>
                    </div>
                    <div id="fb-root"></div>
                    <script>(function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=1618627438366755&autoLogAppEvents=1';
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="iframe-custom" style="width: 100%; overflow: hidden;">
                        <div class="fb-like" style="width: 100%; overflow: hidden"
                             data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"
                             data-layout="standard" data-action="like" data-size="small" data-show-faces="true"
                             data-share="true"></div></div>
                    <div class="box-filter right-detail mt20">
                        <form method="post" action="" enctype="multipart/form-data"
                              class="validate-form" id="form">
                            {{csrf_field()}}
                            <div class="pl15 pr15">
                                <input type="hidden" name="auto_id" value="284204">
                                <div class="txt-white">Tôi quan tâm đến xe này</div>
                                <div>
                                    <p class="error2 text-center alert alert-danger hidden"
                                       style="border-radius: 0px"></p>
                                    <input type="text" class="form-control" placeholder="Họ tên" name="name" id="name"
                                           value="{{ @Auth::user()['name'] }}">
                                </div>
                                <div class="row row-input">
                                    <div class="col-md-6 col-sm-6">
                                        <p class="error1 text-center alert alert-danger hidden"
                                           style="border-radius: 0px"></p>
                                        <input type="text" class="form-control" placeholder="Điện thoại" name="tel"
                                               id="tel"
                                               value="{{ @Auth::user()['tel'] }}">
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <p class="error text-center alert alert-danger hidden"
                                           style="border-radius: 0px"></p>
                                        <input type="text" class="form-control" placeholder="Email" name="email"
                                               id="email"
                                               value="{{ @Auth::user()['email'] }}">
                                    </div>
                                </div>
                                <style type="text/css">
                                    @media (min-width: 768px) {
                                        .row-input .col-md-6:first-child {
                                            padding-right: 5px;
                                        }

                                        .row-input .col-md-6:last-child {
                                            padding-left: 5px;
                                        }
                                    }

                                    @media (max-width: 992px) {
                                        .row-input button {
                                            margin-bottom: 10px;
                                        }

                                        .row-input div:last-child button {
                                            margin-bottom: 0;
                                        }
                                    }
                                </style>
                                <label class="lbinput lb-white mt10">
                                </label>
                            </div>
                            <div class="pl15 pr15">
                                <div class="row row-input">
                                    <div class="col-md-4 col-sm-12">
                                        <button type="button" name="type" data-author_id="{{ @$product['author_id'] }}"
                                                data-user_id="{{ @Auth::user()['id']  }}"
                                                data-product_id="{{ @$product['id'] }}"
                                                data-value="GỌI NGAY CHO TÔI"
                                                class="btn btn-orange-48 mb-10 font16 btn-lien-he">GỌI NGAY CHO TÔI
                                        </button>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <button type="button" name="type" data-author_id="{{ @$product['author_id'] }}"
                                                data-user_id="{{ @Auth::user()['id']  }}"
                                                data-product_id="{{ @$product['id'] }}"
                                                data-value="EMAIL CHI TIẾT CHO TÔI"
                                                class="btn btn-blue-48 btn-lien-he"
                                                href="#">
                                            EMAIL CHI TIẾT CHO TÔI
                                        </button>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <button type="button" name="type"
                                                data-author_id="{{ @$product['author_id'] }}"
                                                data-user_id="{{ @Auth::user()['id']  }}"
                                                data-product_id="{{ @$product['id'] }}"
                                                data-value="ĐĂNG KÝ LÁI THỬ XE"
                                                class="btn btn-blue2-48 btn-lien-he"
                                                data-toggle="modal" data-target="#myModal2">ĐĂNG KÝ LÁI THỬ XE
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @php
                    $user = \App\Http\Helpers\CommonHelper::getFromCache('user_by_author_id_first');
                    if (!$user){
                        $user = \App\Models\User::where('id',@$product->author_id)->first();
                        \App\Http\Helpers\CommonHelper::putToCache('user_by_author_id_first',$user);
                    }
                @endphp
                <div class="col-left fix-col-mb mt15mb" id="col-fix"
                     style="width: 255px; left: 974.188px;  top: 75px;">
                    <div class="box-info-nban row m0"
                         style="padding: 2px; border-radius: 0; margin-top: 0 !important; background: #8ba7b2;">
                        <div class="box-gia-ban hidden-xs"
                             style="background: #f5f5f5; border: 0; border-bottom: 1px solid #ccc; padding: 10px 15px; ">
                            <!--                        Giá bán: <br>-->
                            <span class="txt-gia">{{ @$product->price }} {{ @$product->price_text }}</span>

                            <a class="btn btn-orange-40 hidden-xs" id="show-hide" data-object-id="290476"
                               data-object-type="auto"
                               onclick="ga(ga_tracker_name + '.send', 'event', 'Tin', 'Xem thông tin liên hệ', '290476'); dataLayer.push({'event':'Button_XemDienThoaiLienHe'});">
                                Xem số điện thoại
                            </a>
                            <a class="btn btn-orange-48 visible-xs call-button" href="tel:{{ @$user->tel }}"
                               data-object-id="{{ @$product->id }}" data-object-type="auto"
                               onclick="dataLayer.push({'event':'Button_GoiDien'});"><i
                                        class="icon-sprite-24 icon-phone-white"></i> GỌI {{ @$user->tel }}</a>
                            <div class="deta-info"
                                 style="margin-bottom: 0; padding-top: 20px; border-top: 1px solid #ccc;">
                                <p class="sp-txt relative">

                                    <i class="icon-sprite-24 icon-address-big"></i>@if(!empty($product->address)){{$product->address}}@else
                                        Đang cập nhật...@endif </a>
                                </p>
                                <p class="relative"><i class="icon-sprite-24 icon-phone"></i>@if(!empty($user->tel)){{ @$user->tel }} @if(!empty($product->phone2))- {{ @$product->phone2 }} @endif @else
                                        Đang cập nhật... @endif</p>
                            </div>
                        </div>
                        <div class="bg-white" style="width: 100%; padding: 10px 15px; border-radius: 0;">
                            <div class="txt-info" style="padding-left: 70px; height: 75px;">
                                <a class="" href="">
                                    <img src="@if(@$user->image) {{  asset('public/filemanager/userfiles/' . @$user['image']) }} @else {{ asset('public/frontend/assets/frontend/img/avatar_default.png') }} @endif"
                                         height="60" width="60">
                                </a>
                                <div class="" style="margin-top: -15px;">
                                    <a class=""
                                       href="{{ URL::to('/tim-kiem?user_id='. @$user->id) }}">{{ @$user->name }}</a>
                                    <div class="rating">
                                        <span class="star star-yellow"></span>
                                        <span class="star star-yellow"></span>
                                        <span class="star star-yellow"></span>
                                        <span class="star star-half"></span>
                                        <span class="star star-grey"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="short-info ">
                                <a>
                                    <span class="sp-txt"><a href="\tim-kiem?province_id={{ @$product->province_id }}">
                                            <i class="icon-sprite-24 icon-address-big"></i> {{ @$province_index->name }}</a></span>
                                    </p>
                            </div>
                            @if(@$user->role == 'Cá nhân')
                            @else
                                <div class="hidden-xs" style="text-align: center;">
                                    <img src="{{ asset('public/filemanager/userfiles/icon_verified/' . @$settings['icon_verified']) }}"
                                         style="max-width: 160px;">
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m0 mt15 mt0mb">
                <div class="col-right pr30">
                    <div class="deal table-xe">

                        @include('frontend.partials.same_car')
                    </div>
                    @php
                        $provinceIds = \App\Models\Product::where('status', 1)->where('manufacturer_model_id', @$product->manufacturer_model_id)->groupBy('province_id')->pluck('province_id')->toArray();
                        $data = \App\Models\Province::whereIn('id', $provinceIds)->pluck('name', 'id');
                    @endphp
                    <div class="deal mt25 tinh-thanh hidden-xs">
                        <div class="title-underline">Các tỉnh thành khác</div>
                        <div class="row m0 mt20 mt10mb">
                            @foreach($data as $id => $name)
                                <div class="col-xs-6 col-sm-4 p0 mt5">
                                    <a class="city"
                                       href="/tim-kiem?province_id={{ $id }}&manufacturer_model_id={{ $product['manufacturer_model_id'] }}"><i
                                                class="icon-24 icon-sprite-24 icon-address"></i>
                                        <h3>{{ @$Manufacturer['name'] }} {{ @$Manufacturer_model['name'] }} {{ @$name }}
                                            ({{ count(@$data) }})</h3>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        <hr class="visible-xs m0 mt5">
                        <div class="a-see-all">
                            <a class="" href="/tim-kiem?manufacturer_model_id={{ $product['manufacturer_model_id'] }}">Xem
                                tất cả Tỉnh thành »</a>
                        </div>
                    </div>
                </div>
                @php
                    $datadongxe = CommonHelper::getFromCache('manufacturermodel_by_manufacturer_id_pluck');
                   if (!$datadongxe) {
                       $datadongxe = \App\Models\ManufacturerModel::select('id','name','manufacturer_id')->where('manufacturer_id',@$product->manufacturer_id)->pluck('name','id');
                       CommonHelper::putToCache('manufacturermodel_by_manufacturer_id_pluck', $datadongxe);
                   }
                @endphp
                <div class="col-left hidden-xs">
                    <div class="quick-filter mt15 hidden-xs">
                        <div class="title-bg">Dòng xe của {{@$Manufacturer->name }}</div>
                        <div class="list-tit list-loai-xe" style="padding-bottom: 0;">
                            <ul class="list-link">
                                @foreach($datadongxe as $id => $name)
                                    <li style="text-align: left">
                                        <span style="color: #999;">→</span> <a
                                                href="/tim-kiem?manufacturer_model_id={{ @$id }}">{{ @$Manufacturer->name }}
                                            {{ @$name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Column Left -->
            </div>
        </div>
    </div>
@endsection
@section('custom_header')
    <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/css/product_slide.css') }}">
    <style>
        .tab_ddk,
        .tab_ddk tr,
        .tab_ddk tbody {
            width: 100%;
            display: inline-block
        }
        .tab_ddk td {
            width: 24%;
            display: inline-block
        }
        @media (max-width: 768px) {
            .tab_ddk td {
                width: 100%;
                display: inline-block;
                float: left;
            }
        }
        .slideshow-container {
            height: 253px;
        }
    </style>
@endsection
@section('custom_footer')
    <script type="text/javascript">
        $('.btn-lien-he').click(function () {
            var user_id = $(this).data('user_id');
            var author_id = $(this).data('author_id');
            var product_id = $(this).data('product_id');
            var name = $('#name').val();
            var tel = $('#tel').val();
            var email = $('#email').val();
            var type = $(this).data('value');
            // if (name == '' && tel == '' && email == '') {
            //     alert('Yêu cầu nhập đầy đủ thông tin!');
            // } else {
            $.ajax({
                type: 'POST',
                url: '{{action("Frontend\ProductController@postCare")}}',
                data: {
                    '_token': '{{ csrf_token() }}',
                    'user_id': user_id,
                    'author_id': author_id,
                    'product_id': product_id,
                    'name': name,
                    'tel': tel,
                    'email': email,
                    'type': type,
                },
                success: function (data) {
                    if ((data.errors)) {
                        if (data.errors.name) {
                            $('.error2').removeClass('hidden');
                            $('.error2').text(data.errors.name);
                        }
                        if (data.errors.tel) {
                            $('.error1').removeClass('hidden');
                            $('.error1').text(data.errors.tel);
                        }
                        if (data.errors.email) {
                            $('.error').removeClass('hidden');
                            $('.error').text(data.errors.email);
                        }
                    } else {
                        alert('Gửi thông tin thành công');
                        location.reload();
                    }
                }
            });
            // }
        });
    </script>
@endsection