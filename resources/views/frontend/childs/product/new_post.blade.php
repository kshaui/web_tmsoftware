@extends('frontend.layouts.master_account')
@section('main_body')
    <!-- Body -->
    <div class="main-content bg-grey pt0 row m0 pb40">
        <div class="baner07 relative baner_mb">
            <div class="container relative z10">
                <div class="row m0">
                    <div class="content-baner07">
                        <div class="tittle-baner07 textfff text-center">Đăng tin bán Ô tô</div>
                        <div class="col-xs-4 col-sm-4 col-md-4 hidden-xs">
                            <i class="icon-sprite-24 icon-sell"></i>
                            <p class="font16 text-center textfff mt30 ">Đăng tin qua 4 bước đơn giản</p>
                        </div><!--end col-->
                        <div class="col-xs-4 col-sm-4 col-md-4 hidden-xs">
                            <i class="icon-sprite-24 icon-timkiem"></i>
                            <p class="font16 text-center textfff mt30">Hàng triệu lượt tìm kiếm mỗi tháng</p>
                        </div><!--end col-->
                        <div class="col-xs-4 col-sm-4 col-md-4 hidden-xs">
                            <i class="icon-sprite-24 icon-giaodich"></i>
                            <p class="font16 text-center textfff mt30">Hàng ngàn giao dịch mỗi tháng</p>
                        </div><!--end col-->
                    </div><!--end content-baner07-->
                </div>
            </div>
        </div><!--end baner07-->
        <div class="row m0 sl-comm-mb">
            <div class="container">
                <div class="row m0">
                    <div class="info-banxe">
                        <form method="post" action="{{action('Frontend\ProductController@postNewProduct')}}"
                              enctype="multipart/form-data" class="validate-form">
                            <div class="item-contentbanxe" style="padding-top: 0;">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="title-underline">Thông tin xe</div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="info-dki-email p0 banxe-vali">
                                            <div class="font14 text2b4560 mb25">Tình trạng xe</div>
                                            <div class="groupradio">
                                                <label for="otocu" class="check-dnhap font16 check-satus">
                                                    <input type="radio" class="icheck" id="" name="tinhtrangxe"
                                                           checked="true" value="Xe cũ"/>
                                                    <span>Xe cũ</span>
                                                </label>
                                                <label for="otomoi" class="check-dnhap font16 check-satus">
                                                    <input type="radio" class="icheck" id="otomoi" name="tinhtrangxe"
                                                           value="Xe mới"/>
                                                    <span>Xe mới</span>
                                                </label>
                                            </div><!--end groupradio-->
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="info-dki-email p0 banxe-vali">
                                            <div class="font14 text2b4560 mb10">Số Km đã đi</div>
                                            <input type="text" class="form-control input-banxe nhapso-vali recolor89"
                                                   placeholder="Nhập số" name="odo" value=""/>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="sl-filter xe-dki banxe m0 banxe-vali">
                                            <div class="font14 text2b4560 mb10">Muốn bán tại tình/thành</div>
                                            @php
                                                $data = CommonHelper::getFromCache('all_province_pluck');
                                            if (!$data) {
                                            $data = \App\Models\Province::select(['id', 'name'])->pluck('name', 'id');
                                            CommonHelper::putToCache('all_province_pluck', $data);
                                            }
                                            @endphp
                                            <select class="form-control bantinh-vali" id="bantaitinh" name="province_id">
                                                @foreach($data as $id => $name)
                                                    <option value="{{ $id }}">{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div><!--end col-->
                                </div><!--end row-->
                                <div class="row box__banxe">
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="">
                                            <div class="sl-filter xe-dki banxe m0 banxe-vali">
                                                <div class="font14 text2b4560 mb10">Hãng sản xuất</div>
                                                @php
                                                    $data = CommonHelper::getFromCache('all_manufacturer_pluck');
                                                     if (!$data) {
                                                         $data = \App\Models\Manufacturer::select(['id', 'name'])->pluck('name', 'id');
                                                         CommonHelper::putToCache('all_manufacturer_pluck', $data);
                                                     }
                                                @endphp
                                                <select name="manufacturer_id" id="manufacturer"
                                                        class="styled select2-offscreen" title=""
                                                        tabindex="-1">
                                                    <option value="" selected="">Hãng sản xuất</option>
                                                    @foreach($data as $id => $name)
                                                        <option value="{{ $id }}"
                                                                @if(@$_GET['manufacturer_id'] == $id) selected @endif> {{ $name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="">
                                            <div class="sl-filter xe-dki banxe m0 banxe-vali">
                                                <div class="font14 text2b4560 mb10">Model</div>
                                                @php
                                                    if(isset($_GET['manufacturer_id']) && $_GET['manufacturer_id'] != '') {
                                                        $data = CommonHelper::getFromCache('get_manufacturermodel_by_manufacturer_id_pluck' . $_GET['manufacturer_id']);
                                                        if (!$data) {
                                                            $data = \App\Models\ManufacturerModel::select(['id', 'name'])->where('manufacturer_id', @$_GET['manufacturer_id'])->pluck('name', 'id');
                                                            CommonHelper::putToCache('get_manufacturermodel_by_manufacturer_id_pluck' . $_GET['manufacturer_id'], $data);
                                                        }
                                                    } else {
                                                        $data = [];
                                                    }
                                                @endphp
                                                <select name="manufacturer_model_id" id="model"
                                                        class="styled select2-offscreen"
                                                        title=""
                                                        tabindex="-1">
                                                    <option value="0"> Chọn Model</option>
                                                    @foreach($data as $id => $name)
                                                        <option value="{{ @$id }}"
                                                                @if(@$_GET['manufacturer_id'] == $id) selected @endif>
                                                            {{ $name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div><!--end col-->
                                    <script>
                                        $(document).ready(function () {
                                            $("#manufacturer").select2();
                                            $("#model").select2();
                                            $('select[name=manufacturer_id]').change(function () {
                                                console.log('manufacturer_model_id');
                                                loading();
                                                $.ajax({
                                                    url: '/ajax-get-model',
                                                    type: 'GET',
                                                    data: {
                                                        manufacturer_id: $(this).val()
                                                    },
                                                    success: function (resp) {
                                                        $('select[name=manufacturer_model_id]').html(resp);
                                                        stopLoading();
                                                    },
                                                    error: function () {
                                                        stopLoading();
                                                        alert('Có lỗi xảy ra. Vui lòng load lại trang và thử lại!');
                                                    }
                                                })
                                            });
                                        });
                                    </script>
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="">
                                            <div class="info-dki-email p0 banxe m0 banxe-vali">
                                                <div class="font14 text2b4560 mb10">Phiên bản</div>
                                                <input type="text" name="sub_model"
                                                       class="form-control input-banxe nhapso-vali recolor89"
                                                       placeholder="Nhập phiên bản xe" value=""/>
                                            </div>
                                        </div>
                                    </div><!--end col-->
                                </div><!--end row-->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="">
                                            <div class="sl-filter xe-dki banxe m0 banxe-vali">
                                                <div class="font14 text2b4560 mb10">Hộp số</div>
                                                <select class="form-control hoso-vali" id="hopso-banxe"
                                                        name="transmission">
                                                    <option value="">Chọn hộp số</option>
                                                    <option value="Số sàn">Số sàn</option>
                                                    <option value="Số tự động">Số tự động</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="">
                                            <div class="sl-filter xe-dki banxe m0 banxe-vali">
                                                <div class="font14 text2b4560 mb10">Nhiên liệu</div>
                                                <select class="form-control nhienlieu-vali" id="nhienlieu-banxe"
                                                        name="fuel">
                                                    <option value="">Chọn nhiên liệu</option>
                                                    <option value="Xăng">Xăng</option>
                                                    <option value="Dầu">Dầu</option>
                                                    <option value="Hỗn hợp">Hỗn hợp</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="">
                                            <div class="sl-filter xe-dki banxe m0 banxe-vali">
                                                <div class="font14 text2b4560 mb10">Kiểu dẫn động</div>
                                                <select class="form-control dandong-vali field-select2"
                                                        id="dandong-banxe" name="drivertrain">
                                                    <option value="">Chọn kiểu dẫn động</option>
                                                    <option value="FWD: Cầu trước">FWD: Cầu trước</option>
                                                    <option value="RWD: Cầu sau">RWD: Cầu sau</option>
                                                    <option value="4WD: 4 bánh">4WD: 4 bánh</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div><!--end col-->
                                </div><!--end row-->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="">
                                            <div class="sl-filter xe-dki banxe m0 banxe-vali">
                                                <div class="font14 text2b4560 mb10">Dòng xe</div>
                                                @php
                                                    $data = CommonHelper::getFromCache('all_producttype_pluck');
                                                     if (!$data) {
                                                         $data = \App\Models\ProductType::select(['id', 'name'])->pluck('name', 'id');
                                                         CommonHelper::putToCache('all_producttype_pluck', $data);
                                                     }
                                                @endphp
                                                <select class="form-control dongxebx-vali" id="dongxe-banxe"
                                                        name="producttype_id">
                                                    <option value="">Chọn loại xe</option>
                                                    @foreach($data as $id => $name)
                                                        <option value="{{$id}}">{{$name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="">
                                            <div class="sl-filter xe-dki banxe m0 banxe-vali">
                                                <div class="font14 text2b4560 mb10">Màu ngoại thất</div>
                                                @php
                                                    $data = CommonHelper::getFromCache('all_colors_pluck');
                                                     if (!$data) {
                                                         $data = \App\Models\Colors::select(['id', 'name'])->pluck('name', 'id');
                                                         CommonHelper::putToCache('all_colors_pluck', $data);
                                                     }
                                                @endphp
                                                <select class="form-control maxebx-vali" id="mauxe-banxe"
                                                        name="exterior">
                                                    <option value="">Chọn màu xe</option>
                                                    @foreach($data as $id => $name)
                                                        <option value="{{$id}}">{{$name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="">
                                            <div class="sl-filter xe-dki banxe m0 banxe-vali">
                                                <div class="font14 text2b4560 mb10">Màu nội thất</div>
                                                <select class="form-control mauxenoithat-vali field-select2"
                                                        id="mauxenoithat-banxe" name="interior">
                                                    <option value="">Chọn màu xe</option>
                                                    @foreach($data as $id => $name)
                                                        <option value="{{$id}}">{{$name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div><!--end col-->
                                </div><!--end row-->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="">
                                            <div class="sl-filter xe-dki banxe m0 banxe-vali">
                                                <div class="font14 text2b4560 mb10">Năm sản xuất</div>
                                                <select class="form-control namsxbx-vali" id="namsx-banxe" name="year">
                                                    <option value="">Chọn năm sản xuất</option>
                                                    @for($i = 0; $i <= 30; $i ++)
                                                        <option value="{{ date("Y",strtotime("-".$i." year")) }}">{{ date("Y",strtotime("-".$i." year")) }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-xs-12 col-sm-4 col-md-4 mb37">
                                        <div class="">
                                            <div class="sl-filter xe-dki banxe m0 banxe-vali">
                                                <div class="font14 text2b4560 mb10">Xuất xứ</div>
                                                <select class="form-control xuatxu-vali" id="xuatxu-banxe"
                                                        name="country">
                                                    <option value="">Chọn xuất xứ</option>
                                                    <option value="Trong nước">Trong nước</option>
                                                    <option value="Nhập khẩu">Nhập khẩu</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div><!--end col-->
                                </div><!--end row-->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="title-underline">Đặt giá bán</div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="datgiaban text-center">
                                            <input type="text" class="nhapgia font24 fw700 mb5 recolor89"
                                                   id="input-price" name="price" placeholder="Đặt giá bán" value=" "/>
                                            <div class="font16 mb5 text75 hidden" id="txt-price">Một tỷ tám trăm triệu
                                                đồng
                                            </div>
                                            <div class="text-red font16 hidden error">Vui lòng chỉ nhập số</div>
                                        </div><!--end datgiaban-->
                                    </div>
                                </div><!--end row-->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 mt30">
                                        <div class="title-underline">Thông tin liên hệ</div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 mb37">
                                        <div class="info-dki-email p0 banxe-vali">
                                            <div class="font14 text2b4560 mb10">Họ tên</div>
                                            <input type="text" class="form-control input-banxe recolor89"
                                                   placeholder="Nhập họ tên" name="name"
                                                   value="{{@Auth::user()->name}}"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 mb37">
                                        <div class="info-dki-email p0 banxe-vali">
                                            <div class="font14 text2b4560 mb10">Số điện thoại 1</div>
                                            <input type="text" class="form-control input-banxe recolor89"
                                                   placeholder="Nhập SĐT" name="phone1"
                                                   value="{{@Auth::user()->tel}}"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 mb37">
                                        <div class="info-dki-email p0 banxe-vali">
                                            <div class="font14 text2b4560 mb10">Số điện thoại 2</div>
                                            <input type="text" class="form-control input-banxe recolor89 skip-feild"
                                                   placeholder="Nhập SĐT" name="phone2" value=""/>

                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 mb37">
                                        <div class="info-dki-email p0 banxe-vali">
                                            <div class="font14 text2b4560 mb10">Email</div>
                                            <input type="text" class="form-control input-banxe recolor89"
                                                   placeholder="Nhập Email" name="email"
                                                   value="{{@Auth::user()->email}}"/>

                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 mb37">
                                        <div class="info-dki-email p0 banxe-vali">
                                            <div class="font14 text2b4560 mb10">Địa chỉ</div>
                                            <input type="text" class="form-control input-banxe recolor89"
                                                   placeholder="Nhập địa chỉ" name="address" value=""/>

                                        </div>
                                    </div>
                                </div><!--end row-->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 mt30">
                                        <div class="title-underline">Ảnh và Mô tả chi tiết</div>
                                    </div>

                                    <div class="b_dt_4">
                                        <script>

                                            $(document).ready(function () {
                                                // my upload image ver 1.0
                                                $(".upload").click(function () {

                                                    var id = $(this).attr("value");
                                                    var valueTest = $("#my_file_va_" + id).attr("value");

                                                    if (valueTest == "" || valueTest == "imgs/image.png") $("input[id='my_file" + id + "']").click();

                                                    $("input[id='my_file" + id + "']").change(function () {

                                                        var filename = $("#my_file" + id).val();

                                                        // Use a regular expression to trim everything before final dot
                                                        var extension = filename.replace(/^.*\./, '');

                                                        // Iff there is no dot anywhere in filename, we would have extension == filename,
                                                        // so we account for this possibility now
                                                        if (extension == filename) {
                                                            extension = '';
                                                        } else {
                                                            // if there is an extension, we convert to lower case
                                                            // (N.B. this conversion will not effect the value of the extension
                                                            // on the file upload.)
                                                            extension = extension.toLowerCase();
                                                        }

                                                        switch (extension) {
                                                            case 'jpg':/* do no thing*/
                                                                ;
                                                                break;
                                                            case 'jpeg':/* do no thing*/
                                                                ;
                                                                break;
                                                            case 'png': /* do no thing*/
                                                                ;
                                                                break;

                                                            // uncomment the next line to allow the form to submitted in this case:
                                                            //          break;

                                                            default:
                                                                // Cancel the form submission
                                                                alert("Loại tập tin này không được chấp nhận");
                                                                submitEvent.preventDefault();
                                                        }

                                                        obj = this;
                                                        var fd;
                                                        fd = new FormData();
                                                        fd.append('file', $('#my_file' + id)[0].files[0]);

                                                        $("#image_u" + id).attr({
                                                            src: "/public/frontend/images/loading.gif",
                                                            title: "loading",
                                                            alt: "loading Logo"
                                                        });

                                                        $.ajax({
                                                            type: "POST",
                                                            url: '{{ URL::to('ajax-up-file') }}?id=' + id,
                                                            processData: false,
                                                            contentType: false,
                                                            data: fd,
                                                            cache: false,
                                                            success: function (data) {
                                                                $("#image_u" + id).attr({
                                                                    src: '/public/filemanager/userfiles/' + data,
                                                                    title: "loading",
                                                                    alt: "loading Logo"
                                                                });
                                                                $("#my_file_va_" + id).attr("value", data);
                                                                $("#close_my_file_va_" + id).css("display", "block");
                                                            }
                                                        });
                                                    });
                                                    ;

                                                });

                                                $(".close_t").click(function () {
                                                    var id = $(this).attr("value");
                                                    $("#image_u" + id).attr({
                                                        src: "http://www.bdstphcm.org/imgs/image.png",
                                                        title: "loading",
                                                        alt: "loading Logo"
                                                    });
                                                    $("#my_file_va_" + id).attr("value", "");
                                                    $(this).css("display", "none");
                                                });

                                            });
                                        </script>
                                        <style>
                                            .ul_img_bdt1 li {
                                                width: 110px !important;
                                                height: 110px !important;
                                                float: left;
                                                background-color: #fff;
                                                border: 1px solid #cdcdcd;
                                                border-radius: 4px;
                                                color: white;
                                                display: inline-block;
                                                font-weight: bold;
                                                text-decoration: none;
                                                margin-left: 20px;
                                                margin-top: 5px !important;
                                            }

                                            .up_img_t {
                                                width: 100px;
                                                height: 100px;
                                                float: left;
                                                position: relative;
                                                padding: 5px;
                                            }

                                            .up_img_t img {
                                                width: 100%;
                                                height: 100%;
                                            }

                                            .close_t {
                                                width: 10px;
                                                height: 10px;
                                                position: absolute;
                                                left: 0;
                                                top: 0;
                                                background: url(http://www.bdstphcm.org/imgs/close.png) no-repeat center #FFFFFF;
                                                display: none;
                                                padding: 4px;
                                            }
                                        </style>
                                        <ul class="ul_img_bdt1">
                                            <li class="clearfix">
                                                <div class="up_img_t">
                                                    <img class="upload" value="1" id="image_u1"
                                                         src="http://www.bdstphcm.org/imgs/image.png">
                                                    <input type="file" name="my_file" class="my_file" valuee="1"
                                                           id="my_file1"
                                                           style=" visibility:hidden;">
                                                    <input type="hidden" id="my_file_va_1" name="my_file_va_1" value="">
                                                    <span class="close_t" id="close_my_file_va_1" value="1"
                                                          style=" "> </span>
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <div class="up_img_t">
                                                    <img class="upload" value="2" id="image_u2"
                                                         src="http://www.bdstphcm.org/imgs/image.png">
                                                    <input type="file" name="my_file1" class="my_file" valuee="2"
                                                           id="my_file2"
                                                           style=" visibility:hidden;">
                                                    <input type="hidden" id="my_file_va_2" name="my_file_va_2" value="">
                                                    <span class="close_t" id="close_my_file_va_2" value="2"
                                                          style=" "> </span>
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <div class="up_img_t">
                                                    <img class="upload" value="3" id="image_u3"
                                                         src="http://www.bdstphcm.org/imgs/image.png">
                                                    <input type="file" name="my_file" class="my_file" valuee="3"
                                                           id="my_file3"
                                                           style=" visibility:hidden;">
                                                    <input type="hidden" id="my_file_va_3" name="my_file_va_3" value="">
                                                    <span class="close_t" id="close_my_file_va_3" value="3"
                                                          style=" "> </span>
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <div class="up_img_t">
                                                    <img class="upload" value="4" id="image_u4"
                                                         src="http://www.bdstphcm.org/imgs/image.png">
                                                    <input type="file" name="my_file" class="my_file" valuee="4"
                                                           id="my_file4"
                                                           style=" visibility:hidden;">
                                                    <input type="hidden" id="my_file_va_4" name="my_file_va_4" value="">
                                                    <span class="close_t" id="close_my_file_va_4" value="4"
                                                          style=" "> </span>
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <div class="up_img_t">
                                                    <img class="upload" value="5" id="image_u5"
                                                         src="http://www.bdstphcm.org/imgs/image.png">
                                                    <input type="file" name="my_file" class="my_file" valuee="5"
                                                           id="my_file5"
                                                           style=" visibility:hidden;">
                                                    <input type="hidden" id="my_file_va_5" name="my_file_va_5" value="">
                                                    <span class="close_t" id="close_my_file_va_5" value="5"
                                                          style=" "> </span>
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <div class="up_img_t">
                                                    <img class="upload" value="6" id="image_u6"
                                                         src="http://www.bdstphcm.org/imgs/image.png">
                                                    <input type="file" name="my_file" class="my_file" valuee="6"
                                                           id="my_file6"
                                                           style=" visibility:hidden;">
                                                    <input type="hidden" id="my_file_va_6" name="my_file_va_6" value="">
                                                    <span class="close_t" id="close_my_file_va_6" value="6"
                                                          style=" "> </span>
                                                </div>
                                            </li>
                                            <div class="clear"></div>
                                        </ul>
                                        <div class="clear"></div>
                                    </div><!-- End .b_dt_4 -->

                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="fw700 mb5 font16mb">Mô tả chi tiết</div>
                                        <textarea class="form-control text-motachitiet recolor75 font16mb"
                                                  placeholder="Nhập thông tin mô tả chi tiết xe, tính năng, khuyến mại, phụ kiện đi kèm..."
                                                  name="description"></textarea>
                                    </div><!--end motachitiet-->
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="fw700 mb5 font16mb">Thông tin thêm</div>

                                </div>
                                <div class="ol-xs-12 col-sm-12 col-md-12">
                                    <div class="col-xs-12 col-sm-6 col-md-6 mb37">
                                        <div class="info-dki-email p0 banxe-vali">
                                            <div class="font14 text2b4560 mb10">Tiêu đề hiển thị trên trình duyệt web:
                                            </div>
                                            <input name="meta_title" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 mb37">
                                        <div class="info-dki-email p0 banxe-vali">
                                            <div class="font14 text2b4560 mb10">
                                                Mô tả thông tin rao vặt khi tìm kiếm:
                                            </div>
                                            <input name="meta_description" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 mb37">
                                        <div class="info-dki-email p0 banxe-vali">
                                            <div class="font14 text2b4560 mb10">Từ khóa dùng cho việc tìm kiếm:</div>
                                            <input name="meta_keywords" class="form-control">
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <!--end row-->
                                <button type="submit" class="btn-tieptuc font18 fw700 btn-nextbanxe" id="btn-nextbanxe">
                                    Tiếp tục
                                </button>
                            </div><!--end item-contentbanxe-->
                        </form>
                    </div><!--end info-banxe-->
                </div>
            </div>
        </div>
    </div>
@stop
