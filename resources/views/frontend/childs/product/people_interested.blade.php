@extends('frontend.layouts.master')
@section('main_content')
    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="ql_account dflex">
                    {{--menu left--}}
                    @include('frontend.childs.product.menu_left_product')
                    <div class="flex">
                        <div class="map-link hidden-xs cbreadcrumb">
                            <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    <a class="" href="/" itemprop="item"><span
                                                itemprop="name">{{ @$settings['name'] }}</span></a>
                                </li>
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    &gt; <a href="#"
                                            itemprop="item"><span itemprop="name">Danh sách quan tâm</span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="deal">
                            <div class="title-underline mt25">Danh sách quan tâm</div>
                            <div class="mt20 mb20 list-product-style2">
                                @if(count(@$contact)>0)
                                    <table class="table list-news">
                                        <tbody>

                                        @foreach($contact as $contact)
                                            @php
                                                $product = CommonHelper::getFromCache('product_nguoiquantam_by_product_id');
                                                if (!$product){
                                                    $product = \App\Models\Product::where('id',$contact->product_id)->where('status',1)->paginate(10);
                                                    CommonHelper::putToCache('product_nguoiquantam_by_product_id',$product);
                                                    echo json_decode($product);
                                                }
                                            @endphp
                                            @foreach($product as $item)
                                                @php
                                                    /* Manufacturer id*/
                                                    $Manufacturer = CommonHelper::getFromCache('manufacturer_by_manufacturer_id_first');
                                                    if (!$Manufacturer) {
                                                        $Manufacturer = \App\Models\Manufacturer::select(['id', 'name'])->where('id',$item->manufacturer_id)->first();
                                                        CommonHelper::putToCache('manufacturer_by_manufacturer_id_first', $Manufacturer);
                                                    }
                                                /*$Manufacturer model id*/
                                                 $Manufacturer_model = CommonHelper::getFromCache('manufacturermodel_by_manufacturer_model_id_first');
                                                    if (!$Manufacturer_model) {
                                                        $Manufacturer_model = \App\Models\ManufacturerModel::select(['id', 'name'])->where('id',$item->manufacturer_model_id)->first();
                                                        CommonHelper::putToCache('manufacturermodel_by_manufacturer_model_id_first', $Manufacturer_model);
                                                    }
                                                @endphp
                                                <tr>
                                                    <td class="row news  items-286821 pos-0">
                                                        <div class="news-detail">
                                                            <div class="info-left">
                                                                <a href="{{ URL::to('chi-tiet-san-pham/' . @$item['id']) }}"
                                                                   class="img"><img class=""
                                                                                    src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$item['image'], 175, 118) }}"
                                                                                    width="175" height="118"
                                                                                    alt="{{ @$item->tinhtrangxe }} {{ @$Manufacturer->name }} {{ @$Manufacturer_model->name }} {{ @$item->sub_model }} {{ @$item->year }}"></a>
                                                                <div class="block-txt-mb">
                                                                    <a class="tit-news"
                                                                       href="{{ URL::to('chi-tiet-san-pham/' . @$item['id']) }}">
                                                                        <h2>{{ @$item->tinhtrangxe }} {{ @$Manufacturer->name }} {{ @$Manufacturer_model->name }} {{ @$item->sub_model }} {{ @$item->year }}</h2>
                                                                    </a>
                                                                    <div class="price visible-xs"> {{ $item->price }} {{ @$item->price_text }}
                                                                    </div>


                                                                </div>
                                                                <div class="block-txt-mb">
                                                                    <div class="info-detail-sp"
                                                                         style="background: transparent; font-size: 13px; padding-left: 0; padding-right: 0; border-top: 1px solid #e8e8e8; margin-top: 8px; padding-top: 2px; padding-bottom: 0;">
                                                                        <div class="text75 p0">
                                                                            <p class="m0">
                                                                                <span class="mr15">{{ rand(999,9999) }} lượt xem</span>
                                                                                <span class="mr15">0 xem thông tin liên hệ</span>
                                                                            </p>
                                                                        </div>
                                                                        <div style="margin-top: -5px;">
                                                                            <a class="text-1069c9 edit-banxe btn-edit__dangban"
                                                                               style="border: none;background: none;padding: 0 0 0 28px;margin-left: 25px;margin-left: 0; "
                                                                               href="{{action('Frontend\ProductController@getEditPostCar',@$item->id)}}"><i
                                                                                        class="icon-sprite-24 icon-pencilbx"
                                                                                        style="left: 0;top: 10px;"></i>Chỉnh
                                                                                sửa tin</a>
                                                                            <a type="button"
                                                                               href="{{action('Frontend\ProductController@getDelete',@$item->id)}}"
                                                                               class="btn-xoa-new text-1069c9"><i
                                                                                        class="icon-sprite-24 icon-xoa-blue"></i>Xoá
                                                                                tin
                                                                                bán
                                                                                xe
                                                                            </a>
                                                                        </div>
                                                                        <div class="text75 p0">
                                                                            <p class="m0">
                                                                                <span class="mr15"><b>Người quan tâm:</b> {{ @$contact->name }}</span>
                                                                                <span class="mr15">{{ @$contact->email }}</span>
                                                                                <span>{{ @$contact->tel }}</span>
                                                                            </p>
                                                                        </div>
                                                                    </div><!--info-detail-sp-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="price-addr txr hidden-xs">
                                                            <div class="price">{{ @$item->price }} {{ @$item->price_text }}</div>
                                                            <div class="old-news"> {{date(" H:i a d-m-Y",strtotime(@$item->updated_at)) }}</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                        <div class="news-rong font16 mt20">
                                            <div class="txt-red bold mb5">Không có người quan tâm.</div>
                                        </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection