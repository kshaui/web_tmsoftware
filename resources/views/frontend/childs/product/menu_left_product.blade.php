<ul class="list-ql_account nonemb">
    <li class="li-ql_account"><b>Bán</b></li>
    <li class="li-ql_account"><a href="{{action('Frontend\ProductController@getNewProduct')}}">Đăng
            tin bán ô tô </a></li>
    <li class="li-ql_account"><a
                href="{{URL::to('xe-dang-rao-ban')}}">Danh
            sách tin đã đăng</a></li>
    <li class="li-ql_account"><a href="{{ URL::to('thong-bao') }}">Danh sách người
            quan tâm</a>
    </li>
    <li class="li-ql_account"><b>Mua</b></li>
    @php
        $Favorite_count = \App\Http\Helpers\CommonHelper::getFromCache('favorite_by_user_id_count');
        if (!$Favorite_count){
            $Favorite_count = \App\Models\Favorite::where('user_id',@Auth::user()->id)->count();
            \App\Http\Helpers\CommonHelper::putToCache('favorite_by_user_id_count',$Favorite_count);
        }
    @endphp
    <li class="li-ql_account"><a href="{{ URL::to('danh-sach-yeu-thich') }}">Xe yêu thích <span
                    class="txt-num"> @if(isset($Favorite_count)) ({{ $Favorite_count }}) @else
                                        (0) @endif</span></a></li>
    <li class="li-ql_account"><b>Tài khoản</b></li>
</ul>