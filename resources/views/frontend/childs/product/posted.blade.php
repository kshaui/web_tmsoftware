@extends('frontend.layouts.master')
@section('main_content')
    <div class="main-content">
        <div class="container">
            <div class="row m0">
                <div class="ql_account dflex">
                    <ul class="list-ql_account nonemb">
                        <li class="li-ql_account"><b>Bán</b></li>
                        <li class="li-ql_account"><b>Bán</b></li>
                        <li class="li-ql_account"><a href="{{action('Frontend\ProductController@getNewProduct')}}">Đăng
                                tin bán ô tô </a></li>
                        <li class="li-ql_account" style="font-weight: bold"><a
                                    href="{{action('Frontend\ProductController@getPostedProduct)}}">Danh
                                sách tin đã đăng</a></li>
                        <li class="li-ql_account"><a href="https://choxe.net/thong-bao">Danh sách người quan tâm</a>
                        </li>
                        <li class="li-ql_account"><b>Mua</b></li>
                        <li class="li-ql_account"><a href="https://choxe.net/danh-sach-yeu-thich">Xe yêu thích <span
                                        class="txt-num">(0)</span></a></li>
                        <li class="li-ql_account"><b>Tài khoản</b></li>
                    </ul>
                    <div class="flex">
                        <div class="map-link hidden-xs cbreadcrumb">
                            <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    <a class="" href="https://choxe.net/" itemprop="item"><span
                                                itemprop="name">Chợ xe</span></a>
                                </li>
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    &gt; <a href="https://choxe.net/https://choxe.net/quan-ly-oto/tu-choi"
                                            itemprop="item"><span itemprop="name">Xe đang bán</span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="deal">
                            <div class="title-underline mt25">Danh sách tin đã đăng</div>
                            <div class="btn-group btn-action mt25">
                                <a href="https://choxe.net/quan-ly-oto/dang-ban" class="btn btn-default ">Đang bán <span
                                            class="txt-num-tab">(0)</span></a>
                                <a href="https://choxe.net/quan-ly-oto/da-ban" class="btn btn-default ">Đã bán <span
                                            class="txt-num-tab">(0)</span></a>
                                <a href="https://choxe.net/quan-ly-oto/cho-duyet" class="btn btn-default ">Đang chờ
                                    duyệt
                                    <span class="txt-num-tab">(0)</span></a>
                                <a href="https://choxe.net/quan-ly-oto/thay-doi-cho-duyet" class="btn btn-default ">Thay
                                    đổi
                                    chờ duyệt <span class="txt-num-tab">(0)</span></a>
                                <a href="https://choxe.net/quan-ly-oto/tu-choi" class="btn btn-default active">Từ chối
                                    <span
                                            class="txt-num-tab">(1)</span></a>
                                <a href="https://choxe.net/quan-ly-oto/het-han" class="btn btn-default ">Hết hạn <span
                                            class="txt-num-tab">(0)</span></a>
                            </div>
                            <div class="mt20 mb20 list-product-style2">
                                <table class="table list-news">
                                    <tbody>
                                    <tr>
                                        <td class="row news  items-284572 pos-0">
                                            <div class="news-detail">
                                                <div class="info-left">
                                                    <a href="https://choxe.net/xe-moi/ban-xe-moi-bmw-5-fdsdfs-2015-tai-ho-chi-minh-284572.html"
                                                       class="img"><img class=""
                                                                        src="/uploads/auto/201811/284572/228x153-gallery5bf4d31894ce6.jpg"
                                                                        width="175" height="118"
                                                                        alt="Xe Mới BMW 5 Fdsdfs 2015 284572"></a>
                                                    <div class="block-txt-mb">
                                                        <a class="tit-news"
                                                           href="https://choxe.net/xe-moi/ban-xe-moi-bmw-5-fdsdfs-2015-tai-ho-chi-minh-284572.html">
                                                            <h2>Xe Mới BMW 5 Fdsdfs 2015</h2></a>
                                                        <div class="price visible-xs"> 3 tỷ</div>


                                                    </div>
                                                    <div class="block-txt-mb">
                                                        <div class="info-detail-sp"
                                                             style="background: transparent; font-size: 13px; padding-left: 0; padding-right: 0; border-top: 1px solid #e8e8e8; margin-top: 8px; padding-top: 2px; padding-bottom: 0;">
                                                            <div class="text75 p0">
                                                                <p class="m0">
                                                                    <span class="mr15"> lượt xem</span>
                                                                    <span class="mr15">0 xem thông tin liên hệ</span>
                                                                    <span class="mr15">0 lưu tin</span>
                                                                    <a href="https://choxe.net/thong-bao?object_id=284572"><span
                                                                                class="mr15">0 phản hồi</span></a>
                                                                </p>
                                                            </div>
                                                            <div style="margin-top: -5px;">
                                                                <a class="text-1069c9 edit-banxe btn-edit__dangban"
                                                                   style="border: none;background: none;padding: 0 0 0 28px;margin-left: 25px;margin-left: 0; "
                                                                   href=""><i
                                                                            class="icon-sprite-24 icon-pencilbx"
                                                                            style="left: 0;top: 10px;"></i>Chỉnh sửa tin</a>
                                                                <button type="button" class="btn-xoa-new text-1069c9"
                                                                        data-toggle="modal"
                                                                        data-target="#delete-284572"><i
                                                                            class="icon-sprite-24 icon-xoa-blue"></i>Xoá
                                                                    tin bán
                                                                    xe
                                                                </button>
                                                                <div class="modal fade modal26_pc" id="delete-284572"
                                                                     role="dialog">
                                                                    <div class="modal-dialog">
                                                                        <!-- Modal content-->
                                                                        <div class="modal-content">
                                                                            <div class="modal-body p0">
                                                                                <p class="font16 text21">Bạn muốn xoá
                                                                                    <span
                                                                                            class="text-red">“Xe Mới BMW 5 Fdsdfs 2015”?</span>
                                                                                </p>
                                                                            </div>
                                                                            <div class="modal-footer p0 mt25">
                                                                                <button type="button"
                                                                                        class="textfff fw700 font16 btn btn-popup__no"
                                                                                        data-dismiss="modal">Không Xóa
                                                                                </button>
                                                                                <a href="https://choxe.net/xoa-tin-dang/284572"
                                                                                   type="button"
                                                                                   class=" textfff fw700 font16 btn btn-popup__yes">Xóa</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!--info-detail-sp-->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-addr txr hidden-xs">
                                                <div class="price">3 tỷ</div>
                                                <div class="old-news">11 tiếng trước</div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div><!--end list-product-->
                        </div><!--end main-content-phukien-->
                        <div class="txc page-news style_lineheight mb20">
                            <div class="txc page-news hidden-xs">
                                <ul class="pagination m0">


                                </ul>
                            </div>

                            <!-- Pagination of mobile -->
                            <div class="visible-xs page-mb">
                                <div class="sl-page">
                                    <select name="page" id="sl-page">
                                        <option value="https://choxe.net/quan-ly-oto/tu-choi?page=1"
                                                selected="selected">
                                            Trang 1/1
                                        </option>
                                    </select>
                                    <i class="icon-sprite-24 icon-down-small"></i>
                                </div>
                            </div>
                            <!-- End Pagination -->                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop