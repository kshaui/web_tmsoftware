@extends('frontend.layouts.master_post')
@section('main_content')
    <div id="page" class="home-page">
        <div id="content" class="article">
            <h1 class="postsby">
                <span>Category: Salon ô tô</span>
            </h1>
            @foreach($posts as $item)
                <article class="post excerpt">
                    <div class="post-date-ribbon">
                        <div class="corner"></div>
                        {{date(" H:i a d-m-Y",strtotime(@$item->updated_at)) }}
                    </div>
                    <header>
                        <h2 class="title">
                            <a href="{{ URL::to('blog'.@$item->category->slug .'/'. $item->slug) }}"
                               title="{{ $item->name }}" rel="bookmark">
                                {{ $item->name }}
                            </a>
                        </h2>

                    </header><!--.header-->
                    <a href="{{ URL::to('blog'.@$item->category->slug .'/'. $item->slug) }}"
                       title="{{ $item->name  }}" id="featured-thumbnail">

                        <div class="featured-thumbnail">
                            <img width="450" height="300"
                                 src="{{ CommonHelper::getUrlImageThumb($item->image, 450, 300) }}"
                                 class="attachment-ribbon-lite-featured size-ribbon-lite-featured wp-post-image" alt=""
                                 title=""></div>
                    </a>
                    <div class="post-content">
                        {{ $item->intro }}
                        <div class="readMore">
                            <a href="{{ URL::to('blog'.@$item->category->slug .'/'. $item->slug) }}"
                               title="{{ $item->name }}">
                                Đọc thêm </a>
                        </div>
                    </div>
                </article>
            @endforeach
            {{ $posts->links() }}
            @include('frontend.partials.menu_post')
        </div>
        @include('frontend.partials.sidebar_post')
        <script type="text/javascript"
                src="{{asset('public/frontend/wp-content/plugins/gd-rating-system/js/rating.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/frontend/wp-includes/js/wp-embed.min.js')}}"></script>

        <div id="fb-root" class=" fb_reset">
            <div style="position: absolute; top: -10000px; height: 0px; width: 0px;">
                <div></div>
            </div>
        </div>
    </div>
@endsection