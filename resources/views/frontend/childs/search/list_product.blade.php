@extends('frontend.layouts.master')
@section('main_content')
    <div class="main-content bg-light-blue">
        <div class="container">
            <div class="row m0">
                @include('frontend.partials.left_page')
                <div class="col-right pl30" style="max-width: 850px">
                    @include('frontend.partials.breadcrumb_automaker')

                    @php
                        /**manufacturer*/
                        $manufacturer =CommonHelper::getFromCache('get_search_manufacturer');
                        if (!$manufacturer){
                            $manufacturer = \App\Models\Manufacturer::select('id','name')->where('id',@$_GET['manufacturer_id'])->first();
                            CommonHelper::putToCache('get_search_manufacturer',$manufacturer);
                        }
                    /**manufacturer_model*/
                        $manufacturer_model =CommonHelper::getFromCache('get_search_manufacturer_model');
                        if (!$manufacturer_model){
                            $manufacturer_model = \App\Models\ManufacturerModel::select('id','name')->where('id',@$_GET['manufacturer_model_id'])->first();
                            CommonHelper::putToCache('get_search_manufacturer_model',$manufacturer_model);
                        }
                    /**tìm kiếm thành phố*/
                        $province=CommonHelper::getFromCache('get_search_province');
                        if (!$province){
                            $province = \App\Models\Province::select('id','name')->where('id',@$_GET['province_id'])->first();
                            CommonHelper::putToCache('get_search_province',$province);
                        }
                    /**xe của user*/
                       $user=CommonHelper::getFromCache('get_search_user');
                        if (!$user){
                            $user = \App\Models\User::select('id','name')->where('id',@$_GET['user_id'])->first();
                            CommonHelper::putToCache('get_search_user',$user);
                        }
                    @endphp
                    @if(count($products)>0)
                        <h4 class="title-underline">@if(@$_GET['key'] != '' && !empty($_GET['key'])) @else Xe @endif @if( @$_GET['user_id'] != '' && !empty($_GET['user_id']))
                                của {{ @$user->name }} @endif {{ @$manufacturer->name }} {{ @$manufacturer_model->name }} @if( @$_GET['tinhtrangxe'] != '' && !empty($_GET['tinhtrangxe'])) @if(@$_GET['tinhtrangxe'] == 'Xe mới')
                                mới @else cũ @endif @else @if(@$_GET['key'] != '' && !empty($_GET['key'])) {{ @$_GET['key'] }} @else cũ mới @endif @endif @if( @$province->name )
                                ở {{ @$province->name }}@endif</h4>
                        <p class="font16 mt15 p15mb font14mb">Tìm được <span
                                    class="txt-orange">({{ @$total }})</span>
                            tin bán xe</p>
                    @else
                        <h4 class="title-underline">
                            @if(@$_GET['key'] != '' && !empty($_GET['key'])) @else Xe @endif {{ @$manufacturer->name }} {{ @$manufacturer_model->name }} @if( @$_GET['tinhtrangxe'] != '' && !empty($_GET['tinhtrangxe'])) @if(@$_GET['tinhtrangxe'] == 'Xe mới')
                                @if(@$_GET['key'] != '' && !empty($_GET['key'])) {{ @$_GET['key'] }} @else mới @endif @else  @if(@$_GET['key'] != '' && !empty($_GET['key'])) {{ @$_GET['key'] }} @else cũ @endif @endif @else  @if(@$_GET['key'] != '' && !empty($_GET['key'])) {{ @$_GET['key'] }} @else cũ mới @endif @endif @if( @$province->name )
                                ở {{ @$province->name }}@endif</h4>
                        <p class="font16 mt15 p15mb font14mb">Tìm được <span
                                    class="txt-orange">({{ @$total }})</span>
                            tin bán xe</p>
                        <div class="news-rong font16 mt20">
                            <div class="txt-red bold mb5">Không có tin bán xe nào thỏa điều kiện lọc. Vui lòng thử
                                lại điều kiện khác.
                            </div>
                        </div>
                        <div class="title-underline"
                             style="overflow: hidden; text-overflow: ellipsis; margin: 30px 0 0;">Xe cũ mới nổi bật
                        </div>
                        <table class="table list-news">
                            <tbody>
                            @php
                                $new_important_product = CommonHelper::getFromCache('get_new_important_product');
                                if (!$new_important_product){
                                    $new_important_product = \App\Models\Product::wherein('pending',['Đang bán'])->where('status',1)->where('new_important',1)->get();
                                    CommonHelper::putToCache('get_new_important_product',$new_important_product);
                                }
                            $data = $new_important_product;
                            @endphp
                            @include('frontend.childs.product.show_item')
                            </tbody>
                        </table>
                    @endif
                    @if(count($products) > 0)
                        <table class="table list-news">
                            <tbody>
                            @php $data = $products; @endphp
                            @include('frontend.childs.product.show_item')
                            </tbody>
                        </table>
                        {!! $products->links() !!}
                    @endif

                </div>
            </div>
        </div>
    </div>
@stop