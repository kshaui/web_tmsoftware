@extends('frontend.layouts.master')
@section('title')
    Thông báo
    @endsection

@section('main_content')
    <div class="main-content">
        <div class="container">
            <div class="row m0">
                <div class="ql_account dflex">
                    <ul class="list-ql_account nonemb">
                        <li class="li-ql_account"><b>Bán</b></li>
                        <li class="li-ql_account"><a href="{{ URL::to('/dang-tin') }}">Đăng tin bán ô tô </a></li>
                        <li class="li-ql_account"><a href="{{ URL::to('/quan-ly-oto') }}">Danh sách tin đã đăng</a></li>
                        <li class="li-ql_account"><a href="{{ URL::to('/thong-bao') }}">Danh sách người quan tâm</a></li>
                        <li class="li-ql_account"><b>Mua</b></li>
                        <li class="li-ql_account"><a href="{{ URL::to('/danh-sach-yeu-thich') }}">Xe yêu thích <span
                                        class="txt-num">(0)</span></a></li>
                        <li class="li-ql_account"><b>Tài khoản</b></li>
                    </ul>
                    <div class="flex">
                        <div class="map-link hidden-xs cbreadcrumb">
                            <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    <a class="" href="/" itemprop="item"><span
                                                itemprop="name">Chợ xe</span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="mt30">
                            <div class="fRight mt15">
                                <span class="mr10">Sắp xếp theo: </span>
                                <select name="" id="sl_ss2" class="icon select2-hidden-accessible" tabindex="-1"
                                        aria-hidden="true">
                                    <option value="1" selected="">Đăng ký nhận Email</option>
                                    <option value="2">Đăng ký nhận Gọi cho tôi</option>
                                    <option value="2">Đăng ký nhận Lái thử xe</option>
                                </select><span class="select2 select2-container select2-container--default" dir="ltr"
                                               style="width: 188px;"><span class="selection"><span
                                                class="select2-selection select2-selection--single fontSize14 sl_city icon-sl"
                                                role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1"
                                                aria-labelledby="select2-sl_ss2-container"><span class="select2-selection__rendered"
                                                                                                 id="select2-sl_ss2-container"
                                                                                                 title="Đăng ký nhận Email">Đăng ký nhận Email</span><span
                                                    class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span
                                            class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                        </div>
                        <div class="mb20 notification_user">
                            <div class="filter-sms">
                            </div>
                            <div class="dflex">
                                <ul class="list-notification" id="list-notification" role="tablist" tabindex="0"
                                    style="outline: none; overflow: hidden;">

                                </ul>
                                <div style="position: absolute; z-index: 1; margin: 0px; padding: 0px; display: none; left: 742.5px; top: 352px;">
                                    <div class="enscroll-track track3" style="position: relative; height: 650px;"><a href=""
                                                                                                                     class="handle3"
                                                                                                                     style="position: absolute; z-index: 1; height: 650px;">
                                            <div class="top"></div>
                                            <div class="bottom"></div>
                                        </a></div>
                                </div>
                                <div class="flex tab-content">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @stop
