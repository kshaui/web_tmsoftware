@extends('frontend.layouts.master')
@section('main_content')
    <div class="main-content">
        <div class="container">
            <div class="row m0 nonemb">
                @include('frontend.partials.breadcrumb_automaker')
            </div>
            <br>
            <div class="salon">
                <div class="title-list">
                    <h1>Danh sách Đại lý</h1>
                </div>
                <div class="list-salon row rowmb">
                    @foreach($salons as $item)
                        <a href="{{ URL::to('/tim-kiem?user_id='. @$item->id) }}"
                           class="css_a detail-salon salon-vip-new">
                            <div class="bor-col">
                                <img class=""
                                     src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$item->image, 251, 169) }}"
                                     alt="{{ @$item->name }}" width="251" height="169">
                                <div class="content-salon">
                                    <h4 class="name_salon">{{ @$item->name }}</h4>
                                    <div class="add-salon dflex">
                                        <i class="icon-24 icon-sprite-24 icon-address"></i>
                                        <span>{{ @$item->address }}</span>
                                    </div>
                                    <div class="phone_salon dflex">
                                        <i class="icon-sprite-24 icon_phone2"></i>
                                        <span>{{ @$item->tel }}</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
                {!! $salons->links() !!}
            </div>
        </div>
    </div>
@stop