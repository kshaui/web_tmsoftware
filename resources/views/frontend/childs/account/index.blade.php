@extends('frontend.layouts.master')
@section('main_content')
    <div class="main-content">
        <div class="container">
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        {!! $err !!} <br>
                    @endforeach
                </div>
            @endif
            @if(Session('flash_message'))
                <div class="alert alert-success">
                    {!! Session('flash_message') !!}
                </div>
            @endif
            <div class="row m0">
                <div class="ql_account dflex dis-33">
                    <ul class="list-ql_account nonemb">
                        <li class="li-ql_account"><b>Bán</b></li>
                        <li class="li-ql_account"><a href="{{ URL::to('/dang-tin') }}">Đăng tin bán ô tô </a></li>
                        <li class="li-ql_account"><a href="{{ action('Frontend\ProductController@getPending') }}">Danh
                                sách tin đã đăng</a></li>
                        <li class="li-ql_account"><a href="{{ URL::to('/thong-bao') }}">Danh sách người quan tâm</a>
                        </li>
                        <li class="li-ql_account"><b>Mua</b></li>
                        @php
                            $Favorite_count = CommonHelper::getFromCache('favorite_by_user_id_count');
                            if (!$Favorite_count){
                                $Favorite_count = \App\Models\Favorite::where('user_id',@Auth::user()->id)->count();
                                CommonHelper::putToCache('favorite_by_user_id_count',$Favorite_count);
                            }
                        @endphp
                        <li class="li-ql_account"><a href="{{ URL::to('danh-sach-yeu-thich') }}">Xe yêu thích <span
                                        class="txt-num">@if(isset($Favorite_count)) ({{ (@$Favorite_count) }}) @else
                                        (0) @endif</span></a></li>
                        <li class="li-ql_account"><b>Tài khoản</b></li>
                    </ul>
                    <div class="flex">
                        <div class="map-link hidden-xs cbreadcrumb">
                            <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    <a class="" href="/" itemprop="item"><span
                                                itemprop="name">Chợ xe</span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="mt30 mt0mb pt20mb">
                            <h4 class="title-contact">Quản lý tài khoản</h4>
                        </div>
                        <div class="dflex mb60 qltkmb dis-33">
                            <div class="flex mr15">
                                <div class="info_account">
                                    <p class="line_info_acc fontSize18"><b>Loại tài khoản</b></p>
                                    <p style="padding-top: 10px;">
                                        <b>Loại: </b><span>{{ @Auth::user()->role }}
                                    </p>
                                </div>
                                <div class="info_account">
                                    <p class="line_info_acc fontSize18"><b>Thông tin Đăng nhập</b></p>
                                    <p class="line_info_acc" id="hien_thi_email_user"><b>Tên tài
                                            khoản: </b><span>{{Auth::user()->email}}</span>
                                    </p>
                                    <p class="line_info_acc" id="hien_thi_email_sua">
                                        <b>Email: </b><span>{{Auth::user()->email}}</span><a
                                                href="#" data-id_email="{{@Auth::user()->id}}" type="button"
                                                data-toggle="modal" data-target="#sua_email"
                                                class="edit-info"><i
                                                    class="icon-sprite-24 icon-pen-blue"></i><span>Sửa</span></a></p>
                                    <div class="modal fade" id="sua_email" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;
                                                    </button>
                                                    <h4 class="modal-title">Thay đổi Email</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="" method="POST" enctype="multipart/form-data">
                                                        {{csrf_field()}}
                                                        <div class="form-group">
                                                            <label for="pwd">Email mới:</label>
                                                            <input type="email" required class="form-control"
                                                                   placeholder="Nhập email mới" id="email">
                                                            <p class="error1 text-center alert alert-danger hidden"></p>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                        Close
                                                    </button>
                                                    <button type="button" data-id_email="{{@Auth::user()->id}}"
                                                            id="email_edit" class="btn btn-default">Sửa
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $('#email_edit').click(function () {
                                            var id_email = $(this).data('id_email');
                                            var email = $('#email').val();
                                            var object = $(this);
                                            $.ajax({
                                                type: 'POST',
                                                url: '/sua-email',
                                                data: {
                                                    '_token': '{{ csrf_token() }}',
                                                    'id_email': id_email,
                                                    'email': email,
                                                }, success: function (data) {
                                                    if ((data.errors)) {
                                                        $('.error1').removeClass('hidden');
                                                        $('.error1').text(data.errors.email);
                                                    } else {
                                                        location.reload();
                                                        console.log(data);
                                                    }
                                                }
                                            })
                                            $('#email').val('');
                                        })
                                    </script>
                                    <p class="line_info_acc"><b>Mật khẩu: </b><span>******</span><a
                                                href="#" type="button" data-toggle="modal" data-target="#sua_password"
                                                class="edit-info" class="edit-info"><i
                                                    class="icon-sprite-24 icon-pen-blue"></i><span>Sửa</span></a></p>
                                    <!-- Modal sủa mật khẩu-->
                                    <div class="modal fade" id="sua_password" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;
                                                    </button>
                                                    <h4 class="modal-title">Thay đổi mật khẩu</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="" method="POST" enctype="multipart/form-data">
                                                        {{csrf_field()}}
                                                        <div class="form-group">
                                                            <label for="pwd">Mật khẩu mới:</label>
                                                            <input type="password" required class="form-control"
                                                                   placeholder="Nhập mật khẩu mới" id="password">
                                                            <p class="error text-center alert alert-danger hidden"></p>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                        Close
                                                    </button>
                                                    <button type="button" data-id_password="{{@Auth::user()->id}}"
                                                            id="password_edit" class="btn btn-default">Sửa
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $('#password_edit').click(function () {
                                            var id_password = $(this).data('id_password');
                                            var password = $('#password').val();
                                            $.ajax({
                                                type: 'POST',
                                                url: '/sua-password',
                                                data: {
                                                    '_token': '{{ csrf_token() }}',
                                                    'id_password': id_password,
                                                    'password': password,
                                                }, success: function (data) {
                                                    if ((data.errors)) {
                                                        $('.error').removeClass('hidden');
                                                        $('.error').text(data.errors.password);
                                                    } else {
                                                        location.reload();
                                                    }
                                                }
                                            })
                                            $('#password').val('');
                                        })
                                    </script>
                                </div>
                            </div>
                            <div class="flex">
                                <div class="info_user">
                                    <p class="line_info_acc fontSize18"><b>Thông tin Cá nhân</b></p>
                                    <div class="mb20 mt20">
                                        <div class="edit_avatar" style="height: auto">
                                            <img src="@if(@Auth::user()->image) {{ asset('public/filemanager/userfiles/' . @Auth::user()['image']) }} @else {{ asset('public/frontend/assets/frontend/img/avatar_default.png') }} @endif"
                                                 alt=""
                                                 id="avatar">
                                        </div>
                                    </div>
                                    <p><b>Tên chủ tài khoản: </b><span>{{Auth::user()->name}}</span></p>
                                    @php
                                        $data = CommonHelper::getFromCache('province_by_user_id_city_first');
                                    if (!$data) {
                                    $data = \App\Models\Province::select(['id', 'name'])->
                                    where('id',Auth::user()->id_city)->first();
                                    CommonHelper::putToCache('province_by_user_id_city_first', $data);
                                    }
                                    @endphp
                                    <p><b>Tỉnh/Thành phố: {{@$data->name}}</b><span></span></p>
                                    <p><b>Địa chỉ:{{@Auth::user()->address}} </b><span></span></p>
                                    <p><b>Số điện thoại liên hệ: </b><span>{{@Auth::user()->tel}}</span></p>
                                    <a class="btn btn-edit_user"
                                       href="{{action('Frontend\AccountController@getEditAccount')}}">
                                        <i class="icon-sprite-24 icon-pen-blue"></i>
                                        <span>Chỉnh sửa</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection