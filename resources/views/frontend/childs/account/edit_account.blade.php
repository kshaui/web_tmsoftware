@extends('frontend.layouts.master_account')
@section('main_body')
    <div class="main-content bg-grey">
        <div class="container">
            <div class="row m0 ">
                <div class="form_action">
                    <h4 class="title-form">Chỉnh sửa thông tin cá nhân</h4>
                    <div class="div_form">
                        <form method="post" action="{{action('Frontend\AccountController@postEditAccount')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group mb30">
                                <div class="inner-img">
                                    <div class="edit_avatar text-center">
                                        <img src="@if(@Auth::user()->image){{ asset('public/filemanager/userfiles/' . @Auth::user()['image']) }}@else{{ asset('public/frontend/assets/frontend/img/avatar_default.png') }}@endif" alt=""
                                             id="avatar">
                                        <label class="btn btn-edit-avt" for="uploadfile">
                                            <i class="icon-sprite-24 icon-camera"></i>
                                            <span>Đổi ảnh</span>
                                            <input type="file" id="uploadfile" class="uploadfile" name="image"
                                                   onchange="loadFile(event)">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="">Tên chủ tài khoản </label>
                                <input type="text"  name="name" class="form-control" placeholder="{{Auth::user()->name}}"
                                       value="{{Auth::user()->name}}"/>
                            </div>
                            @php
                                $data = CommonHelper::getFromCache('all_province_pluck');

                            if (!$data) {
                            $data = \App\Models\Province::select(['id', 'name'])->pluck('name', 'id');
                            CommonHelper::putToCache('all_province_pluck', $data);
                            }
                            @endphp
                            <div class="form-group">
                                <label for="" class="">Tỉnh/Thành phố </label>
                                <select class="form-control" id="sl_tp"  name="province_id">
                                    @foreach($data as $id => $name)
                                        <option value="@if(@Auth::user()->id_city == $id) {{@Auth::user()->id_city}} @else {{@$id}} @endif" @if(@Auth::user()->id_city == $id) selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="" class="">Địa chỉ </label>
                                <input type="text" name="address" class="form-control" placeholder="" value="{{@Auth::user()->address}}"/>
                            </div>
                            <div class="form-group">
                                <label for="" class="">Số điện thoại </label>
                                <input type="text" name="tel" class="form-control" placeholder="" value="{{@Auth::user()->tel}}"/>
                            </div>
                            <div class="form-group text-right mb0">
                                <a href="/tai-khoan" type="button" class="btn btn-cancel w83 mr10">HỦY</a>
                                <button type="submit" class="btn btn-accept w74">LƯU</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop