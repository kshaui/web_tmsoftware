@php
    if(Session::get('cart') == null) {
        $products = [];
    } else {
        $cart = Session::get('cart');
        $product_id_arr = array_keys($cart['items']);
        $products = \App\Models\Product::select(['id', 'name', 'slug', 'multi_cat', 'image', 'author_id', 'code', 'base_price', 'final_price', 'type', 'ebook_price', 'ebook_price'])
            ->where('status', 1)->whereIn('id', $product_id_arr)->get();
    }
@endphp

<div class="col-sm-4 col-md-4 col-lg-4">
    <div class="ghBox">
        <h3>Đơn hàng ({{ Session::has('cart') ? array_sum(Session::get('cart')['items']) : 0 }} sản phẩm)</h3>
        @foreach($products as $item)
            @php $price_product = Session::get('cart')['items'][$item->id]['attributes']['product_types'] == 1 ? $item->final_price : $item->ebook_price; @endphp
        <ul class="ghBoxLst">
            <li class="clearfix"><span>{{Session::get('cart')['items'][$item->id]['quantity']}} x {{$item->name}}</span><span class="ghPrice">{{ number_format(Session::get('cart')['items'][$item->id]['quantity'] * $price_product, 0, '.', '.') }} ₫</span></li>
        </ul>
        @endforeach
        <p class="ghtt clearfix"><span>Tạm tính</span><span class="ghttPrice">{{ number_format(Session::get('cart')['total_price'], 0, '.', '.') }} ₫</span></p>
        <p class="ghtt2 clearfix"><span>Thành tiền:</span><span class="ghtt2Price">{{ number_format(Session::get('cart')['total_price'], 0, '.', '.') }} ₫<em>Đã có VAT</em></span></p>
    </div>
</div>
