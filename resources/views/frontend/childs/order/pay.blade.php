@extends('frontend.layouts.master')
@section('main_content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="path">
                        <ul class="clearfix">
                            <li>3. Thanh toán & Đặt mua</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="box-white mt30 pb30 clearfix">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="step clearfix">
                                <ul class="clearfix">
                                    <li class="active"><span>Đăng nhập</span><span class="number">1</span></li>
                                    <li class="active"><span>Địa chỉ giao hàng</span><span class="number">2</span></li>
                                    <li class="active"><span>Thanh toán &amp; đặt mua</span><span class="number">3</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="dnBox clearfix">
                                <h3>Chọn hình thức thanh toán cho khosach.net</h3>
                            </div>
                        </div>
                        <div class="ghBoxOut">
                            <div class="col-sm-8 col-md-8 col-lg-8">
                                <form action="{{ route('order.createBill') }}" method="POST" class="dcBox clearfix">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="bill[user_id]" value="{{ \Auth::user()->id }}">
                                    <input type="hidden" name="bill[user_tel]" value="{{ Session::get('cart_delivery')['tel'] }}">
                                    <input type="hidden" name="bill[user_name]" value="{{ Session::get('cart_delivery')['name'] }}">
                                    <input type="hidden" name="bill[user_email]" value="{{ \Auth::user()->email }}">
                                    <input type="hidden" name="bill[user_address]" value="{{ isset(Session::get('cart_delivery')['address']) ? Session::get('cart_delivery')['address'] : '' }}">
                                    <input type="hidden" name="bill[user_wards]" value="{{ isset(Session::get('cart_delivery')['wards']) ? Session::get('cart_delivery')['wards'] : '' }}">
                                    <input type="hidden" name="bill[user_city_id]" value="{{ isset(Session::get('cart_delivery')['city_id']) ? Session::get('cart_delivery')['city_id'] : '' }}">
                                    <input type="hidden" name="bill[user_district_id]" value="{{ isset(Session::get('cart_delivery')['district_id']) ? Session::get('cart_delivery')['district_id'] : '' }}">
                                    <input type="hidden" name="bill[user_address_type]" value="{{ isset(Session::get('cart_delivery')['address_type']) ? Session::get('cart_delivery')['address_type'] : '' }}">

                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <h3 class="sh_dn">Chọn hình thức thanh toán</h3>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <ul class="dcLst2 clearfix">
                                            <li class="cash">
                                                <input type="radio" id="check1" name="bill[receipt_method]" value="Thanh toán tiền mặt khi nhận hàng">
                                                <label for="check1">Thanh toán tiền mặt khi nhận hàng</label>
                                            </li>
                                            <li class="visa">
                                                <input type="radio" id="check2" name="bill[receipt_method]" value="Thanh toán bằng thẻ quốc tế Visa, Master, JCB">
                                                <label for="check2">Thanh toán bằng thẻ quốc tế Visa, Master, JCB</label>
                                                <div class="visa_show">
                                                    Visa Card
                                                </div>
                                            </li>
                                            <li class="atm">
                                                <input type="radio" id="check3" name="bill[receipt_method]" value="Thẻ ATM nội địa/Internet Banking (Miễn phí thanh toán)">
                                                <label for="check3">Thẻ ATM nội địa/Internet Banking (Miễn phí thanh toán)</label>
                                                <div class="atm_show">
                                                    <img src="{{ URL::asset('frontend/images/bank.png') }}" alt="thanh toán" />
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <h3 class="sh_dn">Thông tin người mua:</h3>
                                        <ul class="dcLst clearfix">
                                            <li>
                                                <input type="checkbox" id="check4" name="radio-group">
                                                <label for="check4">Sử dụng Họ tên & Số điện thoại của địa chỉ giao hàng </label>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <p class="btn_dn">
                                            <button id="" type="submit">Đặt mua</button>
                                        </p>
                                    </div>
                                </form>
                            </div>
                            @include('frontend.childs.order.block_order')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('custom_footer')

@endsection