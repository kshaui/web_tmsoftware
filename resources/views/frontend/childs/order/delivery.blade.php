@extends('frontend.layouts.master')
@section('main_content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="path">
                        <ul class="clearfix">
                            <li>2. Địa chỉ giao hàng</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="box-white mt30 pb30 clearfix">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="step clearfix">
                                <ul class="clearfix">
                                    <li class="active"><span>Đăng nhập</span><span class="number">1</span></li>
                                    <li class="active"><span>Địa chỉ giao hàng</span><span class="number">2</span></li>
                                    <li><span>Thanh toán &amp; đặt mua</span><span class="number">3</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="dnBox clearfix">
                                <h3>Nhập địa chỉ để khosac.net giao hàng đến cho bạn</h3>
                            </div>
                        </div>
                        <div class="ghBoxOut">
                            <div class="col-sm-8 col-md-8 col-lg-8">
                                <form action="{{ route('order.getDelivery') }}" method="POST" class="dcBox clearfix">
                                    {!! csrf_field() !!}
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <h3 class="sh_dn">địa chỉ giao hàng</h3>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="dn_col1">
                                            <ul>
                                                <li>
                                                    <label for="">Họ tên</label>
                                                    <input type="text" id="" name="name" placeholder="Nhập họ tên" required
                                                           value=""/>
                                                </li>
                                                <li>
                                                    <label for="">Điện thoại di động</label>
                                                    <input type="text" id="" name="tel" placeholder="Nhập số điện thoại" required
                                                           value=""/>
                                                </li>
                                                <li>
                                                    <label for="">Địa chỉ</label>
                                                    <input type="text" id="" name="address" placeholder="Nhập địa chỉ" required
                                                           value=""/>
                                                </li>
                                                <li>
                                                    <label for="">Phường/Xã</label>
                                                    <input type="text" id="" name="wards" placeholder="Nhập phường/xã"
                                                           value=""/>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="dn_col1">
                                            <ul>
                                                <li>
                                                    <label for="">Tỉnh/Thành Phố</label>
                                                    @php
                                                        $data = CommonHelper::getFromCache('all_province_pluck');
                                                            if (!$data) {
                                                                $data = \App\Models\Province::pluck('name', 'id');
                                                                CommonHelper::putToCache('all_province_pluck', $data);
                                                            }

                                                    @endphp
                                                    <select name="city_id" class="form-control">
                                                        <option value="0">Chọn tỉnh thành</option>
                                                        @foreach($data as $id => $v)
                                                            <option value="{{ $id }}">{{ $v }}</option>
                                                        @endforeach
                                                    </select>
                                                </li>
                                                <li>
                                                    <label for="">Quận/Huyện</label>
                                                    <select name="district_id" class="form-control">
                                                        <option value="0">Chọn quận huyện</option>
                                                        <option value="1">Vui lòng chọn tỉnh/thành phố trước</option>
                                                    </select>
                                                </li>
                                            </ul>
                                            <p class="dcType">Loại địa chỉ</p>
                                            <ul class="dcLst clearfix">
                                                <li>
                                                    <input type="radio" id="check1" name="address_type"
                                                           value="Nhà riêng/Chung cư">
                                                    <label for="check1">Nhà riêng/Chung cư</label>
                                                </li>
                                                <li>
                                                    <input type="radio" id="check2" name="address_type"
                                                           value="Cơ quan/Công ty<">
                                                    <label for="check2">Cơ quan/Công ty</label>
                                                </li>
                                            </ul>
                                            <p class="btn_dn">
                                                <button id="" type="submit">Giao đến địa chỉ này</button>
                                            </p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            @include('frontend.childs.order.block_order')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('custom_header')
    <style>
        select.form-control {
            height: 37px;
            margin-top: 9px;
        }
    </style>
@endsection

@section('custom_footer')
    <script>
        $(document).ready(function () {
            $('select[name=city_id]').change(function () {
                $('select[name=district_id]').attr('disabled', 'disabled');
                var city_id = $(this).val();
                $.ajax({
                    url: '{{ URL::to('ajax-get-district') }}',
                    type: 'GET',
                    data: {
                        city_id : city_id
                    },
                    success: function (result) {
                        $('select[name=district_id]').removeAttr('disabled');
                        $('select[name=district_id]').html(result);
                    },
                    error: function () {
                        alert('Có lỗi xảy ra. Vui lòng F5 lại website!');
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#BB-nav').affix({
                offset: {
                    top: $('#BB-nav').height()
                }
            });
        });
    </script>
    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "100%";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }

        $(document).ready(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 500) {
                    $('.scrollToTop').fadeIn();
                } else {
                    $('.scrollToTop').fadeOut();
                }
            });
            $('.scrollToTop').click(function () {
                $('html, body').animate({scrollTop: 0}, 800);
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function (e) {
            $('.search-panel .dropdown-menu').find('a').click(function (e) {
                e.preventDefault();
                var param = $(this).attr("href").replace("#", "");
                var concept = $(this).text();
                $('.search-panel span#search_concept').text(concept);
                $('.input-group #search_param').val(param);
            });
        });
    </script>
    <script>
        var x, i, j, selElmnt, a, b, c;
        x = document.getElementsByClassName("custom-select");
        for (i = 0; i < x.length; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0];
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            x[i].appendChild(a);
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            for (j = 1; j < selElmnt.length; j++) {
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.addEventListener("click", function (e) {
                    var i, s, h;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < s.length; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            break;
                        }
                    }
                    h.click();
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);
            a.addEventListener("click", function (e) {
                e.stopPropagation();
                closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
            });
        }

        function closeAllSelect(elmnt) {
            var x, y, i, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            for (i = 0; i < y.length; i++) {
                if (elmnt == y[i]) {
                    arrNo.push(i)
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < x.length; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }
        }

        document.addEventListener("click", closeAllSelect);
    </script>
@endsection