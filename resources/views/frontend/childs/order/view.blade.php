@extends('frontend.layouts.master')
@section('main_content')
    @php
        $product_types = [
                '1' => 'Sách giấy',
                '2' => 'Audio',
                '3' => 'Sách Ebook'
            ];
    @endphp
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="path">
                        <ul class="clearfix">
                            <li>Giỏ hàng có ({{ Session::has('cart') ? count(Session::get('cart')['items']) : 0 }} sản
                                phẩm)
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <div class="box-white mt30 clearfix">
                        <ul class="cartLst clearfix">
                            @foreach($products as $item)
                                <li class="clearfix">
                                    <p class="cartImg"><a href="{{ CommonHelper::getProductSlug($item) }}"><img
                                                    src="{{ URL::asset('public/filemanager/userfiles/' . $item->image) }}"
                                                    alt="{{ $item->name }}"/></a></p>
                                    <div class="cartInfo">
                                        <h3><a href="{{ CommonHelper::getProductSlug($item) }}">{{ $item->name }}</a>
                                        </h3>
                                        <p>
                                            <span class="c_tg">Tác giả: {{ is_object($item->author) ? $item->author->name : '' }}</span><span
                                                    class="c_sku">SKU: {{$item->code}}</span></p>
                                        <p>Loại sản
                                            phẩm: {{ $product_types[Session::get('cart')['items'][$item->id]['attributes']['product_type']] }}</p>
                                        <p><a href="javascript:;" rel="nofollow" class="remove-item-cart"
                                              data-product_id="{{ $item->id }}">Xoá</a></p>
                                    </div>
                                    <div class="cartPrice clearfix">
                                        <div class="cP_left">
                                            @if(Session::get('cart')['items'][$item->id]['attributes']['product_types'] == 1)
                                                <span class="cP_left1">{{ number_format($item->final_price, 0, '.', '.') }}
                                                    đ</span>
                                                <span class="cP_left2">@php $discount = CommonHelper::discount($item->base_price, $item->final_price); @endphp @if($discount != ''){{ $discount }}@endif</span>
                                                <span class="cP_left3">{{ number_format($item->base_price, 0, '.', '.') }}
                                                    đ</span>
                                            @else
                                                <span class="cP_left1">{{ number_format($item->ebook_price, 0, '.', '.') }}
                                                    đ</span>
                                            @endif
                                        </div>
                                        <div class="cP_right"><span class="sl">Số lượng:</span>
                                            <div class="numbers-row">
                                                <input type="text" name="" id=""
                                                       value="{{ Session::get('cart')['items'][$item->id]['quantity'] }}">
                                            </div>
                                            <div class="update-number" style="margin-left: 24px;">
                                                <a href="javascript:;" rel="nofollow" class="cart-update"
                                                   data-product_id="{{ $item->id }}">Cập nhật</a>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <div class="ghBox2">
                        <p class="ghtt clearfix"><span>Tạm tính</span><span class="ghttPrice">{{ number_format(Session::get('cart')['total_price'], 0, '.', '.') }}
                                ₫</span></p>
                        <p class="ghtt2 clearfix"><span>Thành tiền:</span><span class="ghtt2Price">{{ number_format(Session::get('cart')['total_price'], 0, '.', '.') }}
                                ₫<em>Đã có VAT</em></span></p>
                    </div>
                    <p class="btnTT"><a href="{{ route('order.getDelivery') }}">Tiến hành thanh toán</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="box-white mt30 clearfix">
                        <div class="spSlide">
                            @php
                                $data = CommonHelper::getFromCache('products_by_hot_sale_1_limit_8');
                            if (!$data) {
                                $data = \App\Models\Product::select(['name', 'image', 'slug', 'multi_cat', 'base_price', 'final_price', 'type', 'ebook_price'])->where('status', 1)->where('hot_sale', 1)
                                        ->orderBy('order_no', 'desc')->orderBy('updated_at', 'desc')->limit(8)->get();
                                CommonHelper::putToCache('products_by_hot_sale_1_limit_8', $data);
                            }

                            @endphp
                            <h3>Sản phẩm giảm giá</h3>
                            <div class="spSlideInner">
                                <div id="spgg" class="owl-carousel">
                                    @foreach($data as $item)
                                        <div class="item">
                                            <div class="bookItem">
                                                <p class="bookImg"><a
                                                            href="{{ CommonHelper::getProductSlug($item) }}"><img
                                                                src="{{ CommonHelper::getUrlImageThumb($item->image, 129, 190) }}"
                                                                alt="{{ $item->name }}"></a></p>
                                                <p class="bookTtl"><a
                                                            href="{{ CommonHelper::getProductSlug($item) }}">{{ $item->name }}</a>
                                                </p>
                                                <p class="bookPrice clearfix">
                                                    {!! CommonHelper::getPriceHtml($item) !!}
                                                </p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="customNavigation"><a class="prev2"><img
                                                src="{{ URL::asset('public/frontend/images/prev3.png') }}" alt=""/></a>
                                    <a class="next2"><img src="{{ URL::asset('frontend/images/next3.png') }}"
                                                          alt=""/></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('custom_footer')
    <script type="text/javascript">
        $(function () {
            $('#BB-nav').affix({
                offset: {
                    top: $('#BB-nav').height()
                }
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            var owl = $("#spgg");
            owl.owlCarousel({
                items: 4,
                itemsDesktop: [1200, 4],
                itemsDesktopSmall: [1000, 3],
                itemsTablet: [600, 2],
                itemsMobile: [500, 1],
                autoPlay: 3000
            });

            $(".next2").click(function () {
                owl.trigger('owl.next');
            })
            $(".prev2").click(function () {
                owl.trigger('owl.prev');
            })
        });
    </script>
    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "100%";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }

        $(document).ready(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 500) {
                    $('.scrollToTop').fadeIn();
                } else {
                    $('.scrollToTop').fadeOut();
                }
            });
            $('.scrollToTop').click(function () {
                $('html, body').animate({scrollTop: 0}, 800);
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function (e) {
            $('.search-panel .dropdown-menu').find('a').click(function (e) {
                e.preventDefault();
                var param = $(this).attr("href").replace("#", "");
                var concept = $(this).text();
                $('.search-panel span#search_concept').text(concept);
                $('.input-group #search_param').val(param);
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $(".numbers-row").append('<div class="inc button">+</div><div class="dec button">-</div>');
            $(".button").on("click", function () {
                var $button = $(this);
                var oldValue = $button.parent().find("input").val();
                if ($button.text() == "+") {
                    var newVal = parseFloat(oldValue) + 1;
                } else {
                    // Don't allow decrementing below zero
                    if (oldValue > 0) {
                        var newVal = parseFloat(oldValue) - 1;
                    } else {
                        newVal = 0;
                    }
                }
                $button.parent().find("input").val(newVal);
            });
        });
    </script>

    <script>
        //  Custom code
        $('.cart-update').click(function () {
            loading();
            var product_id = $(this).data('product_id');
            var quantity = $(this).parents('.cP_right').find('input').val();
            $.ajax({
                url: '{{ route('order.update_cart') }}',
                type: 'GET',
                data: {
                    product_id: product_id,
                    quantity: quantity
                },
                success: function (result) {
                    stopLoading();
                    if (result.status == true) {
                        location.reload();
                    } else {
                        alert(result.msg);
                    }
                },
                error: function () {
                    stopLoading();
                    alert('Có lỗi xảy ra. Vui lòng F5 lại website!');
                }
            });
        });
    </script>
@endsection