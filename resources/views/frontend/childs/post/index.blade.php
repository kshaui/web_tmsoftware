@extends('frontend.layouts.master')
@section('main_content')
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">{{ $post->name }}</h1>
                @include('frontend.partials.breadcrumb')
            </header>
            <div itemprop='aggregateRating' itemscope='' itemtype='http://schema.org/AggregateRating'
                 style='color:#3B5998;font-size:13px;'>Điểm <span id='csdreviewValue' itemprop='ratingValue'>4.6</span>/5
                dựa vào <span id='csdreviewCount' itemprop='reviewCount'>87</span> đánh giá
            </div>
            <span id='csdratingpercent'
                  style='display: block; width: 65px; height: 13px; background: url(https://1.bp.blogspot.com/-H77E0v75Pu0/WDvD_PK5htI/AAAAAAAAUyc/62DswWzezfkeCK_WDDs-sQWCw4tKuwkfQCLcB/s1600/star-rating-sprite.png) 0 0;'>
</span>
            <script type='text/javascript'>
                //<![CDATA[
                function csdGetMetaContentByName(name) {
                    var info = document.getElementsByTagName('meta');
                    try {
                        return [].filter.call(info, function (val) {
                            if (val.name.toLowerCase().trim() === name.toLowerCase().trim()) return val;
                        })[0].content;
                    }
                    catch (err) {
                        return '';
                    }
                }
                function CSDCreateVote() {
                    var csdReviewCountPlus = 10;
                    var csdtitlePost = document.title.trim();
                    var csddescription = csdGetMetaContentByName('description');
                    if (csddescription == '') {
                        csddescription = csdtitlePost;
                    }
                    var csdtitleLen = (csdtitlePost.length == 0 ? 160 : csdtitlePost.length);
                    var csddescriptionLen = (csddescription.length == 0 ? 200 : csddescription.length);
                    var csdReviewCount = csdtitleLen + csdReviewCountPlus;
                    var csdPoint = (csdtitleLen % 5) + 1;
                    var csdX = (csddescriptionLen / 10);
                    var csdDecimal = parseFloat((csdX - Math.floor(csdX)).toFixed(1));
                    csdPoint = parseFloat((csdPoint <= 3 ? 4 : csdPoint));
                    if (csdPoint == 5) {
                    }
                    else {
                        csdPoint += csdDecimal;
                    }
                    //alert(csdPoint);
                    if (document.getElementById('csdreviewCount')) {

                        document.getElementById('csdreviewCount').innerHTML = csdReviewCount;
                        document.getElementById('csdreviewValue').innerHTML = csdPoint;
                        var csdpercent = (csdPoint / 5).toFixed(2) * 100;
                        // alert(csdpercent);
                        document.getElementById('csdratingpercent').innerHTML = '<span style="display: block; width: ' + csdpercent + '%; height: 13px; background: url(https://1.bp.blogspot.com/-H77E0v75Pu0/WDvD_PK5htI/AAAAAAAAUyc/62DswWzezfkeCK_WDDs-sQWCw4tKuwkfQCLcB/s1600/star-rating-sprite.png) 0 -13px;"></span>';

                    }
                }
                window.onload = function () {
                    CSDCreateVote();
                }
                //]]>
            </script>

            <div class="page-content">
                <div class="row page-row">
                    <div class="news-wrapper col-md-8 col-sm-7">
                        <article class="news-item">
                            <div id="fb-root"></div>
                            <script>(function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=1618627438366755";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>

                            <div class="fb-like" data-href="{{ URL::to($post->category->slug .'/'. $post->slug) }}"
                                 data-layout="standard" data-action="like" data-show-faces="true"
                                 data-share="true"></div>
                            <br>
                            {!! $post->content !!}
                            <br><br><br>
                            <div class="col-xs-12">
                                <section class="course-finder">
                                    <h1 class="section-heading text-highlight"><span class="line">Kiểm tra trình độ miễn phí</span>
                                    </h1>
                                    @if(session('success')) <span
                                            class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
                                    @if(session('error')) <span
                                            class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif
                                    <div class="section-content">
                                        <form method="POST" action="{{ URL::to('contact') }}" class="form-dang-ky">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input class="form-control pull-left" type="text"
                                                           placeholder="Họ tên" name="name" required/>
                                                </div>
                                                <div class="col-md-6">
                                                    <input class="form-control pull-left" type="text"
                                                           placeholder="Số điện thoại" required
                                                           name="tel"/>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-md-6">
                                                    <input class="form-control pull-left" type="email"
                                                           placeholder="Email"
                                                           name="email"/>
                                                </div>
                                                <div class="col-md-6 subject">
                                                    <select class="form-control subject" name="job">
                                                        <option value="Học sinh">Học sinh</option>
                                                        <option value="Học sinh">Sinh viên</option>
                                                        <option value="Học sinh">Người đi làm</option>
                                                        <option value="Học sinh">Khác</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-md-7 col-sm-7">
                                                    <button type="submit" class="btn btn-theme dang-ky-test">Đăng ký
                                                    </button>
                                                </div>
                                            </div>
                                        </form><!--//course-finder-form-->
                                    </div><!--//section-content-->
                                </section><!--//course-finder-->
                            </div>
                            <br><br><br>

                            {!! $setting->chuky !!}
                            <br><br>
                            <div id="fb-root"></div>
                            <script>(function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=1618627438366755";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                            <div class="fb-comments"
                                 data-href="{{ URL::to($post->category->slug . '/' . $post->slug) }}" data-width="100%"
                                 data-numposts="5"></div>
                        </article><!--//news-item-->
                    </div><!--//news-wrapper-->
                    <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1 col-xs-12">
                        <section class="widget has-divider">
                            <h1 class="section-heading text-highlight"><span class="line">Bài viết liên quan</span></h1>
                            <?php
                            $listPost = \App\Models\Post::where('category_id', $post->category_id)->limit(4)->orderBy('created_at', 'desc')->get();
                            ?>
                            @if(count($listPost) > 0)
                                @foreach($listPost as $item)
                                    <article class="news-item row">
                                        <figure class="thumb col-md-2 col-sm-3 col-xs-3">
                                            <img src="{{ getUrlImageThumb($item->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}"
                                                 alt="{{ $item->name }}" title="{{ $item->name }}"/>
                                        </figure>
                                        <div class="details col-md-10 col-sm-9 col-xs-9">
                                            <h4 class="title"><a
                                                        href="{{ URL::to($item->category->slug .'/'. $item->slug) }}">{{ ucfirst($item->name) }}</a>
                                            </h4>
                                        </div>
                                    </article><!--//news-item-->
                                @endforeach
                            @endif
                        </section><!--//widget-->
                        @include('frontend.partials.khoahoc')
                        @include('frontend.partials.giangvien')
                    </aside>
                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-->
    </div><!--//content-->

    @if(!isset($_SESSION['form_download']))
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary btn-lg btn-open-form" data-toggle="modal" data-target="#myModal"
            style="display: none;">
        Launch demo modal
    </button>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Đăng ký email của bạn để được download tài liệu miễn
                        phí</h4>
                    <button style="display: none;" type="button" class="close btn-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form class="modal-body form-dang-ky2" id="dangky">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <input name="email" class="form-control" type="email" placeholder="user@gmail.com" required>
                    </div>
                    <input type="hidden" name="form_download" value="true">
                    <button type="submit" class="btn btn-success">Download tài liệu</button>
                </form>
            </div>
        </div>
    </div>
        <input type="hidden" id="link_direct">
    @endif
@stop
@section('custom_header')
    <style>
        p {
            font-size: 15px;
        }
    </style>
@stop
@section('custom_footer')
    @if($post->id == 1)
        <script>
            $('input[name=_token]').val('{{ csrf_token() }}');
            $('input[name=slug]').val('{{ $_SERVER['REQUEST_URI'] }}');
        </script>
    @endif

    <script>
        $('form.form-dang-ky').submit(function (e) {
            e.preventDefault();
            var data = $(this).serialize();
            $(this).find('input').val('');
            $(this).find('select').val('Cơ bản mất gốc');
            $.ajax({
                url: '{{ URL::to('contact') }}',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function () {
                    alert('Gửi đăng ký thành công!')
                },
                error: function (resp) {
                    alert('Có lỗi xảy ra. Vui lòng F5 lại website!');
                }
            });
        });

        @if(!isset($_SESSION['form_download']) && strpos($_SERVER['REQUEST_URI'], 'download-tai-lieu') != false)
        //  show form input email when click link download document
        $('article.news-item a').each(function () {
            $(this).click(function (e) {
                e.preventDefault();
                $('.btn-open-form').click();
                $('#link_direct').val($(this).attr('href'));
            });
        });

        // Submit form dang ky
        $('form.form-dang-ky2').submit(function (e) {
            e.preventDefault();
            $('.btn-close').click();
            var data = $(this).serialize();
            $(this).find('input').val('');
            $.ajax({
                url: '{{ URL::to('contact') }}',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function () {
                    window.location.href = $('#link_direct').val();
                },
                error: function () {
                    alert('Có lỗi xảy ra. Vui lòng F5 lại trang!');
                }
            });
        });
        @endif
    </script>
@stop
