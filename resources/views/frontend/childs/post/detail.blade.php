@extends('frontend.layouts.master_post')
@section('main_content')
    <div id="page" class="single">
        <div class="content">
            <article class="article">
                <div id="post-5639"
                     class="post post-5639 type-post status-publish format-standard has-post-thumbnail hentry category-salon-o-to tag-dai-li-mazda tag-dai-li-mazda-phu-my-hung tag-dai-li-mazda-tai-tphcm tag-mazda-phu-my-hung tag-salon-mazda-phu-my-hung tag-showroom-mazda">
                    <div class="single_post">
                        <div class="post-date-ribbon">
                            <div class="corner"></div>
                            {{date(" H:i a d-m-Y",strtotime(@$post->updated_at)) }}
                        </div>
                        @include('frontend.partials.breadcrumb_post')
                        <header>
                            <h1 class="title single-title">{!!   $post->name !!}</h1>
                        </header>
                        {!! $post->content !!}
                        <div id="fb-root"></div>
                        <script>(function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=1618627438366755&autoLogAppEvents=1';
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                        <div class="fb-comments"
                             data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"
                             data-numposts="5"></div>
                        <div class="related-posts">
                            @php
                                $data = CommonHelper::getFromCache('post_is_related_1');
                                 if (!$data) {
                                     $data = \App\Models\Post::select(['name','image', 'slug','intro'])->where('is_related',1)->where('status', 1)->get();
                                     CommonHelper::putToCache('post_is_related_1', $data);
                                 }
                            @endphp
                            <div class="postauthor-top"><h3>Bài viết liên quan</h3></div>
                            @foreach($data->chunk(3) as $chunkdata)
                                <div class="row">
                                    @foreach($chunkdata as $item)
                                        <article class="post excerpt  ">
                                            <a href="{{ URL::to('/blog/'.@$item->slug) }}"
                                               title="{{ $item->name }}" id="featured-thumbnail">
                                                <div class="featured-thumbnail">
                                                    <img width="400" height="250"
                                                         src="{{ CommonHelper::getUrlImageThumb($item->image, 825, 350) }}"
                                                         class="attachment-ribbon-lite-related size-ribbon-lite-related wp-post-image"
                                                         alt="{{ $item->name }}" title="{{ $item->name }}"/></div>
                                                <header>
                                                    <h4 class="title front-view-title">{{ $item->name }}</h4>
                                                </header>
                                            </a>
                                        </article>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </article>
            @include('frontend.partials.sidebar_post')
        </div>
    </div>
@endsection