@extends('frontend.layouts.master_post')
@section('main_content')
    <div id="page" class="home-page">
        <div id="content" class="article">
            <h1 class="postsby">
                <span>Category: Salon ô tô</span>
            </h1>
            @if(isset($blogs))
                @foreach($blogs as $item)
                    <article class="post excerpt">
                        <div class="post-date-ribbon">
                            <div class="corner"></div>
                            {{ $item->created_at }}
                        </div>
                        <header>
                            <h2 class="title">
                                <a href="{{ URL::to('blog'.@$item->category->slug .'/'. $item->slug) }}"
                                   title="{{ $item->name }}" rel="bookmark">
                                    {{ $item->name }}
                                </a>
                            </h2>
                        </header><!--.header-->
                        <a href="{{ URL::to('blog'.@$item->category->slug .'/'. $item->slug) }}"
                           title="{{ $item->name  }}" id="featured-thumbnail">
                            <div class="featured-thumbnail">
                                <img width="450" height="300"
                                     src="{{ CommonHelper::getUrlImageThumb($item->image, 450, 300) }}"
                                     class="attachment-ribbon-lite-featured size-ribbon-lite-featured wp-post-image"
                                     alt=""
                                     title=""></div>
                        </a>
                        <div class="post-content">
                            {{ $item->intro }}
                            <div class="readMore">
                                <a href="{{ URL::to('blog'.@$item->category->slug .'/'. $item->slug) }}"
                                   title="{{ $item->name }}">
                                    Đọc thêm </a>
                            </div>
                        </div>
                    </article>
                @endforeach
                    {!! $blogs->links() !!}
            @else
                @if(isset($message))
                    {{ $message }}
                @endif
            @endif
        </div>
        @include('frontend.partials.sidebar_post')
        @include('frontend.partials.footer_post')
        <script type="text/javascript"
                src="{{asset('public/frontend/wp-content/plugins/gd-rating-system/js/rating.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/frontend/wp-includes/js/wp-embed.min.js')}}"></script>
        <div id="fb-root" class=" fb_reset">
            <div style="position: absolute; top: -10000px; height: 0px; width: 0px;">
            </div>
        </div>
    </div>
@endsection
@section('custom_header')
    <style>

        .pagination {
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px
        }

        .pager li, .pagination > li {
            display: inline
        }

        .pagination > li > a, .pagination > li > span {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd
        }

        .pagination > li:first-child > a, .pagination > li:first-child > span {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px
        }

        .pagination > li:last-child > a, .pagination > li:last-child > span {
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px
        }

        .pagination > li > a:focus, .pagination > li > a:hover, .pagination > li > span:focus, .pagination > li > span:hover {
            z-index: 2;
            color: #23527c;
            background-color: #eee;
            border-color: #ddd
        }

        .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
            z-index: 3;
            color: #fff;
            cursor: default;
            background-color: #337ab7;
            border-color: #337ab7
        }

        .pagination > .disabled > a, .pagination > .disabled > a:focus, .pagination > .disabled > a:hover, .pagination > .disabled > span, .pagination > .disabled > span:focus, .pagination > .disabled > span:hover {
            color: #777;
            cursor: not-allowed;
            background-color: #fff;
            border-color: #ddd
        }

        .pagination-lg > li > a, .pagination-lg > li > span {
            padding: 10px 16px;
            font-size: 18px;
            line-height: 1.3333333
        }

        .pagination-lg > li:first-child > a, .pagination-lg > li:first-child > span {
            border-top-left-radius: 6px;
            border-bottom-left-radius: 6px
        }

        .pagination-lg > li:last-child > a, .pagination-lg > li:last-child > span {
            border-top-right-radius: 6px;
            border-bottom-right-radius: 6px
        }

        .pagination-sm > li > a, .pagination-sm > li > span {
            padding: 5px 10px;
            font-size: 12px;
            line-height: 1.5
        }

        .badge, .label {
            font-weight: 700;
            line-height: 1;
            white-space: nowrap;
            text-align: center
        }

        .pagination-sm > li:first-child > a, .pagination-sm > li:first-child > span {
            border-top-left-radius: 3px;
            border-bottom-left-radius: 3px
        }

        .pagination-sm > li:last-child > a, .pagination-sm > li:last-child > span {
            border-top-right-radius: 3px;
            border-bottom-right-radius: 3px
        }
        .pagination > .active > span {
            margin-right: 8px;
        }
        .pagination > li:first-child > span {
            margin-right: 8px;
        }
    </style>
@endsection
