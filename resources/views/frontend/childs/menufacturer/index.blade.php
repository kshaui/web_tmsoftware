@extends('frontend.layouts.master')
@section('main_content')
    <div class="main-content bg-light-blue">
        <div class="container">
            <div class="row m0">
               @include('frontend.partials.left_page')
                <div class="col-right pl30">
                    @include('frontend.partials.breadcrumb_automaker')
                    <div class="deal mt10 sl-arrow-mb sl-comm-mb bg-white-mb p15mb">
                        <div class="title-underline"> Các dòng xe</div>
                        @php
                            $data = CommonHelper::getFromCache('all_manufacturermodel');
                             if (!$data) {
                                 $data = \App\Models\ManufacturerModel::select(['id', 'full_name'])->get();
                                 CommonHelper::putToCache('all_manufacturermodel', $data);
                             }
                        @endphp
                        <div class="clearfix"></div>
                        <div class="ds-dong-xe mt25">
                            <ul class="row m0 list-hang-xe">
                                @foreach($data as $item)
                                    <li>
                                        <a href="{{ URL::to('/tim-kiem?manufacturer_model_id=' . $item->id) }}"> Giá
                                            xe {{ $item->full_name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="txc page-news mt25 hidden-xs">
                        <div class="visible-xs page-mb">
                            <div class="sl-page">
                                <select name="page" id="sl-page">
                                    <option value="{{ URL::to('/dong-xe')}}" selected="selected">Trang 1/1
                                    </option>
                                </select>
                                <i class="icon-sprite-24 icon-down-small"></i>
                            </div>
                        </div>
                    </div>
                    @include('frontend.partials.folding_sale')
                </div>
            </div>
        </div>
    </div>
@stop