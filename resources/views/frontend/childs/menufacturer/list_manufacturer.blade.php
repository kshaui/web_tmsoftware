@extends('frontend.layouts.master')
@section('main_content')
    <div class="main-content bg-light-blue">
        <div class="container">
            <div class="row m0">
                @include('frontend.partials.left_page')
                <div class="col-right pl30">
                    @include('frontend.partials.breadcrumb_automaker')
                    <div class="deal mt10 tinh-thanh  sl-arrow-mb sl-comm-mb bg-white-mb p15mb">
                        <div class="title-underline">Tất cả hãng xe</div>
                        <div class="clearfix"></div>
                        <div class="ds-hang-xe mt15">
                            @php
                                $data = CommonHelper::getFromCache('all_manufacturer');
                                 if (!$data) {
                                     $data = \App\Models\Manufacturer::select(['id','name', 'image'])->where('status', 1)->get();
                                     CommonHelper::putToCache('all_manufacturer', $data);
                                 }
                            @endphp
                            <ul class="row logo-hang">
                                @foreach($data as $item)
                                    <li>
                                        <a class="" href="{{ URL::to('/tim-kiem?manufacturer_id='. $item->id) }}">
                                            <div class="img-logo-xe">
                                                <img src="{{ CommonHelper::getUrlImageThumb($item->image, 104, 69) }}"
                                                     width="auto" height="auto">
                                            </div>
                                            {{ $item->name }}
                                            <span class="txt-num"></span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                        @include('frontend.partials.folding_sale')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop