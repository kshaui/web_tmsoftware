@extends('frontend.layouts.master')
@section('main_content')
    <div class="main-content bg-light-blue">
        <div class="container">
            <div class="row m0">
                @include('frontend.partials.left_page')
                <div class="col-right pl30">
                    <div class="deal table-xe" id="block-2nd">
                        <div class="title-underline">
                            Xe yêu thích
                        </div>
                        <table class="table list-news" style="max-width: 900px">
                            <tbody>
                            @if(count($listCarLove)>0)
                                @foreach($listCarLove as $item)
                                    @php
                                        $product = CommonHelper::getFromCache('product_by_product_id');
                                       if (!$product) {
                                           $product = \App\Models\Product::where('id',$item->product_id)->get();
                                           CommonHelper::putToCache('product_by_product_id', $product);
                                       }
                                       if (!is_object($product))
                                           abort(404);
                                           $data = $product;
                                    @endphp
                                    @include('frontend.childs.product.show_item')
                                @endforeach
                            @else
                                <tr>
                                    <td class="row news  items-284624 pos-0">
                                        <div class="news-detail">
                                            <p style="color: red">Chưa có tin bán xe nào được lưu</p>
                                            <p><a href="/">Bấn đây để tìm kiếm xe</a></p>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        {{ @$listCarLove->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop