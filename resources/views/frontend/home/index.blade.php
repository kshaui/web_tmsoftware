@extends('frontend.layouts.master')
@section('main_content')
    <div class="main-content bg-light-blue">
        <div class="container">
            <div class="row m0">
                @include('frontend.partials.left_page')
                <div class="col-right pl30">
                    <div class="row m0 mt20 dong-xe-mb">
                        <div class="col-525">
                            @php
                                $data = CommonHelper::getFromCache('manufacturermodel_show_homepage_1');
                                 if (!$data) {
                                     $data = \App\Models\ManufacturerModel::select(['name','full_name','show_homepage','id'])->where('status', 1)->where('show_homepage',1)->get();
                                     CommonHelper::putToCache('manufacturermodel_show_homepage_1', $data);
                                 }
                            @endphp
                            <h1 class="title-underline">Mua bán xe ô tô cũ mới</h1>
                            <ul class="row m0 list-hang-xe">
                                @foreach($data as $item)
                                    <li>
                                        <a href="{{ URL::to('/tim-kiem?manufacturer_model_id='.$item->id) }}">
                                            <strong>{{ $item->full_name }}</strong>
                                        </a>
                                    </li>
                                @endforeach
                                <li>
                                    <a href="{{ URL::to('/dong-xe') }}">
                                        Xem thêm...
                                    </a>
                                </li>
                            </ul>
                            <hr class="visible-xs m0 mt5">
                        </div>
                    </div>
                    @include('frontend.partials.view_car_menufacturer')
                    @include('frontend.partials.folding_sale')
                    @include('frontend.partials.view_car_manufacturer_mobile')
                    <div class="deal mt25 tinh-thanh hidden-xs">
                        @php
                            $data = CommonHelper::getFromCache('province_show_homepage_1');
                            if (!$data) {
                            $data = \App\Models\Province::select(['name', 'id'])->where('status', 1)->where('show_homepage',1)->get();
                            CommonHelper::putToCache('province_show_homepage_1', $data);
                            }
                        @endphp
                        <div class="title-underline">Xem xe theo tỉnh thành</div>
                        <div class="row m0 mt20 mt10mb">
                            @foreach($data as $item)
                                <div class="col-xs-6 col-sm-3 p0 mt5">
                                    <a class="city" href="\tim-kiem?province_id={{ $item->id }}">
                                        <i class="icon-24 icon-sprite-24 icon-address"></i>{{ $item->name }}</a>
                                </div>
                            @endforeach
                        </div>
                        <hr class="visible-xs m0 mt5">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop